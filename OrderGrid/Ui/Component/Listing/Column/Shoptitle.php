<?php
namespace Pricewhirl\OrderGrid\Ui\Component\Listing\Column;
 
use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;
use \Magento\Framework\Api\SearchCriteriaBuilder;
use \Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory as CustomFactory;
use \Webkul\Marketplace\Helper\Data as Webkulhelper;
use Webkul\Marketplace\Model\SaleslistFactory;
 
class Shoptitle extends Column
{
 
    protected $_orderRepository;
    protected $_searchCriteria;
    protected $_customfactory;
    public $saleslistFactory;
 
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $criteria,
        CustomFactory $customFactory,
        Webkulhelper $webkulhelper,
        SaleslistFactory $saleslistFactory,
        array $components = [], array $data = [])
    {
        $this->_orderRepository = $orderRepository;
        $this->_searchCriteria  = $criteria;
        $this->_customfactory = $customFactory;
        $this->_webkulhelper = $webkulhelper;
        $this->saleslistFactory = $saleslistFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }


    public function getUserInfo($productId)
    {
        $sellerId = 0;
        $marketplaceSalesCollection = $this->saleslistFactory->create()
        ->getCollection()
        ->addFieldToFilter(
            'mageproduct_id',
            ['eq' => $productId]
        );
        
        if (count($marketplaceSalesCollection)) {
            foreach ($marketplaceSalesCollection as $mpSales) {
                $sellerId = $mpSales->getSellerId();
            }
        }
        return $sellerId;
    }
 
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $order  = $this->_orderRepository->get($item["entity_id"]);
                $shopname = array();
                foreach ($order->getAllVisibleItems() as $_item) {         
                    $sellerId = $this->getUserInfo($_item->getProductId());                    
                    $sellerdata = $this->_webkulhelper->getSellerDataBySellerId($sellerId);
                    foreach ($sellerdata as $key => $seller) {
                         $shopname[] = $seller->getShopTitle();
                    }                    
                } 

                $shopname = array_unique($shopname);
                $shoptitle = implode(', ',$shopname); 
                $item[$this->getData('name')] = $shoptitle;
            } 
        }
        return $dataSource;
    }
}