<?php
namespace Magemonkeys\CustomSorting\Plugin\Model;

use Magento\Store\Model\StoreManagerInterface;
 
class Config
{
    protected $_storeManager;
 
    public function __construct(
        StoreManagerInterface $storeManager
    )
    {
        $this->_storeManager = $storeManager;
    }
 
    public function afterGetAttributeUsedForSortByArray(\Magento\Catalog\Model\Config $catalogConfig, $options)
    {
        
        unset($options['price']);
        unset($options['instock_availability']);

        $newOptions['low_to_high'] = __('Price - Low To High');
        $newOptions['high_to_low'] = __('Price - High To Low');
        
        $options = array_merge($newOptions, $options);               
              
        return $options;
    }
}