<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace PetzzcoApi\SalesApi\Api;

use Magento\Framework\Exception\InputException;

/**
 * Interface for managing sales.
 * @api
 * @since 100.0.2
 */
interface SalesManagementInterface
{
    /**
     * get user order list.
     *
     * @param int $customerId The customer ID.
     * @param string $page
     * @return array
     */
    public function getCustomerOrderList($customerId, $page);
    
    /**
     * get user order detail.
     *
     * @param int $customerId The customer ID.
     * @param string $order_id
     * @return array
     */
    public function getCustomerOrderDetail($customerId, $order_id);
    
    /**
     * create customer reorder by order id.
     *
     * @param int $customerId The customer ID.
     * @param string $order_id
     * @return array
     */
    public function reorder($customerId, $order_id);
    
    
}
