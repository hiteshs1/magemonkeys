<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace PetzzcoApi\SalesApi\Model;

use PetzzcoApi\SalesApi\Api\SalesManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\EmailNotConfirmedException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\InvalidEmailOrPasswordException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\ExpiredException;
use Magento\Framework\Exception\State\InputMismatchException;
use Magento\Framework\Exception\State\InvalidTransitionException;
use Magento\Framework\Exception\State\UserLockedException;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use PetzzcoApi\CustomerApi\Helper\Data as ApiCommonHelper;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\App\Config\ScopeConfigInterface;


/**
 * Handle various sales actions.
 *
 */
class SalesManagement implements SalesManagementInterface
{
	//const SANDBOX = 'https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction';
    //const PRODUCTION = 'https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction';
    const XML_PATH_EMAIL_RECIPIENT_NAME  = 'trans_email/ident_support/name';
    const XML_PATH_EMAIL_RECIPIENT_EMAIL = 'trans_email/ident_support/email';
    const ORDER_VERIFY_RESENDOTP 		 = 'orderverify/general/resendotp';
    
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;
    
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    
    /**
     *@var \Magento\Catalog\Model\Product
     */
    protected $_productload;

    /**
     *@var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $_orderCollectionFactory;
    
    /**
     *@var \Magento\Sales\Model\Order\Config
     */
    protected $_orderConfig;
    
    /**
     * @var ShipmentRepositoryInterface
     */
    private $shipmentRepository;
    
    /**
     * @var PetzzcoApi\CustomerApi\Helper\Data;
     */
    protected $_apiCommonHelper;
    
    /**
     * @var OrderRepositoryInterface
     */
    protected $_orderRepository;

    /**
     * @var \Magento\Sales\Model\Service\InvoiceService
     */
    protected $_invoiceService;

    /**
     * @var \Magento\Sales\Model\Order\Email\Sender\InvoiceSender
     */
    protected $_invoiceSender;
    
    /**
     * @var OrderManagementInterface
     */
    protected $orderManagement;
    
    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $_inlineTranslation;
    
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;
    
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
    
    /**
     * Message factory.
     *
     * @var \Magento\GiftMessage\Model\MessageFactory
     */
    protected $messageFactory;
    
    /**
     * smsHelper.
     *
     * @var \Meetanshi\Sms\Helper\Data
     */
    protected $smsHelper;
    
    /**
     * @param \Magento\Catalog\Model\Product $productload
     * @param CustomerRepositoryInterface $customerRepository
     * @param StoreManagerInterface $storeManager
     * @param OrderCollectionFactory $orderCollectionFactory
     * @param \Magento\Sales\Model\Order\Config $orderConfig
     * @param \Magento\Sales\Api\ShipmentRepositoryInterface $shipmentRepository
     * @param ApiCommonHelper $apiCommonHelper
     * @param \Magento\Sales\Model\Service\InvoiceService           $invoiceService
     * @param \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoiceSender
     * @param OrderManagementInterface                              $orderManagement
     * @param OrderRepositoryInterface                              $orderRepository
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param MessageFactory $messageFactory     
     */
    public function __construct(
		Context $context,
        StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $productload,
        CustomerRepositoryInterface $customerRepository,
        OrderCollectionFactory $orderCollectionFactory,
        \Magento\Sales\Model\Order\Config $orderConfig,
        ShipmentRepositoryInterface $shipmentRepository,
        ApiCommonHelper $apiCommonHelper,
        OrderRepositoryInterface $orderRepository,
        \Magento\Framework\DB\Transaction $transaction,
        \Magento\Sales\Model\Service\InvoiceService $invoiceService,
        \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoiceSender,
        OrderManagementInterface $orderManagement,
        \Magento\Sales\Model\Order\StatusFactory $orderStatusFactory,
        StateInterface $inlineTranslation,
        TransportBuilder $transportBuilder,
        ScopeConfigInterface $scopeConfig,
        \Magento\GiftMessage\Model\MessageFactory $messageFactory        
     ) {
		$this->_urlBuilder = $context->getUrl();
        $this->storeManager 			= $storeManager;
        $this->_productload 			= $productload;
        $this->customerRepository 		= $customerRepository;
        $this->_orderCollectionFactory 	= $orderCollectionFactory;
        $this->_orderConfig 			= $orderConfig;
        $this->shipmentRepository 		= $shipmentRepository;
        $this->_apiCommonHelper 		= $apiCommonHelper;
        $this->_orderRepository 		= $orderRepository;
        $this->_invoiceService 			= $invoiceService;
        $this->_transaction 			= $transaction;
        $this->_invoiceSender 			= $invoiceSender;
        $this->orderManagement 			= $orderManagement;
        $this->orderStatusFactory 		= $orderStatusFactory;
        $this->_inlineTranslation	 	= $inlineTranslation;
        $this->_transportBuilder 		= $transportBuilder;
        $this->_scopeConfig 			= $scopeConfig;
        $this->messageFactory 			= $messageFactory;        
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCustomerOrderList($customerId, $page)
    {
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			try {
				$customer = $this->customerRepository->getById($customerId);
			} catch (NoSuchEntityException $e) {
				throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
			}
			$order_data = array();
			$orderCollection = $this->_orderCollectionFactory->create($customerId)
									 ->addFieldToSelect('*')
									 ->addFieldToFilter('status',
											['in' => $this->_orderConfig->getVisibleOnFrontStatuses()]
										)
									 ->setOrder('created_at', 'desc')
									 ->setPageSize(10)
									 ->setCurPage($page);
			$order_count = $orderCollection->getSize();						 
			if ( $orderCollection->count() ) {
				foreach ($orderCollection as $order) {
					$order_data[] = array( "order_id" 				=> $order->getId(),
										   "increment_id" 			=> $order->getIncrement_id(),
										   "order_date" 			=> date('d/m/Y',strtotime($order->getCreated_at())),
										   "order_amount" 			=> $order->getGrand_total()*1,
										   "order_status" 			=> $order->getStatus(),
										   "order_items_count" 		=> $order->getTotal_item_count(),
										   "order_currency_code"	=> $order->getOrder_currency_code(), 
									    );
				}
			}	
			$response['order_data']   	= $order_data;
			$response['order_count']   	= $order_count;
			$responseFlag = true;	
			$message = __('Successfully.');					 
		} catch(InputMismatchException $e){
			$message  = $e->getMessage();
		} catch(InvalidEmailOrPasswordException $e){
			$message  = $e->getMessage();
		} catch(LocalizedException $e){
			$message  = $e->getMessage();
		} catch(NoSuchEntityException $e){
			$message  = $e->getMessage();
		} catch(UserLockedException $e){
			$message  = $e->getMessage();
		} catch(\Exception $e){
			$message  = $e->getMessage();
		}
        $response['response']   	= $responseFlag;
		$response['message']    	= $message;
		echo json_encode($response);
	}
	
    /**
     * {@inheritdoc}
     */
    public function getCustomerOrderDetail($customerId, $order_id)
    {
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			try {
				$customer = $this->customerRepository->getById($customerId);
			} catch (NoSuchEntityException $e) {
				throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
			}
			$order_data 		= array();
			$objectManager 		= ObjectManager::getInstance();
			$formatedAddress 	= $objectManager->get('\Magento\Sales\Model\Order\Address\Renderer');
			$order 				= $objectManager->create('\Magento\Sales\Model\Order')->load($order_id);
			if($customerId == $order->getCustomer_id()){
				$increment_id    		= $order->getRealOrderId();
				$order_status    		= $order->getStatus();
				$order_status    		= $order->getStatus();
				$grand_total     		= $order->getGrandTotal() * 1;
				$order_currency_code 	= $order->getOrder_currency_code();
				$subtotal        		= $order->getSubtotal() * 1;
				$tax_amount      		= $order->getTaxAmount() * 1;
				$shipping_amount 		= $order->getShippingAmount() * 1;
				$discount_amount 		= $order->getDiscount_amount() * 1;
				$discount_description 	= $order->getDiscount_description();
				$cash_on_delivery 		= $order->getCash_on_delivery_fee() * 1;
									
				$order_date      = date('d/m/Y',strtotime($order->getCreated_at()));
				$shipping_method = $order->getShippingDescription();
				$payment_method  = $order->getPayment()->getMethodInstance()->getTitle();
				$billingAddress  = implode("\n",array_filter(preg_split('/\n|\r/',strip_tags($formatedAddress->format($order->getBillingAddress(), 'html')))));
				$shippingAddress = implode("\n",array_filter(preg_split('/\n|\r/',strip_tags($formatedAddress->format($order->getShippingAddress(), 'html')))));
				
				/*** Start: get order item detail. ***/
				$orderItemsCollection = $order->getAllVisibleItems();
				$product_data = array();
				foreach ($orderItemsCollection as $item){	
					$product 		= $this->_productload->load($item->getProduct_id()); 
					$product_image 	= $this->_apiCommonHelper->getImageUrl($product, 'product_base_image');
					$customOption   = array();
					$options 		= $item->getProductOptions();  
					if (isset($options['options']) && !empty($options['options'])) {        
				        foreach ($options['options'] as $option) {
				        	$customOption[] = array("option_label" => $option['label'], "option_vaule" => $option['value']);
				        }
				    }
					$product_data[] = array("product_id" 		=> 	$item->getProductId(), 
											"product_name"		=>	$item->getName(), 
											"product_sku"		=>	$item->getSku(), 
											"product_image"		=>	$product_image, 
											"product_price" 	=> 	$item->getPrice()*1, 
											"order_qty"			=>	round($item->getQtyOrdered()),
											"invoice_qty"		=>	round($item->getQtyInvoiced()),
											"shipped_qty"		=>	round($item->getQtyShipped()), 
											"canceled_qty"		=>	round($item->getQtyCanceled()), 
											"refunded_qty"		=>	round($item->getQtyRefunded()), 
											"product_subtotal"	=> 	$item->getRowTotal()*1,
											"custom_option"		=> 	$customOption
											);
				}
				/*** End: get order item detail. ***/
				
				/*** Start: get order invoice detail. ***/
				$invoiceData 	= $this->getOrderInvoiceDetail($order, $cash_on_delivery);
				/*** End: get order invoice detail. ***/
				
				/*** Start: get order shipment detail. ***/
				$shipmentData 	= $this->getOrderShipmentDetail($order);
				/*** End: get order shipment detail. ***/
				
				/*** Start: get order credit memo detail. ***/
				$refundData 	= $this->getOrderCreditmemosDetail($order);
				/*** End: get order credit memo detail. ***/
				
				$giftMessageId = $order->getGiftMessageId();
				if ($giftMessageId) {
					$giftMessage = $this->messageFactory->create()->load($giftMessageId);
					$response['sender'] 			= $giftMessage->getSender();
					$response['recipient'] 			= $giftMessage->getRecipient(); 
					$response['gift_message'] 			= $giftMessage->getMessage();
				}
				if($order->getMagecomp_order_comment()){
					$response['gst_number'] 		= $order->getMagecomp_order_comment();
				}
					
				
				$response['order_id'] 				= $order_id;
				$response['increment_id'] 			= $increment_id; 
				$response['order_status'] 			= $order_status;
				$response['order_date'] 			= $order_date;
				$response['discount_amount'] 		= $discount_amount; 
				$response['discount_description'] 	= $discount_description; 
				$response['shipping_amount'] 		= $shipping_amount; 
				$response['tax_amount'] 			= $tax_amount;
				$response['cash_on_delivery']		= $cash_on_delivery;
				$response['subtotal'] 				= $subtotal;
				$response['grand_total'] 			= $grand_total; 
				$response['order_currency_code'] 	= $order_currency_code; 
				$response['shipping_method'] 		= $shipping_method; 
				$response['payment_method'] 		= $payment_method;
				$response['billing_address'] 		= $billingAddress; 
				$response['shipping_address'] 		= $shippingAddress; 
				$response['product_data'] 			= $product_data;
				$response['invoice_data'] 			= $invoiceData;
				$response['shipment_data'] 			= $shipmentData;
				$response['refund_data'] 			= $refundData;
				$responseFlag 						= true;	
				$message = __('Successfully.');	
			} else {
				$message = __('Invalid.');
			}				 
		} catch(InputMismatchException $e){
			$message  = $e->getMessage();
		} catch(InvalidEmailOrPasswordException $e){
			$message  = $e->getMessage();
		} catch(LocalizedException $e){
			$message  = $e->getMessage();
		} catch(NoSuchEntityException $e){
			$message  = $e->getMessage();
		} catch(UserLockedException $e){
			$message  = $e->getMessage();
		} catch(\Exception $e){
			$message  = $e->getMessage();
		}
        $response['response']   	= $responseFlag;
		$response['message']    	= $message;
		echo json_encode($response);
	}
	
	/**
     * get order invoices detail
     *
     * @param \Magento\Sales\Model\Order $order.
     * @param int $cash_on_delivery.
     * @return array.
     */
	private function getOrderInvoiceDetail($order, $cash_on_delivery)
	{
		$invoices = $order->getInvoiceCollection();
		$invoiceData = array();
		foreach ($invoices as $invoice)
		{
			/*** Start: get invoice item detail. ***/
			$invoiceItemData = array();
			$invoiceItems = $invoice->getAllItems();
			foreach ($invoiceItems as $item){	
				$product 		= $this->_productload->load($item->getProduct_id()); 
				$product_image 	= $this->_apiCommonHelper->getImageUrl($product, 'product_base_image');
				$customOption   = array();
				$options 		= $item->getOrderItem()->getProductOptions();  
				if (isset($options['options']) && !empty($options['options'])) {        
			        foreach ($options['options'] as $option) {
			        	$customOption[] = array("option_label" => $option['label'], "option_vaule" => $option['value']);
			        }
			    }
				$invoiceItemData[] = array("product_id" 	=> 	$item->getProductId(), 
										"product_name"		=>	$item->getName(), 
										"product_sku"		=>	$item->getSku(), 
										"product_image"		=>	$product_image, 
										"product_price" 	=> 	$item->getPrice()*1, 
										"qty"				=>	round($item->getQty()),
										"product_subtotal"	=> 	$item->getRowTotal()*1,
										"custom_option"		=> 	$customOption 
										);
				
			}
			/*** End: get invoice item detail. ***/
			$invoiceData[] = array("invoice_id" 			=> 	$invoice->getId(), 
									"increment_id"			=>	$invoice->getIncrement_id(), 
									"subtotal"				=>	$invoice->getSubtotal() * 1, 
									"shipping_amount" 		=> 	$invoice->getShippingAmount() * 1, 
									"tax_amount" 			=> 	$invoice->getTaxAmount() * 1, 
									"discount_amount"		=>	$invoice->getDiscount_amount() * 1, 
									"discount_description" 	=> 	$invoice->getDiscount_description(), 
									"cash_on_delivery" 		=> 	$cash_on_delivery, 
									"grand_total" 			=> 	$invoice->getGrandTotal() * 1, 
									"order_currency_code" 	=> 	$invoice->getOrder_currency_code(), 
									"product_data"			=> 	$invoiceItemData
									);
			
		}
		return $invoiceData;
	}
	
	/**
     * get order shipments detail
     *
     * @param \Magento\Sales\Model\Order $order.
     * @return array.
     */
	private function getOrderShipmentDetail($order)
	{
		$shipments = $order->getShipmentsCollection();
		$shipmentData = array();
		foreach ($shipments as $shipment)
		{
			/*** Start: get shipment item detail. ***/
			$shipmentItemData = array();
			$shipmentItems = $shipment->getAllItems();
			foreach ($shipmentItems as $item){	
				$product 		= $this->_productload->load($item->getProduct_id()); 
				$product_image 	= $this->_apiCommonHelper->getImageUrl($product, 'product_base_image');
				$customOption   = array();
				$options 		= $item->getOrderItem()->getProductOptions();  
				if (isset($options['options']) && !empty($options['options'])) {        
			        foreach ($options['options'] as $option) {
			        	$customOption[] = array("option_label" => $option['label'], "option_vaule" => $option['value']);
			        }
			    }
				$shipmentItemData[] = array("product_name"		=>	$item->getName(), 
											"product_sku"		=>	$item->getSku(), 
											"product_image"		=>	$product_image, 
											"qty"				=>	round($item->getQty()),
											"custom_option"		=> 	$customOption
											);
				
			}
			/*** End: get shipment item detail. ***/
			
			/*** Start: get shipment tracks detail. ***/
			$shipmentTrack = $this->shipmentRepository->get($shipment->getId())->getTracks();
			$trackData = array();
			foreach ($shipmentTrack as $track) {
				$trackData[] = array("title"			=> $track->getTitle(),
									 "carrier_code"		=> $track->getCarrierCode(),
									 "track_number" 	=> $track->getTrack_number()
									);
			}
			/*** End: get shipment tracks detail. ***/
			
			$shipmentData[] = array("shipment_id" 			=> 	$shipment->getId(), 
									"increment_id"			=>	$shipment->getIncrement_id(), 
									"track_data"			=>	$trackData,
									"product_data"			=> 	$shipmentItemData
									);
			
		}
		return $shipmentData;
	}
	
	/**
     * get order creditmemos detail
     *
     * @param \Magento\Sales\Model\Order $order.
     * @return array.
     */
	private function getOrderCreditmemosDetail($order)
	{
		$refunds = $order->getCreditmemosCollection();
		$refundData = array();
		foreach ($refunds as $refund)
		{
			$refundItemData = array();
			$refundItems 	= $refund->getAllItems();
			foreach ($refundItems as $item){	
				$product 		= $this->_productload->load($item->getProduct_id()); 
				$product_image 	= $this->_apiCommonHelper->getImageUrl($product, 'product_base_image');
				$customOption   = array();
				$options 		= $item->getOrderItem()->getProductOptions();  
				if (isset($options['options']) && !empty($options['options'])) {        
			        foreach ($options['options'] as $option) {
			        	$customOption[] = array("option_label" => $option['label'], "option_vaule" => $option['value']);
			        }
			    }
				$refundItemData[] = array("product_id" 			=> 	$item->getProductId(), 
										  "product_name"		=>	$item->getName(), 
										  "product_sku"			=>	$item->getSku(), 
										  "product_image"		=>	$product_image, 
										  "product_price" 		=> 	$item->getPrice()*1, 
										  "qty"					=>	round($item->getQty()),
										  "product_subtotal"	=> 	$item->getRowTotal()*1,
										  "discount_amount"		=> 	$item->getDiscount_amount()*1 ,
										  "product_rowtotal"	=> 	$item->getRowTotal()*1 - $item->getDiscount_amount()*1,
										  "custom_option"		=> 	$customOption
										 );
				
			}
			$refundData[] = array(	"refund_id" 				=> 	$refund->getId(), 
									"increment_id"				=>	$refund->getIncrement_id(), 
									"subtotal"					=>	$refund->getSubtotal() * 1, 
									"shipping_amount" 			=> 	$refund->getShippingAmount() * 1, 
									"tax_amount" 				=> 	$refund->getTaxAmount() * 1, 
									"discount_amount"			=>	$refund->getDiscount_amount() * 1, 
									"discount_description" 		=> 	$refund->getDiscount_description(), 
									"adjustment_refund"			=>	$refund->getAdjustment_positive() * 1, 
									"adjustment_fee"			=>	$refund->getAdjustment_negative() * -1, 
									"grand_total" 				=> 	$refund->getGrandTotal() * 1, 
									"order_currency_code" 		=> 	$refund->getOrder_currency_code(), 
									"product_data"				=> 	$refundItemData
								 );
			
		}
		return $refundData;
	}
	
	/**
     * {@inheritdoc}
     */
	public function reorder($customerId, $order_id)
	{
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			try {
				$customer = $this->customerRepository->getById($customerId);
			} catch (NoSuchEntityException $e) {
				throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
			}
			$storeId = $this->storeManager->getStore()->getId();
			$objectManager = ObjectManager::getInstance();
			$customer = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
			$customerSession = $objectManager->create('\Magento\Customer\Model\Session');
			$customerSession->setCustomerAsLoggedIn($customer);
			$order = $objectManager->create('\Magento\Sales\Model\Order')->load($order_id);
			if($customerId == $order->getCustomer_id()){
				$cart = $objectManager->get('Magento\Checkout\Model\Cart');
				$items = $order->getItemsCollection();
				foreach ($items as $item) {
					try {
						$cart->addOrderItem($item);
					} catch (\Exception $e) {
						throw new LocalizedException(__($e->getMessage().' We can\'t add this item to your shopping cart right now.'));
					}
				}
				$cart->save();
				$responseFlag = true;	
				$message = __('Successfully.');	
			} else {
				$message = __('Invalid.');
			}				 
		} catch(InputMismatchException $e){
			$message  = $e->getMessage();
		} catch(InvalidEmailOrPasswordException $e){
			$message  = $e->getMessage();
		} catch(LocalizedException $e){
			$message  = $e->getMessage();
		} catch(UserLockedException $e){
			$message  = $e->getMessage();
		} catch(\Exception $e){
			$message  = $e->getMessage();
		}
        $response['response']   	= $responseFlag;
		$response['message']    	= $message;
		echo json_encode($response);
	}

    public function deleteInvoice($orderId)
    {
        $order = $this->_orderRepository->get($orderId);
        $invoices = $order->getInvoiceCollection();
        try {
            foreach ($invoices as $key => $invoice) {
                $this->deleteObject($invoice);
            }
        } catch (\Exception $e) {
            $this->logger->addInfo($e->getMessage());
        }
    }

    public function deleteObject($invoice)
    {
        $invoice->delete();
    }
    
}
