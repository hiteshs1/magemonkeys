<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace PetzzcoApi\CatalogApi\Api;

use Magento\Framework\Exception\InputException;

/**
 * Interface for managing wishlist.
 * @api
 * @since 100.0.2
 */
interface CatalogManagementInterface
{
	 /**
     * get sub category list
     *
     * @param string $category_id
     * @return array
     */
    public function getCategoryList($category_id);
	
	/**
     * get tax class list     
     * @return array
     */
    public function getTaxclassList();
	
	/**
     * get stock AvailabilityListlist     
     * @return array
     */
    public function getStockAvailabilityList();
	
	/**
     * get visibility list     
     * @return array
     */
    public function getVisibilityList();
	
	/**
     * get Product Type List     
     * @return array
     */
    public function getProductTypeList();
	
	/**
     * get Brand List List     
     * @return array
     */
    public function getBrandListList();
	
	/**
     * get Unit List
     * @return array
     */
    public function getUnitList();
	
	/**
     * add simple product.
     *	 
	 * @param int $customerId    
	 * @param string $product_id
	 * @param mixed $data[]
     * @return array
     */
    public function productAddSimple($customerId, $product_id='',$data);
	
	/**
     * seller my product list.
     *	 
	 * @param int $customerId    
	 * @return array
     */
    public function productMyProductList($customerId);
	
	/**
     * seller my product list.
     *	 
	 * @param int $customerId  
	 * @param mixed $product_ids[]	 
	 * @return array
     */
    public function productMyProductDelete($customerId,$product_ids);
	
	/**
     * seller my product Edit.
     *	 
	 * @param int $customerId  
	 * @param int $product_id	 
	 * @return array
     */
    public function productMyProductEdit($customerId,$product_id);
	
	/**
     * get product list
     *
     * @param string $user_id.
     * @param string $device_id
     * @param string $page
     * @param string $cat_id
     * @param string $my_product
     * @param string $list_type
     * @param string $search_text     
     * @param string $sorting
     * @param string $sorting_type
	 * @param string $brand
     * @return array
     */
    public function getProductList($user_id, $device_id, $page, $cat_id, $my_product, $list_type, $search_text, $sorting, $sorting_type, $brand);
    
	/**
     * get customer cart item count
     *
     * @param int $customerId The customer ID.
     * @return array
     */
    public function getCartItemCount($customerId);
	
	/**
     * get product detail, review & related product
     *
     * @param string $user_id.
     * @param string $device_id
     * @param string $product_id
     * @return array
     */
    public function getProductDetail($user_id, $device_id, $product_id);
    
    
}
