<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace PetzzcoApi\CatalogApi\Model;

use PetzzcoApi\CatalogApi\Api\CatalogManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Store\Model\App\Emulation as AppEmulation;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\Exception\InvalidEmailOrPasswordException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\InputMismatchException;
use Magento\Framework\Exception\State\UserLockedException;
use Magento\Store\Model\Store;
use Magento\Framework\Stdlib\DateTime\DateTime;
use PetzzcoApi\CustomerApi\Helper\Data as ApiCommonHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\DB\Select;
use Webkul\Marketplace\Helper\Data as HelperData;
use Webkul\Marketplace\Helper\Data as MarketplaceHelperData;
use Webkul\Marketplace\Model\Product as SellerProduct;
use Magento\Catalog\Api\CategoryLinkManagementInterface;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as MpProductCollection;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\Webapi\Rest\Request;
use Webkul\Marketplace\Helper\Data as MpHelper;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory as SellerCollection;
use Magento\Framework\Registry;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;


/**
 * Handle various wish list actions.
 *
 */
class CatalogManagement implements CatalogManagementInterface
{
	 /**
     * @var \Magento\Catalog\Model\Product\Initialization\Helper\ProductLinks
     */
    protected $_productLinks;
	/**
     * @var \Magento\Catalog\Model\Product\Link\Resolver
     */
    protected $linkResolver;
	/**
     * @var SellerProduct
     */
    protected $_sellerProductCollectionFactory;
	/**
     * Core registry.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;
	/**
     * @var PriceCurrencyInterface
     */
    protected $_priceCurrency;
	
	/**
     * @var \Magento\Catalog\Helper\Image
     */
    protected $_imageHelper;
	
	/**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;
	
	/**
     * @var MpProductCollection
     */
    protected $mpProductCollection;
	
	/**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute
     */
    protected $eavAttribute;
	
	/** @var \Magento\Catalog\Model\Product */
    protected $_productlists;
	
	/**
     * @var MpHelper
     */
    protected $mpHelper;
	
	protected $request;
	/**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_helper;
	/** @var \Magento\Catalog\Api\ProductRepositoryInterface  */
    protected $_productRepositoryInterface;
	
	/**
     * @var CategoryFactory
     */
    protected $categoryModel;
	/**
     * @var CustomerFactory
     */
    protected $customerModel;
	 /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;
	/**
     * @var CategoryLinkManagementInterface
     */
    protected $categoryLinkManagement;
	
	 /**
     * @var \Magento\Catalog\Model\Product\TypeTransitionManager
     */
    protected $_catalogProductTypeManager;

	/**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;
	
	/**
     * @var eventManager
     */
    protected $_eventManager;
	
	/**
     * @var Webkul\Marketplace\Model\Product
     */
    protected $SellerProduct;
    
	
	/**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;
	
	 /**
     * @var MarketplaceHelperData
     */
    protected $_marketplaceHelperData;
	
	/**
     * @var HelperData
     */
    protected $helper;
	
	protected $eavConfig;
	
	 /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;
	
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;
    
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    
    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;
    
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_connection;
    
    /**
     *@var \Magento\Store\Model\App\Emulation
     */
    protected $appEmulation;
    
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

	/**
     * @var \Magento\Wishlist\Model\Wishlist
     */
    protected $wishlist;
    
    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $_catalogProductVisibility;
    
    
    /**
     * @var PetzzcoApi\CustomerApi\Helper\Data;
     */
    protected $_apiCommonHelper;
    
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;
    
    /**
     * @var  \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $_stockItemRepository;
	
	
	protected $_scopeConfig;
	
	/**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    
    /**
	 * @param \Magento\Framework\Event\Manager              $eventManager
	 * @param HelperData       								$helper
     * @param CustomerRepositoryInterface 					$customerRepository
     * @param StoreManagerInterface 						$storeManager
     * @param \Magento\Catalog\Model\CategoryFactory 		$categoryFactory
     * @param ProductCollectionFactory 						$productCollectionFactory
     * @param \Magento\Framework\App\ResourceConnection 	$resourceConnection
     * @param \Magento\Wishlist\Model\Wishlist 				$wishlist
     * @param \Magento\Catalog\Model\Product\Visibility 	$catalogProductVisibility
     * @param \Magento\Framework\Stdlib\DateTime\DateTime 	$dateTime
     * @param ApiCommonHelper 								$apiCommonHelper
     * @param \Magento\Customer\Model\CustomerFactory       $customerFactory
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockItemRepository
	 * @param MarketplaceHelperData                         $marketplaceHelperData
	 * @param \Magento\Framework\Registry                   $registry
	 * @param Webkul\Marketplace\Model\Product              $SellerProduct
	 * @param \Magento\Catalog\Model\ProductFactory 		$productFactory
	 * @param CustomerFactory                               $customerModel
	 * @param CategoryFactory                               $categoryModel
	 * @param \Magento\Catalog\Api\ProductRepositoryInterface     $productRepositoryInterface
	 * @param MpHelper                                  	$mpHelper
	 * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute
	 * @param MpProductCollection                       	$mpProductCollection
	 * @param Context                                   	$context
	 * @param PriceCurrencyInterface                    	$priceCurrency
	 * @param SellerCollection 								$sellerCollectionFactory
	 * @param Registry          							$coreRegistry
	 * @param SellerProduct     							$sellerProductCollectionFactory
	 * @param ProductRepositoryInterface 					$productRepository
	 * @param Product                                		$product
	 * @param \Magento\Catalog\Model\Product\Link\Resolver  $linkResolver
	 * @param \Magento\Catalog\Model\Product\Initialization\Helper\ProductLinks $productLinks
	 
	 
     */
    public function __construct(		
		\Magento\Catalog\Model\Product\Initialization\Helper\ProductLinks $productLinks,
		\Magento\Catalog\Model\Product\Link\Resolver $linkResolver,
		Product $product,
		ProductRepositoryInterface $productRepository = null,
		SellerProduct $sellerProductCollectionFactory,
		Registry $coreRegistry,
		SellerCollection $sellerCollectionFactory,
		PriceCurrencyInterface $priceCurrency,
		\Magento\Catalog\Block\Product\Context $context,
		MpProductCollection $mpProductCollection,
		\Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute,
		MpHelper $mpHelper,
		Request $request,
		\Webkul\Marketplace\Helper\Data $webkulhelper,
		\Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
		CategoryLinkManagementInterface $categoryLinkManagement,
		\Magento\Catalog\Model\ProductFactory $productFactory,
		\Magento\Framework\Event\Manager $eventManager,
		\Magento\Framework\Registry $registry,
		\Magento\Eav\Model\Config $eavConfig,
		\Magento\Framework\ObjectManagerInterface $objectManager,
        AppEmulation $appEmulation,
        StoreManagerInterface $storeManager,
        CustomerRepositoryInterface $customerRepository,
        CategoryFactory $categoryFactory,
        ProductCollectionFactory $productCollectionFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Wishlist\Model\Wishlist $wishlist,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        DateTime $dateTime,
        ApiCommonHelper $apiCommonHelper,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockItemRepository,		
        \Magento\Framework\Pricing\Helper\Data $priceHelper,		
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		MarketplaceHelperData $marketplaceHelperData,
		HelperData $helper,
		\Magento\Catalog\Model\Product\TypeTransitionManager $catalogProductTypeManager,
		SellerProduct $SellerProduct,
		MpProductCollection $mpProductCollectionFactory,
		\Webkul\Marketplace\Model\ProductFactory $mpProductFactory,
		\Magento\Framework\Stdlib\DateTime\DateTime $date,
		CustomerFactory $customerModel,
		CategoryFactory $categoryModel
    ) {
		$this->_coreRegistry 				= $coreRegistry;
		$this->_priceCurrency 				= $priceCurrency;
		$this->eavAttribute 				= $eavAttribute;
		$this->mpHelper 					= $mpHelper;
		$this->request 						= $request;
		$this->_webkulhelper				= $webkulhelper;
		$this->_productRepositoryInterface  = $productRepositoryInterface;
		$this->_catalogProductTypeManager   = $catalogProductTypeManager;
		$this->_productFactory 				= $productFactory;
		$this->_eventManager 				= $eventManager;
		$this->helper 						= $helper;
		$this->eavConfig 					= $eavConfig;
		$this->_objectManager 				= $objectManager;
        $this->storeManager 				= $storeManager;
        $this->appEmulation 				= $appEmulation;
        $this->customerRepository 			= $customerRepository;
        $this->_categoryFactory 			= $categoryFactory;
        $this->_productCollectionFactory 	= $productCollectionFactory;
        $this->_connection 					= $resourceConnection->getConnection();
        $this->wishlist 					= $wishlist;
        $this->_catalogProductVisibility 	= $catalogProductVisibility;
        $this->dateTime     				= $dateTime;
        $this->_apiCommonHelper 			= $apiCommonHelper;
        $this->customerFactory 				= $customerFactory;
        $this->_stockItemRepository 		= $stockItemRepository;		
		$this->priceHelper 					= $priceHelper;
		$this->_scopeConfig 				= $scopeConfig;//for getting seller setting
		$this->_marketplaceHelperData 		= $marketplaceHelperData;
		$this->_registry 					= $registry;
		$this->_SellerProduct 				= $SellerProduct;
		$this->categoryLinkManagement 		= $categoryLinkManagement;
		$this->_mpProductCollectionFactory  = $mpProductCollectionFactory;
		$this->_mpProductFactory 			= $mpProductFactory;
		$this->_date 						= $date;
		$this->customerModel 				= $customerModel;
		$this->categoryModel 				= $categoryModel;
		$this->mpProductCollection 			= $mpProductCollection;
		$this->productFactory 				= $productFactory;
		$this->_imageHelper 				= $context->getImageHelper();
		$this->_sellerCollectionFactory 	= $sellerCollectionFactory;
		$this->_sellerProductCollectionFactory = $sellerProductCollectionFactory;
		$this->productRepository 			= $productRepository;
		$this->_product 					= $product;
		$this->_linkResolver 				= $linkResolver;
		$this->_productLinks 				= $productLinks;
		
    }
	
	 /**
     * Get round price without currency symbol
     */
    public function getRoundPrice($price)
    {
        $price = $this->_priceCurrency->round($price);
        return $price;
    }

	/**
     * {@inheritdoc}
     */
    public function getCategoryList($category_id){
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			
			if($category_id != '') {				
				$categoryArr = explode(',',$category_id);
			} else {
				$categoryArr[] = 2;
			}
			$category_data = array();
			//print_r($categoryArr);die;				
			foreach($categoryArr as $category_id){
				$category = $this->_categoryFactory->create()->load($category_id);
				$childrenCategories = $category->getChildrenCategories();		
				foreach ($childrenCategories as $subcat) {
					$_category = $this->_categoryFactory->create()->load($subcat->getId());
					if(!$_category->getInclude_in_menu()) {
						continue;
					}
					$cat_image = ($_category->getImageUrl())? $this->storeManager->getStore()->getBaseUrl().$_category->getImageUrl() : '';
					$subCategory = $_category->getChildrenCategories();
					$is_sub = ($subCategory->count())? true : false ;
					$category_data[] = array("cat_id" => $_category->getId() , "cat_name" => $_category->getName() ,"cat_image" => $cat_image, "is_sub" => $is_sub );
				}
			}	
			
			$response['category_data'] = $category_data;
			$message = __('successfully.');
			$responseFlag = true;
		} catch(Exception $e){
			$message  = $e->getMessage();
		}
		
		$response['response'] 	= $responseFlag;
		$response['message']  	= $message;
		echo json_encode($response);
	}
	
	/**
     * {@inheritdoc}
     */
    public function getTaxclassList(){
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			
			$taxClassList = $this->_objectManager->create(\Magento\Tax\Model\ClassModel::class)
            ->getCollection()
            ->addFieldToFilter('class_type', 'PRODUCT');
			$tax_class_data = array();
			$tax_class_data[] = array("tax_class_id" => 0 , "tax_class_name" => "None");
			foreach ($taxClassList as $tax):
				$tax_class_data[] = array("tax_class_id" => $tax->getId() , "tax_class_name" => $tax->getClassName());
			endforeach;
			
			$response['tax_class_data'] = $tax_class_data;
			$message = __('successfully.');
			$responseFlag = true;
		} catch(Exception $e){
			$message  = $e->getMessage();
		}
		
		$response['response'] 	= $responseFlag;
		$response['message']  	= $message;
		echo json_encode($response);
	}
	
	/**
     * {@inheritdoc}
     */
    public function getStockAvailabilityList(){
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			
			$is_in_stock = array();
			$is_in_stock[] = array("is_in_stock_id" => 1 , "is_in_stock_name" => "In Stock");
			$is_in_stock[] = array("is_in_stock_id" => 0 , "is_in_stock_name" => "Out of Stock");
			
			$response['is_in_stock_data'] = $is_in_stock;
			$message = __('successfully.');
			$responseFlag = true;
		} catch(Exception $e){
			$message  = $e->getMessage();
		}
		
		$response['response'] 	= $responseFlag;
		$response['message']  	= $message;
		echo json_encode($response);
	}
    
	/**
     * {@inheritdoc}
     */
    public function getVisibilityList(){
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			
			$visibility = array();
			$visibility[] = array("visibility_id" => 1 , "visibility_name" => "Not Visible Individually");
			$visibility[] = array("visibility_id" => 2 , "visibility_name" => "Catalog");
			$visibility[] = array("visibility_id" => 3 , "visibility_name" => "Search");
			$visibility[] = array("visibility_id" => 4 , "visibility_name" => "Catalog, Search");
			
			$response['visibility_data'] = $visibility;
			$message = __('successfully.');
			$responseFlag = true;
		} catch(Exception $e){
			$message  = $e->getMessage();
		}
		
		$response['response'] 	= $responseFlag;
		$response['message']  	= $message;
		echo json_encode($response);
	}
	
	
	/**
     * {@inheritdoc}
     */
    public function getProductTypeList(){		
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			
			$alloweds = explode(',', $this->getAllowedProductType());
			$data = [
				'simple' => __('Simple'),
				'downloadable' => __('Downloadable'),
				'virtual' => __('Virtual'),
				'configurable' => __('Configurable'),
				'grouped' => __('Grouped Product'),
				'bundle' => __('Bundle Product'),
			];
			$allowedproducts = [];
			if (isset($alloweds)) {
				foreach ($alloweds as $allowed) {
					if (!empty($data[$allowed])) {
						array_push(
							$allowedproducts,
							['value' => $allowed, 'label' => $data[$allowed]]
						);
					}
				}
			}
			
			$response['allowed_product_types_data'] = $allowedproducts;
			$message = __('successfully.');
			$responseFlag = true;
		} catch(Exception $e){
			$message  = $e->getMessage();
		}
		
		$response['response'] 	= $responseFlag;
		$response['message']  	= $message;
		echo json_encode($response);
	}
    
	/* get allowed product type*/
	 public function getAllowedProductType()
    {
        $productTypes = $this->_scopeConfig->getValue(
            'marketplace/product_settings/allow_for_seller',            
			\Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $data = explode(',', $productTypes);
        foreach ($data as $key => $value) {
            if ($value == 'grouped') {
                if ($this->_moduleManager->isEnabled('Webkul_MpGroupedProduct')) {
                    $data['grouped'] = __('Grouped Product');
                } else {
                    unset($data[$key]);
                }
            }
            if ($value == 'bundle') {
                if ($this->_moduleManager->isEnabled('Webkul_MpBundleProduct')) {
                    $data['bundle'] = __('Bundle Product');
                } else {
                    unset($data[$key]);
                }
            }
        }
        return implode(',', $data);
    }


	/**
     * {@inheritdoc}
     */
    public function getBrandListList(){			
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			
			$this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
			->get('Magento\Framework\App\ResourceConnection');
			$connection= $this->_resources->getConnection();

			$tableName = $this->_resources->getTableName('bf_brand_entity');
						
			$sql = "select * from " . $tableName . " where visibility=1";
			$getData = $connection->fetchAll($sql);
			$brandData = [];
			$siteUrl = $this->storeManager->getStore()->getBaseUrl();
			if($getData){	
				foreach($getData as $data){
					$brandData[] = array("brand_id" => $data['entity_id'],"brand_name" => $data['name'],"url_key" => $data['url_key'],"image" => $siteUrl.'pub/media/catalog/brand/'.$data['image'],"description" => $data['description']); 				
				}
			}
			
			
			$response['brand_data'] = $brandData;
			$message = __('successfully.');
			$responseFlag = true;
		} catch(Exception $e){
			$message  = $e->getMessage();
		}
		
		$response['response'] 	= $responseFlag;
		$response['message']  	= $message;
		echo json_encode($response);
	}
	
	/**
     * {@inheritdoc}
     */
    public function getUnitList(){			
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			$attribute = $this->eavConfig->getAttribute('catalog_product', 'unit');
			$options = $attribute->getSource()->getAllOptions();
			$unit = [];
			if($options){
				foreach($options as $attributeVal){
					if(!empty($attributeVal['value'])){
						$unit[] = array("label" => $attributeVal['label'],"value" => $attributeVal['value']); 				
					}
				}
			}
			
			$response['unit_data'] = $unit;
			$message = __('successfully.');
			$responseFlag = true;
		} catch(Exception $e){
			$message  = $e->getMessage();
		}
		
		$response['response'] 	= $responseFlag;
		$response['message']  	= $message;
		echo json_encode($response);
	}
	
	/**
     * {@inheritdoc}
     */
    public function productAddSimple($customerId, $product_id='',$data){			
		$responseFlag = false;
		$message = '';
		$sku = '';
		$response = array();
		try{
						
			//echo print_r($this->request->getParams());die;
			$helper = $this->helper;
			$isPartner = $helper->isSellerMobile($customerId);
			if ($isPartner == 1) {
				$productId = $product_id;
				$wholedata = $data;
				
				$wholedata['set'] = 4; // set static value for attribute set
				$wholedata['type'] = 'simple'; // set static value for product type
				
				$wholedata['product']['stock_data']['manage_stock'] = 1; // set static value for manage_stock
				$wholedata['product']['stock_data']['use_config_manage_stock'] = 1; // set static value for use_config_manage_stock
				if($wholedata['product']['weight']>0){
					$wholedata['product']['product_has_weight'] = 1; // set static value for product_has_weight
				}
				
				$wholedata['id'] = $product_id;
				
				
				try {
					$returnArr = [];
					//if ($this->getRequest()->isPost()) {
						
						$skuType = $helper->getSkuType();
						$skuPrefix = $helper->getSkuPrefix();
						if ($skuType == 'dynamic' && !$productId) {							
							$sku = $skuPrefix.$wholedata['product']['name'];
							$wholedata['product']['sku'] = $this->checkSkuExist($sku);
						}
						//print_r($wholedata);die;
						//list($errors, $wholedata) = $this->validatePost($wholedata);

						//if (empty($errors)) {
							$returnArr = self::saveProductData(
								$customerId,
								$wholedata
							);
							$productId = $returnArr['product_id']; 
					
							/*get product sku by id*/
							$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
							$product = $objectManager->get('Magento\Catalog\Api\ProductRepositoryInterface')->getById($productId);
							$sku = $product->getSku();
														
						/*} else {
							foreach ($errors as $message) {
								$this->messageManager->addError($message);
							}
							$this->getDataPersistor()->set('seller_catalog_product', $wholedata);
						}*/
					//}
					
					$helper->clearCache();
					$responseFlag 	= true;
					$message  	= __('Your product has been successfully saved');
					
				} catch (\Magento\Framework\Exception\LocalizedException $e) {
					
					$message = $e->getMessage();
					$responseFlag = false;
					
				} catch (\Exception $e) {
					$message = $e->getMessage();
					$responseFlag = false;
				}
			} else {				
				$message = __('You need to register as seller.');
				$responseFlag = false;
			}
			
			
		} catch(Exception $e){
			$message  = $e->getMessage();
		}
		
		$response['response'] 	= $responseFlag;
		$response['sku'] 	= $sku;
		$response['message']  	= $message;
		echo json_encode($response);
	}
	
	
	/**
     * {@inheritdoc}
     */
    public function productMyProductList($customerId){			
		$responseFlag = false;
		$message = '';		
		$response = array();
		$product_data = '';
		$products_data = array();
		try{
			$storeId = $this->mpHelper->getCurrentStoreId();
            $websiteId = $this->mpHelper->getWebsiteId();
            /*if (!($customerId = $this->_customerSession->getCustomerId())) {
                return false;
            }*/
			
			//echo print_r($this->request->getParams());die;
            if (!$this->_productlists) {
				$responseFlag = true;
				$paramData = $this->request->getParams();
                $filter = '';
                $filterStatus = '';
                $filterDateFrom = '';
                $filterDateTo = '';
                $from = null;
                $to = null;
				$orderBy = 'mageproduct_id';
				$ascDesc = 'DESC';
				$brandId = '';
				
                if (isset($paramData['brand']) && $paramData['brand']!='') {
                    $brandId = $paramData['brand'];
                }
				if (isset($paramData['order_by']) && $paramData['order_by']!='') {
                    $orderBy = $paramData['order_by'];
                }
                if (isset($paramData['asc_desc']) && $paramData['asc_desc']!='') {
                    $ascDesc = $paramData['asc_desc'];
                }
				if (isset($paramData['s'])) {
                    $filter = $paramData['s'] != '' ? $paramData['s'] : '';
                }
                if (isset($paramData['status'])) {
                    $filterStatus = $paramData['status'] != '' ? $paramData['status'] : '';
                }
                if (isset($paramData['from_date'])) {
                    $filterDateFrom = $paramData['from_date'] != '' ? $paramData['from_date'] : '';
                }
                if (isset($paramData['to_date'])) {
                    $filterDateTo = $paramData['to_date'] != '' ? $paramData['to_date'] : '';
                }
                if ($filterDateTo) {
                    $todate = date_create($filterDateTo);
                    $to = date_format($todate, 'Y-m-d 23:59:59');
                }
                if (!$to) {
                    $to = date('Y-m-d 23:59:59');
                }
                if ($filterDateFrom) {
                    $fromdate = date_create($filterDateFrom);
                    $from = date_format($fromdate, 'Y-m-d H:i:s');
                }
                if (!$from) {
                    $from = date('Y-m-d 23:59:59', strtotime($from));
                }

                $eavAttribute = $this->eavAttribute;
                $proAttId = $eavAttribute->getIdByCode('catalog_product', 'name');
                $proStatusAttId = $eavAttribute->getIdByCode('catalog_product', 'status');
                $proBrandAttId = $eavAttribute->getIdByCode('catalog_product', 'brand');

                $catalogProductEntity = $this->mpProductCollection->create()->getTable('catalog_product_entity');
                $catalogProductEntityVarchar = $this->mpProductCollection->create()
                                              ->getTable('catalog_product_entity_varchar');
                $catalogProductEntityInt = $this->mpProductCollection->create()
                                          ->getTable('catalog_product_entity_int');

                /* Get Seller Product Collection for current Store Id */

                $storeCollection = $this->mpProductCollection->create()
                                  ->addFieldToFilter(
                                      'seller_id',
                                      $customerId
                                  )->addFieldToSelect(
                                      ['mageproduct_id']
                                  );
                $storeCollection->getSelect()->join(
                    $catalogProductEntityVarchar.' as cpev',
                    'main_table.mageproduct_id = cpev.entity_id'
                )->where(
                    'cpev.store_id = '.$storeId.' AND
                  cpev.value like "%'.$filter.'%" AND
                  cpev.attribute_id = '.$proAttId
                );

                $storeCollection->getSelect()->join(
                    $catalogProductEntityInt.' as cpei',
                    'main_table.mageproduct_id = cpei.entity_id'
                )->where(
                    'cpei.store_id = '.$storeId.' AND
                  cpei.attribute_id = '.$proStatusAttId
                );
				
				
				/*sr brand filter*/
				if($brandId!=''){
					$storeCollection->getSelect()->join(
						$catalogProductEntityInt.' as cpbr',
						'main_table.mageproduct_id = cpbr.entity_id'
					)->where(
						'cpbr.store_id = '.$storeId.' AND				
						cpbr.attribute_id = '.$proBrandAttId
					);
				}
				
				/*$storeCollection->getSelect()->where(
                        'cpbr.value = '.$brandId
                    );*/

                if ($filterStatus) {
                    $storeCollection->getSelect()->where(
                        'cpei.value = '.$filterStatus
                    );
                }

                $storeCollection->getSelect()->join(
                    $catalogProductEntity.' as cpe',
                    'main_table.mageproduct_id = cpe.entity_id'
                );

                if ($from && $to) {
                    $storeCollection->getSelect()->where(
                        "cpe.created_at BETWEEN '".$from."' AND '".$to."'"
                    );
                }

                $storeCollection->getSelect()->group('mageproduct_id');

                $storeProductIDs = $storeCollection->getAllIds();

                /* Get Seller Product Collection for 0 Store Id */

                $adminStoreCollection = $this->mpProductCollection->create();

                $adminStoreCollection->addFieldToFilter(
                    'seller_id',
                    $customerId
                )->addFieldToSelect(
                    ['mageproduct_id']
                );

                $adminStoreCollection->getSelect()->join(
                    $catalogProductEntityVarchar.' as cpev',
                    'main_table.mageproduct_id = cpev.entity_id'
                )->where(
                    'cpev.store_id = 0 AND
                  cpev.value like "%'.$filter.'%" AND
                  cpev.attribute_id = '.$proAttId
                );
				
				/*sr brand filter*/
				if($brandId!=''){
					$adminStoreCollection->getSelect()->join(
						$catalogProductEntityInt.' as cpbr',
						'main_table.mageproduct_id = cpbr.entity_id'
					)->where(
						'cpbr.store_id = 0 AND				
						cpbr.attribute_id = '.$proBrandAttId
					);
									
					$adminStoreCollection->getSelect()->where(
							'cpbr.value = '.$brandId
						);
				}
				
				
                $adminStoreCollection->getSelect()->join(
                    $catalogProductEntityInt.' as cpei',
                    'main_table.mageproduct_id = cpei.entity_id'
                )->where(
                    'cpei.store_id = 0 AND
                  cpei.attribute_id = '.$proStatusAttId
                );

                if ($filterStatus) {
                    $adminStoreCollection->getSelect()->where(
                        'cpei.value = '.$filterStatus
                    );
                }

                $adminStoreCollection->getSelect()->join(
                    $catalogProductEntity.' as cpe',
                    'main_table.mageproduct_id = cpe.entity_id'
                );
                if ($from && $to) {
                    $adminStoreCollection->getSelect()->where(
                        "cpe.created_at BETWEEN '".$from."' AND '".$to."'"
                    );
                }

                $adminStoreCollection->getSelect()->group('mageproduct_id');

                $adminProductIDs = $adminStoreCollection->getAllIds();

                $productIDs = array_merge($storeProductIDs, $adminProductIDs);

                $collection = $this->mpProductCollection->create()
							
                            ->addFieldToFilter(
                                'seller_id',
                                $customerId
                            )							
                            ->addFieldToFilter(
                                'mageproduct_id',
                                ['in' => $productIDs]
                            );
				//$collection->joinProductTable(); 		
                $collection->setOrder($orderBy,$ascDesc);
				$product_data = $collection;
				       
				foreach ($product_data as $products) {
					$product = self::getProductData($products->getMageproductId());
					$image_url = self::imageHelperObj()->init($product, 'product_page_image_small')
								->setImageFile($product->getImage())
								->getUrl();
					
				
				   if ($product->getPrice()*1) {
						$price = $this->getRoundPrice($product->getPrice());
					} else {
						$price = '0.00';
					}
					
					$products_data[] = array('id'=>$product->getId(),'name'=>$product->getName(),'image'=>$image_url,'price'=>$price,'brand'=>$product->getAttributeText('brand'));
					
				}
				
				
            }
			
		} catch(Exception $e){
			$message  = $e->getMessage();
		}
		
		$response['response'] 	= $responseFlag;
		$response['product_data'] = $products_data;
		$response['message']  	= $message;
		echo json_encode($response);
	}
	
	public function getProductData($id = '')
    {
        return $this->productFactory->create()->load($id);
    }
	
	public function imageHelperObj()
    {
        return $this->_imageHelper;
    }
	
	/**
     * Get formatted by price and currency.
     *
     * @param   $price
     * @param   $currency
     *
     * @return array || float
     */
    public function getFormatedPrice($price, $currency)
    {
        return $this->_priceCurrency->format(
            $price,
            true,
            2,
            null,
            $currency
        );
    }
	 /**
     * Get Seller Collection
     *
     * @return \Webkul\Marketplace\Model\ResourceModel\Seller\Collection
     */
    public function getSellerCollection()
    {
        return $this->_sellerCollectionFactory->create();
    }
	
	/**
     * Return the Seller Model Collection Object.
     *
     * @return \Webkul\Marketplace\Model\ResourceModel\Seller\Collection
     */
    public function getSellerCollectionObj($sellerId)
    {
        $collection = $this->getSellerCollection();
        $collection->addFieldToFilter('seller_id', $sellerId);
        $collection->addFieldToFilter('store_id', 0);
        // If seller data doesn't exist for current store

        if (!$collection->getSize()) {
            $collection = $this->getSellerCollection();
            $collection->addFieldToFilter('seller_id', $sellerId);
            $collection->addFieldToFilter('store_id', 0);
        }

        return $collection;
    }

	
	public function isSeller($customerId)
    {
        $sellerStatus = 0;        
        $model = $this->getSellerCollectionObj($customerId);
        foreach ($model as $value) {
            if ($value->getIsSeller() == 1) {
                $sellerStatus = $value->getIsSeller();
            }
        }

        return $sellerStatus;
    }

	/**
     * {@inheritdoc}
     */
    public function productMyProductDelete($customerId, $product_ids){			
		$responseFlag = false;
		$message = '';
		$sku = '';
		$response = array();
		try{
			$isPartner = self::isSeller($customerId);			
            $assignIds = [];
			//echo $isPartner;die;
            if ($isPartner == 1) {
				//echo print_r($product_ids);
				
					$wholedata = $this->request->getParams();

                    $ids = $product_ids;

                    $sellerId = $customerId;
                    $this->_coreRegistry->register('isSecureArea', 1);
                    $deletedIdsArr = [];
                    $sellerProducts = $this->_sellerProductCollectionFactory
                    //->create()
					->getCollection()
                    ->addFieldToFilter(
                        'mageproduct_id',
                        ['in' => $ids]
                    )->addFieldToFilter(
                        'seller_id',
                        $sellerId
                    );
					//echo "<pre>";print_r($sellerProducts);die;
                    foreach ($sellerProducts as $sellerProduct) {
                        array_push($deletedIdsArr, $sellerProduct['mageproduct_id']);
                        $wholedata['id'] = $sellerProduct['mageproduct_id'];
                        $this->_eventManager->dispatch(
                            'mp_delete_product',
                            [$wholedata]
                        );
                        /*if ($this->_customerSession->getAssignProductIds()) {
                            $assignIds = $this->_customerSession->getAssignProductIds();
                        }*/
                        //if (!in_array($sellerProduct['mageproduct_id'], $assignIds)) {
                            $sellerProduct->delete();
                        //}
                    }

                    foreach ($deletedIdsArr as $id) {
                        try {
                           // if (!in_array($id, $assignIds)) {
                                $product = $this->_productRepositoryInterface->getById($id);
                                $this->_productRepositoryInterface->delete($product);
                           // }
                        } catch (\Exception $e) {
                            $message  = $e->getMessage();
                        }
                    }
                    $unauthIds = array_diff($ids, $deletedIdsArr);
                    $this->_coreRegistry->unregister('isSecureArea');
                    if (!count($unauthIds)) {
                        // clear cache
                        $this->helper->clearCache();                       
						$message = __('Products are successfully deleted from your account.');
						$responseFlag = true;
                    }
				
				
				
			}else{
				$message = 'You are not allowed to delete these records';
			}			
		} catch(Exception $e){
			$message  = $e->getMessage();
		}
		
		$response['response'] 	= $responseFlag;	
		$response['message']  	= $message;
		echo json_encode($response);
	}
	
	
	/**
     * {@inheritdoc}
     */
    public function productMyProductEdit($customerId, $product_id){			
		$responseFlag = false;
		$message = '';
		$productData = '';
		$response = array();
		try{
						
			$product_coll = self::getProduct($product_id);
			$productData = array(
							'name'=>$product_coll->getName(),
							'description'=>$product_coll->getDescription(),
							'short_description'=>$product_coll->getShortDescription(),
							'status'=>$product_coll->getStatus(),
							'sku'=>$product_coll->getsku(),
							'price'=>$this->getRoundPrice($product_coll->getPrice()),
							'special_price'=>$this->getRoundPrice($product_coll->getSpecialPrice()),
							'brand'=>$product_coll['brand'],
							'unit'=>$product_coll['unit'],
							'price_specification'=>$product_coll['price_specification'],
							'delivery_estimate_time'=>$product_coll['delivery_estimate_time'],
							'package_height'=>$product_coll['package_height'],
							'package_width'=>$product_coll['package_width'],
							'directions'=>$product_coll['directions'],
							'ingredients'=>$product_coll['ingredients'],
							'pcs_per_package'=>$product_coll['pcs_per_package'],
							'qty'=>$product_coll['quantity_and_stock_status']['qty'],
							'tax_class_id'=>$product_coll->getData('tax_class_id'),
							'weight'=>$product_coll->getWeight(),
							'category_ids'=>$product_coll->getCategoryIds()
							
							
							);
			$responseFlag = true;
			//echo "<pre>";print_r($productData);die;
						
		} catch(Exception $e){
			$message  = $e->getMessage();
		}
		
		$response['product_data'] 	= $productData;	
		$response['response'] 	= $responseFlag;	
		$response['message']  	= $message;
		echo json_encode($response);
	}
	
	public function getProduct($id)
    {
        return $this->_product->load($id);
    }
	
	
	
	/**
     * {@inheritdoc}
     */
	public function getProductList($user_id, $device_id, $page, $cat_id, $my_product, $list_type, $search_text, $sorting, $sorting_type, $brand)
	{
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			try {
				$wishlistProductIds = array();
				if($user_id) {
					$customer = $this->customerRepository->getById($user_id);
					if ($customer->getCustomAttribute('device_id')->getValue() == $device_id) {
						/******* Start: get customer Wishlist Data *******/
						$wishlistCollection = $this->wishlist->loadByCustomerId($user_id)->getItemCollection();
						foreach ($wishlistCollection as $item){
							$wishlistProductIds[] = $item->getProduct()->getId();
						}
						/******* End: get customer Wishlist Data *******/
					} else {
						$responseFlag = true;
					}
				}
				if ($responseFlag) {
					//$responseFlag = false;
					//throw new InputMismatchException(__('Oops. Something went wrong. Please try again later.'));
				}	
			} catch (NoSuchEntityException $e) {
				throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
			}
			
			$currentStore = $this->storeManager->getStore();
			$baseUrl  = $currentStore->getBaseUrl();
			$mediaUrl = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
			
			$objectManager = ObjectManager::getInstance();
			
			$imageHelper  	= $objectManager->get('\Magento\Catalog\Helper\Image');
			$storeId = $this->storeManager->getStore()->getId();
			$attributes = $objectManager->get('Magento\Catalog\Model\Config')->getProductAttributes();
			$_productsCollection = $this->_productCollectionFactory->create()
										->addAttributeToSelect($attributes)
										->addAttributeToSelect('brand')
										->setStoreId($storeId);
										
			$product_data		= array();
			if ($list_type != '') {
				if($list_type == "bestselling") {
					/*$select = $this->_connection->select()
											->from($this->_connection->getTableName('sales_order_item'), 'product_id')
											->order('sum(`qty_ordered`) Desc')
											->group('product_id')
											->limit(100);
					$producIds = array(); 
					foreach ($this->_connection->query($select)->fetchAll() as $row) {
					   $producIds[] = $row['product_id'];
					}
					$_productsCollection->addAttributeToFilter('entity_id', array('in'=>$producIds));*/
					$_category =  $this->_categoryFactory->create()->load(283); //best seller category id
					$_productsCollection->addCategoryFilter($_category);
				} elseif ($list_type == "valuepacks") {
						$_category =  $this->_categoryFactory->create()->load(287); // value pack category id
						$_productsCollection->addCategoryFilter($_category);
				} elseif ($list_type == "newrelease") {
					$todayDate= date('Y-m-d', time());
					$_productsCollection->addAttributeToFilter('news_from_date', array('date'=>true, 'to'=> $todayDate))
										->addAttributeToFilter('news_to_date', array('date'=>true, 'from'=> $todayDate))
										->addAttributeToSort('news_from_date','desc');	
				} 
			} elseif ($cat_id !== '') {
				//$cID = ($sub_cat_id != '')? $sub_cat_id : $cat_id;
				//$_category  =  $this->_categoryFactory->create()->load($cID);
				//$_productsCollection->addCategoryFilter($_category);
				$_productsCollection->addCategoriesFilter(['in' => explode(',',$cat_id)]);
			}
			
			if($search_text != '') {
				$_productsCollection->addAttributeToFilter(	 [
															   ['attribute' => 'name', 'like' => '%'.$search_text.'%'],
															   ['attribute' => 'sku', 'like' => '%'.$search_text.'%'],
															   ['attribute' => 'description', 'like' => '%'.$search_text.'%'],
															   ['attribute' => 'short_description', 'like' => '%'.$search_text.'%'],
															   ['attribute' => 'price', 'like' => '%'.$search_text.'%']
															 ]);
			}
			
			$_productsCollection->addMinimalPrice()
								->addFinalPrice()
								->addTaxPercents()
								->addUrlRewrite()
								->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());
								
			if ($sorting != '') {
				$sorting_type = ($sorting_type != '') ? $sorting_type : 'ASC';
				if ($sorting == 'position') {
					$_productsCollection->addAttributeToSort('position', $sorting_type);
				} elseif ($sorting == 'product_name') {
					$_productsCollection->addAttributeToSort('name', $sorting_type);
				} elseif ($sorting == 'date') {
					$_productsCollection->addAttributeToSort('created_at', $sorting_type);
					$_productsCollection->addAttributeToSort('news_from_date', $sorting_type);
				} else {
					$_productsCollection->addAttributeToSort('price', $sorting_type);
				}
			} else {
				$_productsCollection->addAttributeToSort('created_at', 'ASC');
				$_productsCollection->addAttributeToSort('news_from_date', 'ASC');
			}
			/*addded brand filter for multiple brand*/
			if($brand !=''){
				$brandArr = explode(',',$brand);
				$brandFilterArr = array();
				foreach($brandArr as $brandVal){
					$brandFilterArr[] = array('attribute' => 'brand','eq' => $brandVal);
				}
				$_productsCollection->addAttributeToFilter($brandFilterArr);
			}
			
			/*if($my_product==1){
				//echo $user_id;die;
				//$_productsCollection->addFieldToFilter('entity_id', array('in' => array(113)));	

				$_productsCollection = $this->_productCollectionFactory->create();
				$_productsCollection->addAttributeToSelect('*');
				$_productsCollection->addFieldToFilter('entity_id', array('in' => array(113)));			
			}*/
			
				
			$total_product_count = $_productsCollection->getSize();
			
			$_productsCollection->setPageSize(10)->setCurPage($page);
			if($_productsCollection->count()) {
				$product_data = $this->getProductCollectionData($_productsCollection, $wishlistProductIds);
			}
			
			$response['product_data'] 			= $product_data;
			$response['total_product_count'] 	= $total_product_count;			
			$message = __('successfully.');
			$responseFlag = true;
		} catch(InputMismatchException $e){
			$message  = $e->getMessage();
		} catch(LocalizedException $e){
			$message  = $e->getMessage();
		} catch(InvalidEmailOrPasswordException $e){
			$message  = $e->getMessage();
		} catch(NoSuchEntityException $e){
			$message  = $e->getMessage();
		} catch(UserLockedException $e){
			$message  = $e->getMessage();
		} catch(Exception $e){
			$message  = $e->getMessage();
		}
		
		$response['response'] 					= $responseFlag;
		$response['message']  					= $message;
		echo json_encode($response);
	}
	
	
	/*
	 * get product collection
	 */
    protected function getProductCollectionData($productCollection, $wishlistProductIds)
	{
		$product_data = array();
		foreach($productCollection as $_product): 			
			$image_url = $this->_apiCommonHelper->getImageUrl($_product, 'product_page_image_small');
			$product_mrp = $product_price = number_format((float)$_product->getPrice(), 2, '.', '');
			
			$specialprice 			= $_product->getSpecialPrice();
			$specialPriceFromDate 	= $_product->getSpecialFromDate();
			$specialPriceToDate		= $_product->getSpecialToDate();
			$today = time();
			if ($specialprice) {
				if ($today >= strtotime($specialPriceFromDate) && $today <= strtotime($specialPriceToDate) || $today >= strtotime($specialPriceFromDate) && is_null($specialPriceToDate)) { 
					$product_price = number_format((float)$specialprice, 2, '.', '');
				}
			}
			if ($_product->getIsSalable()):
				$product_in_stock = 'true';
			else:
				$product_in_stock = 'false';
			endif;
			if (in_array($_product->getId(),$wishlistProductIds)):
				$wishlistFlag =  'true';
			else:
				$wishlistFlag =  'false';
			endif;
			$product_data[]  	= array("product_id" => $_product->getId(),'brand'=>$_product->getAttributeText('brand'), "product_sku" => $_product->getSku(), "product_image" => $image_url, "product_name" => $_product->getName(), "product_price" => $product_price, "product_mrp" => $product_mrp, "is_wishlist" => $wishlistFlag, "product_in_stock" => $product_in_stock, "product_rating" => rand(40,50)/10);
		endforeach;
		return $product_data;
	}
	
	
	
	/**
     * saveProductData method for seller's product save action.
     *
     * @param $sellerId
     * @param $wholdata
     *
     * @return array
     */
    public function saveProductData($sellerId, $wholedata)
    {		
        $returnArr = [];
        $returnArr['error'] = 0;
        $returnArr['product_id'] = '';
        $returnArr['message'] = '';
        $wholedata['new-variations-attribute-set-id'] = $wholedata['set'];
        $wholedata['product']['attribute_set_id'] = $wholedata['set'];

        $helper = $this->_marketplaceHelperData;

        $this->_registry->register('mp_flat_catalog_flag', 1);

        if (!empty($wholedata['id'])) {
            $mageProductId = $wholedata['id'];
            $editFlag = 1;
            $storeId = $helper->getCurrentStoreId();
            $savedWebsiteIds = $this->_productRepositoryInterface
            ->getById(
                $mageProductId
            )->getWebsiteIds();
            if (!isset($wholedata['product']['website_ids'])) {
                $wholedata['product']['website_ids'] = $savedWebsiteIds;
            }
            $this->_eventManager->dispatch(
                'mp_customattribute_deletetierpricedata',
                [$wholedata]
            );
			
            $wholedata = $this->adminStoreMediaImages($mageProductId, $wholedata);
        } else {
            $mageProductId = '';
            $editFlag = 0;
            $storeId = 0;
            if (!isset($wholedata['product']['website_ids'])) {
                $wholedata['product']['website_ids'][] = $helper->getWebsiteId();
            }
            $wholedata['product']['url_key'] = $wholedata['product']['url_key'] ?? '';
        }
		
        if ($mageProductId) {
            $status1 = $helper->getIsProductEditApproval() ?
            $this->_SellerProduct::STATUS_DISABLED : $this->_SellerProduct::STATUS_ENABLED;
            if ($helper->getIsProductApproval() && !$helper->getIsProductEditApproval()) {
                $sellerProductColls = $this->_mpProductCollectionFactory->create()
                ->addFieldToFilter(
                    'mageproduct_id',
                    $mageProductId
                )->addFieldToFilter(
                    'seller_id',
                    $sellerId
                );
                foreach ($sellerProductColls as $sellerProductColl) {
                    $status1 = !$sellerProductColl->getIsApproved() ?
                    SellerProduct::STATUS_DISABLED : SellerProduct::STATUS_ENABLED;
                }
            }
        } else {
            $status1 = $helper->getIsProductApproval() ?
            SellerProduct::STATUS_DISABLED : SellerProduct::STATUS_ENABLED;
        }

        $status = isset($wholedata['status'])?$wholedata['status']:$status1;

        $wholedata['store'] = $storeId;
        /*
        * Marketplace Product save before Observer
        */
        $this->_eventManager->dispatch(
            'mp_product_save_before',
            [$wholedata]
        );

        $catalogProductTypeId = $wholedata['type'];

        /*
        * Product Initialize method to set product data
        */
		
        $catalogProduct = self::productInitialize(
            self::build($wholedata, $storeId),
            $wholedata
        );

        /*for downloadable products start*/

        $catalogProduct = self::buildDownloadableProduct($catalogProduct, $wholedata);

        /*for downloadable products end*/

        /*for configurable products start*/

        $associatedProductIds = [];

        $resultData = self::buildConfigurableProduct($catalogProduct, $wholedata);

        $catalogProduct = $resultData['catalogProduct'];
        $associatedProductIds = $resultData['associatedProductIds'];

        /*for configurable products end*/

        $this->_catalogProductTypeManager->processProduct($catalogProduct);

        $set = $catalogProduct->getAttributeSetId();

        $type = $catalogProduct->getTypeId();

        if (isset($set) && isset($type)) {
            $allowedsets = explode(',', $helper->getAllowedAttributesetIds());
            $allowedtypes = explode(',', $helper->getAllowedProductType());
            if (!in_array($type, $allowedtypes) || !in_array($set, $allowedsets)) {
                $returnArr['error'] = 1;
                $returnArr['message'] = __('Product Type Invalid Or Not Allowed');

                return $returnArr;
            }
        } else {
            $returnArr['error'] = 1;
            $returnArr['message'] = __('Product Type Invalid Or Not Allowed');

            return $returnArr;
        }
        if (isset($type) && $type == 'configurable') {
            $allowedtypes = explode(',', $helper->getAllowedProductType());
            if (!in_array('virtual', $allowedtypes) && empty($wholedata['product']['weight'])) {
                $returnArr['error'] = 1;
                $returnArr['message'] = __(
                    'You are not allowed to add virtual products, so please set product weight.'
                );

                return $returnArr;
            }
        }

        if ($catalogProduct->getSpecialPrice() == '') {
            $catalogProduct->setSpecialPrice(null);
            $catalogProduct->getResource()->saveAttribute($catalogProduct, 'special_price');
        }

        $originalSku = $catalogProduct->getSku();
        $catalogProduct->setStatus($status)->save();
		
        $mageProductId = $catalogProduct->getId();
        $this->handleImageRemoveError($wholedata, $mageProductId);
        $this->getCategoryLinkManagement()->assignProductToCategories(
            $catalogProduct->getSku(),
            $catalogProduct->getCategoryIds()
        );

        /*for configurable associated products save start*/

        $this->saveConfigurableAssociatedProducts($wholedata, $storeId);

        /*for configurable associated products save end*/

        $wholedata['id'] = $mageProductId;
        $this->_eventManager->dispatch(
            'mp_customoption_setdata',
            [$wholedata]
        );

        /* Update marketplace product*/
        $this->saveMaketplaceProductTable(
            $mageProductId,
            $sellerId,
            $status1,
            $editFlag,
            $associatedProductIds
        );

        /*
        * Marketplace Custom Attribute Set Tier Price Observer
        */
        $this->_eventManager->dispatch(
            'mp_customattribute_settierpricedata',
            [$wholedata]
        );

        /*
        * Marketplace Product Save After Observer
        */
        $this->_eventManager->dispatch(
            'mp_product_save_after',
            [$wholedata]
        );

        /*
        * Marketplace Product Send Mail Function
        */
        $this->sendProductMail($wholedata, $sellerId, $editFlag);

        $returnArr['product_id'] = $mageProductId;

        /*
        * Create duplicate product
        */
        if (isset($wholedata['back']) && $wholedata['back'] === 'duplicate') {
            // get and save product for admin store id
            $storeId = 0;
            $duplicateCatalogProduct = self::build($wholedata, $storeId);

            $status1 = $helper->getIsProductApproval() ?
            SellerProduct::STATUS_DISABLED : SellerProduct::STATUS_ENABLED;
            //$status = isset($wholedata['status'])?$wholedata['status']:$status1;

            $newProduct = $this->productCopier->copy($duplicateCatalogProduct);

            // setting stock quantity and status
            $this->updateStockData($newProduct->getId());

            $this->_messageManager->addSuccessMessage(__('You duplicated the product.'));
            $newProductId = $newProduct->getEntityId();
            $wholedata['id'] = $newProductId;
            $editFlag = 0;

            $this->_eventManager->dispatch(
                'mp_customoption_setdata',
                [$wholedata]
            );

            $newAssociatedProductIds = [];

            /* Update marketplace product for duplicate product*/
            $this->saveMaketplaceProductTable(
                $newProductId,
                $sellerId,
                $status1,
                $editFlag,
                $newAssociatedProductIds
            );

            /*
            * Marketplace Custom Attribute Set Tier Price Observer for duplicate product
            */
            $this->_eventManager->dispatch(
                'mp_customattribute_settierpricedata',
                [$wholedata]
            );

            /*
            * Marketplace Product Save After Observer for duplicate product
            */
            $this->_eventManager->dispatch(
                'mp_product_save_after',
                [$wholedata]
            );

            /*
            * Marketplace Product Send Mail Function for duplicate product
            */
            $this->sendProductMail($wholedata, $sellerId, $editFlag);
            $returnArr['product_id'] = $newProductId;
        }

        return $returnArr;
    }
	
	
	/**
     * Product initialize function before saving.
     *
     * @param \Magento\Catalog\Model\Product $catalogProduct
     * @param $requestData
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function productInitialize(\Magento\Catalog\Model\Product $catalogProduct, $requestData)
    {
        $helper = $this->_marketplaceHelperData;
        $requestProductData = $requestData['product'];
        unset($requestProductData['custom_attributes']);
        unset($requestProductData['extension_attributes']);

        /*
        * Manage seller product Stock data
        */
        $requestProductData = $this->manageSellerProductStock($requestProductData);

        $requestProductData = $this->normalizeProductData($requestProductData);

        if (!empty($requestProductData['is_downloadable'])) {
            $requestProductData['product_has_weight'] = 0;
        }

        $requestProductData = $this->manageProductCategoryWebsiteData($requestProductData);

        $wasLockedMedia = false;
        if ($catalogProduct->isLockedAttribute('media')) {
            $catalogProduct->unlockAttribute('media');
            $wasLockedMedia = true;
        }

        $requestProductData = $this->manageProductDateTimeFilter($catalogProduct, $requestProductData);

        if (isset($requestProductData['options'])) {
            $productOptions = $requestProductData['options'];
            unset($requestProductData['options']);
        } else {
            $productOptions = [];
        }

        $catalogProduct->addData($requestProductData);

        if ($wasLockedMedia) {
            $catalogProduct->lockAttribute('media');
        }

        if ($helper->getSingleStoreModeStatus()) {
            $catalogProduct->setWebsiteIds([$helper->getWebsiteId()]);
        }

        /*
         * Check for "Use Default Value" field value
         */
        $catalogProduct = $this->manageProductForDefaultAttribute($catalogProduct, $requestData);

        /*
         * Set Downloadable links if available
         */
        $catalogProduct = $this->manageProductDownloadableData($catalogProduct, $requestData);

        /*
         * Set Product options to product if exist
         */
        $catalogProduct = $this->manageProductOptionData($catalogProduct, $productOptions);

        /*
         * Set Product Custom options status to product
         */
        if (empty($requestData['affect_product_custom_options'])) {
            $requestData['affect_product_custom_options'] = '';
        }

        $catalogProduct->setCanSaveCustomOptions(
            (bool) $requestData['affect_product_custom_options']
            && !$catalogProduct->getOptionsReadonly()
        );

        return $catalogProduct;
    }
	
	
	/**
     * Set Downloadable Data in Product Model.
     *
     * @param \Magento\Catalog\Model\Product $catalogProduct
     * @param array $wholedata
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function buildDownloadableProduct($catalogProduct, $wholedata)
    {
        if (!empty($wholedata['downloadable']) && $downloadableParamData = $wholedata['downloadable']) {
            $downloadableParamData = $links = $this->getDownloadableParamData($downloadableParamData);

            $catalogProduct->setDownloadableData($downloadableParamData);

            $extension = $catalogProduct->getExtensionAttributes();

            if (isset($downloadableParamData['link']) && is_array($downloadableParamData['link'])) {
                $links = [];
                $links = $this->getDownloabaleLinkData($downloadableParamData, $links);
                $extension->setDownloadableProductLinks($links);
            }
            if (isset($downloadableParamData['sample']) && is_array($downloadableParamData['sample'])) {
                $samples = [];
                $samples = $this->getDownloabaleSampleData($downloadableParamData, $samples);
                $extension->setDownloadableProductSamples($samples);
            }
            $catalogProduct->setExtensionAttributes($extension);
            if ($catalogProduct->getLinksPurchasedSeparately()) {
                $catalogProduct->setTypeHasRequiredOptions(true)->setRequiredOptions(true);
            } else {
                $catalogProduct->setTypeHasRequiredOptions(false)->setRequiredOptions(false);
            }
        }
        return $catalogProduct;
    }

    /**
     * Set Downloadable Data in Product Model.
     *
     * @param \Magento\Catalog\Model\Product $catalogProduct
     * @param array $wholedata
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function buildConfigurableProduct($catalogProduct, $wholedata)
    {
        $associatedProductIds = [];
        if (!empty($wholedata['attributes'])) {
            $requestProductData = $wholedata['product'];
            $attributes = $wholedata['attributes'];
            $setId = $wholedata['set'];
            $catalogProduct->setAttributeSetId($setId);
            $this->_catalogProductTypeConfigurable->setUsedProductAttributeIds(
                $attributes,
                $catalogProduct
            );

            $extensionAttributes = $catalogProduct->getExtensionAttributes();

            $catalogProduct->setNewVariationsAttributeSetId($setId);
            $configurableOptions = [];

            $extensionAttributes->setConfigurableProductOptions($configurableOptions);

            if (!empty($wholedata['associated_product_ids'])) {
                $associatedProductIds = $wholedata['associated_product_ids'];
            }
            // Get variationsMatrix
            $variationsMatrix = [];
            if (!empty($wholedata['variations-matrix'])) {
                foreach ($wholedata['variations-matrix'] as $key => $value) {
                    if (empty($value['weight'])) {
                        if (!empty($wholedata['product']['weight'])) {
                            $productWeight = $wholedata['product']['weight'];
                            $wholedata['variations-matrix'][$key]['weight'] = $productWeight;
                        } else {
                            $wholedata['variations-matrix'][$key]['product_has_weight'] = 0;
                        }
                    }
                }
                $variationsMatrix = $wholedata['variations-matrix'];
            }

            if ($associatedProductIds || $variationsMatrix) {
                $this->_variationHandler->prepareAttributeSet($catalogProduct);
            }

            if (!empty($variationsMatrix)) {
                $generatedProductIds = $this->_variationHandler->generateSimpleProducts(
                    $catalogProduct,
                    $variationsMatrix
                );
                $associatedProductIds = array_merge($associatedProductIds, $generatedProductIds);
            }
            $extensionAttributes->setConfigurableProductLinks(array_filter($associatedProductIds));
            if (!isset($wholedata['affect_configurable_product_attributes'])) {
                $wholedata['affect_configurable_product_attributes'] = '';
            }
            $catalogProduct->setCanSaveConfigurableAttributes(
                (bool) $wholedata['affect_configurable_product_attributes']
            );

            $catalogProduct->setExtensionAttributes($extensionAttributes);
        }
        return ["catalogProduct" => $catalogProduct, "associatedProductIds" => $associatedProductIds];
    }

	/**
     * Build product based on requestData.
     *
     * @param $requestData
     *
     * @return \Magento\Catalog\Model\Product $mageProduct
     */
    public function build($requestData, $store = 0)
    {
        if (!empty($requestData['id'])) {
            $mageProductId = (int) $requestData['id'];
        } else {
            $mageProductId = '';
        }
        /** @var $mageProduct \Magento\Catalog\Model\Product */
        $mageProduct = $this->_productFactory->create();
        if (!empty($requestData['set'])) {
            $mageProduct->setAttributeSetId($requestData['set']);
        }
        if (!empty($requestData['type'])) {
            $mageProduct->setTypeId($requestData['type']);
        }
        $mageProduct->setStoreId($store);
		
        if ($mageProductId) {
            try {
				/*sandipranipa commented for helper issue*/
                /*$isPartner = $this->_webkulhelper->isSeller();
                $flag = false;
                if ($isPartner == 1) {
                    $rightseller = $this->_webkulhelper->isRightSeller($mageProductId);
                    if ($rightseller) {
                        $flag = true;
                    }
                }
                if ($flag) {
                    $mageProduct->load($mageProductId);
                }*/
				$mageProduct->load($mageProductId);
            } catch (\Exception $e) {				
                $this->helper->logDataInLogger(
                    "Controller_Product_Builder execute : ".$e->getMessage()
                );
            }
        }
        if (!$this->_registry->registry('product')) {
            $this->_registry->register('product', $mageProduct);
        }
        if (!$this->_registry->registry('current_product')) {
            $this->_registry->register('current_product', $mageProduct);
        }
        return $mageProduct;
    }
	
	/**
     * @param array $requestProductData
     *
     * @return array
     */
    private function manageSellerProductStock($requestProductData)
    {
        if ($requestProductData) {
            if (isset($requestProductData['quantity_and_stock_status']['qty'])) {
                if ($requestProductData['quantity_and_stock_status']['qty'] < 0) {
                    $requestProductData['quantity_and_stock_status']['qty'] = abs(
                        $requestProductData['quantity_and_stock_status']['qty']
                    );
                    $requestProductData['stock_data']['qty'] = $requestProductData['quantity_and_stock_status']['qty'];
                }
            }
            $stockData = isset($requestProductData['stock_data']) ?
            $requestProductData['stock_data'] : [];
            if (isset($stockData['qty']) && (double) $stockData['qty'] > 99999999.9999) {
                $stockData['qty'] = 99999999.9999;
            }
            if (isset($stockData['min_qty']) && (int) $stockData['min_qty'] < 0) {
                $stockData['min_qty'] = 0;
            }
            if (!isset($stockData['use_config_manage_stock'])) {
                $stockData['use_config_manage_stock'] = 0;
            }
            if ($stockData['use_config_manage_stock'] == 1 && !isset($stockData['manage_stock'])) {
                $stockData['manage_stock'] = $this->stockConfiguration
                ->getManageStock();
            }
            if (!isset($stockData['is_decimal_divided']) || $stockData['is_qty_decimal'] == 0) {
                $stockData['is_decimal_divided'] = 0;
            }
            $requestProductData['stock_data'] = $stockData;
        }
        return $requestProductData;
    }
	
	 /**
     * Internal normalization
     *
     * @param array $requestProductData
     *
     * @return array
     */
    private function normalizeProductData(array $requestProductData)
    {
        foreach ($requestProductData as $key => $value) {
            if (is_scalar($value)) {
                if ($value === 'true') {
                    $requestProductData[$key] = '1';
                } elseif ($value === 'false') {
                    $requestProductData[$key] = '0';
                }
            } elseif (is_array($value)) {
                $requestProductData[$key] = $this->normalizeProductData($value);
            }
        }

        return $requestProductData;
    }
	
	/**
     * @param array $requestProductData
     *
     * @return array
     */
    private function manageProductCategoryWebsiteData($requestProductData)
    {
        foreach (['category_ids', 'website_ids'] as $field) {
            if (!isset($requestProductData[$field])) {
                $requestProductData[$field] = [
                    0=>$this->_marketplaceHelperData->getRootCategoryIdByStoreId()
                ];
            }
        }
        foreach ($requestProductData['website_ids'] as $websiteId => $checkboxValue) {
            if (!$checkboxValue) {
                unset($requestProductData['website_ids'][$websiteId]);
            }
        }
        return $requestProductData;
    }
	
	 /**
     * @param Magento/Catalog/Model/Product $catalogProduct
     * @param array $requestProductData
     *
     * @return array
     */
    private function manageProductDateTimeFilter($catalogProduct, $requestProductData)
    {
        $dateFieldFilters = [];
        $attributes = $catalogProduct->getAttributes();
        foreach ($attributes as $attrKey => $attribute) {
            if ($attribute->getBackend()->getType() == 'datetime') {
                if (array_key_exists($attrKey, $requestProductData) && $requestProductData[$attrKey]!='') {
                    $dateFieldFilters[$attrKey] = $this->_dateFilter;
                }
            }
        }
        $inputFilter = new \Zend_Filter_Input(
            $dateFieldFilters,
            [],
            $requestProductData
        );
        $requestProductData = $inputFilter->getUnescaped();
        return $requestProductData;
    }

    /**
     * @param Magento/Catalog/Model/Product $catalogProduct
     * @param array $requestData
     *
     * @return Magento/Catalog/Model/Product
     */
    private function manageProductForDefaultAttribute($catalogProduct, $requestData)
    {
        if (!empty($requestData['use_default'])) {
            foreach ($requestData['use_default'] as $attributeCode => $useDefaultState) {
                if ($useDefaultState) {
                    $catalogProduct->setData($attributeCode, null);
                    if ($catalogProduct->hasData('use_config_'.$attributeCode)) {
                        $catalogProduct->setData('use_config_'.$attributeCode, false);
                    }
                }
            }
        }
        return $catalogProduct;
    }

    /**
     * @param Magento/Catalog/Model/Product $catalogProduct
     * @param array $requestData
     *
     * @return Magento/Catalog/Model/Product
     */
    private function manageProductDownloadableData($catalogProduct, $requestData)
    {
        if (!empty($requestData['links'])) {
            $links = $this->_linkResolver->getLinks();

            $catalogProduct->setProductLinks([]);

            $catalogProduct = $this->_productLinks->initializeLinks($catalogProduct, $links);
            $productLinks = $catalogProduct->getProductLinks();

            $linkTypes = [
                'related' => $catalogProduct->getRelatedReadonly(),
                'upsell' => $catalogProduct->getUpsellReadonly(),
                'crosssell' => $catalogProduct->getCrosssellReadonly()
            ];
            foreach ($linkTypes as $linkType => $readonly) {
                if (isset($links[$linkType]) && !$readonly) {
                    foreach ((array) $links[$linkType] as $linkData) {
                        if (empty($linkData['id'])) {
                            continue;
                        }

                        $linkProduct = $this->_productRepositoryInterface->getById($linkData['id']);
                        $link = $this->_productLinkFactory->create();
                        $link->setSku($catalogProduct->getSku())
                            ->setLinkedProductSku($linkProduct->getSku())
                            ->setLinkType($linkType)
                            ->setPosition(isset($linkData['position']) ? (int)$linkData['position'] : 0);
                        $productLinks[] = $link;
                    }
                }
            }
            $catalogProduct->setProductLinks($productLinks);
        }
        return $catalogProduct;
    }

    /**
     * @param Magento/Catalog/Model/Product $catalogProduct
     * @param $productOptions
     *
     * @return Magento/Catalog/Model/Product
     */
    public function manageProductOptionData($catalogProduct, $productOptions)
    {
        if ($productOptions && !$catalogProduct->getOptionsReadonly()) {
            // mark custom options that should to fall back to default value
            $options = $this->mergeProductOptions(
                $productOptions,
                []
            );
            $customOptions = [];
            foreach ($options as $customOptionData) {
                if (empty($customOptionData['is_delete'])) {
                    if (empty($customOptionData['option_id'])) {
                        $customOptionData['option_id'] = null;
                    }
                    if (isset($customOptionData['values'])) {
                        $customOptionData['values'] = array_filter(
                            $customOptionData['values'],
                            function ($valueData) {
                                return empty($valueData['is_delete']);
                            }
                        );
                    }
                    $customOption = $this->productCustomOption->create(['data' => $customOptionData]);
                    $customOption->setProductSku($catalogProduct->getSku());
                    $customOptions[] = $customOption;
                }
            }
            $catalogProduct->setOptions($customOptions);
        }
        return $catalogProduct;
    }

    /**
     * Merge product and default options for product.
     *
     * @param array $productOptions   product options
     * @param array $overwriteOptions default value options
     *
     * @return array
     */
    public function mergeProductOptions($productOptions, $overwriteOptions)
    {
        if (!is_array($productOptions)) {
            return [];
        }

        if (!is_array($overwriteOptions)) {
            return $productOptions;
        }

        foreach ($productOptions as $index => $option) {
            $optionId = $option['option_id'];

            if (!isset($overwriteOptions[$optionId])) {
                continue;
            }

            foreach ($overwriteOptions[$optionId] as $fieldName => $overwrite) {
                if ($overwrite && isset($option[$fieldName]) && isset($option['default_'.$fieldName])) {
                    $productOptions[$index][$fieldName] = $option['default_'.$fieldName];
                }
            }
        }

        return $productOptions;
    }
	
	
    public function getDownloadableParamData($downloadableParamData)
    {
        if (isset($downloadableParamData['link']) && is_array($downloadableParamData['link'])) {
            foreach ($downloadableParamData['link'] as $key => $linkData) {
                if ($linkData['link_id'] == 0) {
                    $linkData['link_id'] = null;
                }
                $linkData['file'] = $this->_jsonHelper->jsonDecode($linkData['file']);
                $linkData['sample']['file'] = $this->_jsonHelper->jsonDecode($linkData['sample']['file']);
                $downloadableParamData['link'][$key]['link_id'] = $linkData['link_id'];
                $downloadableParamData['link'][$key]['file'] = $linkData['file'];
                $downloadableParamData['link'][$key]['sample']['file'] = $linkData['sample']['file'];
            }
        }
        if (isset($downloadableParamData['sample']) && is_array($downloadableParamData['sample'])) {
            foreach ($downloadableParamData['sample'] as $key => $sampleData) {
                if ($sampleData['sample_id'] == 0) {
                    $sampleData['sample_id'] = null;
                }
                $sampleData['file'] = $this->_jsonHelper->jsonDecode($sampleData['file']);
                $downloadableParamData['sample'][$key]['sample_id'] = $sampleData['sample_id'];
                $downloadableParamData['sample'][$key]['file'] = $sampleData['file'];
            }
        }
        return $downloadableParamData;
    }

    /**
     * Get Product Link Data from Post Downloadable Data.
     *
     * @param array $downloadableParamData
     * @param array $links
     *
     * @return array
     */
    public function getDownloabaleLinkData($downloadableParamData, $links)
    {
        foreach ($downloadableParamData['link'] as $linkData) {
            if (!$linkData || (isset($linkData['is_delete']) && $linkData['is_delete'])) {
                continue;
            } else {
                $links[] = $this->linkBuilder->setData(
                    $linkData
                )->build(
                    $this->getLinkFactory()->create()
                );
            }
        }
        return $links;
    }

    /**
     * Get Product Sample Data from Post Downloadable Data.
     *
     * @param array $downloadableParamData
     * @param array $sampleData
     *
     * @return array
     */
    public function getDownloabaleSampleData($downloadableParamData, $samples)
    {
        foreach ($downloadableParamData['sample'] as $sampleData) {
            if (!$sampleData || (isset($sampleData['is_delete']) && (bool) $sampleData['is_delete'])) {
                continue;
            } else {
                $samples[] = $this->sampleBuilder->setData(
                    $sampleData
                )->build(
                    $this->getSampleFactory()->create()
                );
            }
        }
        return $samples;
    }

    /**
     * @param array  $data
     * @param string $sellerId
     * @param bool   $editFlag
     */
    public function sendProductMail($data, $sellerId, $editFlag = null)
    {
        $helper = $this->_marketplaceHelperData;

        $customer = $this->customerModel->create()->load($sellerId);

        $sellerName = $customer->getFirstname().' '.$customer->getLastname();
        $sellerEmail = $customer->getEmail();

        if (isset($data['product']) && !empty($data['product']['category_ids'])) {
            $categoriesy = $this->categoryModel->create()->load(
                $data['product']['category_ids'][0]
            );
            $categoryname = $categoriesy->getName();
        } else {
            $categoryname = '';
        }

        $emailTempVariables = [];
        $adminStoremail = $helper->getAdminEmailId();
        $adminEmail = $adminStoremail ?
        $adminStoremail : $helper->getDefaultTransEmailId();
        $adminUsername = $helper->getAdminName();

        $emailTempVariables['myvar1'] = $data['product']['name'];
        $emailTempVariables['myvar2'] = $categoryname;
        $emailTempVariables['myvar3'] = $adminUsername;
        if ($editFlag == null) {
            $emailTempVariables['myvar4'] = __(
                'I would like to inform you that recently I have added a new product in the store.'
            );
        } else {
            $emailTempVariables['myvar4'] = __(
                'I would like to inform you that recently I have updated a  product in the store.'
            );
        }
        $senderInfo = [
            'name' => $sellerName,
            'email' => $sellerEmail,
        ];
        $receiverInfo = [
            'name' => $adminUsername,
            'email' => $adminEmail,
        ];
        if (($editFlag == null && $helper->getIsProductApproval() == 1)
            || ($editFlag && $helper->getIsProductEditApproval() == 1)) {
            $this->mpEmailHelper->sendNewProductMail(
                $emailTempVariables,
                $senderInfo,
                $receiverInfo,
                $editFlag
            );
        }
    }

    /**
     * Notify seller when image was not deleted in specific case.
     *
     * @param array $wholedata
     * @param int $mageProductId
     * @return void
     */
    public function handleImageRemoveError($wholedata, $mageProductId)
    {
        if (isset($wholedata['product']['media_gallery']['images'])) {
            $removedImagesCount = 0;
            foreach ($wholedata['product']['media_gallery']['images'] as $image) {
                if (!empty($image['removed'])) {
                    $removedImagesCount++;
                }
            }
            if ($removedImagesCount) {
                $expectedImagesCount = count($wholedata['product']['media_gallery']['images']) - $removedImagesCount;
                $catalogProduct = $this->_productRepositoryInterface->getById($mageProductId);
                if ($expectedImagesCount != count($catalogProduct->getMediaGallery('images'))) {
                    $this->_messageManager->addNotice(
                        __('The image cannot be removed as it has been assigned to the other image role')
                    );
                }
            }
        }
    }

    /**
     * @return CategoryLinkManagementInterface
     */
    public function getCategoryLinkManagement()
    {
        return $this->categoryLinkManagement;
    }

    /**
     * Update Stock Data of Product
     *
     * @param integer $productId
     * @param integer $qty
     * @param integer $isInStock
     */
    public function updateStockData($productId, $qty = 0, $isInStock = 0)
    {
        try {
            $scopeId = $this->_stockConfiguration->getDefaultScopeId();
            $stockItem = $this->_stockRegistryProvider->getStockItem($productId, $scopeId);
            if ($stockItem->getItemId()) {
                $stockItem->setQty($qty);
                $stockItem->setIsInStock($isInStock);
                $stockItem->save();
            }
        } catch (\Exception $e) {
            $this->_marketplaceHelperData->logDataInLogger(
                "Controller_Product_SaveProduct updateStockData : ".$e->getMessage()
            );
            $error = $e->getMessage;
        }
    }
	
	public function saveConfigurableAssociatedProducts($wholedata, $storeId)
    {
        $configurations = [];
        if (!empty($wholedata['configurations'])) {
            $configurations = $wholedata['configurations'];
        }

        if (!empty($configurations)) {
            $configurations = $this->_variationHandler
            ->duplicateImagesForVariations($configurations);
            foreach ($configurations as $associtedProductId => $associtedProductData) {
                $associtedProduct = $this->_productRepositoryInterface
                ->getById(
                    $associtedProductId,
                    false,
                    $storeId
                );
                $associtedProductData = $this->_variationHandler
                ->processMediaGallery(
                    $associtedProduct,
                    $associtedProductData
                );
                $associtedProduct->addData($associtedProductData);
                if ($associtedProduct->hasDataChanges()) {
                    $associtedProduct->save();
                }
            }
        }
    }
	
	 /**
     * Set Product Records in marketplace_product table.
     *
     * @param int $mageProductId
     * @param int $sellerId
     * @param int $status
     * @param int $editFlag
     * @param array $associatedProductIds
     */
    public function saveMaketplaceProductTable(
        $mageProductId,
        $sellerId,
        $status,
        $editFlag,
        $associatedProductIds
    ) {
        $savedIsApproved = 0;
        $sellerProductId = 0;
        $helper = $this->_marketplaceHelperData;
        if ($mageProductId) {
            $sellerProductColls = $this->_mpProductCollectionFactory->create()
            ->addFieldToFilter(
                'mageproduct_id',
                $mageProductId
            )->addFieldToFilter(
                'seller_id',
                $sellerId
            );
            foreach ($sellerProductColls as $sellerProductColl) {
                $sellerProductId = $sellerProductColl->getId();
                $savedIsApproved = $sellerProductColl->getIsApproved();
            }
            $collection1 = $this->_mpProductFactory->create()->load($sellerProductId);
            $collection1->setMageproductId($mageProductId);
            $collection1->setSellerId($sellerId);
            $collection1->setStatus($status);
            $isApproved = 1;
            if ($helper->getIsProductEditApproval()) {
                $collection1->setAdminPendingNotification(2);
            }
            if (!$editFlag) {
                $collection1->setCreatedAt($this->_date->gmtDate());
                if ($helper->getIsProductApproval()) {
                    $isApproved = 0;
                    $collection1->setAdminPendingNotification(1);
                }
            } elseif (!$helper->getIsProductEditApproval()) {
                $isApproved = $savedIsApproved;
            } else {
                $isApproved = 0;
            }
            $collection1->setIsApproved($isApproved);
            $collection1->setUpdatedAt($this->_date->gmtDate());
            $collection1->save();
        }

        foreach ($associatedProductIds as $associatedProductId) {
            if ($associatedProductId) {
                $sellerAssociatedProductId = 0;
                $sellerProductColls = $this->_mpProductCollectionFactory->create()
                ->addFieldToFilter(
                    'mageproduct_id',
                    $associatedProductId
                )
                ->addFieldToFilter(
                    'seller_id',
                    $sellerId
                );
                foreach ($sellerProductColls as $sellerProductColl) {
                    $sellerAssociatedProductId = $sellerProductColl->getId();
                }
                $collection1 = $this->_mpProductFactory->create()->load($sellerAssociatedProductId);
                $collection1->setMageproductId($associatedProductId);
                if (!$editFlag) {
                    /* If new product is added*/
                    $collection1->setStatus(SellerProduct::STATUS_ENABLED);
                    $collection1->setCreatedAt($this->_date->gmtDate());
                }
                if ($editFlag) {
                    $collection1->setAdminPendingNotification(2);
                }
                $collection1->setUpdatedAt($this->_date->gmtDate());
                $collection1->setSellerId($sellerId);
                $collection1->setIsApproved(1);
                $collection1->save();
            }
        }
    }
	
	public function adminStoreMediaImages($productId, $wholedata, $storeId = 0)
    {
        if (!empty($wholedata['product']['media_gallery'])) {
            $flag = 0;
            foreach ($wholedata['product']['media_gallery']['images'] as $key => $value) {
                if ($value['media_type'] == 'external-video') {
                    $flag = 1;
                }
            }
            if ($flag == 1) {
                $catalogProduct = $this->_productRepositoryInterface
                ->getById(
                    $productId,
                    false,
                    $storeId
                );
                /*for downloadable products start*/

                $catalogProduct = $this->buildDownloadableProduct($catalogProduct, $wholedata);

                /*for downloadable products end*/
                $catalogProduct->setMediaGallery($wholedata['product']['media_gallery'])->save();
                foreach ($wholedata['product']['media_gallery']['images'] as $key => $value) {
                    if ($value['value_id'] == '') {
                        unset($wholedata['product']['media_gallery']['images'][$key]);
                    }
                }
            }
        }

        return $wholedata;
    }
	
	 /**
     * {@inheritdoc}
     */
    public function getCartItemCount($customerId)
    {
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			try {
				$customer = $this->customerRepository->getById($customerId);
			} catch (NoSuchEntityException $e) {
				throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
			}
			
			/******* Start: get cart qty  data *******/
            $objectManager   = ObjectManager::getInstance();
            $customer = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
			$customerSession = $objectManager->create('\Magento\Customer\Model\Session');
			$customerSession->setCustomerAsLoggedIn($customer);
            
			$cartHelper = $objectManager->get('\Magento\Checkout\Helper\Cart');
			$response['cart_count'] = $cartHelper->getItemsCount();
			/******* End: get cart qty  data *******/
			
			$response['notification_count'] = 0;
			$message = __('successfully.');
			$responseFlag = true;
		} catch(InputMismatchException $e){
			$message  = $e->getMessage();
		} catch(LocalizedException $e){
			$message  = $e->getMessage();
		} catch(InvalidEmailOrPasswordException $e){
			$message  = $e->getMessage();
		} catch(NoSuchEntityException $e){
			$message  = $e->getMessage();
		} catch(UserLockedException $e){
			$message  = $e->getMessage();
		} catch(Exception $e){
			$message  = $e->getMessage();
		}
		
		$response['response'] 					= $responseFlag;
		$response['message']  					= $message;
		echo json_encode($response);
	}
	
	/**
     * {@inheritdoc}
     */
	public function getProductDetail($user_id, $device_id, $product_id)
	{
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			try {
				$wishlistProductIds = array();
				if($user_id) {
					$customer = $this->customerRepository->getById($user_id);
					if ($customer->getCustomAttribute('device_id')->getValue() == $device_id) {
						/******* Start: get customer Wishlist Data *******/
						$wishlistCollection = $this->wishlist->loadByCustomerId($user_id)->getItemCollection();
						foreach ($wishlistCollection as $item){
							$wishlistProductIds[] = $item->getProduct()->getId();
						}
						/******* End: get customer Wishlist Data *******/
					} else {
						$responseFlag = true;
					}
				}
				if ($responseFlag) {
					$responseFlag = false;
					throw new InputMismatchException(__('Oops. Something went wrong. Please try again later.'));
				}	
			} catch (NoSuchEntityException $e) {
				throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
			}
			$product_data = $product_image = $review_data = $related_product_data = $related_post_data = array();
			$storeId = $this->storeManager->getStore()->getId();
			$objectManager 	= ObjectManager::getInstance();
			$imageHelper  	= $objectManager->get('\Magento\Catalog\Helper\Image');
			
			/**** Start: get product detail ****/
			$_product 		= $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
			//$image_url 		= $this->_apiCommonHelper->getImageUrl($_product, 'product_page_image_small');
			$product_mrp 	= $product_price = number_format((float)$_product->getPrice(), 2, '.', '');
			
			$specialprice 			= $_product->getSpecialPrice();
			$specialPriceFromDate 	= $_product->getSpecialFromDate();
			$specialPriceToDate		= $_product->getSpecialToDate();
			$today = time();
			if ($specialprice) {
				if ($today >= strtotime($specialPriceFromDate) && $today <= strtotime($specialPriceToDate) || $today >= strtotime($specialPriceFromDate) && is_null($specialPriceToDate)) { 
					$product_price = number_format((float)$specialprice, 2, '.', '');
				}
			}
			if ($_product->getIsSalable()):
				$product_in_stock = 'true';
			else:
				$product_in_stock = 'false';
			endif;
			if (in_array($_product->getId(),$wishlistProductIds)):
				$wishlistFlag =  'true';
			else:
				$wishlistFlag =  'false';
			endif;
			
			$description 		= ($_product->getDescription())? '<div style="text-align:justify">' . str_replace("\"","'",$_product->getDescription()) . '</div>' : '';
			$short_description 	= ($_product->getShort_description())? '<div style="text-align:justify">' . str_replace("\"","'",$_product->getShort_description()) . '</div>' : '';			
			/*$str 	= $_product->getDescription();
			$tags 	= array("<p class=\"set_deioin\">\r\n",'</span>','<span>','<div>','</div>','<p>','</p>','<br/>','<br>','<hr/>','<hr>','</h1>','</h2>','</h3>','</h4>','</h5>','</h6>');
			$description  = str_replace($tags,"#",$str);
			$tags 		  = array("<br style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);\">","<span style=\"color: rgb(51, 51, 51); font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(255, 255, 255);\">"); 	
			$description  = str_replace($tags,"",$description);
			$description  = str_replace("&nbsp;"," ", strip_tags($description)); 
			$description  = rtrim($description,'#');
			$description  = str_replace("\n","", strip_tags($description)); 
			$description  = str_replace("\t","", strip_tags($description)); 
			$description  = str_replace("##","#", $description); 
			$description  = str_replace("# #","#", $description); 
			$description  = preg_replace('!\s+!', ' ', $description);*/
			
			$response['product_sku']  	    = $_product->getSku();
			$response['product_name'] 	    = $_product->getName();
			$response['product_url'] 	    = $_product->getProductUrl();
			$response['product_price'] 	    = $product_price;
			$response['product_mrp'] 	    = $product_mrp;
			$response['is_wishlist'] 	    = $wishlistFlag;
			$response['product_in_stock'] 	= $product_in_stock;
			$response['short_description'] 	= $short_description;
			$response['description'] 		= $description;
			
			$response['brand']  	    = $_product->getAttributeText('brand');
			$response['unit']  	    = $_product->getAttributeText('unit');
			$response['price_specification']  	    = $_product->getPriceSpecification();
			$response['delivery_estimate_time']  	    = $_product->getDeliveryEstimateTime();
			$response['package_height']  	    = $_product->getPackageHeight();
			$response['package_width']  	    = $_product->getPackageWidth();
			$response['directions']  	    = $_product->getDirections();
			$response['ingredients']  	    = $_product->getIngredients();
			$response['pcs_per_package']  	    = $_product->getPcsPerPackage();
			
			
			/**** End: get product detail ****/
			
			/**** Start: get product image ****/
			$gallery_images = $_product->getMediaGalleryImages();
			foreach($gallery_images as $g_image) {
				$product_image[] = array('image_url' => $g_image['url']);
			}
			/**** End: get product image ****/
			
			/**** Start: get product review ****/
			$review_data = $this->getProductReview($_product->getId(), 10);
			/**** End: get product review ****/
			
			/**** Start: get related product data ****/
			$allRelatedProductIds = $_product->getRelatedProductIds();
			if (!empty($allRelatedProductIds)) {
				$related_product_data = $this->getRelatedProductData($allRelatedProductIds, $wishlistProductIds);
			}
			/**** End: get related product data ****/
			
			/**** Start: Getting average of ratings/reviews ****/
			$rating = $objectManager->get("Magento\Review\Model\ResourceModel\Review\CollectionFactory");
			$reviews = $rating->create()->addStatusFilter(\Magento\Review\Model\Review::STATUS_APPROVED)
										->addEntityFilter('product',$_product->getId())
										->setDateOrder()
										->addRateVotes()
										->getItems();
			
			$avgRatings = 0;
			$ratings = array();
			$product_total_review = count($reviews);
			if (count($reviews) > 0) {
				foreach ($reviews as $review) {
					$rating_votes 		= $review['rating_votes'];
					$rating_votes_items = $rating_votes->getData();
					foreach ( $rating_votes_items as $rating_votes_item ) {
						$ratings[] = $rating_votes_item['percent']/20; 
					}
				}
				if(count($ratings)) 
					$avgRatings = array_sum($ratings)/count($ratings);
			}
			$response['product_rating'] 		= number_format($avgRatings,2);
			/**** End: Getting average of ratings/reviews ****/
			
			$response['product_image'] 			= $product_image;
			$response['review_data'] 			= $review_data ;
			$response['review_count'] 			= $product_total_review ;						
			$response['related_product_data'] 	= $related_product_data ;
			$message = __('successfully.');
			$responseFlag = true;
		} catch(InputMismatchException $e){
			$message  = $e->getMessage();
		} catch(LocalizedException $e){
			$message  = $e->getMessage();
		} catch(InvalidEmailOrPasswordException $e){
			$message  = $e->getMessage();
		} catch(NoSuchEntityException $e){
			$message  = $e->getMessage();
		} catch(UserLockedException $e){
			$message  = $e->getMessage();
		} catch(Exception $e){
			$message  = $e->getMessage();
		}
		
		$response['response'] 					= $responseFlag;
		$response['message']  					= $message;
		echo json_encode($response);
	}
	
	
    /**
     * get product review by product id
     */
    protected function getProductReview($product_id, $limit = '')
	{
		$objectManager = ObjectManager::getInstance();
		$rating = $objectManager->get("Magento\Review\Model\ResourceModel\Review\CollectionFactory");
		$reviews = $rating->create()->addStatusFilter(\Magento\Review\Model\Review::STATUS_APPROVED)
									->addEntityFilter('product',$product_id)
									->setDateOrder()
									->addRateVotes()
									->getItems();
		$review_data = array();
		if(!empty($reviews)){	
			$reviewCount = 0;
			foreach($reviews as $review){ 
				if($limit != '' && $limit == $reviewCount) {
					break;
				}
				$nickname 			= $review['nickname']; 
				$summaryofreview 	= $review['title'];
				$created_at 		= $review['created_at']; 
				$reviewDetails 		= $review['detail']; 
				$posteddate 		= date("m/d/Y", strtotime($created_at));
				$rating_votes 		= $review['rating_votes'];
				$rating_votes_items = $rating_votes->getData();
				$rating = array();
				foreach($rating_votes_items as $rating_votes_item){ 
					$rating[$rating_votes_item['rating_code']] = $rating_votes_item['percent']/20; 
				}  
				$price_rating	= (isset($rating['Price'])) ? $rating['Price'] : '';
				$quality_rating	= (isset($rating['Quality'])) ? $rating['Quality'] : '';
				$value_rating 	= (isset($rating['Value'])) ? $rating['Value'] : '';
				$review_data[] = array( "title" 			=> $summaryofreview,
										"user_name" 		=> $nickname,
										"description" 		=> $reviewDetails,
										"price_rating" 		=> $price_rating,
										"quality_rating" 	=> $quality_rating,
										"value_rating" 		=> $value_rating,
										"date_time" 		=> $posteddate,
									   );
				$reviewCount++;
			}
		}
		return $review_data;
	}
		
		/**
     * get related product
     */
	 protected function getRelatedProductData($allRelatedProductIds, $wishlistProductIds)
	 {
		$product_data = array();
		$storeId = $this->storeManager->getStore()->getId();
		$objectManager 	= ObjectManager::getInstance();
		$attributes = $objectManager->get('Magento\Catalog\Model\Config')->getProductAttributes();
		$_productsCollection = $this->_productCollectionFactory->create()
									->addAttributeToSelect($attributes)
									->setStoreId($storeId);
		$_productsCollection->addAttributeToFilter('entity_id', array('in'=>$allRelatedProductIds));
		$_productsCollection->addMinimalPrice()
							->addFinalPrice()
							->addTaxPercents()
							->addUrlRewrite()
							->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());
		if($_productsCollection->count()) {
			$product_data = $this->getProductCollectionData($_productsCollection, $wishlistProductIds);
		}
		return $product_data;
	 }

}
