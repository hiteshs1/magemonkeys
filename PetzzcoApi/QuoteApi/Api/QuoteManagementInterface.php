<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace PetzzcoApi\QuoteApi\Api;

use Magento\Framework\Exception\InputException;

/**
 * Interface for managing wishlist.
 * @api
 * @since 100.0.2
 */
interface QuoteManagementInterface
{
    /**
     * Returns quote totals data for a specified cart.
     *
     * @param int $cartId The cart ID.
     * @return array.
     */
    public function getQuoteItems($cartId);
    
    /**
     * get payment method information
     *
     * @param int $cartId The cart ID.
     * @return array.
     */
    public function getPaymentMethodInfo($cartId);
    
    /**
     * Adds a coupon by code to a specified cart.
     *
     * @param int $cartId The cart ID.
     * @param string $couponCode The coupon code data.
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException The specified cart does not exist.
     * @throws \Magento\Framework\Exception\CouldNotSaveException The specified coupon could not be added.
     */
    public function setCoupons($cartId, $couponCode);
    
    
}
