<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace PetzzcoApi\QuoteApi\Model;

use PetzzcoApi\QuoteApi\Api\QuoteManagementInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Payment\Model\Config;
use Magento\SalesRule\Model\CouponFactory;

/**
 * Handle various wish list actions.
 *
 */
class QuoteManagement implements QuoteManagementInterface
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    
    /**
     *@var \Magento\Catalog\Model\Product
     */
    protected $_productload;
    
    /**
     * Quote repository.
     *
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $quoteRepository;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var Config
     */
    protected $_paymentModelConfig;
    
    /**
     * Coupon factory
     *
     * @var \Magento\SalesRule\Model\CouponFactory
     */
    protected $couponFactory;
   
    
    /**
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\Product $productload
     * @param CartRepositoryInterface $quoteRepository
     * @param ScopeConfigInterface $scopeConfig
     * @param Config               $paymentModelConfig
     * @param \Magento\SalesRule\Model\CouponFactory $couponFactory
     */
    public function __construct(
		StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $productload,
        CartRepositoryInterface $quoteRepository,
        ScopeConfigInterface $scopeConfig,
        CouponFactory $couponFactory,
        Config $paymentModelConfig		
    ) {
        $this->storeManager 		= $storeManager;
        $this->_productload 		= $productload;
        $this->quoteRepository 		= $quoteRepository;
        $this->scopeConfig 			= $scopeConfig;
        $this->_paymentModelConfig 	= $paymentModelConfig;
        $this->couponFactory 		= $couponFactory;		
    }
    

	/**
     * {@inheritdoc}
     */
    public function getPaymentMethodInfo($cartId){
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			$quote = $this->quoteRepository->getActive($cartId);
				
			$objectManager = ObjectManager::getInstance();
			$payments = $this->_paymentModelConfig->getActiveMethods();
			$methods = array();
			foreach ($payments as $paymentCode => $paymentModel) {
				$methods[] = $paymentCode;
			}
			/*if(in_array('banktransfer',$methods)) {
				$response['banktransfer'] = array('error' => 0, 'message' => $this->scopeConfig->getValue('payment/banktransfer/instructions'));
			}
			if(in_array('mpcashondelivery',$methods)) {
				$codHelper  = $objectManager->get('\Lof\CashOnDelivery\Helper\Data');
				$dataCod = $codHelper->getAppliedPriceRulesApi($quote);
				$response['mpcashondelivery'] = $dataCod;
			}*/
			$responseFlag = true;
		} catch (\NoSuchEntityException $e) {
			$message = __($e->getMessage());
        } catch (\Exception $e) {
			$message = __($e->getMessage());
		}
        $response['response']   	= $responseFlag;
		$response['message']    	= $message;
		echo json_encode($response);
	}
    
    /**
     * {@inheritDoc}
     */
    public function getQuoteItems($cartId)
    {
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			/** @var \Magento\Quote\Model\Quote $quote */
			$quote = $this->quoteRepository->getActive($cartId);
			$objectManager = ObjectManager::getInstance();
			$helperImport  = $objectManager->get('\PetzzcoApi\CustomerApi\Helper\Data');
			$itemData = array();
			$currency_code = $this->storeManager->getStore()->getCurrentCurrencyCode();
			$currencyRate  = $this->storeManager->getStore()->getBaseCurrency()->getRate($currency_code);
			foreach ($quote->getAllVisibleItems() as $item) {
				$_product = $this->_productload->load($item->getProduct()->getId());
				$productImageURL = $helperImport->getImageUrl($_product, 'product_base_image');
				$itemData[$item->getId()] = array('item_name' 		=> $item->getName(), 
												  'item_sku' 		=> $_product->getSku(),
												  'item_image' 		=> $productImageURL, 
												  'item_price' 		=> number_format((float)($item->getPrice() * $currencyRate), 2, '.', '')*1, 
												  'item_qty' 		=> $item->getQty(), 
												  'item_row_total' 	=> $item->getRowTotal()*1, 
												  'product_id' 		=> $_product->getId(),
												  'quote_id' 		=> $cartId
												  );
			}
			if(!empty($itemData)){
				$response['itemData'] = $itemData;
				$message = __('Successfully.');
				$responseFlag = true;
			} else {
				$message = __('You have no items in your shopping cart.');
			}
			
        } catch (\NoSuchEntityException $e) {
			$message = __($e->getMessage());
        } catch (\Exception $e) {
			$message = __($e->getMessage());
		}
        
		$response['response']   	= $responseFlag;
		$response['message']    	= $message;
		echo json_encode($response);
	}
	
	/**
     * @inheritDoc
     */
    public function setCoupons($cartId, $couponCode)
    {
        /** @var  \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);
        if (!$quote->getItemsCount()) {
            throw new NoSuchEntityException(__('The "%1" Cart doesn\'t contain products.', $cartId));
        }
        if (!$quote->getStoreId()) {
            throw new NoSuchEntityException(__('Cart isn\'t assigned to correct store'));
        }
        $quote->getShippingAddress()->setCollectShippingRates(true);

        try {
			$codeLength 		= strlen($couponCode);
			$isCodeLengthValid 	= $codeLength && $codeLength <= \Magento\Checkout\Helper\Cart::COUPON_CODE_MAX_LENGTH;
			$coupon = $this->couponFactory->create();
			$coupon->load($couponCode, 'code');
                
            $quote->setCouponCode(($isCodeLengthValid && $coupon->getId()) ? $couponCode : '');
            $this->quoteRepository->save($quote->collectTotals());
        } catch (LocalizedException $e) {
            throw new CouldNotSaveException(__('The coupon code couldn\'t be applied: ' .$e->getMessage()), $e);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(
                __("The coupon code couldn't be applied. Verify the coupon code and try again."),
                $e
            );
        }
        if ($quote->getCouponCode() != $couponCode) {
            throw new NoSuchEntityException(__("The coupon code isn't valid. Verify the code and try again."));
        }
        return true;
    }
}
