<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace PetzzcoApi\WishlistApi\Model;

use PetzzcoApi\WishlistApi\Api\WishlistManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Wishlist\Model\ResourceModel\Item\CollectionFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Wishlist\Model\ResourceModel\Item as ItemResource;
use PetzzcoApi\CustomerApi\Helper\Data as ApiCommonHelper;

/**
 * Handle various wish list actions.
 *
 */
class WishlistManagement implements WishlistManagementInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;
    
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    
    /**
     * @var CollectionFactory
     */
    protected $_wishlistCollectionFactory;

    /**
     * @var WishlistRepository
     */
    protected $_wishlistRepository;

    /**
     * @var ProductRepository
     */
    protected $_productRepository;

    /**
     *@var \Magento\Catalog\Model\Product
     */
    protected $_productload;

    /**
	* @var ItemResource
	*/
    protected $itemResource;
    
    /**
     * @var PetzzcoApi\CustomerApi\Helper\Data;
     */
    protected $_apiCommonHelper;
    
    /**
     * @var  \Magento\CatalogInventory\Model\Stock\StockItemRepository
     */
    protected $_stockItemRepository;

    /**
     * @param CollectionFactory $wishlistCollectionFactory
     * @param \Magento\Framework\Math\Random $mathRandom
     * @param \Magento\Framework\Stdlib\DateTime $dateTime
     * @param ProductRepositoryInterface $productRepository
     * @param CustomerRepositoryInterface $customerRepository
     * @param StoreManagerInterface $storeManager
     * @param ItemResource $itemResource
     * @param ApiCommonHelper $apiCommonHelper
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockItemRepository  
     */
    public function __construct(
        CollectionFactory $wishlistCollectionFactory,
        StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $productload,
        \Magento\Wishlist\Model\WishlistFactory $wishlistRepository,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        CustomerRepositoryInterface $customerRepository,
        ItemResource $itemResource,
        ApiCommonHelper $apiCommonHelper,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockItemRepository  
    ) {
        $this->_wishlistCollectionFactory 	= $wishlistCollectionFactory;
        $this->_wishlistRepository 			= $wishlistRepository;
        $this->_productRepository 			= $productRepository;
        $this->storeManager 				= $storeManager;
        $this->_productload 				= $productload;
        $this->customerRepository 			= $customerRepository;
        $this->itemResource 				= $itemResource;
        $this->_apiCommonHelper 			= $apiCommonHelper;
        $this->_stockItemRepository 		= $stockItemRepository;
    }

    /**
     * @inheritdoc
     */
     public function getWishlist($customerId)
     {
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			try {
				$customer = $this->customerRepository->getById($customerId);
			} catch (NoSuchEntityException $e) {
				throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
			}
			//if ($currency_code != '') {
			   //$this->storeManager->getStore()->setCurrentCurrencyCode($currency_code);
			   //$currencyRate = $this->storeManager->getStore()->getBaseCurrency()->getRate($currency_code);
			//} else {
				$currency_code = $this->storeManager->getStore()->getCurrentCurrencyCode();
				$currencyRate  = $this->storeManager->getStore()->getBaseCurrency()->getRate($currency_code);
			//}
			$currentStore = $this->storeManager->getStore();
			$baseUrl  = $currentStore->getBaseUrl();
			$mediaUrl = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
			
			$wishlistCollection = $this->_wishlistCollectionFactory->create()
										->addCustomerIdFilter($customerId);
            $objectManager =\Magento\Framework\App\ObjectManager::getInstance();
            $wishlist_data = array();
            foreach ($wishlistCollection as $item) {
				$_product = $this->_productload->load($item->getProduct()->getId());
                $productImageURL = $this->_apiCommonHelper->getImageUrl($_product, 'product_base_image');
                $product_mrp 	= $product_price = number_format((float)($_product->getPrice() * $currencyRate), 2, '.', '');
                
                $specialprice 			= $_product->getSpecialPrice();
				$specialPriceFromDate 	= $_product->getSpecialFromDate();
				$specialPriceToDate		= $_product->getSpecialToDate();
				$today = time();
				if ($specialprice) {
					if ($today >= strtotime($specialPriceFromDate) && $today <= strtotime($specialPriceToDate) || $today >= strtotime($specialPriceFromDate) && is_null($specialPriceToDate)) { 
						$product_price = number_format((float)($specialprice * $currencyRate), 2, '.', '');
					}
				}
				//$_productStock  = $this->_stockItemRepository->get($_product->getId());
				$_productStock  = $this->_stockItemRepository->getStockItem($_product->getId());
				if ($_productStock->getIsInStock()):
					$product_in_stock = 'true';
				else:
					$product_in_stock = 'false';
				endif;

				$customOptions = $objectManager->get('Magento\Catalog\Model\Product\Option')->getProductOptionCollection($_product);
				$productCustomOption = array();
				if(count($customOptions) > 0) {
					foreach($customOptions as $optionKey => $optionVal):
						//print_r($optionVal->getData());exit;
						$productCustomChildOption = array();
					    foreach($optionVal->getValues() as $valuesKey => $valuesVal) :
					   		//print_r($valuesVal->getData());exit;
					   		$productCustomChildOption[] = array( "option_id" => $optionVal->getId(), "option_value" => $valuesVal->getId(), "option_title" => $valuesVal->getTitle(), "price_type" => $valuesVal->getPriceType(), "option_price" => number_format((float)($valuesVal->getPrice() * $currencyRate), 2, '.', '')) ;
		                endforeach;
		                $productCustomOption[] = array( "group_option_id" => $optionVal->getId(), "group_option_type" => $optionVal->getType(), "is_require" => $optionVal->getIsRequire(), "group_option_title" => $optionVal->getTitle(), "group_option" => $productCustomChildOption);
					endforeach;
				}

				$data = array(
                    "wishlist_item_id" => $item->getWishlistItemId(),
                    "wishlist_id"      => $item->getWishlistId(),
                    "product_id"       => $_product->getId(),
                    "product_sku"      => $_product->getSku(),
                    "product_name"     => $_product->getName(),
                    "product_image"    => $productImageURL,
                    "product_in_stock" => $product_in_stock,
                    "product_price"    => $product_price,
                    "product_mrp"      => $product_mrp,
                    "custom_options"   => $productCustomOption
                );
                $wishlist_data[] = $data;
            }
            $response['wishlist_data'] = $wishlist_data;
			$message = 'Successfully';	
			$responseFlag = true;		
			
		} catch(InputMismatchException $e){
			$message  = $e->getMessage();
		} catch(LocalizedException $e){
			$message  = $e->getMessage();
		} catch(InvalidEmailOrPasswordException $e){
			$message  = $e->getMessage();
		} catch(NoSuchEntityException $e){
			$message  = $e->getMessage();
		} catch(UserLockedException $e){
			$message  = $e->getMessage();
		} catch(Exception $e){
			$message  = $e->getMessage();
		}
		
		$response['response'] = $responseFlag;
		$response['message']  = $message;
		echo json_encode($response);
	 }
	 
	 /**
     * @inheritdoc
     */
     public function addWishlist($customerId, $product_id)
     {
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			try {
				$customer = $this->customerRepository->getById($customerId);
			} catch (NoSuchEntityException $e) {
				throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
			}
			
			if ($product_id == null) {
				throw new LocalizedException(__('Invalid product, Please select a valid product'));
			}
			try {
				$product = $this->_productRepository->getById($product_id);
			} catch (NoSuchEntityException $e) {
				throw new LocalizedException(__('Invalid product, Please select a valid product'));
			}
			try {
				$wishlist = $this->_wishlistRepository->create()->loadByCustomerId($customerId, true);
				$wishlist->addNewItem($product);
				$wishlist->save();
				$message = 'Product has been added to your Wish List.';	
				$responseFlag = true;		
			} catch (NoSuchEntityException $e) {
				throw new NoSuchEntityException(__($e->getMessage()));
			}
				
		} catch(InputMismatchException $e){
			$message  = $e->getMessage();
		} catch(LocalizedException $e){
			$message  = $e->getMessage();
		} catch(InvalidEmailOrPasswordException $e){
			$message  = $e->getMessage();
		} catch(NoSuchEntityException $e){
			$message  = $e->getMessage();
		} catch(UserLockedException $e){
			$message  = $e->getMessage();
		} catch(Exception $e){
			$message  = $e->getMessage();
		}
		
		$response['response'] = $responseFlag;
		$response['message']  = $message;
		echo json_encode($response);
	 }
	 
	 /**
     * @inheritdoc
     */
     public function removeWishlist($customerId, $product_id)
     {
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			try {
				$customer = $this->customerRepository->getById($customerId);
			} catch (NoSuchEntityException $e) {
				throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
			}
			
			$objectManager = ObjectManager::getInstance();
			$wishlistModel = $objectManager->get('\Magento\Wishlist\Model\Wishlist');
			$wishlistModelObject = $wishlistModel->loadByCustomerId($customerId, true);
			$items = $wishlistModelObject->getItemCollection();
			//$item = $wishlist->getItem($wishlist_item_id);
			//if (!$item) {
				//throw new LocalizedException(__('Invalid wishlist product, Please select a valid product'));
			//}
			$removeFlag = false;
			foreach ($items as $item) {
               if ($item->getProductId() == $product_id) {
					$item->delete();
					$wishlistModelObject->save();
					$removeFlag = true;
               }
            }
            if(!$removeFlag){
				throw new LocalizedException(__('Invalid wishlist product, Please select a valid product'));
			}
			
			//$items = $wishlist->getItemCollection();
			
			//$this->itemResource->delete($item);
			$message = 'Product has been has been removed from your Wish List. ';	
			$responseFlag = true;	
		} catch(InputMismatchException $e){
			$message  = $e->getMessage();
		} catch(LocalizedException $e){
			$message  = $e->getMessage();
		} catch(InvalidEmailOrPasswordException $e){
			$message  = $e->getMessage();
		} catch(NoSuchEntityException $e){
			$message  = $e->getMessage();
		} catch(UserLockedException $e){
			$message  = $e->getMessage();
		} catch(Exception $e){
			$message  = $e->getMessage();
		}
		
		$response['response'] = $responseFlag;
		$response['message']  = $message;
		echo json_encode($response);
	 }
	
}
