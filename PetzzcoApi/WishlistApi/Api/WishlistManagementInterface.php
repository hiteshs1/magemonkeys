<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace PetzzcoApi\WishlistApi\Api;

use Magento\Framework\Exception\InputException;

/**
 * Interface for managing wishlist.
 * @api
 * @since 100.0.2
 */
interface WishlistManagementInterface
{
    /**
     * get user wishlist data.
     *
     * @param int $customerId The customer ID.
     * @return array
     */
    public function getWishlist($customerId);
    
    /**
     * get add wishlist data.
     *
     * @param int $customerId The customer ID.
     * @param string $product_id
     * @return array
     */
    public function addWishlist($customerId, $product_id);
    
    /**
     * get remove wishlist data.
     *
     * @param int $customerId The customer ID.
     * @param string $product_id
     * @return array
     */
    public function removeWishlist($customerId, $product_id);
}
