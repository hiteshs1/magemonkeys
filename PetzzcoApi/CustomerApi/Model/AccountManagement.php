<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace PetzzcoApi\CustomerApi\Model;

use PetzzcoApi\CustomerApi\Api\AccountManagementInterface;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerMetadataInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\AddressInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\CustomerRegistry;
use Magento\Customer\Model\AddressRegistry;
use Magento\Customer\Model\AccountConfirmation;
use Magento\Customer\Model\EmailNotificationInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Api\Data\ValidationResultsInterfaceFactory;
use Magento\Customer\Helper\View as CustomerViewHelper;
use Magento\Customer\Model\Config\Share as ConfigShare;
use Magento\Customer\Model\Customer as CustomerModel;
use Magento\Customer\Model\Customer\CredentialsValidator;
use Magento\Customer\Model\Data\Customer;
use Magento\Customer\Model\Metadata\Validator;
use Magento\Customer\Model\ResourceModel\Visitor\CollectionFactory;
use Magento\Directory\Model\AllowedCountries;
use Magento\Eav\Model\Validator\Attribute\Backend;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObjectFactory as ObjectFactory;
use Magento\Framework\Encryption\EncryptorInterface as Encryptor;
use Magento\Framework\Encryption\Helper\Security;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\EmailNotConfirmedException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\InvalidEmailOrPasswordException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\ExpiredException;
use Magento\Framework\Exception\State\InputMismatchException;
use Magento\Framework\Exception\State\InvalidTransitionException;
use Magento\Framework\Exception\State\UserLockedException;
use Magento\Framework\Intl\DateTimeFactory;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Math\Random;
use Magento\Framework\Phrase;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;
use Magento\Framework\Session\SaveHandlerInterface;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\Stdlib\StringUtils as StringHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface as PsrLogger;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Integration\Model\Oauth\Token as Token;
use Magento\Integration\Model\Oauth\TokenFactory as TokenModelFactory;
use Magento\Integration\Model\ResourceModel\Oauth\Token\CollectionFactory as TokenCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Framework\Filesystem;
use Magento\Quote\Api\CartTotalRepositoryInterface;
use Magento\UrlRewrite\Model\UrlRewriteFactory;
use Webkul\Marketplace\Helper\Email as MpEmailHelper;
use Webkul\Marketplace\Model\SellerFactory as MpSellerFactory;
use Webkul\Marketplace\Helper\Data as MpHelper;


use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Translate\Inline\StateInterface;

/**
 * Handle various customer account actions.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class AccountManagement implements AccountManagementInterface
{
    /**
     * Configuration paths for email templates and identities
     *
     * @deprecated
     */
    const XML_PATH_REGISTER_EMAIL_TEMPLATE = 'customer/create_account/email_template';

    /**
     * @deprecated
     */
    const XML_PATH_REGISTER_NO_PASSWORD_EMAIL_TEMPLATE = 'customer/create_account/email_no_password_template';

    /**
     * @deprecated
     */
    const XML_PATH_REGISTER_EMAIL_IDENTITY = 'customer/create_account/email_identity';

    /**
     * @deprecated
     */
    const XML_PATH_REMIND_EMAIL_TEMPLATE = 'customer/password/remind_email_template';

    /**
     * @deprecated
     */
    const XML_PATH_FORGOT_EMAIL_TEMPLATE = 'customer/password/forgot_email_template';
    
    /**
     * @deprecated
     */
    const XML_PATH_FORGOT_EMAIL_OTP_TEMPLATE = 'mobileapp/mobileapp_option/forgot_email_otp_template';

	/**
     * @deprecated
     */
    const XML_PATH_REGISTRATION_EMAIL_OTP_TEMPLATE = 'mobileapp/mobileapp_option/registration_email_otp_template';

    /**
     * @deprecated
     */
    const XML_PATH_FORGOT_EMAIL_IDENTITY = 'customer/password/forgot_email_identity';

    /**
     * @deprecated
     * @see AccountConfirmation::XML_PATH_IS_CONFIRM
     */
    const XML_PATH_IS_CONFIRM = 'customer/create_account/confirm';

    /**
     * @deprecated
     */
    const XML_PATH_CONFIRM_EMAIL_TEMPLATE = 'customer/create_account/email_confirmation_template';

    /**
     * @deprecated
     */
    const XML_PATH_CONFIRMED_EMAIL_TEMPLATE = 'customer/create_account/email_confirmed_template';

    /**
     * Constants for the type of new account email to be sent
     *
     * @deprecated
     */
    const NEW_ACCOUNT_EMAIL_REGISTERED = 'registered';

    /**
     * Welcome email, when password setting is required
     *
     * @deprecated
     */
    const NEW_ACCOUNT_EMAIL_REGISTERED_NO_PASSWORD = 'registered_no_password';

    /**
     * Welcome email, when confirmation is enabled
     *
     * @deprecated
     */
    const NEW_ACCOUNT_EMAIL_CONFIRMATION = 'confirmation';

    /**
     * Confirmation email, when account is confirmed
     *
     * @deprecated
     */
    const NEW_ACCOUNT_EMAIL_CONFIRMED = 'confirmed';

    /**
     * Constants for types of emails to send out.
     * pdl:
     * forgot, remind, reset email templates
     */
    const EMAIL_REMINDER = 'email_reminder';

    const EMAIL_RESET = 'email_reset';

    /**
     * Configuration path to customer password minimum length
     */
    const XML_PATH_MINIMUM_PASSWORD_LENGTH = 'customer/password/minimum_password_length';

    /**
     * Configuration path to customer password required character classes number
     */
    const XML_PATH_REQUIRED_CHARACTER_CLASSES_NUMBER = 'customer/password/required_character_classes_number';

    /**
     * @deprecated
     */
    const XML_PATH_RESET_PASSWORD_TEMPLATE = 'customer/password/reset_password_template';

    /**
     * @deprecated
     */
    const MIN_PASSWORD_LENGTH = 6;


    
	
	protected $_customer;
    protected $_customerFactory;
	
	/**
     * @var MpSellerFactory
     */
	 
    protected $mpSellerFactory;
	
	/**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;
	
	/**
     * @var UrlRewriteFactory
     */
    protected $urlRewriteFactory;
	
	/**
     * @var MpHelper
     */
    protected $mpHelper;
	
    /**
     * @var CustomerFactory
     */
    private $customerFactory;

    /**
     * @var \Magento\Customer\Api\Data\ValidationResultsInterfaceFactory
     */
    private $validationResultsDataFactory;

    /**
     * @var ManagerInterface
     */
    private $eventManager;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Random
     */
    private $mathRandom;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var CustomerMetadataInterface
     */
    private $customerMetadataService;

    /**
     * @var PsrLogger
     */
    protected $logger;

    /**
     * @var Encryptor
     */
    private $encryptor;

    /**
     * @var CustomerRegistry
     */
    private $customerRegistry;

    /**
     * @var ConfigShare
     */
    private $configShare;

    /**
     * @var StringHelper
     */
    protected $stringHelper;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var SessionManagerInterface
     */
    private $sessionManager;

    /**
     * @var SaveHandlerInterface
     */
    private $saveHandler;

    /**
     * @var CollectionFactory
     */
    private $visitorCollectionFactory;

    /**
     * @var DataObjectProcessor
     */
    protected $dataProcessor;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var CustomerViewHelper
     */
    protected $customerViewHelper;

    /**
     * @var DateTime
     */
    protected $dateTime;

    /**
     * @var ObjectFactory
     */
    protected $objectFactory;

    /**
     * @var \Magento\Framework\Api\ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;

    /**
     * @var CustomerModel
     */
    protected $customerModel;

    /**
     * @var AuthenticationInterface
     */
    protected $authentication;

    /**
     * @var EmailNotificationInterface
     */
    private $emailNotification;

    /**
     * @var \Magento\Eav\Model\Validator\Attribute\Backend
     */
    private $eavValidator;

    /**
     * @var CredentialsValidator
     */
    private $credentialsValidator;

    /**
     * @var DateTimeFactory
     */
    private $dateTimeFactory;

    /**
     * @var AccountConfirmation
     */
    private $accountConfirmation;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var AddressRegistry
     */
    private $addressRegistry;

    /**
     * @var AllowedCountries
     */
    private $allowedCountriesReader;
    
    /**
     * @var \Magento\Customer\Model\Url
     */
    protected $customerUrl;
    
    /**
     * Token Model
     *
     * @var TokenModelFactory
     */
    private $tokenModelFactory;
    
    /**
     * Token Collection Factory
     *
     * @var TokenCollectionFactory
     */
    private $tokenModelCollectionFactory;
    
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;
    
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_connection;
    
    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $_catalogProductVisibility;
    
    /**
     * @var \Magento\Store\Model\App\Emulation
     */
    protected $_appEmulation;
    
    /**
     * @var \Magento\Framework\View\Element\BlockFactory
     */
    protected $_blockFactory;
    
    /**
     * @var \Magento\Wishlist\Model\Wishlist
     */
    protected $wishlist;
    
    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;
    
    /**
     * @var CustomerTokenService
     */
    private $customerToken;
    
    /**
     * @var \Magento\Directory\Model\Data\CountryInformationFactory
     */
    protected $countryInformationFactory;

    /**
     * @var \Magento\Directory\Model\Data\RegionInformationFactory
     */
    protected $regionInformationFactory;

    /**
     * @var \Magento\Directory\Helper\Data
     */
    protected $directoryHelper;
    
    /**
     * @var SubscriberFactory
     */
    protected $_subscriberFactory;
    
    /**
     * @var Filesystem
     */    
    private $cartTotalRepository;

	/**
     * Quote repository.
     *
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;



     /**
     * @var StateInterface
     */
    private $inlineTranslation;
 

    
    /**
     * @param CustomerFactory $customerFactory
     * @param ManagerInterface $eventManager
     * @param StoreManagerInterface $storeManager
     * @param Random $mathRandom
     * @param Validator $validator
     * @param ValidationResultsInterfaceFactory $validationResultsDataFactory
     * @param AddressRepositoryInterface $addressRepository
     * @param CustomerMetadataInterface $customerMetadataService
     * @param CustomerRegistry $customerRegistry
     * @param PsrLogger $logger
     * @param Encryptor $encryptor
     * @param ConfigShare $configShare
     * @param StringHelper $stringHelper
     * @param CustomerRepositoryInterface $customerRepository
     * @param ScopeConfigInterface $scopeConfig
     * @param TransportBuilder $transportBuilder
     * @param DataObjectProcessor $dataProcessor
     * @param Registry $registry
     * @param CustomerViewHelper $customerViewHelper
     * @param DateTime $dateTime
     * @param CustomerModel $customerModel
     * @param ObjectFactory $objectFactory
     * @param CustomerUrl $customerUrl
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param CredentialsValidator|null $credentialsValidator
     * @param DateTimeFactory|null $dateTimeFactory
     * @param AccountConfirmation|null $accountConfirmation
     * @param SessionManagerInterface|null $sessionManager
     * @param SaveHandlerInterface|null $saveHandler
     * @param CollectionFactory|null $visitorCollectionFactory
     * @param SearchCriteriaBuilder|null $searchCriteriaBuilder
     * @param AddressRegistry|null $addressRegistry
     * @param AllowedCountries|null $allowedCountriesReader
     * @param TokenModelFactory $tokenModelFactory
     * @param TokenCollectionFactory $tokenModelCollectionFactory
     * @param ProductCollectionFactory $productCollectionFactory
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility
     * @param \Magento\Framework\View\Element\BlockFactory $blockFactory
     * @param \Magento\Store\Model\App\Emulation $appEmulation
     * @param \Magento\Wishlist\Model\Wishlist $wishlist
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Integration\Model\CustomerTokenService $customerToken
     * @param \Magento\Directory\Model\Data\CountryInformationFactory $countryInformationFactory
     * @param \Magento\Directory\Model\Data\RegionInformationFactory $regionInformationFactory
     * @param \Magento\Directory\Helper\Data $directoryHelper
     * @param SubscriberFactory $subscriberFactory     
     * @param Filesystem $fileSystem
	 * @param MpHelper                                    $mpHelper
     * @param MpSellerFactory                             $mpSellerFactory
     * @param UrlRewriteFactory                           $urlRewriteFactory
     */
    public function __construct(
		\Magento\Customer\Model\Customer $customers,
		\Magento\Framework\Stdlib\DateTime\DateTime $date,
		UrlRewriteFactory $urlRewriteFactory,
		MpHelper $mpHelper,
		MpSellerFactory $mpSellerFactory,
        CustomerFactory $customerFactory,
        ManagerInterface $eventManager,
        StoreManagerInterface $storeManager,
        Random $mathRandom,
        Validator $validator,
        ValidationResultsInterfaceFactory $validationResultsDataFactory,
        AddressRepositoryInterface $addressRepository,
        CustomerMetadataInterface $customerMetadataService,
        CustomerRegistry $customerRegistry,
        PsrLogger $logger,
        Encryptor $encryptor,
        ConfigShare $configShare,
        StringHelper $stringHelper,
        CustomerRepositoryInterface $customerRepository,
        ScopeConfigInterface $scopeConfig,
        TransportBuilder $transportBuilder,
        DataObjectProcessor $dataProcessor,
        Registry $registry,
        CustomerViewHelper $customerViewHelper,
        DateTime $dateTime,
        CustomerModel $customerModel,
        ObjectFactory $objectFactory,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        CustomerUrl $customerUrl,
        TokenModelFactory $tokenModelFactory,
        TokenCollectionFactory $tokenModelCollectionFactory,
        ProductCollectionFactory $productCollectionFactory,
        CredentialsValidator $credentialsValidator = null,
        DateTimeFactory $dateTimeFactory = null,
        AccountConfirmation $accountConfirmation = null,
        SessionManagerInterface $sessionManager = null,
        SaveHandlerInterface $saveHandler = null,
        CollectionFactory $visitorCollectionFactory = null,
        SearchCriteriaBuilder $searchCriteriaBuilder = null,
        AddressRegistry $addressRegistry = null,
        AllowedCountries $allowedCountriesReader = null,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Framework\View\Element\BlockFactory $blockFactory,
		\Magento\Store\Model\App\Emulation $appEmulation,
		\Magento\Wishlist\Model\Wishlist $wishlist,
		CategoryFactory $categoryFactory,
		\Magento\Integration\Model\CustomerTokenService $customerToken,
		\Magento\Directory\Model\Data\CountryInformationFactory $countryInformationFactory,
        \Magento\Directory\Model\Data\RegionInformationFactory $regionInformationFactory,
        \Magento\Directory\Helper\Data $directoryHelper,
        SubscriberFactory $subscriberFactory,        
        Filesystem $fileSystem,
        CartTotalRepositoryInterface $cartTotalRepository,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,

        Context $context,
        StateInterface $inlineTranslation


    ) {		
        $this->_customer = $customers;
		$this->_date = $date;
		$this->urlRewriteFactory = $urlRewriteFactory;
		$this->mpHelper = $mpHelper;
		$this->mpSellerFactory = $mpSellerFactory;
        $this->customerFactory = $customerFactory;
        $this->eventManager = $eventManager;
        $this->storeManager = $storeManager;
        $this->mathRandom = $mathRandom;
        $this->validator = $validator;
        $this->validationResultsDataFactory = $validationResultsDataFactory;
        $this->addressRepository = $addressRepository;
        $this->customerMetadataService = $customerMetadataService;
        $this->customerRegistry = $customerRegistry;
        $this->logger = $logger;
        $this->encryptor = $encryptor;
        $this->configShare = $configShare;
        $this->stringHelper = $stringHelper;
        $this->customerRepository = $customerRepository;
        $this->scopeConfig = $scopeConfig;
        $this->transportBuilder = $transportBuilder;
        $this->dataProcessor = $dataProcessor;
        $this->registry = $registry;
        $this->customerViewHelper = $customerViewHelper;
        $this->dateTime = $dateTime;
        $this->customerModel = $customerModel;
        $this->objectFactory = $objectFactory;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->credentialsValidator =
            $credentialsValidator ?: ObjectManager::getInstance()->get(CredentialsValidator::class);
        $this->dateTimeFactory = $dateTimeFactory ?: ObjectManager::getInstance()->get(DateTimeFactory::class);
        $this->accountConfirmation = $accountConfirmation ?: ObjectManager::getInstance()
            ->get(AccountConfirmation::class);
        $this->sessionManager = $sessionManager
            ?: ObjectManager::getInstance()->get(SessionManagerInterface::class);
        $this->saveHandler = $saveHandler
            ?: ObjectManager::getInstance()->get(SaveHandlerInterface::class);
        $this->visitorCollectionFactory = $visitorCollectionFactory
            ?: ObjectManager::getInstance()->get(CollectionFactory::class);
        $this->searchCriteriaBuilder = $searchCriteriaBuilder
            ?: ObjectManager::getInstance()->get(SearchCriteriaBuilder::class);
        $this->addressRegistry = $addressRegistry
            ?: ObjectManager::getInstance()->get(AddressRegistry::class);
        $this->allowedCountriesReader = $allowedCountriesReader
            ?: ObjectManager::getInstance()->get(AllowedCountries::class);
        $this->customerUrl = $customerUrl;
        $this->tokenModelFactory = $tokenModelFactory;
        $this->tokenModelCollectionFactory = $tokenModelCollectionFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_connection = $resourceConnection->getConnection();
        $this->_catalogProductVisibility = $catalogProductVisibility;
        $this->_blockFactory = $blockFactory;
		$this->_appEmulation = $appEmulation;
		$this->wishlist = $wishlist;
		$this->_categoryFactory = $categoryFactory;
		$this->customerToken = $customerToken;
		$this->countryInformationFactory = $countryInformationFactory;
        $this->regionInformationFactory = $regionInformationFactory;
        $this->directoryHelper = $directoryHelper;
        $this->_subscriberFactory = $subscriberFactory;
        $this->_fileSystem = $fileSystem;       
        $this->cartTotalRepository = $cartTotalRepository;
        $this->quoteRepository = $quoteRepository;

        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->logger = $logger;
        //parent::__construct($context);
    }
    
    /**
     * {@inheritdoc}
     */
    public function setCurrencyCode($currency_code, $cart_id)
    {
		/*
		try {
			$om = \Magento\Framework\App\ObjectManager::getInstance();
			$filesystem = $om->get('Magento\Framework\Filesystem');
			$directoryList = $om->get('Magento\Framework\App\Filesystem\DirectoryList');
			$media = $filesystem->getDirectoryWrite($directoryList::MEDIA);    
			$pubMediaDir = $directoryList->getPath($directoryList::MEDIA);			
			$media->writeFile("visitor-count/request.txt",$currency_code.'+'.$cart_id);
		} catch(Exception $e) {
			echo $e->getMessage();
		}*/
		
		
        $responseFlag = false;
		$message = '';
		$response = array();
		try{
			if ($currency_code != '') {
			   $this->storeManager->getStore()->setCurrentCurrencyCode($currency_code);
			} else {
				$this->storeManager->getStore()->setCurrentCurrencyCode("INR");
			}
			if($cart_id != ''){
				$quote = $this->quoteRepository->getActive($cart_id);
				if ($quote->getItemsCount()) {
					$quote->getShippingAddress()->setCollectShippingRates(true);
					$this->quoteRepository->save($quote->collectTotals());
				}
			}
			
			$response['currency_code']  = $currency_code;
			$message = __('Successfully.');
			$responseFlag = true;
		} catch (\Exception $e) {
			$message = __('Something wrong please try again.');
		}
        
        $response['response']   = $responseFlag;
		$response['message']    = $message;
		echo json_encode($response);
    }

	/**
     * {@inheritdoc}
     */
    public function getCountriesInfo()
    {
        $countriesInfo = array();
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			/** @var \Magento\Store\Model\Store $store */
			$store = $this->storeManager->getStore();

			$storeLocale = $this->scopeConfig->getValue(
				'general/locale/code',
				\Magento\Store\Model\ScopeInterface::SCOPE_STORES,
				$store->getCode()
			);

			$countries = $this->directoryHelper->getCountryCollection($store);
			$regions = $this->directoryHelper->getRegionData();
			foreach ($countries as $data) {
				$countryInfo = $this->setCountryInfo($data, $regions, $storeLocale);
				$countriesInfo[] = $countryInfo;
			}
			$response['country']  = $countriesInfo;
			$message = __('Successfully.');
			$responseFlag = true;
		} catch (\Exception $e) {
			$message = __('Something wrong please try again.');
		}
        
        $response['response']   = $responseFlag;
		$response['message']    = $message;
		echo json_encode($response);
    }

    /**
     * {@inheritdoc}
     */
    public function getCountryInfo($countryId)
    {
		$countriesInfo = array();
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			$store = $this->storeManager->getStore();
			$storeLocale = $this->scopeConfig->getValue(
				'general/locale/code',
				\Magento\Store\Model\ScopeInterface::SCOPE_STORES,
				$store->getCode()
			);

			$countriesCollection = $this->directoryHelper->getCountryCollection($store)->load();
			$regions = $this->directoryHelper->getRegionData();
			$country = $countriesCollection->getItemById($countryId);

			if (!$country) {
				throw new NoSuchEntityException(
					__(
						'Requested country is not available.'
					)
				);
			}
			$countryInfo[] = $this->setCountryInfo($country, $regions, $storeLocale);
			$response['country']  = $countryInfo;
			$message = __('Successfully.');
			$responseFlag = true;
		} catch (NoSuchEntityException $e) {
			$message = __($e->getMessage());
		} catch (\Exception $e) {
			$message = __('Something wrong please try again.');
		}
		$response['response']   = $responseFlag;
		$response['message']    = $message;
		echo json_encode($response);
    }

    /**
     * Creates and initializes the information for \Magento\Directory\Model\Data\CountryInformation
     *
     * @param \Magento\Directory\Model\ResourceModel\Country $country
     * @param array $regions
     * @param string $storeLocale
     * @return array
     */
    protected function setCountryInfo($country, $regions, $storeLocale)
    {
        $countryId = $country->getCountryId();
        $countryInfo = array();
        $countryInfo['id'] = $countryId;
		$countryInfo['two_letter_abbreviation'] = $country->getData('iso2_code');
		$countryInfo['three_letter_abbreviation'] = $country->getData('iso3_code');
		$countryInfo['full_name_locale'] = $country->getName($storeLocale);
		$countryInfo['full_name_english'] = $country->getName('en_US');
        if (array_key_exists($countryId, $regions)) {
            $regionsInfo = array();
            foreach ($regions[$countryId] as $id => $regionData) {
               $regionsInfo[] = array('id' => $id, 'code' => $regionData['code'], 'name' => $regionData['name'] );
            }
            $countryInfo['available_regions'] = $regionsInfo;
        }
        return $countryInfo;
    }
	
	/**
     * @inheritdoc
     */
	public function login($email, $password = '', $device_id, $device_token, $device_type, $login_type = '', $first_name = '', $last_name = '', $social_id = '',$profile_picture='',$mobilenumber='')  
	{
		$login = array();
		$responseFlag = false;
		try{
			if($email)
				$login['username'] = $email;
			if($password)
				$login['password'] = $password;
				
			if($device_id == '' || $device_type == '') {
				$message = __('Device id and type are required.');
			} else if (!empty($login['username']) && !empty($login['password']) && $login_type == 'normal') {
				try {
					$customer = $this->authenticate($login['username'], $login['password']);
					$responseFlag = true;
				} catch (EmailNotConfirmedException $e) {
					$value = $this->customerUrl->getEmailConfirmationUrl($login['username']);
					$message = __(
								 'This account is not confirmed.' .
								 ' <a href="%1">Click here</a> to resend confirmation email.',
								 $value
								);
				} catch (AuthenticationException $e) {
					$message = __('Invalid login or password.');
				} catch (\Exception $e) {
					$message = __('Invalid login or password.');
				}
			} else if (!empty($login['username']) && ($login_type == 'facebook' || $login_type == 'google' || $login_type == 'apple' || $login_type == 'linkedin')) {
				if ($first_name != '' && $last_name != '' && $social_id != '') {
					$storeId = $this->storeManager->getStore()->getId();
					$websiteId = $this->storeManager->getStore($storeId)->getWebsiteId();
		 			$notEmailExit = $this->isEmailAvailable($login['username'], $websiteId);
		 			if($notEmailExit){
						$objectManager = ObjectManager::getInstance();
						$customer = $objectManager->get('\Magento\Customer\Api\Data\CustomerInterfaceFactory')->create();
						$customer->setStoreId($storeId);
						$customer->setWebsiteId($websiteId);
						$storeName = $this->storeManager->getStore($customer->getStoreId())->getName();
						$customer->setCreatedIn($storeName);
						$customer->setEmail($login['username']);
                                                
                                                
                                                if($mobilenumber!=''){
                                                    /*check mobile already used or not in any account*/
                                                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                                                    $customerObj = $objectManager->create('Magento\Customer\Model\ResourceModel\Customer\Collection');
                                                    $collection = $customerObj->addAttributeToSelect('*')
                                                                  ->addAttributeToFilter('mobilenumber',$mobilenumber)
                                                                  ->load();
                                                    $c_data=$collection->getData();                                                   
                                                    if($c_data){
                                                        $responseFlag = false;
                                                        $message = __('A customer with the same mobile number already exists in an associated website.');
                                                        $response['response'] = $responseFlag;
                                                        $response['message']  = $message;
                                                        echo json_encode($response);
                                                        die;   
                                                    }
                                                    $customer->setCustomAttribute('mobilenumber',$mobilenumber);
                                                }else{
                                                    /*return false if first time social login, for enter mobile number screen*/
                                                    $response['response'] = false;
                                                    $response['message']  = 'mobile';
                                                    echo json_encode($response);
                                                    die;
                                                }
                                                
						$customer->setFirstname($first_name);
						$customer->setLastname($last_name);
						$customer->setDob(date('Y-m-d'));
						$customer->setCustomAttribute('social_id',$social_id);
                                                
                                                /*set profile picture with social login*/
                                                if($profile_picture != '') {
                                                    $profilePictureName = $social_id."_".time().".png";
                                                    $profilePicturePath = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/profile_picture/'.$profilePictureName;
                                                    $ifp = fopen( $profilePicturePath, "wb" ); 
                                                    fwrite( $ifp, base64_decode( $profile_picture) ); 
                                                    fclose( $ifp );                                                    
                                                }
                                                if($profile_picture != '') {                                                        
                                                    $customer->setCustomAttribute('profile_picture',$profilePictureName);   
                                                }                                                
						$customer->setCustomAttribute('user_type','Pet Lover');
                                                $customer->setCustomAttribute('privacy_status','Public'); // set default value when register
						$customer->setCustomAttribute('login_type',$login_type);
						$customer = $this->customerRepository->save($customer, null);
						$this->customerRegistry->remove($customer->getId());
                                                
                                                 /*insert static follower data of admin*/
                                                $insertStaticData = self::insertStaticData($customer->getId());
                                                
						$newLinkToken = $this->mathRandom->getUniqueHash();
						$this->changeResetPasswordLinkToken($customer, $newLinkToken);
						$this->sendEmailConfirmation($customer, '', $extensions = []);
					} else {
						$customer = $this->customerRepository->get($login['username']);
						$customer->setCustomAttribute('social_id',$social_id);
                                                
                                                /*set profile picture with social login*/
                                                if($profile_picture != '') {
                                                    $profilePictureName = $social_id."_".time().".png";
                                                    $profilePicturePath = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/profile_picture/'.$profilePictureName;
                                                    $ifp = fopen( $profilePicturePath, "wb" ); 
                                                    fwrite( $ifp, base64_decode( $profile_picture) ); 
                                                    fclose( $ifp );                                                    
                                                }
                                                if($profile_picture != '') {                                                        
                                                    $customer->setCustomAttribute('profile_picture',$profilePictureName);   
                                                }
                                                
						$customer = $this->customerRepository->save($customer);
						$this->customerRegistry->remove($customer->getId());
					}
					$responseFlag = true;
				} else {
					$message = __('Oops. Something went wrong. Please try again later.');
				}
				
			} else {
				$message = __('Invalid login or password.');
			}
			if($responseFlag) {			
				$customer = $this->customerRepository->get($login['username']);
				$customer->setCustomAttribute('device_id',$device_id);
				$customer->setCustomAttribute('device_token',$device_token);
				$customer->setCustomAttribute('device_type',$device_type);
				$customer = $this->customerRepository->save($customer);
				$this->customerRegistry->remove($customer->getId());
				try{
					$this->customerToken->revokeCustomerAccessToken($customer->getId());
					$securetoken = $this->tokenModelFactory->create()->createCustomerToken($customer->getId())->getToken();
				} catch(\Exception $e) {
					$securetoken = $this->tokenModelFactory->create()->createCustomerToken($customer->getId())->getToken();
				}
				$message = 'You are successfully Logged in';				
				$response['users_id']    = $customer->getId();
				$response['user_email']  = $customer->getEmail();
				$response['user_name']   = ucwords($customer->getFirstName().' '.$customer->getLastName());
				$response['first_name']  = $customer->getFirstname();
				$response['last_name'] 	 = $customer->getLastname();
				$response['dob'] 		 = date("m-d-Y", strtotime($customer->getDob()));
				$response['email'] 		 = $customer->getEmail();
				$response['user_type'] 		 = ($customer->getCustomAttribute('user_type')) ? $customer->getCustomAttribute('user_type')->getValue() : "";
				//$response['mobilenumber'] 		 = $customer->getCustomAttribute('mobilenumber')->getValue();				
				$response['mobilenumber'] 		 = ($customer->getCustomAttribute('mobilenumber')) ? $customer->getCustomAttribute('mobilenumber')->getValue() : "";
				$response['business_name'] 		 = ($customer->getCustomAttribute('business_name')) ? $customer->getCustomAttribute('business_name')->getValue() : "";
				$response['business_type'] 		 = ($customer->getCustomAttribute('business_type')) ? $customer->getCustomAttribute('business_type')->getValue() : "";
				$response['business_type_id'] 		 = ($customer->getCustomAttribute('business_type_id')) ? $customer->getCustomAttribute('business_type_id')->getValue() : "";
				$response['latitude'] 		 = ($customer->getCustomAttribute('latitude')) ? $customer->getCustomAttribute('latitude')->getValue() : "";
				$response['longitude'] 		 = ($customer->getCustomAttribute('longitude')) ? $customer->getCustomAttribute('longitude')->getValue() : "";
				$response['shop_address'] 		 = ($customer->getCustomAttribute('shop_address')) ? $customer->getCustomAttribute('shop_address')->getValue() : "";
				$response['pin_code'] 		 = ($customer->getCustomAttribute('pin_code')) ? $customer->getCustomAttribute('pin_code')->getValue() : "";
				$response['city'] 		 = ($customer->getCustomAttribute('city')) ? $customer->getCustomAttribute('city')->getValue() : "";
				$response['state'] 		 = ($customer->getCustomAttribute('state')) ? $customer->getCustomAttribute('state')->getValue() : "";
				$response['gstin'] 		 = ($customer->getCustomAttribute('gstin')) ? $customer->getCustomAttribute('gstin')->getValue() : "";
				$response['pan'] 		 = ($customer->getCustomAttribute('pan')) ? $customer->getCustomAttribute('pan')->getValue() : "";
                                $response['privacy_status'] 		 = ($customer->getCustomAttribute('privacy_status')) ? $customer->getCustomAttribute('privacy_status')->getValue() : "Public";
				//$response['mobilenumber'] = ($customer->getMobilenumber())? $customer->getMobilenumber(): '';
				$response['social_id'] 		 = ($customer->getCustomAttribute('social_id')) ? $customer->getCustomAttribute('social_id')->getValue() : "";
                                
                                
				$response['profile_picture'] = '';
				if($customer->getCustomAttribute('profile_picture')) {
					$mediaDir = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/';
					if(file_exists($mediaDir.'profile_picture/'.$customer->getCustomAttribute('profile_picture')->getValue())){
						$response['profile_picture'] = $this->storeManager->getStore()->getBaseUrl().'media/profile_picture/'.$customer->getCustomAttribute('profile_picture')->getValue();
                                        }else{
                                            $response['profile_picture'] = $customer->getCustomAttribute('profile_picture');
                                        }
				}
				$response['cover_picture'] = '';
				if($customer->getCustomAttribute('cover_picture')) {
					$mediaDir = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/';
					if(file_exists($mediaDir.'cover_picture/'.$customer->getCustomAttribute('cover_picture')->getValue())){
						$response['cover_picture'] = $this->storeManager->getStore()->getBaseUrl().'media/cover_picture/'.$customer->getCustomAttribute('cover_picture')->getValue();
					}
				}
				$response['image_one'] = '';
				if($customer->getCustomAttribute('image_one')) {
					$mediaDir = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/';
					if(file_exists($mediaDir.'certificate/'.$customer->getCustomAttribute('image_one')->getValue())){
						$response['image_one'] = $this->storeManager->getStore()->getBaseUrl().'media/certificate/'.$customer->getCustomAttribute('image_one')->getValue();
					}
				}
				$response['image_two'] = '';
				if($customer->getCustomAttribute('image_two')) {
					$mediaDir = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/';
					if(file_exists($mediaDir.'certificate/'.$customer->getCustomAttribute('image_two')->getValue())){
						$response['image_two'] = $this->storeManager->getStore()->getBaseUrl().'media/certificate/'.$customer->getCustomAttribute('image_two')->getValue();
					}
				}
				$response['image_three'] = '';
				if($customer->getCustomAttribute('image_three')) {
					$mediaDir = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/';
					if(file_exists($mediaDir.'certificate/'.$customer->getCustomAttribute('image_three')->getValue())){
						$response['image_three'] = $this->storeManager->getStore()->getBaseUrl().'media/certificate/'.$customer->getCustomAttribute('image_three')->getValue();
					}
				}
                                
				if($customer->getCustomAttribute('account_is_block')->getValue() == 0){
					$message = 'Your account is disabled. Please contact customer service.';
					$responseFlag = false;
					$response = array();
				}else{
					$response['securetoken'] = $securetoken;
				}
			}
		} catch(\Exception $e) {
			$message = __($e->getMessage());
		}
		$response['response'] = $responseFlag;
		$response['message']  = $message;
		echo json_encode($response);
	}

	/**
     * @inheritdoc
     */
    public function registrationAccount(CustomerInterface $customer, $password = null, $redirectUrl = '', $is_seller='',$profileurl='', $extensions = [])
    {    
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			if($password != null || $password != '') {
                            
                                //$customer->setIsSubscribed(false);
                            
				//$this->checkPasswordStrength($password);
				$customerEmail = $customer->getEmail();
				
				// get attributes value for check validation				
				//print_r($customer->getCustomAttributes());
				//die;
				$this->credentialsValidator->checkPasswordDifferentFromEmail($customerEmail, $password);
				$hash = $this->createPasswordHash($password);
				
				// Make sure we have a storeId to associate this customer with.
				if (!$customer->getStoreId()) {
					if ($customer->getWebsiteId()) {
						$storeId = $this->storeManager->getWebsite($customer->getWebsiteId())->getDefaultStore()->getId();
					} else {
						$storeId = $this->storeManager->getStore()->getId();
					}
					$customer->setStoreId($storeId);
				}

				// Associate website_id with customer
				if (!$customer->getWebsiteId()) {
					$websiteId = $this->storeManager->getStore($customer->getStoreId())->getWebsiteId();
					$customer->setWebsiteId($websiteId);
				}

				// Update 'created_in' value with actual store name
				if ($customer->getId() === null) {
					$storeName = $this->storeManager->getStore($customer->getStoreId())->getName();
					$customer->setCreatedIn($storeName);
				}
                                
				
				try {
					// If customer exists existing hash will be used by Repository
                                        $customer->setCustomAttribute('privacy_status','Public'); // set default value when register
					$customer = $this->customerRepository->save($customer, $hash);
					$this->customerRegistry->remove($customer->getId());
					$customer = $this->customerRepository->getById($customer->getId());
					$newLinkToken = $this->mathRandom->getUniqueHash();
					$this->changeResetPasswordLinkToken($customer, $newLinkToken);
					$this->sendEmailConfirmation($customer, $redirectUrl, $extensions);
					$response['users_id']   = $customer->getId();
					$response['user_name']  = $customer->getFirstname().' '.$customer->getLastname();
					$response['user_email'] = $customer->getEmail();
					$responseFlag = true;
					$message  = "You are successfully registered.";
                                        
                                        /*insert static follower data of admin*/
                                        $insertStaticData = self::insertStaticData($customer->getId());
                                        
				} catch (AlreadyExistsException $e) {
					throw new InputException(
						__('A customer with the same email already exists in an associated website.')
					);
				} catch (LocalizedException $e) {
					throw new InputException(
						__($e->getMessage())
					);
				}
			}  else {
				$message  = "Please enter password.";
			}
			
			
			/*seller registration*/
			$paramData['profileurl'] = $profileurl;
			$paramData['is_seller'] = $is_seller;
			if (!empty($paramData['is_seller']) && $paramData['is_seller'] == 1) {
                //$customer = $observer->getCustomer();

                /*$profileurlcount = $this->mpSellerFactory->create()->getCollection();
                $profileurlcount->addFieldToFilter(
                    'shop_url',
                    $paramData['profileurl']
                );*/
                
                    $partnerApprovalStatus = $this->mpHelper->getIsPartnerApproval();
                    $status = $partnerApprovalStatus ? 0 : 1;
                    $customerid = $customer->getId();
                    $model = $this->mpSellerFactory->create();
                    $model->setData('is_seller', $status);
                    //$model->setData('shop_url', $paramData['profileurl']);
                    $model->setData('seller_id', $customerid);
                    $model->setData('store_id', 0);
                    $model->setCreatedAt($this->_date->gmtDate());
                    $model->setUpdatedAt($this->_date->gmtDate());
                    if ($status == 0) {
                        $model->setAdminNotification(1);
                    }
                    $model->save();
                    /*$loginUrl = $this->urlInterface->getUrl("marketplace/account/dashboard");
                    $this->customerSession->setBeforeAuthUrl($loginUrl);
                    $this->customerSession->setAfterAuthUrl($loginUrl);*/

                    $helper = $this->mpHelper;
                    /*if ($helper->getAutomaticUrlRewrite()) {
                        $this->createSellerPublicUrls($paramData['profileurl']);
                    }*/
                    if ($partnerApprovalStatus) {
                        $adminStoremail = $helper->getAdminEmailId();
                        $adminEmail = $adminStoremail ? $adminStoremail : $helper->getDefaultTransEmailId();
                        $adminUsername = $helper->getAdminName();
                        $senderInfo = [
                            'name' => $customer->getFirstName().' '.$customer->getLastName(),
                            'email' => $customer->getEmail(),
                        ];
                        $receiverInfo = [
                            'name' => $adminUsername,
                            'email' => $adminEmail,
                        ];
                        $emailTemplateVariables['myvar1'] = $customer->getFirstName().' '.
                        $customer->getLastName();
                        $emailTemplateVariables['myvar2'] = $this->urlBackendModel->getUrl(
                            'customer/index/edit',
                            ['id' => $customer->getId()]
                        );
                        $emailTemplateVariables['myvar3'] = $helper->getAdminName();

                        $this->mpEmailHelper->sendNewSellerRequest(
                            $emailTemplateVariables,
                            $senderInfo,
                            $receiverInfo
                        );
                    }
               
            }
			
			
			
		} catch(InputException $e){
			$message  = $e->getMessage();
		}
		$response['response'] = $responseFlag;
		$response['message']  = $message;
		echo json_encode($response);
	}
	
	
	
    public function createSellerPublicUrls($profileurl = '')
    {
        if ($profileurl) {
            $getCurrentStoreId = $this->mpHelper->getCurrentStoreId();

            /*
            * Set Seller Profile Url
            */
            $sourceProfileUrl = 'marketplace/seller/profile/shop/'.$profileurl;
            $requestProfileUrl = $profileurl;
            /*
            * Check if already rexist in url rewrite model
            */
            $urlId = '';
            $profileRequestUrl = '';
            $urlCollectionData = $this->urlRewriteFactory->create()
                              ->getCollection()
                              ->addFieldToFilter('target_path', $sourceProfileUrl)
                              ->addFieldToFilter('store_id', $getCurrentStoreId);
            foreach ($urlCollectionData as $value) {
                $urlId = $value->getId();
                $profileRequestUrl = $value->getRequestPath();
            }
            if ($profileRequestUrl != $requestProfileUrl) {
                $idPath = rand(1, 100000);
                $this->urlRewriteFactory->create()->load($urlId)
                ->setStoreId($getCurrentStoreId)
                ->setIsSystem(0)
                ->setIdPath($idPath)
                ->setTargetPath($sourceProfileUrl)
                ->setRequestPath($requestProfileUrl)
                ->save();
            }

            /*
            * Set Seller Collection Url
            */
            $sourceCollectionUrl = 'marketplace/seller/collection/shop/'.$profileurl;
            $requestCollectionUrl = $profileurl.'/collection';
            /*
            * Check if already rexist in url rewrite model
            */
            $urlId = '';
            $collectionRequestUrl = '';
            $urlCollectionData = $this->urlRewriteFactory->create()
                                ->getCollection()
                                ->addFieldToFilter('target_path', $sourceCollectionUrl)
                                ->addFieldToFilter('store_id', $getCurrentStoreId);
            foreach ($urlCollectionData as $value) {
                $urlId = $value->getId();
                $collectionRequestUrl = $value->getRequestPath();
            }
            if ($collectionRequestUrl != $requestCollectionUrl) {
                $idPath = rand(1, 100000);
                $this->urlRewriteFactory->create()->load($urlId)
                ->setStoreId($getCurrentStoreId)
                ->setIsSystem(0)
                ->setIdPath($idPath)
                ->setTargetPath($sourceCollectionUrl)
                ->setRequestPath($requestCollectionUrl)
                ->save();
            }

            /*
            * Set Seller Feedback Url
            */
            $sourceFeedbackUrl = 'marketplace/seller/feedback/shop/'.$profileurl;
            $requestFeedbackUrl = $profileurl.'/feedback';
            /*
            * Check if already rexist in url rewrite model
            */
            $urlId = '';
            $feedbackRequestUrl = '';
            $urlFeedbackData = $this->urlRewriteFactory->create()
                              ->getCollection()
                              ->addFieldToFilter('target_path', $sourceFeedbackUrl)
                              ->addFieldToFilter('store_id', $getCurrentStoreId);
            foreach ($urlFeedbackData as $value) {
                $urlId = $value->getId();
                $feedbackRequestUrl = $value->getRequestPath();
            }
            if ($feedbackRequestUrl != $requestFeedbackUrl) {
                $idPath = rand(1, 100000);
                $this->urlRewriteFactory->create()->load($urlId)
                ->setStoreId($getCurrentStoreId)
                ->setIsSystem(0)
                ->setIdPath($idPath)
                ->setTargetPath($sourceFeedbackUrl)
                ->setRequestPath($requestFeedbackUrl)
                ->save();
            }

            /*
            * Set Seller Location Url
            */
            $sourceLocationUrl = 'marketplace/seller/location/shop/'.$profileurl;
            $requestLocationUrl = $profileurl.'/location';
            /*
            * Check if already rexist in url rewrite model
            */
            $urlId = '';
            $locationRequestUrl = '';
            $urlLocationData = $this->urlRewriteFactory->create()
                              ->getCollection()
                              ->addFieldToFilter('target_path', $sourceLocationUrl)
                              ->addFieldToFilter('store_id', $getCurrentStoreId);
            foreach ($urlLocationData as $value) {
                $urlId = $value->getId();
                $locationRequestUrl = $value->getRequestPath();
            }
            if ($locationRequestUrl != $requestLocationUrl) {
                $idPath = rand(1, 100000);
                $this->urlRewriteFactory->create()->load($urlId)
                ->setStoreId($getCurrentStoreId)
                ->setIsSystem(0)
                ->setIdPath($idPath)
                ->setTargetPath($sourceLocationUrl)
                ->setRequestPath($requestLocationUrl)
                ->save();
            }

            /**
             * Set Seller Policy Url
             */
            $sourcePolicyUrl = 'marketplace/seller/policy/shop/'.$profileurl;
            $requestPolicyUrl = $profileurl.'/policy';
            /*
            * Check if already rexist in url rewrite model
            */
            $urlId = '';
            $policyRequestUrl = '';
            $urlPolicyData = $this->urlRewriteFactory->create()
                            ->getCollection()
                            ->addFieldToFilter('target_path', $sourcePolicyUrl)
                            ->addFieldToFilter('store_id', $getCurrentStoreId);
            foreach ($urlPolicyData as $value) {
                $urlId = $value->getId();
                $policyRequestUrl = $value->getRequestPath();
            }
            if ($policyRequestUrl != $requestPolicyUrl) {
                $idPath = rand(1, 100000);
                $this->urlRewriteFactory->create()->load($urlId)
                ->setStoreId($getCurrentStoreId)
                ->setIsSystem(0)
                ->setIdPath($idPath)
                ->setTargetPath($sourcePolicyUrl)
                ->setRequestPath($requestPolicyUrl)
                ->save();
            }
        }
    }
	
	
	/**
     * @inheritdoc
     */
    public function forgotPassword($email = '',$otp_type,$mobile_number = '',$resend = '',$signature = '')
    {		
		$responseFlag = false;
		$message = '';
		$response = array();
                $mobileOtpFlag = 0;
                $emailOtpFlag = 0;
		try{
			if($email) {
				$storeId = $this->storeManager->getStore()->getId();
				$websiteId = $this->storeManager->getStore($storeId)->getWebsiteId();
				$notEmailExit = $this->isEmailAvailable($email, $websiteId);
				
				if(!$notEmailExit&& $otp_type=='forgotpassword'){										
					//$OTP = mt_rand(100000, 999999);
                                        
                                        if($resend=='email'){      
                                            /*re-send otp in mail and sms when forgot password*/                                           
                                            if($resend=='email'){
                                                $OTP = self::sendMobileOtp($email,$resend);
                                                $emailOtpFlag = 1;
                                            }
                                        }else{   
                                            /*send otp in mail and sms when forgot password*/                                            
                                            $OTP = self::sendMobileOtp($email);
                                            $emailOtpFlag = 1;
                                        }
                                        
					//$OTP = 123456;					
					// $msg = "Your Petzzco one time password(OTP) is $OTP. its valid till 30 minutes. Do not share this OTP to anyone for security reasons.";
					$customer = $this->customerRepository->get($email);
					$customerNew = $this->customerFactory->create()->load($customer->getId());
					$customerNew->setMobileOtp($OTP);
					$customerNew->save();
					$this->customerRegistry->remove($customer->getId());
					$customer = $this->customerRepository->getById($customer->getId());
					if (!$storeId) {
						$storeId = $this->getWebsiteStoreId($customer);
					}

					/*send otp in mobile number*/
					self::sendMessageOTP($customerNew->getMobilenumber(),$OTP,$signature);

					$customerEmailData = $this->getFullCustomerObject($customer);
					$customerEmailData['otp'] = $OTP;
					$this->sendEmailTemplate(
						$customer,
						self::XML_PATH_FORGOT_EMAIL_OTP_TEMPLATE,
						self::XML_PATH_FORGOT_EMAIL_IDENTITY,
						['customer' => $customerEmailData, 'store' => $this->storeManager->getStore($storeId)],
						$storeId
					);
					//$response['otp'] = $OTP;
				}elseif($otp_type=='social'){						
                                    /*check mobile already registered with another account or not*/
                                        self::mobileIsExistOrNot($mobile_number);
					
					/*generate otp in database*/									
					$OTP = self::sendMobileOtp($mobile_number,$resend);
					$emailOtpFlag = 1;					
					/*send otp in mobile number*/
					self::sendMessageOTP($mobile_number,$OTP,$signature);
					
				}else{
					if($otp_type=='registration'){
                                                /*check mobile and email already registered or not*/
                                                $customerId = "";
                                                try {
                                                    $customerData = $this->customerRepository->get($email);
                                                    $customerId = (int)$customerData->getId();
                                                }catch (NoSuchEntityException $noSuchEntityException){
                                                }                                                                                                                                           
                                                if($customerId){
                                                    $responseFlag = false;
                                                    $message = __('A customer with the same email already exists in an associated website.');
                                                    $response['response'] = $responseFlag;
                                                    $response['message']  = $message;
                                                    echo json_encode($response);
                                                    die;                                                    
                                                }
                                                
                                                 /*check mobile already registered with another account or not*/
                                                self::mobileIsExistOrNot($mobile_number);
                                                                                                                                 
                                                if($resend=='mobile' || $resend=='email'){      
                                                    /*send resend otp in mobile or email as per request*/
                                                    if($resend=='mobile'){                                                        
                                                        $mobileOTP = self::sendMobileOtp($mobile_number,$resend);
                                                        $mobileOtpFlag = 1;
                                                    }
                                                    if($resend=='email'){
                                                        $OTP = self::sendMobileOtp($email,$resend);
                                                        $emailOtpFlag = 1;
                                                    }
                                                }else{   
                                                    /*send otp in mail and mobile when registration*/
                                                    $mobileOTP = self::sendMobileOtp($mobile_number);
                                                    $OTP = self::sendMobileOtp($email);
                                                    $mobileOtpFlag = 1;
                                                    $emailOtpFlag = 1;
                                                }
                                                
                                                
						/*send otp in mobile number*/		
                                                if($mobileOtpFlag==1){
                                                    self::sendMessageOTP($mobile_number,$mobileOTP,$signature);
                                                }
						
                                                
                                                if($emailOtpFlag==1)
                                                {                                                             
                                                    $transport = $this->transportBuilder
                                                    ->setTemplateIdentifier($this->scopeConfig->getValue(self::XML_PATH_REGISTRATION_EMAIL_OTP_TEMPLATE))
                                                    ->setTemplateOptions(
                                                            [
                                                                    'area' => \Magento\Backend\App\Area\FrontNameResolver::AREA_CODE,
                                                                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                                                            ]
                                                    )
                                                    ->setTemplateVars(['otp' => $OTP])
                                                    ->setFrom([
                                                                            'email' => $email,
                                                                            'name' => $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE)
                                                                    ])
                                                    ->addTo($email)
                                                    ->setReplyTo($email)
                                                    ->getTransport();
                                                    $transport->sendMessage();  
                                                }
					}else{
						$responseFlag = false;
						$message = __('Email ID is not registered with Petzzco, Please enter registered Email ID');
						$response['response'] = $responseFlag;
						$response['message']  = $message;
						echo json_encode($response);
						die;
					}									
				}
			}
			$responseFlag = true;
			$message = __('OTP has been sent successfully');
		} catch(\Exception $e){
			$message  = $e->getMessage();
		}
		$response['response'] = $responseFlag;
		$response['message']  = $message;
		echo json_encode($response);
	}
    
     /**
     * @inheritdoc
     */
    
    /*check mobile already registered with another account or not*/
    public function mobileIsExistOrNot($mobile_number)
    {	
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerObj = $objectManager->create('Magento\Customer\Model\ResourceModel\Customer\Collection');
            $collection = $customerObj->addAttributeToSelect('*')
                          ->addAttributeToFilter('mobilenumber',$mobile_number)
                          ->load();
            $c_data=$collection->getData();                                                   
            if($c_data){
                $responseFlag = false;
                $message = __('A customer with the same mobile number already exists in an associated website.');
                $response['response'] = $responseFlag;
                $response['message']  = $message;
                echo json_encode($response);
                die;   
            }       
    }
        
    /**
     * @inheritdoc
     */
    public function forgotPasswordOtpVerification($email, $otp, $otp_type)
    {		
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			if($email && $otp && $otp_type) {
				if($otp_type=='forgotpassword'){
					$storeId = $this->storeManager->getStore()->getId();
					$websiteId = $this->storeManager->getStore($storeId)->getWebsiteId();
					$notEmailExit = $this->isEmailAvailable($email, $websiteId);
					if(!$notEmailExit){
						$customer = $this->customerRepository->get($email);
						$customerNew = $this->customerFactory->create()->load($customer->getId());
						if($customerNew->getMobileOtp() == $otp) {
                                                        
                                                $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
						->get('Magento\Framework\App\ResourceConnection');
						$connection= $this->_resources->getConnection();

						$tableName = $this->_resources->getTableName('register_otp');
						
						//select otp data
						$sql = "select * from " . $tableName . " where email='$email' and otp=$otp and status=0";
						$getData = $connection->fetchAll($sql);
						if($getData){                                                        
                                                        $otp_date = $getData[0]['otp_date'];
                                                        $current_date = date('Y-m-d H:i:s');
                                                        $different_in_minutes = round(abs(strtotime($otp_date) - strtotime($current_date)) / 60,0);                                                            
                                                        if($different_in_minutes>=30){
                                                            $response['response'] = false;
                                                            $response['message']  = 'The OTP code has expired. Please re-send the verification code to try again';
                                                            echo json_encode($response);
                                                            die;
                                                        }                                                        
							//update otp status
							$sql = "Update " . $tableName . " Set status = 1 where email = '$email'";
							$connection->query($sql);
								
							$message = __('OTP verified successfully.');
							$responseFlag = true;
						}
                                                    
                                                    
							$message = __('OTP verified successfully.');
							$responseFlag = true;
						} else {
							$message = __('Sorry, Entered OTP number is wrong.');
						}
						
					} else {
						$message = __('Account not found.');
					}
				}else{
					$this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
						->get('Magento\Framework\App\ResourceConnection');
						$connection= $this->_resources->getConnection();

						$tableName = $this->_resources->getTableName('register_otp');
						
						//select otp data
						$sql = "select * from " . $tableName . " where email='$email' and otp=$otp and status=0";
						$getData = $connection->fetchAll($sql);
						if($getData){                                                        
                                                        $otp_date = $getData[0]['otp_date'];
                                                        $current_date = date('Y-m-d H:i:s');
                                                        $different_in_minutes = round(abs(strtotime($otp_date) - strtotime($current_date)) / 60,0);    
                                                        if($different_in_minutes>=30){
                                                            $response['response'] = false;
                                                            $response['message']  = 'The OTP code has expired. Please re-send the verification code to try again';
                                                            echo json_encode($response);
                                                            die;
                                                        }                                                        
							//update otp status
							$sql = "Update " . $tableName . " Set status = 1 where email = '$email'";
							$connection->query($sql);
								
							$message = __('OTP verified successfully.');
							$responseFlag = true;
						}else{
							$message = __('Sorry, Entered OTP number is wrong.');
						}							
					
				}
								
				
			} else {
				$message = __('Some parameter missing, please try again.');
			}
			
		} catch(\Exception $e){
			$message  = $e->getMessage();
		}
		$response['response'] = $responseFlag;
		$response['message']  = $message;
		echo json_encode($response);
	}
	
	
	/**
     * @inheritdoc
     */
    public function mobileChangeOtpVerification($customerId, $mobilenumber, $otp)
    {		
	
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			if($mobilenumber && $otp) {
				$this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
						->get('Magento\Framework\App\ResourceConnection');
						$connection= $this->_resources->getConnection();

						$tableName = $this->_resources->getTableName('register_otp');
						
						//select otp data
						$sql = "select * from " . $tableName . " where email='$mobilenumber' and otp=$otp and status=0";
						$getData = $connection->fetchAll($sql);
						if($getData){
							//update otp status
							$sql = "Update " . $tableName . " Set status = 1 where email = '$mobilenumber'";
							$connection->query($sql);
							/*load customer data*/
							$customerNew = $this->customerFactory->create()->load($customerId);
							$customerNew->setMobilenumber($mobilenumber);
							$customerNew->save();
							$message = __('Mobile number has been successfully updated.');
							$responseFlag = true;
						}else{
							$message = __('Sorry, Entered OTP number is wrong.');
						}	
								
				
			} else {
				$message = __('Some parameter missing, please try again.');
			}
			
		} catch(\Exception $e){
			$message  = $e->getMessage();
		}
		$response['response'] = $responseFlag;
		$response['message']  = $message;
		echo json_encode($response);
	}
    
    /**
     * @inheritdoc
     */
    public function resetPasswordWithOTP($email, $password, $otp)
    {
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			if($email && $password && $otp) {
				$storeId = $this->storeManager->getStore()->getId();
				$websiteId = $this->storeManager->getStore($storeId)->getWebsiteId();
				$notEmailExit = $this->isEmailAvailable($email, $websiteId);
				if(!$notEmailExit){
					$customer = $this->customerRepository->get($email);
					$customerNew = $this->customerFactory->create()->load($customer->getId());
					if($customerNew->getMobileOtp() == $otp) {
						$customerNew->setPassword($password);
						$customerNew->setMobileOtp('');
						$customerNew->save();
						$message = __('Password changed successfully.');
						$responseFlag = true;
					} else {
						$message = __('Something wrong please try again.');
					}
					
				} else {
					$message = __('Something wrong please try again.');
				}
			} else {
				$message = __('Some parameter missing, please try again.');
			}
		} catch(\Exception $e){
			$message  = $e->getMessage();
		}
		$response['response'] = $responseFlag;
		$response['message']  = $message;
		echo json_encode($response);
	}
	
	/**
     * @inheritdoc
     */
	public function changeUserPassword($customerId, $old_password, $new_password)
	{
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			try {
				$customer = $this->customerRepository->getById($customerId);
			} catch (NoSuchEntityException $e) {
				throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
			}
            $this->changePasswordForCustomer($customer, $old_password, $new_password);
			$message = __('Password changed successfully.');
			$responseFlag = true;
		} catch(InputMismatchException $e){
			$message  = $e->getMessage();
		} catch(InvalidEmailOrPasswordException $e){
			$message  = $e->getMessage();
		} catch(LocalizedException $e){
			$message  = $e->getMessage();
		} catch(NoSuchEntityException $e){
			$message  = $e->getMessage();
		} catch(UserLockedException $e){
			$message  = $e->getMessage();
		} catch(\Exception $e){
			$message  = $e->getMessage();
		}
		$response['response'] = $responseFlag;
		$response['message']  = $message;
		echo json_encode($response);
	}
	
	/**
     * @inheritdoc
     */
	public function changeSubscribe($customerId, $subscribe)
	{
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			try {
				$customer = $this->customerRepository->getById($customerId);
			} catch (NoSuchEntityException $e) {
				throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
			}
			if($subscribe) {
				$status = $this->_subscriberFactory->create()->subscribe($customer->getEmail());
				$message = __('subscribe successfully.');
			} else {
				$status = $this->_subscriberFactory->create()->loadByEmail($customer->getEmail())->unsubscribe();
				$message = __('unsubscribe successfully.');
			}
			$responseFlag = true;
		} catch(InputMismatchException $e){
			$message  = $e->getMessage();
		} catch(InvalidEmailOrPasswordException $e){
			$message  = $e->getMessage();
		} catch(LocalizedException $e){
			$message  = $e->getMessage();
		} catch(NoSuchEntityException $e){
			$message  = $e->getMessage();
		} catch(UserLockedException $e){
			$message  = $e->getMessage();
		} catch(\Exception $e){
			$message  = $e->getMessage();
		}
		$response['response'] = $responseFlag;
		$response['message']  = $message;
		echo json_encode($response);
	}
	
	/**
     * @inheritdoc
     */
	public function checkSubscribe($customerId)
	{
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			try {
				$customer = $this->customerRepository->getById($customerId);
			} catch (NoSuchEntityException $e) {
				throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
			}
			$checkSubscriber = $this->_subscriberFactory->create()->loadByEmail($customer->getEmail());
			if($checkSubscriber->isSubscribed()) {
				$responseFlag = true;
			}
		} catch(InputMismatchException $e){
			$message  = $e->getMessage();
		} catch(InvalidEmailOrPasswordException $e){
			$message  = $e->getMessage();
		} catch(LocalizedException $e){
			$message  = $e->getMessage();
		} catch(NoSuchEntityException $e){
			$message  = $e->getMessage();
		} catch(UserLockedException $e){
			$message  = $e->getMessage();
		} catch(\Exception $e){
			$message  = $e->getMessage();
		}
		$response['response'] = $responseFlag;
		$response['message']  = $message;
		echo json_encode($response);
	}
	
	/**
     * @inheritdoc
     */
	public function manageProfile($customerId, $first_name, $last_name, $email, $mobilenumber, $profile_picture = '', $cover_picture = '',$privacy_status,$signature = '')
	{
		/*$writer = new \Zend\Log\Writer\Stream( BP . '/var/log/api_log.log');
		$zendLogger = new \Zend\Log\Logger();
		$zendLogger->addWriter($writer);
		$zendLogger->info('hello');*/
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			try {
				$customer = $this->customerRepository->getById($customerId);
			} catch (NoSuchEntityException $e) {
				throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
			}
			
			try {
				if($profile_picture != '') {
					$profilePictureName = $customerId."_".time().".png";
					$profilePicturePath = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/profile_picture/'.$profilePictureName;
					$ifp = fopen( $profilePicturePath, "wb" ); 
					fwrite( $ifp, base64_decode( $profile_picture) ); 
					fclose( $ifp );
					if($customer->getCustomAttribute('profile_picture')){
						$oldProfilePicturePath = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/profile_picture/'.$customer->getCustomAttribute('profile_picture')->getValue();
						if(file_exists($oldProfilePicturePath)){
							unlink($oldProfilePicturePath);
						}
					}
				}
				if($cover_picture != '') {
					$coverPictureName = $customerId."_".time().rand(111,333).".png";
					$coverPicturePath = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/cover_picture/'.$coverPictureName;
					$ifp = fopen( $coverPicturePath, "wb" ); 
					fwrite( $ifp, base64_decode( $cover_picture) ); 
					fclose( $ifp );
					if($customer->getCustomAttribute('cover_picture')){
						$oldCoverPicturePath = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/cover_picture/'.$customer->getCustomAttribute('cover_picture')->getValue();
						if(file_exists($oldCoverPicturePath)){
							unlink($oldCoverPicturePath);
						}
					}
				}
				
				$customerNew = $this->customerFactory->create()->load($customer->getId());
				//$customerNew->setEmail($email);
				$customerNew->setFirstname($first_name);
				$customerNew->setLastname($last_name);
                                $customerNew->setPrivacyStatus($privacy_status);
				//$customerNew->setDob($dob);
				
				if($customerNew->getMobilenumber() == $mobilenumber){				
					$customerNew->setMobilenumber($mobilenumber);
				}
				
				if($profile_picture != '') {
					$customerNew->setProfilePicture($profilePictureName);
				}
				if($cover_picture != '') {
					$customerNew->setCoverPicture($coverPictureName);
				}
				$customerNew->save();
				$response['is_mobile_update'] = false;
				$message = __('Profile has been updated successfully.');
				if($customerNew->getMobilenumber() != $mobilenumber){		
                                    /*check mobile already registered with another account or not*//*check mobile already registered with another account or not*/
                                    self::mobileIsExistOrNot($mobilenumber);
                                    
					/*send otp in mobile code pending*/
					$OTP = Self::sendMobileOtp($mobilenumber);
					
					/*send otp in mobile number*/
					self::sendMessageOTP($mobilenumber,$OTP,$signature);
					
					$response['is_mobile_update'] = true;					
					$message = 'Otp has been sent on your mobile number.';
				}
				
				$response['user_name']   = ucwords($customerNew->getFirstname().' '.$customerNew->getLastname());
				$response['first_name'] 	= $customerNew->getFirstname();
				$response['last_name'] 		= $customerNew->getLastname();
				//$response['dob'] 			= date("m-d-Y", strtotime($customerNew->getDob()));
				$response['email'] 			= $customerNew->getEmail();
				$response['mobilenumber'] 	= ($customerNew->getMobilenumber())? $customerNew->getMobilenumber(): '';				
                                $response['privacy_status'] 	= ($customerNew->getPrivacyStatus())? $customerNew->getPrivacyStatus(): '';
				$response['profile_picture'] = '';
				if($customerNew->getProfilePicture()) {
					$mediaDir = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/';
					if(file_exists($mediaDir.'profile_picture/'.$customerNew->getProfilePicture())){
						$response['profile_picture'] = $this->storeManager->getStore()->getBaseUrl().'media/profile_picture/'.$customerNew->getProfilePicture();
					}
				}
				$response['cover_picture'] = '';
				if($customerNew->getCoverPicture()) {
					$mediaDir = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/';
					if(file_exists($mediaDir.'cover_picture/'.$customerNew->getCoverPicture())){
						$response['cover_picture'] = $this->storeManager->getStore()->getBaseUrl().'media/cover_picture/'.$customerNew->getCoverPicture();
					}
				}
				$responseFlag = true;
			} catch (AlreadyExistsException $e) {
				throw new InputException(
					__('A customer with the same email already exists in an associated website.')
				);
			} 	
		} catch(InputMismatchException $e){
			$message  = $e->getMessage();
		} catch(InvalidEmailOrPasswordException $e){
			$message  = $e->getMessage();
		} catch(LocalizedException $e){
			$message  = $e->getMessage();
		} catch(NoSuchEntityException $e){
			$message  = $e->getMessage();
		} catch(UserLockedException $e){
			$message  = $e->getMessage();
		} catch(\Exception $e){
			$message  = $e->getMessage();
		}
		$response['response'] = $responseFlag;
		$response['message']  = $message;
		echo json_encode($response);
	}
	
	
	public function sendMobileOtp($mobileOrEmail,$resend=''){
		$OTP = mt_rand(100000, 999999);
		//$OTP = 123456;
		
		/*send otp in mobile number*/
		//self::sendMessageOTP($mobileOrEmail,$OTP);
		
		// $msg = "Your Petzzco one time password(OTP) is $OTP. its valid till 30 minutes. Do not share this OTP to anyone for security reasons.";
		$this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
			->get('Magento\Framework\App\ResourceConnection');
			$connection= $this->_resources->getConnection();

			$tableName = $this->_resources->getTableName('register_otp');
			
			//select otp data
			$sql = "select * from " . $tableName . " where email='$mobileOrEmail'";
			$getData = $connection->fetchRow($sql);
                        
			if($getData){
                            /*check otp send limit max 5 otp send within 1 hour*/                            
                            $otp_count = $getData['otp_count']+1;
                            
                            $otp_date = $getData['otp_date'];
                            $current_date = date('Y-m-d H:i:s');
                                                        
                            $different_in_minutes = round(abs(strtotime($otp_date) - strtotime($current_date)) / 60,0);

                            /*re-send otp limit*/
                            if($getData['resend_count']>=3 && $different_in_minutes<=60){
                                $response['response'] = false;
                                $response['message']  = 'You can request for an OTP to be re-sent to you up to maximum of 3 times. Please try after some time.';
                                echo json_encode($response);
                                die;
                            }
                            
                            /*otp request limit*/
                            if($getData['otp_count']>=4 && $different_in_minutes<=60){
                                $response['response'] = false;
                                $response['message']  = 'You can request for an OTP to you up to maximum of 5 times. Please try after some time.';
                                echo json_encode($response);
                                die;
                            }
                            
                            $resend_count = 0;
                            if($resend!=''){
                                $resend_count = $getData['resend_count']+1;
                            }             
                            
                            /*reset count after 60 minut*/
                            if($different_in_minutes>=60){
                                $resend_count = 0;
                                $otp_count = 0;
                            }
                            
                            //update otp
                            $sql = "Update " . $tableName . " Set otp = $OTP , status = 0 , otp_count=$otp_count, resend_count = $resend_count where email = '$mobileOrEmail'";
                            $connection->query($sql);                            
			}else{
                            // insert otp
                            $dataTime = date('Y-m-d H:i:s');
                            $sql = "INSERT INTO " . $tableName . "(email, otp,otp_date) VALUES ('$mobileOrEmail', $OTP,'$dataTime')";
                            $connection->query($sql);							
			}
			return $OTP;
	}
	
	/**
     * @inheritdoc
     */
	public function manageServiceProfile($customerId, $first_name, $last_name, $email, $mobilenumber, $profile_picture = '', $cover_picture = '', $business_name, $business_type, $shop_address, $pin_code, $city, $state, $gstin, $pan, $image_one='', $image_two='', $image_three='',$business_type_id,$latitude,$longitude,$privacy_status,$signature='')
	{
		/*$writer = new \Zend\Log\Writer\Stream( BP . '/var/log/api_log.log');
		$zendLogger = new \Zend\Log\Logger();
		$zendLogger->addWriter($writer);
		$zendLogger->info('hello');*/
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			try {
				$customer = $this->customerRepository->getById($customerId);
			} catch (NoSuchEntityException $e) {
				throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
			}
			
			try {
				if($profile_picture != '') {
					$profilePictureName = $customerId."_".time().".png";
					$profilePicturePath = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/profile_picture/'.$profilePictureName;
					$ifp = fopen( $profilePicturePath, "wb" ); 
					fwrite( $ifp, base64_decode( $profile_picture) ); 
					fclose( $ifp );
					if($customer->getCustomAttribute('profile_picture')){
						$oldProfilePicturePath = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/profile_picture/'.$customer->getCustomAttribute('profile_picture')->getValue();
						if(file_exists($oldProfilePicturePath)){
							unlink($oldProfilePicturePath);
						}
					}
				}
				if($cover_picture != '') {
					$coverPictureName = $customerId."_".time().rand(111,333).".png";
					$coverPicturePath = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/cover_picture/'.$coverPictureName;
					$ifp = fopen( $coverPicturePath, "wb" ); 
					fwrite( $ifp, base64_decode( $cover_picture) ); 
					fclose( $ifp );
					if($customer->getCustomAttribute('cover_picture')){
						$oldCoverPicturePath = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/cover_picture/'.$customer->getCustomAttribute('cover_picture')->getValue();
						if(file_exists($oldCoverPicturePath)){
							unlink($oldCoverPicturePath);
						}
					}
				}
				
				$customerNew = $this->customerFactory->create()->load($customer->getId());
				//$customerNew->setEmail($email);
				$customerNew->setFirstname($first_name);
				$customerNew->setLastname($last_name);
				//$customerNew->setDob($dob);
				
				if($customerNew->getMobilenumber() == $mobilenumber){				
					$customerNew->setMobilenumber($mobilenumber);
				}
                                
				if($profile_picture != '') {
					$customerNew->setProfilePicture($profilePictureName);
				}
				if($cover_picture != '') {
					$customerNew->setCoverPicture($coverPictureName);
				}
				
				$customerNew->setBusinessName($business_name);
				$customerNew->setBusinessType($business_type);
				$customerNew->setBusinessTypeId($business_type_id);
				$customerNew->setLatitude($latitude);
				$customerNew->setLongitude($longitude);
				$customerNew->setShopAddress($shop_address);
				$customerNew->setPinCode($pin_code);
				$customerNew->setCity($city);
				$customerNew->setState($state);
				$customerNew->setGstin($gstin);
				$customerNew->setPan($pan);
                                $customerNew->setPrivacyStatus($privacy_status);
				if($image_one != '') {
					$customerNew->setImageOne($image_one);
				}
				if($image_two != '') {
					$customerNew->setImageTwo($image_two);
				}
				if($image_three != '') {
					$customerNew->setImageThree($image_three);
				}
				
				$customerNew->save();	

				$response['is_mobile_update'] = false;
				$message = __('Profile has been updated successfully.');
				
				if($customerNew->getMobilenumber() != $mobilenumber){
                                        /*check mobile already registered with another account or not*//*check mobile already registered with another account or not*/
                                        self::mobileIsExistOrNot($mobilenumber);
                                    
					/*send otp in mobile code pending*/
					$OTP = Self::sendMobileOtp($mobilenumber);
					
					/*send otp in mobile number*/
					self::sendMessageOTP($mobilenumber,$OTP,$signature);
                                        
					
					$response['is_mobile_update'] = true;					
					$message = 'Otp has been sent on your mobile number.';
				}
                                
				
				$message = __('Profile has been updated successfully.');
				$response['user_name']   = ucwords($customerNew->getFirstname().' '.$customerNew->getLastname());
				$response['first_name'] 	= $customerNew->getFirstname();
				$response['last_name'] 		= $customerNew->getLastname();
				//$response['dob'] 			= date("m-d-Y", strtotime($customerNew->getDob()));
				$response['email'] 			= $customerNew->getEmail();
				$response['mobilenumber'] 	= ($customerNew->getMobilenumber())? $customerNew->getMobilenumber(): '';
				$response['business_name'] 		 = ($customerNew->getBusinessName()) ? $customerNew->getBusinessName() : "";				
				$response['business_type'] 		 = ($customerNew->getBusinessType()) ? $customerNew->getBusinessType() : "";
				$response['shop_address'] 		 = ($customerNew->getShopAddress()) ? $customerNew->getShopAddress() : "";
				$response['pin_code'] 		 = ($customerNew->getPinCode()) ? $customerNew->getPinCode() : "";
				$response['city'] 		 = ($customerNew->getCity()) ? $customerNew->getCity() : "";
				$response['state'] 		 = ($customerNew->getState()) ? $customerNew->getState() : "";
				$response['gstin'] 		 = ($customerNew->getGstin()) ? $customerNew->getGstin() : "";
				$response['pan'] 		 = ($customerNew->getPan()) ? $customerNew->getPan() : "";
                                $response['privacy_status']      = ($customerNew->getPrivacyStatus()) ? $customerNew->getPrivacyStatus() : "Public";
				
				$response['profile_picture'] = '';
				if($customerNew->getProfilePicture()) {
					$mediaDir = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/';
					if(file_exists($mediaDir.'profile_picture/'.$customerNew->getProfilePicture())){
						$response['profile_picture'] = $this->storeManager->getStore()->getBaseUrl().'media/profile_picture/'.$customerNew->getProfilePicture();
					}
				}
				$response['cover_picture'] = '';
				if($customerNew->getCoverPicture()) {
					$mediaDir = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/';
					if(file_exists($mediaDir.'cover_picture/'.$customerNew->getCoverPicture())){
						$response['cover_picture'] = $this->storeManager->getStore()->getBaseUrl().'media/cover_picture/'.$customerNew->getCoverPicture();
					}
				}
				
				$response['image_one'] = '';
				if($customerNew->getImageOne()) {
					$mediaDir = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/';
					if(file_exists($mediaDir.'certificate/'.$customerNew->getImageOne())){
						$response['image_one'] = $this->storeManager->getStore()->getBaseUrl().'media/certificate/'.$customerNew->getImageOne();
					}
				}
				$response['image_two'] = '';
				if($customerNew->getImageTwo()) {
					$mediaDir = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/';
					if(file_exists($mediaDir.'certificate/'.$customerNew->getImageTwo())){
						$response['image_two'] = $this->storeManager->getStore()->getBaseUrl().'media/certificate/'.$customerNew->getImageTwo();
					}
				}
				$response['image_three'] = '';
				if($customerNew->getImageThree()) {
					$mediaDir = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)->getAbsolutePath().'pub/media/';
					if(file_exists($mediaDir.'certificate/'.$customerNew->getImageThree())){
						$response['image_three'] = $this->storeManager->getStore()->getBaseUrl().'media/certificate/'.$customerNew->getImageThree();
					}
				}
				
				
				$responseFlag = true;
			} catch (AlreadyExistsException $e) {
				throw new InputException(
					__('A customer with the same email already exists in an associated website.')
				);
			} 	
		} catch(InputMismatchException $e){
			$message  = $e->getMessage();
		} catch(InvalidEmailOrPasswordException $e){
			$message  = $e->getMessage();
		} catch(LocalizedException $e){
			$message  = $e->getMessage();
		} catch(NoSuchEntityException $e){
			$message  = $e->getMessage();
		} catch(UserLockedException $e){
			$message  = $e->getMessage();
		} catch(\Exception $e){
			$message  = $e->getMessage();
		}
		$response['response'] = $responseFlag;
		$response['message']  = $message;
		echo json_encode($response);
	}
	
	
	/**
     * @inheritdoc
     */
	public function groomingList($latitude,$longitude)
	{		
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
						
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			echo $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->getCollection()			
			 ->addAttributeToSelect("*")
                ->addAttributeToFilter("user_type", array("eq" => "Service Provider"))
				->addAttributeToFilter("latitude", array("eq" => ""))
				->addAttributeToFilter("longitude", array("eq" => ""))
				->addAttributeToFilter("image_one", array("eq" => ""))
				->addAttributeToFilter("shop_address", array("eq" => ""))
				->addAttributeToFilter("city", array("eq" => ""))
				->addAttributeToFilter("state", array("eq" => ""))
				->addAttributeToFilter("pin_code", array("eq" => ""))
				->addAttributeToFilter("mobilenumber", array("eq" => ""))
				
				->getSelect()
                ;die;
			foreach($customerObj as $customerObjdata )
			{
				echo "<pre/>";
				print_r($customerObjdata ->getData());
			}
			die;
			
		} catch(InputMismatchException $e){
			$message  = $e->getMessage();
		} catch(InvalidEmailOrPasswordException $e){
			$message  = $e->getMessage();
		} catch(LocalizedException $e){
			$message  = $e->getMessage();
		} catch(NoSuchEntityException $e){
			$message  = $e->getMessage();
		} catch(UserLockedException $e){
			$message  = $e->getMessage();
		} catch(\Exception $e){
			$message  = $e->getMessage();
		}
		$response['response'] = $responseFlag;
		$response['message']  = $message;
		echo json_encode($response);
	}
	
	
	
	
	/**
     * @inheritdoc
     */
    public function getCustomerAddress($customerId)
    {
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			$customer = $this->customerFactory->create()->load($customerId);
			$customerAddress = array();
			if($customer->getAddresses() != null)
			{
				foreach($customer->getAddresses() as $address){
					$default_billing = ($customer->getDefault_billing() == $address->getId())? true : false;
					$default_shipping = ($customer->getDefault_shipping() == $address->getId())? true : false;
					$customerAddress[] = array('address_id'      => $address->getId(),
											   'customer_id'     => $address->getCustomer_id(),
											   'firstname'       => $address->getFirstname(),
											   'lastname'        => $address->getLastname(),
											   'telephone'       => $address->getTelephone(),
											   'company'         => $address->getCompany(),
											   'street'          => $address->getStreet(),
											   'postcode'        => $address->getPostcode(),
											   'city'            => $address->getCity(),
											   'region'          => $address->getRegion(),
											   'region_id'       => $address->getRegion_id(),
											   'country_id'      => $address->getCountry_id(),
											   'default_billing' => $default_billing,
											   'default_shipping'=> $default_shipping
											   );
				}
			}
			$response['address'] = $customerAddress;
			$message = 'Successfully';
			$responseFlag = true;
		} catch(LocalizedException $e){
			$message  = $e->getMessage();
		} catch(NoSuchEntityException $e){
			$message  = $e->getMessage();
		} catch(UserLockedException $e){
			$message  = $e->getMessage();
		} catch(\Exception $e){
			$message  = $e->getMessage();
		}
		$response['response'] = $responseFlag;
		$response['message']  = $message;
		echo json_encode($response);
	}
    
    /**
     * @inheritdoc
     */
    public function addUpdateCustomerAddress($customerId, $address)
    {
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			$objectManager = ObjectManager::getInstance();
			if($address['address_id']) {
				$addressFactory = $objectManager->get('\Magento\Customer\Model\Address');
				$addressNew = $addressFactory->load($address['address_id']);
				$addressNew->setFirstname($address['firstname']);
				$addressNew->setLastname($address['lastname']);
				$addressNew->setCompany($address['company']);
				$addressNew->setTelephone($address['telephone']);
				$addressNew->setStreet($address['street']);
				$addressNew->setPostcode($address['postcode']);
				$addressNew->setCity($address['city']);
				$addressNew->setRegion_id($address['region_id']);
				$addressNew->setRegion($address['region']);
				$addressNew->setCountry_id($address['country_id']);
				$addressNew->save();
				$message = 'Address has changed successfully.';
			} else {
				$addressFactory = $objectManager->get('\Magento\Customer\Model\AddressFactory');
				$addressNew = $addressFactory->create();
				$addressNew->setFirstname($address['firstname']);
				$addressNew->setLastname($address['lastname']);
				$addressNew->setCompany($address['company']);
				$addressNew->setTelephone($address['telephone']);
				$addressNew->setStreet($address['street']);
				$addressNew->setPostcode($address['postcode']);
				$addressNew->setCity($address['city']);
				$addressNew->setRegion_id($address['region_id']);
				$addressNew->setRegion($address['region']);
				$addressNew->setCountryId($address['country_id']);
				$addressNew->setCustomerId($customerId);
				$addressNew->save();
				$message = 'Address has added successfully.';
			}
			$responseFlag = true;
		} catch(LocalizedException $e){
			$message  = $e->getMessage();
		} catch(NoSuchEntityException $e){
			$message  = $e->getMessage();
		} catch(UserLockedException $e){
			$message  = $e->getMessage();
		} catch(\Exception $e){
			$message  = $e->getMessage();
		}
		$response['response'] = $responseFlag;
		$response['message']  = $message;
		echo json_encode($response);
	}
    
    /**
     * @inheritdoc
     */
    public function deleteDefaultCustomerAddress($customerId, $address_id, $flag)
    {
		$responseFlag = false;
		$message = '';
		$response = array();
		try{
			if($flag == 'delete') {
				$this->addressRepository->deleteById($address_id);
				$message = 'Address has removed successfully.';
			} else {
				$objectManager = ObjectManager::getInstance();
				$addressFactory = $objectManager->get('\Magento\Customer\Model\AddressFactory');
				$address = $addressFactory->create()->load($address_id);
				if ($flag == 'billing') {
					$address->setIsDefaultBilling('1');
				} elseif ($flag == 'shipping') {
					$address->setIsDefaultShipping('1');
				} else {
					$address->setIsDefaultShipping('1');
					$address->setIsDefaultBilling('1');
				}
				$address->save();
				$message = 'Address has changed successfully.';
			}
			$responseFlag = true;
		} catch(LocalizedException $e){
			$message  = $e->getMessage();
		} catch(NoSuchEntityException $e){
			$message  = $e->getMessage();
		} catch(UserLockedException $e){
			$message  = $e->getMessage();
		} catch(\Exception $e){
			$message  = $e->getMessage();
		}
		$response['response'] = $responseFlag;
		$response['message']  = $message;
		echo json_encode($response);
	}
	
	/**
     * @inheritdoc
     */
	
	
	
    /**
     * Get authentication
     *
     * @return AuthenticationInterface
     */
    private function getAuthentication()
    {
        if (!($this->authentication instanceof AuthenticationInterface)) {
            return \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Customer\Model\AuthenticationInterface::class
            );
        } else {
            return $this->authentication;
        }
    }

    /**
     * @inheritdoc
     */
    public function resendConfirmation($email, $websiteId = null, $redirectUrl = '')
    {
        $customer = $this->customerRepository->get($email, $websiteId);
        if (!$customer->getConfirmation()) {
            throw new InvalidTransitionException(__('No confirmation needed.'));
        }

        try {
            $this->getEmailNotification()->newAccount(
                $customer,
                self::NEW_ACCOUNT_EMAIL_CONFIRMATION,
                $redirectUrl,
                $this->storeManager->getStore()->getId()
            );
        } catch (MailException $e) {
            // If we are not able to send a new account email, this should be ignored
            $this->logger->critical($e);
        }
    }

    /**
     * @inheritdoc
     */
    public function activate($email, $confirmationKey)
    {
        $customer = $this->customerRepository->get($email);
        return $this->activateCustomer($customer, $confirmationKey);
    }

    /**
     * @inheritdoc
     */
    public function activateById($customerId, $confirmationKey)
    {
        $customer = $this->customerRepository->getById($customerId);
        return $this->activateCustomer($customer, $confirmationKey);
    }

    /**
     * Activate a customer account using a key that was sent in a confirmation email.
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @param string $confirmationKey
     * @return \Magento\Customer\Api\Data\CustomerInterface
     * @throws InputException
     * @throws InputMismatchException
     * @throws InvalidTransitionException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    private function activateCustomer($customer, $confirmationKey)
    {
        // check if customer is inactive
        if (!$customer->getConfirmation()) {
            throw new InvalidTransitionException(__('Account already active'));
        }

        if ($customer->getConfirmation() !== $confirmationKey) {
            throw new InputMismatchException(__('Invalid confirmation token'));
        }

        $customer->setConfirmation(null);
        // No need to validate customer and customer address while activating customer
        $this->setIgnoreValidationFlag($customer);
        $this->customerRepository->save($customer);
        $this->getEmailNotification()->newAccount($customer, 'confirmed', '', $this->storeManager->getStore()->getId());
        return $customer;
    }

    /**
     * @inheritdoc
     */
    public function authenticate($username, $password)
    {
        try {
            $customer = $this->customerRepository->get($username);
        } catch (NoSuchEntityException $e) {
            throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
        }

        $customerId = $customer->getId();
        if ($this->getAuthentication()->isLocked($customerId)) {
            throw new UserLockedException(__('The account is locked.'));
        }
        try {
            $this->getAuthentication()->authenticate($customerId, $password);
        } catch (InvalidEmailOrPasswordException $e) {
            throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
        }
        if ($customer->getConfirmation() && $this->isConfirmationRequired($customer)) {
            throw new EmailNotConfirmedException(__('This account is not confirmed.'));
        }

        $customerModel = $this->customerFactory->create()->updateData($customer);
        $this->eventManager->dispatch(
            'customer_customer_authenticated',
            ['model' => $customerModel, 'password' => $password]
        );

        $this->eventManager->dispatch('customer_data_object_login', ['customer' => $customer]);

        return $customer;
    }

    /**
     * @inheritdoc
     */
    public function validateResetPasswordLinkToken($customerId, $resetPasswordLinkToken)
    {
        $this->validateResetPasswordToken($customerId, $resetPasswordLinkToken);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function initiatePasswordReset($email, $template, $websiteId = null)
    {
        if ($websiteId === null) {
            $websiteId = $this->storeManager->getStore()->getWebsiteId();
        }
        // load customer by email
        $customer = $this->customerRepository->get($email, $websiteId);

        // No need to validate customer address while saving customer reset password token
        $this->disableAddressValidation($customer);

        $newPasswordToken = $this->mathRandom->getUniqueHash();
        $this->changeResetPasswordLinkToken($customer, $newPasswordToken);

        try {
            switch ($template) {
                case AccountManagement::EMAIL_REMINDER:
                    $this->getEmailNotification()->passwordReminder($customer);
                    break;
                case AccountManagement::EMAIL_RESET:
                    $this->getEmailNotification()->passwordResetConfirmation($customer);
                    break;
                default:
                    throw new InputException(__(
                        'Invalid value of "%value" provided for the %fieldName field. '.
                        'Possible values: %template1 or %template2.',
                        [
                            'value' => $template,
                            'fieldName' => 'template',
                            'template1' => AccountManagement::EMAIL_REMINDER,
                            'template2' => AccountManagement::EMAIL_RESET
                        ]
                    ));
            }

            return true;
        } catch (MailException $e) {
            // If we are not able to send a reset password email, this should be ignored
            $this->logger->critical($e);
        }

        return false;
    }

    /**
     * Match a customer by their RP token.
     *
     * @param string $rpToken
     * @return CustomerInterface
     * @throws ExpiredException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    private function matchCustomerByRpToken(string $rpToken): CustomerInterface
    {

        $this->searchCriteriaBuilder->addFilter(
            'rp_token',
            $rpToken
        );
        $this->searchCriteriaBuilder->setPageSize(1);
        $found = $this->customerRepository->getList(
            $this->searchCriteriaBuilder->create()
        );

        if ($found->getTotalCount() > 1) {
            //Failed to generated unique RP token
            throw new ExpiredException(
                new Phrase('Reset password token expired.')
            );
        }
        if ($found->getTotalCount() === 0) {
            //Customer with such token not found.
            throw NoSuchEntityException::singleField(
                'rp_token',
                $rpToken
            );
        }

        //Unique customer found.
        return $found->getItems()[0];
    }

    /**
     * @inheritdoc
     */
    public function resetPassword($email, $resetToken, $newPassword)
    {
        if (!$email) {
            $customer = $this->matchCustomerByRpToken($resetToken);
            $email = $customer->getEmail();
        } else {
            $customer = $this->customerRepository->get($email);
        }

        // No need to validate customer and customer address while saving customer reset password token
        $this->disableAddressValidation($customer);
        $this->setIgnoreValidationFlag($customer);

        //Validate Token and new password strength
        $this->validateResetPasswordToken($customer->getId(), $resetToken);
        $this->credentialsValidator->checkPasswordDifferentFromEmail(
            $email,
            $newPassword
        );
        //$this->checkPasswordStrength($newPassword);
        //Update secure data
        $customerSecure = $this->customerRegistry->retrieveSecureData($customer->getId());
        $customerSecure->setRpToken(null);
        $customerSecure->setRpTokenCreatedAt(null);
        $customerSecure->setPasswordHash($this->createPasswordHash($newPassword));
        $this->getAuthentication()->unlock($customer->getId());
        $this->destroyCustomerSessions($customer->getId());
        if ($this->sessionManager->isSessionExists() && !headers_sent()) {
            //delete old session and move data to the new session
            //use this instead of $this->sessionManager->regenerateId because last one doesn't delete old session
            // phpcs:ignore Magento2.Functions.DiscouragedFunction
            session_regenerate_id(true);
        }
        $this->customerRepository->save($customer);

        return true;
    }

    /**
     * Make sure that password complies with minimum security requirements.
     *
     * @param string $password
     * @return void
     * @throws InputException
     */
    protected function checkPasswordStrength($password)
    {
        $length = $this->stringHelper->strlen($password);
        if ($length > self::MAX_PASSWORD_LENGTH) {
            throw new InputException(
                __(
                    'Please enter a password with at most %1 characters.',
                    self::MAX_PASSWORD_LENGTH
                )
            );
        }
        $configMinPasswordLength = $this->getMinPasswordLength();
        if ($length < $configMinPasswordLength) {
            throw new InputException(
                __(
                    'Please enter a password with at least %1 characters.',
                    $configMinPasswordLength
                )
            );
        }
        if ($this->stringHelper->strlen(trim($password)) != $length) {
            throw new InputException(__('The password can\'t begin or end with a space.'));
        }

        $requiredCharactersCheck = $this->makeRequiredCharactersCheck($password);
        if ($requiredCharactersCheck !== 0) {
            throw new InputException(
                __(
                    'Minimum of different classes of characters in password is %1.' .
                    ' Classes of characters: Lower Case, Upper Case, Digits, Special Characters.',
                    $requiredCharactersCheck
                )
            );
        }
    }

    /**
     * Check password for presence of required character sets
     *
     * @param string $password
     * @return int
     */
    protected function makeRequiredCharactersCheck($password)
    {
        $counter = 0;
        $requiredNumber = $this->scopeConfig->getValue(self::XML_PATH_REQUIRED_CHARACTER_CLASSES_NUMBER);
        $return = 0;

        if (preg_match('/[0-9]+/', $password)) {
            $counter++;
        }
        if (preg_match('/[A-Z]+/', $password)) {
            $counter++;
        }
        if (preg_match('/[a-z]+/', $password)) {
            $counter++;
        }
        if (preg_match('/[^a-zA-Z0-9]+/', $password)) {
            $counter++;
        }

        if ($counter < $requiredNumber) {
            $return = $requiredNumber;
        }

        return $return;
    }

    /**
     * Retrieve minimum password length
     *
     * @return int
     */
    protected function getMinPasswordLength()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_MINIMUM_PASSWORD_LENGTH);
    }

    /**
     * @inheritdoc
     */
    public function getConfirmationStatus($customerId)
    {
        // load customer by id
        $customer = $this->customerRepository->getById($customerId);
        if ($this->isConfirmationRequired($customer)) {
            if (!$customer->getConfirmation()) {
                return self::ACCOUNT_CONFIRMED;
            }
            return self::ACCOUNT_CONFIRMATION_REQUIRED;
        }
        return self::ACCOUNT_CONFIRMATION_NOT_REQUIRED;
    }

    /**
     * @inheritdoc
     */
    public function createAccount(CustomerInterface $customer, $password = null, $redirectUrl = '', $extensions = [])
    {
		if ($password !== null) {
            //$this->checkPasswordStrength($password);
            $customerEmail = $customer->getEmail();
            try {
                $this->credentialsValidator->checkPasswordDifferentFromEmail($customerEmail, $password);
            } catch (InputException $e) {
                throw new LocalizedException(__('Password cannot be the same as email address.'));
            }
            $hash = $this->createPasswordHash($password);
        } else {
            $hash = null;
        }

        return $this->createAccountWithPasswordHash($customer, $hash, $redirectUrl, $extensions);
    }

    /**
     * @inheritdoc
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function createAccountWithPasswordHash(
        CustomerInterface $customer,
        $hash,
        $redirectUrl = '',
        $extensions = []
    ) {
        // This logic allows an existing customer to be added to a different store.  No new account is created.
        // The plan is to move this logic into a new method called something like 'registerAccountWithStore'
        if ($customer->getId()) {
            $customer = $this->customerRepository->get($customer->getEmail());
            $websiteId = $customer->getWebsiteId();

            if ($this->isCustomerInStore($websiteId, $customer->getStoreId())) {
                throw new InputException(__('This customer already exists in this store.'));
            }
            // Existing password hash will be used from secured customer data registry when saving customer
        }

        // Make sure we have a storeId to associate this customer with.
        if (!$customer->getStoreId()) {
            if ($customer->getWebsiteId()) {
                $storeId = $this->storeManager->getWebsite($customer->getWebsiteId())->getDefaultStore()->getId();
            } else {
                $storeId = $this->storeManager->getStore()->getId();
            }
            $customer->setStoreId($storeId);
        }

        // Associate website_id with customer
        if (!$customer->getWebsiteId()) {
            $websiteId = $this->storeManager->getStore($customer->getStoreId())->getWebsiteId();
            $customer->setWebsiteId($websiteId);
        }

        // Update 'created_in' value with actual store name
        if ($customer->getId() === null) {
            $storeName = $this->storeManager->getStore($customer->getStoreId())->getName();
            $customer->setCreatedIn($storeName);
        }

        $customerAddresses = $customer->getAddresses() ?: [];
        $customer->setAddresses(null);
        try {
            // If customer exists existing hash will be used by Repository
            $customer = $this->customerRepository->save($customer, $hash);
        } catch (AlreadyExistsException $e) {
            throw new InputMismatchException(
                __('A customer with the same email already exists in an associated website.')
            );
        } catch (LocalizedException $e) {
            throw $e;
        }
        try {
            foreach ($customerAddresses as $address) {
                if (!$this->isAddressAllowedForWebsite($address, $customer->getStoreId())) {
                    continue;
                }
                if ($address->getId()) {
                    $newAddress = clone $address;
                    $newAddress->setId(null);
                    $newAddress->setCustomerId($customer->getId());
                    $this->addressRepository->save($newAddress);
                } else {
                    $address->setCustomerId($customer->getId());
                    $this->addressRepository->save($address);
                }
            }
            $this->customerRegistry->remove($customer->getId());
        } catch (InputException $e) {
            $this->customerRepository->delete($customer);
            throw $e;
        }
        $customer = $this->customerRepository->getById($customer->getId());
        $newLinkToken = $this->mathRandom->getUniqueHash();
        $this->changeResetPasswordLinkToken($customer, $newLinkToken);
        $this->sendEmailConfirmation($customer, $redirectUrl, $extensions);

        return $customer;
    }

    /**
     * @inheritdoc
     */
    public function getDefaultBillingAddress($customerId)
    {
        $customer = $this->customerRepository->getById($customerId);
        return $this->getAddressById($customer, $customer->getDefaultBilling());
    }

    /**
     * @inheritdoc
     */
    public function getDefaultShippingAddress($customerId)
    {
        $customer = $this->customerRepository->getById($customerId);
        return $this->getAddressById($customer, $customer->getDefaultShipping());
    }

    /**
     * Send either confirmation or welcome email after an account creation
     *
     * @param CustomerInterface $customer
     * @param string $redirectUrl
     * @param array $extensions
     * @return void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    protected function sendEmailConfirmation(CustomerInterface $customer, $redirectUrl, $extensions = [])
    {
        try {
            $hash = $this->customerRegistry->retrieveSecureData($customer->getId())->getPasswordHash();
            $templateType = self::NEW_ACCOUNT_EMAIL_REGISTERED;
            if ($this->isConfirmationRequired($customer) && $hash != '') {
                $templateType = self::NEW_ACCOUNT_EMAIL_CONFIRMATION;
            } elseif ($hash == '') {
                $templateType = self::NEW_ACCOUNT_EMAIL_REGISTERED_NO_PASSWORD;
            }
            $this->getEmailNotification()->newAccount(
                $customer,
                $templateType,
                $redirectUrl,
                $customer->getStoreId(),
                null,
                $extensions
            );
            $customer->setConfirmation(null);
        } catch (MailException $e) {
            // If we are not able to send a new account email, this should be ignored
            $this->logger->critical($e);
        } catch (\UnexpectedValueException $e) {
            $this->logger->error($e);
        }
    }

    /**
     * @inheritdoc
     */
    public function changePassword($email, $currentPassword, $newPassword)
    {
        try {
            $customer = $this->customerRepository->get($email);
        } catch (NoSuchEntityException $e) {
            throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
        }
        return $this->changePasswordForCustomer($customer, $currentPassword, $newPassword);
    }

    /**
     * @inheritdoc
     */
    public function changePasswordById($customerId, $currentPassword, $newPassword)
    {
        try {
            $customer = $this->customerRepository->getById($customerId);
        } catch (NoSuchEntityException $e) {
            throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
        }
        return $this->changePasswordForCustomer($customer, $currentPassword, $newPassword);
    }

    /**
     * Change customer password.
     *
     * @param CustomerInterface $customer
     * @param string $currentPassword
     * @param string $newPassword
     * @return bool true on success
     * @throws InputException
     * @throws InputMismatchException
     * @throws InvalidEmailOrPasswordException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws UserLockedException
     */
    private function changePasswordForCustomer($customer, $currentPassword, $newPassword)
    {
        try {
            $this->getAuthentication()->authenticate($customer->getId(), $currentPassword);
        } catch (InvalidEmailOrPasswordException $e) {
            throw new InvalidEmailOrPasswordException(__('The password doesn\'t match this account.'));
        }
        $customerEmail = $customer->getEmail();
        $this->credentialsValidator->checkPasswordDifferentFromEmail($customerEmail, $newPassword);
        $customerSecure = $this->customerRegistry->retrieveSecureData($customer->getId());
        $customerSecure->setRpToken(null);
        $customerSecure->setRpTokenCreatedAt(null);
        //$this->checkPasswordStrength($newPassword);
        $customerSecure->setPasswordHash($this->createPasswordHash($newPassword));
        $this->destroyCustomerSessions($customer->getId());
        $this->disableAddressValidation($customer);
        $this->customerRepository->save($customer);

        return true;
    }

    /**
     * Create a hash for the given password
     *
     * @param string $password
     * @return string
     */
    protected function createPasswordHash($password)
    {
        return $this->encryptor->getHash($password, true);
    }

    /**
     * Get EAV validator
     *
     * @return Backend
     */
    private function getEavValidator()
    {
        if ($this->eavValidator === null) {
            $this->eavValidator = ObjectManager::getInstance()->get(Backend::class);
        }
        return $this->eavValidator;
    }

    /**
     * @inheritdoc
     */
    public function validate(CustomerInterface $customer)
    {
        $validationResults = $this->validationResultsDataFactory->create();

        $oldAddresses = $customer->getAddresses();
        $customerModel = $this->customerFactory->create()->updateData(
            $customer->setAddresses([])
        );
        $customer->setAddresses($oldAddresses);

        $result = $this->getEavValidator()->isValid($customerModel);
        if ($result === false && is_array($this->getEavValidator()->getMessages())) {
            return $validationResults->setIsValid(false)->setMessages(
                call_user_func_array(
                    'array_merge',
                    $this->getEavValidator()->getMessages()
                )
            );
        }
        return $validationResults->setIsValid(true)->setMessages([]);
    }

    /**
     * @inheritdoc
     */
    public function isEmailAvailable($customerEmail, $websiteId = null)
    {
        try {
            if ($websiteId === null) {
                $websiteId = $this->storeManager->getStore()->getWebsiteId();
            }
            $this->customerRepository->get($customerEmail, $websiteId);
            return false;
        } catch (NoSuchEntityException $e) {
            return true;
        }
    }

    /**
     * @inheritdoc
     */
    public function isCustomerInStore($customerWebsiteId, $storeId)
    {
        $ids = [];
        if ((bool)$this->configShare->isWebsiteScope()) {
            $ids = $this->storeManager->getWebsite($customerWebsiteId)->getStoreIds();
        } else {
            foreach ($this->storeManager->getStores() as $store) {
                $ids[] = $store->getId();
            }
        }

        return in_array($storeId, $ids);
    }

    /**
     * Validate the Reset Password Token for a customer.
     *
     * @param int $customerId
     * @param string $resetPasswordLinkToken
     * @return bool
     * @throws ExpiredException
     * @throws InputException
     * @throws InputMismatchException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    private function validateResetPasswordToken($customerId, $resetPasswordLinkToken)
    {
        if (empty($customerId) || $customerId < 0) {
            //Looking for the customer.
            $customerId = $this->matchCustomerByRpToken($resetPasswordLinkToken)
                ->getId();
        }
        if (!is_string($resetPasswordLinkToken) || empty($resetPasswordLinkToken)) {
            $params = ['fieldName' => 'resetPasswordLinkToken'];
            throw new InputException(__('%fieldName is a required field.', $params));
        }

        $customerSecureData = $this->customerRegistry->retrieveSecureData($customerId);
        $rpToken = $customerSecureData->getRpToken();
        $rpTokenCreatedAt = $customerSecureData->getRpTokenCreatedAt();

        if (!Security::compareStrings($rpToken, $resetPasswordLinkToken)) {
            throw new InputMismatchException(__('Reset password token mismatch.'));
        } elseif ($this->isResetPasswordLinkTokenExpired($rpToken, $rpTokenCreatedAt)) {
            throw new ExpiredException(__('Reset password token expired.'));
        }

        return true;
    }

    /**
     * Check if customer can be deleted.
     *
     * @param int $customerId
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException If group is not found
     * @throws LocalizedException
     */
    public function isReadonly($customerId)
    {
        $customer = $this->customerRegistry->retrieveSecureData($customerId);
        return !$customer->getDeleteable();
    }

    /**
     * Send email with new account related information
     *
     * @param CustomerInterface $customer
     * @param string $type
     * @param string $backUrl
     * @param string $storeId
     * @param string $sendemailStoreId
     * @return $this
     * @throws LocalizedException
     * @deprecated 100.1.0
     */
    protected function sendNewAccountEmail(
        $customer,
        $type = self::NEW_ACCOUNT_EMAIL_REGISTERED,
        $backUrl = '',
        $storeId = '0',
        $sendemailStoreId = null
    ) {
        $types = $this->getTemplateTypes();

        if (!isset($types[$type])) {
            throw new LocalizedException(__('Please correct the transactional account email type.'));
        }

        if (!$storeId) {
            $storeId = $this->getWebsiteStoreId($customer, $sendemailStoreId);
        }

        $store = $this->storeManager->getStore($customer->getStoreId());

        $customerEmailData = $this->getFullCustomerObject($customer);

        $this->sendEmailTemplate(
            $customer,
            $types[$type],
            self::XML_PATH_REGISTER_EMAIL_IDENTITY,
            ['customer' => $customerEmailData, 'back_url' => $backUrl, 'store' => $store],
            $storeId
        );

        return $this;
    }

    /**
     * Send email to customer when his password is reset
     *
     * @param CustomerInterface $customer
     * @return $this
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @deprecated 100.1.0
     */
    protected function sendPasswordResetNotificationEmail($customer)
    {
        return $this->sendPasswordResetConfirmationEmail($customer);
    }

    /**
     * Get either first store ID from a set website or the provided as default
     *
     * @param CustomerInterface $customer
     * @param int|string|null $defaultStoreId
     * @return int
     * @throws LocalizedException
     * @deprecated 100.1.0
     */
    protected function getWebsiteStoreId($customer, $defaultStoreId = null)
    {
        if ($customer->getWebsiteId() != 0 && empty($defaultStoreId)) {
            $storeIds = $this->storeManager->getWebsite($customer->getWebsiteId())->getStoreIds();
            reset($storeIds);
            $defaultStoreId = current($storeIds);
        }
        return $defaultStoreId;
    }

    /**
     * Get email template types
     *
     * @return array
     * @deprecated 100.1.0
     */
    protected function getTemplateTypes()
    {
        /**
         * self::NEW_ACCOUNT_EMAIL_REGISTERED               welcome email, when confirmation is disabled
         *                                                  and password is set
         * self::NEW_ACCOUNT_EMAIL_REGISTERED_NO_PASSWORD   welcome email, when confirmation is disabled
         *                                                  and password is not set
         * self::NEW_ACCOUNT_EMAIL_CONFIRMED                welcome email, when confirmation is enabled
         *                                                  and password is set
         * self::NEW_ACCOUNT_EMAIL_CONFIRMATION             email with confirmation link
         */
        $types = [
            self::NEW_ACCOUNT_EMAIL_REGISTERED             => self::XML_PATH_REGISTER_EMAIL_TEMPLATE,
            self::NEW_ACCOUNT_EMAIL_REGISTERED_NO_PASSWORD => self::XML_PATH_REGISTER_NO_PASSWORD_EMAIL_TEMPLATE,
            self::NEW_ACCOUNT_EMAIL_CONFIRMED              => self::XML_PATH_CONFIRMED_EMAIL_TEMPLATE,
            self::NEW_ACCOUNT_EMAIL_CONFIRMATION           => self::XML_PATH_CONFIRM_EMAIL_TEMPLATE,
        ];
        return $types;
    }

    /**
     * Send corresponding email template
     *
     * @param CustomerInterface $customer
     * @param string $template configuration path of email template
     * @param string $sender configuration path of email identity
     * @param array $templateParams
     * @param int|null $storeId
     * @param string $email
     * @return $this
     * @throws MailException
     * @deprecated 100.1.0
     */
    protected function sendEmailTemplate(
        $customer,
        $template,
        $sender,
        $templateParams = [],
        $storeId = null,
        $email = null
    ) {
        $templateId = $this->scopeConfig->getValue($template, ScopeInterface::SCOPE_STORE, $storeId);
        if ($email === null) {
            $email = $customer->getEmail();
        }

        $transport = $this->transportBuilder->setTemplateIdentifier($templateId)->setTemplateOptions(
            ['area' => Area::AREA_FRONTEND, 'store' => $storeId]
        )->setTemplateVars($templateParams)->setFrom(
            $this->scopeConfig->getValue($sender, ScopeInterface::SCOPE_STORE, $storeId)
        )->addTo($email, $this->customerViewHelper->getCustomerName($customer))->getTransport();

        $transport->sendMessage();

        return $this;
    }

    /**
     * Check if accounts confirmation is required in config
     *
     * @param CustomerInterface $customer
     * @return bool
     * @deprecated
     * @see AccountConfirmation::isConfirmationRequired
     */
    protected function isConfirmationRequired($customer)
    {
        return $this->accountConfirmation->isConfirmationRequired(
            $customer->getWebsiteId(),
            $customer->getId(),
            $customer->getEmail()
        );
    }

    /**
     * Check whether confirmation may be skipped when registering using certain email address
     *
     * @param CustomerInterface $customer
     * @return bool
     * @deprecated
     * @see AccountConfirmation::isConfirmationRequired
     */
    protected function canSkipConfirmation($customer)
    {
        if (!$customer->getId()) {
            return false;
        }

        /* If an email was used to start the registration process and it is the same email as the one
           used to register, then this can skip confirmation.
           */
        $skipConfirmationIfEmail = $this->registry->registry("skip_confirmation_if_email");
        if (!$skipConfirmationIfEmail) {
            return false;
        }

        return strtolower($skipConfirmationIfEmail) === strtolower($customer->getEmail());
    }

    /**
     * Check if rpToken is expired
     *
     * @param string $rpToken
     * @param string $rpTokenCreatedAt
     * @return bool
     */
    public function isResetPasswordLinkTokenExpired($rpToken, $rpTokenCreatedAt)
    {
        if (empty($rpToken) || empty($rpTokenCreatedAt)) {
            return true;
        }

        $expirationPeriod = $this->customerModel->getResetPasswordLinkExpirationPeriod();

        $currentTimestamp = $this->dateTimeFactory->create()->getTimestamp();
        $tokenTimestamp = $this->dateTimeFactory->create($rpTokenCreatedAt)->getTimestamp();
        if ($tokenTimestamp > $currentTimestamp) {
            return true;
        }

        $hourDifference = floor(($currentTimestamp - $tokenTimestamp) / (60 * 60));
        if ($hourDifference >= $expirationPeriod) {
            return true;
        }

        return false;
    }

    /**
     * Set a new reset password link token.
     *
     * @param CustomerInterface $customer
     * @param string $passwordLinkToken
     * @return bool
     * @throws InputException
     * @throws InputMismatchException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function changeResetPasswordLinkToken($customer, $passwordLinkToken)
    {
        if (!is_string($passwordLinkToken) || empty($passwordLinkToken)) {
            throw new InputException(
                __(
                    'Invalid value of "%value" provided for the %fieldName field.',
                    ['value' => $passwordLinkToken, 'fieldName' => 'password reset token']
                )
            );
        }
        if (is_string($passwordLinkToken) && !empty($passwordLinkToken)) {
            $customerSecure = $this->customerRegistry->retrieveSecureData($customer->getId());
            $customerSecure->setRpToken($passwordLinkToken);
            $customerSecure->setRpTokenCreatedAt(
                $this->dateTimeFactory->create()->format(DateTime::DATETIME_PHP_FORMAT)
            );
            $this->setIgnoreValidationFlag($customer);
            $this->customerRepository->save($customer);
        }

        return true;
    }

    /**
     * Send email with new customer password
     *
     * @param CustomerInterface $customer
     * @return $this
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @deprecated 100.1.0
     */
    public function sendPasswordReminderEmail($customer)
    {
        $storeId = $this->storeManager->getStore()->getId();
        if (!$storeId) {
            $storeId = $this->getWebsiteStoreId($customer);
        }

        $customerEmailData = $this->getFullCustomerObject($customer);

        $this->sendEmailTemplate(
            $customer,
            self::XML_PATH_REMIND_EMAIL_TEMPLATE,
            self::XML_PATH_FORGOT_EMAIL_IDENTITY,
            ['customer' => $customerEmailData, 'store' => $this->storeManager->getStore($storeId)],
            $storeId
        );

        return $this;
    }

    /**
     * Send email with reset password confirmation link
     *
     * @param CustomerInterface $customer
     * @return $this
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @deprecated 100.1.0
     */
    public function sendPasswordResetConfirmationEmail($customer)
    {
        $storeId = $this->storeManager->getStore()->getId();
        if (!$storeId) {
            $storeId = $this->getWebsiteStoreId($customer);
        }

        $customerEmailData = $this->getFullCustomerObject($customer);

        $this->sendEmailTemplate(
            $customer,
            self::XML_PATH_FORGOT_EMAIL_TEMPLATE,
            self::XML_PATH_FORGOT_EMAIL_IDENTITY,
            ['customer' => $customerEmailData, 'store' => $this->storeManager->getStore($storeId)],
            $storeId
        );

        return $this;
    }

    /**
     * Get address by id
     *
     * @param CustomerInterface $customer
     * @param int $addressId
     * @return AddressInterface|null
     */
    protected function getAddressById(CustomerInterface $customer, $addressId)
    {
        foreach ($customer->getAddresses() as $address) {
            if ($address->getId() == $addressId) {
                return $address;
            }
        }
        return null;
    }

    /**
     * Create an object with data merged from Customer and CustomerSecure
     *
     * @param CustomerInterface $customer
     * @return Data\CustomerSecure
     * @throws NoSuchEntityException
     * @deprecated 100.1.0
     */
    protected function getFullCustomerObject($customer)
    {
        // No need to flatten the custom attributes or nested objects since the only usage is for email templates and
        // object passed for events
        $mergedCustomerData = $this->customerRegistry->retrieveSecureData($customer->getId());
        $customerData =
            $this->dataProcessor->buildOutputDataArray($customer, \Magento\Customer\Api\Data\CustomerInterface::class);
        $mergedCustomerData->addData($customerData);
        $mergedCustomerData->setData('name', $this->customerViewHelper->getCustomerName($customer));
        return $mergedCustomerData;
    }

    /**
     * Return hashed password, which can be directly saved to database.
     *
     * @param string $password
     * @return string
     */
    public function getPasswordHash($password)
    {
        return $this->encryptor->getHash($password);
    }

    /**
     * Disable Customer Address Validation
     *
     * @param CustomerInterface $customer
     * @throws NoSuchEntityException
     */
    private function disableAddressValidation($customer)
    {
        foreach ($customer->getAddresses() as $address) {
            $addressModel = $this->addressRegistry->retrieve($address->getId());
            $addressModel->setShouldIgnoreValidation(true);
        }
    }

    /**
     * Get email notification
     *
     * @return EmailNotificationInterface
     * @deprecated 100.1.0
     */
    private function getEmailNotification()
    {
        if (!($this->emailNotification instanceof EmailNotificationInterface)) {
            return \Magento\Framework\App\ObjectManager::getInstance()->get(
                EmailNotificationInterface::class
            );
        } else {
            return $this->emailNotification;
        }
    }

    /**
     * Destroy all active customer sessions by customer id (current session will not be destroyed).
     *
     * Customer sessions which should be deleted are collecting  from the "customer_visitor" table considering
     * configured session lifetime.
     *
     * @param string|int $customerId
     * @return void
     */
    private function destroyCustomerSessions($customerId)
    {
        $sessionLifetime = $this->scopeConfig->getValue(
            \Magento\Framework\Session\Config::XML_PATH_COOKIE_LIFETIME,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $dateTime = $this->dateTimeFactory->create();
        $activeSessionsTime = $dateTime->setTimestamp($dateTime->getTimestamp() - $sessionLifetime)
            ->format(DateTime::DATETIME_PHP_FORMAT);
        /** @var \Magento\Customer\Model\ResourceModel\Visitor\Collection $visitorCollection */
        $visitorCollection = $this->visitorCollectionFactory->create();
        $visitorCollection->addFieldToFilter('customer_id', $customerId);
        $visitorCollection->addFieldToFilter('last_visit_at', ['from' => $activeSessionsTime]);
        $visitorCollection->addFieldToFilter('session_id', ['neq' => $this->sessionManager->getSessionId()]);
        /** @var \Magento\Customer\Model\Visitor $visitor */
        foreach ($visitorCollection->getItems() as $visitor) {
            $sessionId = $visitor->getSessionId();
            $this->saveHandler->destroy($sessionId);
        }
    }

    /**
     * Set ignore_validation_flag for reset password flow to skip unnecessary address and customer validation.
     *
     * @param Customer $customer
     * @return void
     */
    private function setIgnoreValidationFlag(Customer $customer)
    {
        $customer->setData('ignore_validation_flag', true);
    }

    /**
     * Check is address allowed for store
     *
     * @param AddressInterface $address
     * @param int|null $storeId
     * @return bool
     */
    private function isAddressAllowedForWebsite(AddressInterface $address, $storeId): bool
    {
        $allowedCountries = $this->allowedCountriesReader->getAllowedCountries(ScopeInterface::SCOPE_STORE, $storeId);

        return in_array($address->getCountryId(), $allowedCountries);
    }
    
    /**
     * @inheritdoc
     */
    public function contactus($user_id,$name, $email_id, $state = '', $city = '', $subject = '', $message = '')
    {		
        $responseFlag = false;
        try{                
            $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ResourceConnection');
            $connection= $this->_resources->getConnection();

            $themeTable = $this->_resources->getTableName('pet_contactus');
            $sql = "INSERT INTO " . $themeTable . "(user_id, name, email_id, state, city, subject, message) VALUES ($user_id, '$name','$email_id', '$state', '$city', '$subject', '$message')";
            $connection->query($sql);
            
            /* send email to admin */

            //print_r($name);die;


        $templateId = '3'; // template id
        $fromEmail =  $email_id ;// sender Email id
        $fromName =  $name ;    // sender Name

        $toEmail = $this->scopeConfig ->getValue('trans_email/ident_general/email',\Magento\Store\Model\ScopeInterface::SCOPE_STORE); // receiver email id (admin in our case)
        $toName = $this->scopeConfig ->getValue('trans_email/ident_general/name',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        
 
        try {
            // template variables pass here
            $templateVars = [
                'name' => $name,
                'email_id' => $email_id,
                'state' => $state,
                'city' => $city,
                'subject' => $subject,
                'message' => $message
            ];
 
            $storeId = $this->storeManager->getStore()->getId();
           
 
            $from = ['email' => $fromEmail, 'name' => $fromName];
            $this->inlineTranslation->suspend();
 
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        
            $templateOptions = [
                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $storeId
            ];
            $transport = $this->transportBuilder->setTemplateIdentifier($templateId, $storeScope)
                ->setTemplateOptions($templateOptions)
                ->setTemplateVars($templateVars)
                ->setFrom($from)
                ->addTo($toEmail)
                ->getTransport();
            $transport->sendMessage();
        
            $this->inlineTranslation->resume();
           
        } catch (\Exception $e) {
           
            $this->logger->info($e->getMessage());
            //print_r($e->getMessage());die;
        }

        $responseFlag = true;
        $message = 'Success';
      
    }

                
         catch(\Exception $e) {
                $message = __($e->getMessage());
        }
        $response['response'] = $responseFlag;
        $response['message']  = $message;
        echo json_encode($response);
    }


	public function sendMessageOTP($mobile,$otp,$signature)
    {		        
        try{     
			$mobile = '91'.$mobile;
			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.msg91.com/api/v5/otp?template_id=617b7ee4cd76c0341110bf85&mobile=$mobile&authkey=368995A2eYQ2aeqc617a7934P1",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_POSTFIELDS => "{\"CODE\":\"$signature\",\"OTP\":\"$otp\"}",
			  CURLOPT_HTTPHEADER => array(
				"content-type: application/json"
			  ),
			));
			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
			if ($err) {
			  //echo "cURL Error #:" . $err;
			} else {
			  //echo $response;
			}		
		}
		catch(\Exception $e) {
			$message = __($e->getMessage());
        }
        
	}
        
    /*insert static follower data of admin */    
    public function insertStaticData($user_id){
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
                    ->get('Magento\Framework\App\ResourceConnection');
        $connection= $this->_resources->getConnection();
        
         /*condition for static value*/
        $urlInterface = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
        $domain = $urlInterface->getBaseUrl();
        
        $pet_user_followers_val = 80;       
        $pet_page_followers_val = 20;       
        $pet_community_members_val = 30;
        if($domain=='https://petzzco.com/' || $domain=='http://petzzco.com/' || $domain=='https://www.petzzco.com/'){
            $pet_user_followers_val = 113;
            $pet_page_followers_val = 21;
            $pet_community_members_val = 43;
        }        
        
        /*pet_user_followers*/
        $pet_user_followers = $this->_resources->getTableName('pet_user_followers');
        $sql = "INSERT INTO " . $pet_user_followers . "(user_id, member_id) VALUES ($user_id, $pet_user_followers_val)";
        $connection->query($sql);	
                
        /*pet_user_followers_count*/
        $pet_user_followers_count = $this->_resources->getTableName('pet_user_followers_count');
        $sql = "INSERT INTO " . $pet_user_followers_count . "(user_id, followings) VALUES ($user_id, 1)";
        $connection->query($sql);	
        
        /*pet_user_followers_count*/
        $pet_user_followers_count1 = $this->_resources->getTableName('pet_user_followers_count');
        $sql = "UPDATE " . $pet_user_followers_count1 . " SET followers = followers + 1 where user_id = $pet_user_followers_val";
        $connection->query($sql);	
        
        /*pet_page_followers*/
        $pet_page_followers = $this->_resources->getTableName('pet_page_followers');
        $sql = "INSERT INTO " . $pet_page_followers . "(user_id, page_id) VALUES ($user_id, $pet_page_followers_val)";
        $connection->query($sql);
        
          /*pet_community_members*/
        $pet_community_members = $this->_resources->getTableName('pet_community_members');
        $sql = "INSERT INTO " . $pet_community_members . "(community_id, member_id) VALUES ($pet_community_members_val, $user_id)";
        $connection->query($sql);
    }

}

