<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace PetzzcoApi\CustomerApi\Api;

use Magento\Framework\Exception\InputException;

/**
 * Interface for managing customers accounts.
 * @api
 * @since 100.0.2
 */
interface AccountManagementInterface
{
    /**#@+
     * Constant for confirmation status
     */
    const ACCOUNT_CONFIRMED = 'account_confirmed';
    const ACCOUNT_CONFIRMATION_REQUIRED = 'account_confirmation_required';
    const ACCOUNT_CONFIRMATION_NOT_REQUIRED = 'account_confirmation_not_required';
    const MAX_PASSWORD_LENGTH = 256;
    /**#@-*/

	/**
     * Get all countries and regions information for the store.
     *
     * @return array
     */
    public function getCountriesInfo();

    /**
     * Get country and region information for the store.
     *
     * @param string $countryId
     * @return array
     */
    public function getCountryInfo($countryId);

	/**
	 * Login api
	 * @param string $email
	 * @param string $password
	 * @param string $device_id
	 * @param string $device_token
	 * @param string $device_type
	 * @param string $login_type
	 * @param string $first_name
	 * @param string $last_name
	 * @param string $social_id
         * @param string $profile_picture
         * @param string $mobilenumber
	 * @return array
	 */
	public function login($email, $password = '', $device_id, $device_token, $device_type, $login_type = '', $first_name = '', $last_name = '', $social_id = '',$profile_picture='',$mobilenumber='');
	
	/**
	 * registration api
	 * @param mixed $data
	 * @param string $password
	 * @return array
	 */
	//public function registration($data, $password);
	
    /**
     * Create customer account. Perform necessary business operations like sending email.
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @param string $password
     * @param string $redirectUrl
	 * @param string $is_seller
	 * @param string $profileurl
     * @return array
     */
    public function registrationAccount(
        \Magento\Customer\Api\Data\CustomerInterface $customer,
        $password = null,
        $redirectUrl = '',
		$is_seller = '',
		$profileurl = ''
    );
    
    /**
     * Forgot password.
     *
     * @param string $email
     * @param string $mobile_number
     * @param string $otp_type
     * @param string $resend
     * @param string $signature
     * @return array
     */
    public function forgotPassword($email = '',$otp_type,$mobile_number = '',$resend = '',$signature = '');
    
    /**
     * Forgot password OTP verification.
     *
     * @param string $email
     * @param string $otp
	 * @param string $otp_type
     * @return array
     */
    public function forgotPasswordOtpVerification($email, $otp ,$otp_type);
	
	/**
     * mobile number change OTP verification.
     *	 
	 * @param int $customerId
     * @param string $mobilenumber
     * @param string $otp	 
     * @return array
     */
    public function mobileChangeOtpVerification($customerId, $mobilenumber, $otp);
    
    /**
     * reset password with OTP.
     *
     * @param string $email
     * @param string $password
     * @param string $otp
     * @return array
     */
    public function resetPasswordWithOTP($email, $password, $otp);
    
    /**
     * change customer password.
     *
     * @param int $customerId
     * @param string $old_password
     * @param string $new_password
     * @return array
     */
    public function changeUserPassword($customerId, $old_password, $new_password);
    
    /**
     * change subscribe/unsubscribe.
     *
     * @param int $customerId
     * @param string $subscribe
     * @return array
     */
    public function changeSubscribe($customerId, $subscribe);
    
    /**
     * check subscribe/unsubscribe.
     *
     * @param int $customerId
     * @return array
     */
    public function checkSubscribe($customerId);
       
    
    /**
	 * manage customer profile
	 * @param int $customerId
	 * @param string $first_name
	 * @param string $last_name	 
	 * @param string $email
	 * @param string $mobilenumber
	 * @param string $profile_picture
	 * @param string $cover_picture
	 * @param string $privacy_status
	 * @param string $signature
	 * @return array
	 */
	public function manageProfile($customerId, $first_name, $last_name, $email, $mobilenumber, $profile_picture = '', $cover_picture = '',$privacy_status,$signature = ''); 
	
	/**
	 * manage customer profile
	 * @param int $customerId
	 * @param string $first_name
	 * @param string $last_name	 
	 * @param string $email
	 * @param string $mobilenumber
	 * @param string $business_name
	 * @param string $business_type
	 * @param string $business_type_id
	 * @param string $latitude
	 * @param string $longitude
	 * @param string $shop_address
	 * @param string $pin_code
	 * @param string $city
	 * @param string $state
	 * @param string $gstin
	 * @param string $pan
	 * @param string $image_one
	 * @param string $image_two
	 * @param string $image_three
	 * @param string $profile_picture
	 * @param string $cover_picture
         * @param string $privacy_status
         * @param string $signature
	 * @return array
	 */
	public function manageServiceProfile($customerId, $first_name, $last_name, $email, $mobilenumber, $profile_picture = '', $cover_picture = '', $business_name, $business_type, $shop_address, $pin_code, $city, $state, $gstin, $pan, $image_one='', $image_two='', $image_three='',$business_type_id,$latitude,$longitude,$privacy_status,$signature = ''); 
	
	/**
     * grooming List.
     *
     * @param string $latitude
	 * @param string $longitude
     * @return array
     */
    public function groomingList($latitude,$longitude);
	
    /**
     * set currency code.
     *
     * @param string $currency_code
     * @param string $cart_id
     * @return array
     */
    public function setCurrencyCode($currency_code, $cart_id);
    
    /**
     * Get Customer address.
     *
     * @param int $customerId
     * @return array
     */
    public function getCustomerAddress($customerId);
    
    /**
     * Add/Update Customer address.
     *
     * @param int $customerId
     * @param mixed $address
     * @return array
     */
    public function addUpdateCustomerAddress($customerId, $address);
    
    /**
     * Delere/Default Customer address.
     *
     * @param string $customer_id
     * @param int $customerId
     * @param string $flag
     * @return array
     */
    public function deleteDefaultCustomerAddress($customerId, $address_id, $flag);
    
    /**
     * Create customer account. Perform necessary business operations like sending email.
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @param string $password
     * @param string $redirectUrl
     * @return \Magento\Customer\Api\Data\CustomerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createAccount(
        \Magento\Customer\Api\Data\CustomerInterface $customer,
        $password = null,
        $redirectUrl = ''
    );

    /**
     * Create customer account using provided hashed password. Should not be exposed as a webapi.
     *
     * @api
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @param string $hash Password hash that we can save directly
     * @param string $redirectUrl URL fed to welcome email templates. Can be used by templates to, for example, direct
     *                            the customer to a product they were looking at after pressing confirmation link.
     * @return \Magento\Customer\Api\Data\CustomerInterface
     * @throws \Magento\Framework\Exception\InputException If bad input is provided
     * @throws \Magento\Framework\Exception\State\InputMismatchException If the provided email is already used
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createAccountWithPasswordHash(
        \Magento\Customer\Api\Data\CustomerInterface $customer,
        $hash,
        $redirectUrl = ''
    );

    /**
     * Validate customer data.
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @return \Magento\Customer\Api\Data\ValidationResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function validate(\Magento\Customer\Api\Data\CustomerInterface $customer);

    /**
     * Check if customer can be deleted.
     *
     * @api
     * @param int $customerId
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException If group is not found
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isReadonly($customerId);

    /**
     * Activate a customer account using a key that was sent in a confirmation email.
     *
     * @param string $email
     * @param string $confirmationKey
     * @return \Magento\Customer\Api\Data\CustomerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function activate($email, $confirmationKey);

    /**
     * Activate a customer account using a key that was sent in a confirmation email.
     *
     * @api
     * @param int $customerId
     * @param string $confirmationKey
     * @return \Magento\Customer\Api\Data\CustomerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function activateById($customerId, $confirmationKey);

    /**
     * Authenticate a customer by username and password
     *
     * @param string $email
     * @param string $password
     * @return \Magento\Customer\Api\Data\CustomerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function authenticate($email, $password);

    /**
     * Change customer password.
     *
     * @param string $email
     * @param string $currentPassword
     * @param string $newPassword
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function changePassword($email, $currentPassword, $newPassword);

    /**
     * Change customer password.
     *
     * @param int $customerId
     * @param string $currentPassword
     * @param string $newPassword
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function changePasswordById($customerId, $currentPassword, $newPassword);

    /**
     * Send an email to the customer with a password reset link.
     *
     * @param string $email
     * @param string $template
     * @param int $websiteId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function initiatePasswordReset($email, $template, $websiteId = null);

    /**
     * Reset customer password.
     *
     * @param string $email If empty value given then the customer
     * will be matched by the RP token.
     * @param string $resetToken
     * @param string $newPassword
     *
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws InputException
     */
    public function resetPassword($email, $resetToken, $newPassword);

    /**
     * Check if password reset token is valid.
     *
     * @param int $customerId If 0 is given then a customer
     * will be matched by the RP token.
     * @param string $resetPasswordLinkToken
     *
     * @return bool True if the token is valid
     * @throws \Magento\Framework\Exception\State\InputMismatchException If token is mismatched
     * @throws \Magento\Framework\Exception\State\ExpiredException If token is expired
     * @throws \Magento\Framework\Exception\InputException If token or customer id is invalid
     * @throws \Magento\Framework\Exception\NoSuchEntityException If customer doesn't exist
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function validateResetPasswordLinkToken($customerId, $resetPasswordLinkToken);

    /**
     * Gets the account confirmation status.
     *
     * @param int $customerId
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getConfirmationStatus($customerId);

    /**
     * Resend confirmation email.
     *
     * @param string $email
     * @param int $websiteId
     * @param string $redirectUrl
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function resendConfirmation($email, $websiteId, $redirectUrl = '');

    /**
     * Check if given email is associated with a customer account in given website.
     *
     * @param string $customerEmail
     * @param int $websiteId If not set, will use the current websiteId
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isEmailAvailable($customerEmail, $websiteId = null);

    /**
     * Check store availability for customer given the customerId.
     *
     * @param int $customerWebsiteId
     * @param int $storeId
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isCustomerInStore($customerWebsiteId, $storeId);

    /**
     * Retrieve default billing address for the given customerId.
     *
     * @param int $customerId
     * @return \Magento\Customer\Api\Data\AddressInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If the customer Id is invalid
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getDefaultBillingAddress($customerId);

    /**
     * Retrieve default shipping address for the given customerId.
     *
     * @param int $customerId
     * @return \Magento\Customer\Api\Data\AddressInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If the customer Id is invalid
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getDefaultShippingAddress($customerId);

    /**
     * Return hashed password, which can be directly saved to database.
     *
     * @param string $password
     * @return string
     */
    public function getPasswordHash($password);
    
    /**
	 * ContactUs api
         * @param int $user_id
	 * @param string $name
         * @param string $email
	 * @param string $state
	 * @param string $city
	 * @param string $subject
	 * @param string $message	 
	 * @return array
	 */
	public function contactus($user_id,$name, $email, $state = '', $city = '', $subject = '', $message = '');
	
}
