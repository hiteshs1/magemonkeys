<?php
/*
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace PetzzcoApi\CustomerApi\Helper;

use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\App\Emulation as AppEmulation;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Customerapi default helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	/**
	 *@var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $storeManager;
	
	/**
     *@var \Magento\Store\Model\App\Emulation
     */
    protected $appEmulation;
    
    /**
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Store\Model\App\Emulation $appEmulation
     */
     public function __construct(
		\Magento\Framework\App\Helper\Context $context,
        StoreManagerInterface $storeManager,
        AppEmulation $appEmulation        
    ) {
		$this->storeManager = $storeManager;
        $this->appEmulation = $appEmulation;
        parent::__construct($context);
    }
    
    /*
	 * get product image url
	 * 
     * @param mixed $product
     * @param string $imageType
     * @return string
	 */
    public function getImageUrl($product, string $imageType = 'product_page_image_small')
	{
		$storeId = $this->storeManager->getStore()->getId();
		$this->appEmulation->startEnvironmentEmulation($storeId, \Magento\Framework\App\Area::AREA_FRONTEND, true);
		$objectManager =\Magento\Framework\App\ObjectManager::getInstance();
		$helperImport = $objectManager->get('\Magento\Catalog\Helper\Image');

		$imageUrl = $helperImport->init($product, $imageType)
						->setImageFile($product->getImage()) // image,small_image,thumbnail
						->resize(300)
						->getUrl();
		/*$imageBlock = $this->_blockFactory->createBlock('Magento\Catalog\Block\Product\ListProduct');
		$productImage = $imageBlock->getImage($product, $imageType);
		$imageUrl = $productImage->getImageUrl();*/
		$this->appEmulation->stopEnvironmentEmulation();
		return $imageUrl;
	}
}
