<?php
namespace PetzzcoApi\CustomerApi\Setup;
 
use Magento\Customer\Model\Customer;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface 
{
    private $customerSetupFactory;

    public function __construct(\Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory)
    {
        $this->customerSetupFactory = $customerSetupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
		if(version_compare($context->getVersion(), '1.0.3', '<')) {
			$customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
	 
			$customerSetup->addAttribute(
				Customer::ENTITY,
				'user_type',
				[
					'type' => 'text',
					'label' => 'User Type',
					'frontend_input' => 'text',
					'required' => false,
					'visible' => false,
					'system'=> 0,
					'position' => 80,
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true
				]
			);
			

		}
		
		if(version_compare($context->getVersion(), '1.0.4', '<')) {
			$customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
	 
			$customerSetup->addAttribute(
				Customer::ENTITY,
				'business_name',
				[
					'type' => 'text',
					'label' => 'Business Name',
					'frontend_input' => 'text',
					'required' => false,
					'visible' => false,
					'system'=> 0,
					'position' => 80,
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true
				]
			);
			$customerSetup->addAttribute(
				Customer::ENTITY,
				'business_type',
				[
					'type' => 'text',
					'label' => 'Business Type',
					'frontend_input' => 'text',
					'required' => false,
					'visible' => false,
					'system'=> 0,
					'position' => 80,
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true
				]
			);
			$customerSetup->addAttribute(
				Customer::ENTITY,
				'shop_address',
				[
					'type' => 'text',
					'label' => 'Shop Address',
					'frontend_input' => 'text',
					'required' => false,
					'visible' => false,
					'system'=> 0,
					'position' => 80,
					'is_used_in_grid' => false,
					'is_visible_in_grid' => false,
					'is_filterable_in_grid' => false,
					'is_searchable_in_grid' => false
				]
			);
			$customerSetup->addAttribute(
				Customer::ENTITY,
				'pin_code',
				[
					'type' => 'text',
					'label' => 'Pin Code',
					'frontend_input' => 'text',
					'required' => false,
					'visible' => false,
					'system'=> 0,
					'position' => 80,
					'is_used_in_grid' => false,
					'is_visible_in_grid' => false,
					'is_filterable_in_grid' => false,
					'is_searchable_in_grid' => false
				]
			);
			$customerSetup->addAttribute(
				Customer::ENTITY,
				'city',
				[
					'type' => 'text',
					'label' => 'City',
					'frontend_input' => 'text',
					'required' => false,
					'visible' => false,
					'system'=> 0,
					'position' => 80,
					'is_used_in_grid' => false,
					'is_visible_in_grid' => false,
					'is_filterable_in_grid' => false,
					'is_searchable_in_grid' => false
				]
			);
			
			$customerSetup->addAttribute(
				Customer::ENTITY,
				'state',
				[
					'type' => 'text',
					'label' => 'State',
					'frontend_input' => 'text',
					'required' => false,
					'visible' => false,
					'system'=> 0,
					'position' => 80,
					'is_used_in_grid' => false,
					'is_visible_in_grid' => false,
					'is_filterable_in_grid' => false,
					'is_searchable_in_grid' => false
				]
			);
			$customerSetup->addAttribute(
				Customer::ENTITY,
				'gstin',
				[
					'type' => 'text',
					'label' => 'GSTIN',
					'frontend_input' => 'text',
					'required' => false,
					'visible' => false,
					'system'=> 0,
					'position' => 80,
					'is_used_in_grid' => false,
					'is_visible_in_grid' => false,
					'is_filterable_in_grid' => false,
					'is_searchable_in_grid' => false
				]
			);
			
			$customerSetup->addAttribute(
				Customer::ENTITY,
				'pan',
				[
					'type' => 'text',
					'label' => 'PAN',
					'frontend_input' => 'text',
					'required' => false,
					'visible' => false,
					'system'=> 0,
					'position' => 80,
					'is_used_in_grid' => false,
					'is_visible_in_grid' => false,
					'is_filterable_in_grid' => false,
					'is_searchable_in_grid' => false
				]
			);
			
			
		}
		
		if(version_compare($context->getVersion(), '1.0.5', '<')) {
			$customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
			
			$customerSetup->addAttribute(
				Customer::ENTITY,
				'mobilenumber',
				[
					'type' => 'text',
					'label' => 'Mobile Number',
					'frontend_input' => 'text',
					'required' => false,
					'visible' => false,
					'system'=> 0,
					'position' => 80,
					'is_used_in_grid' => false,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => false,
					'is_searchable_in_grid' => false
				]
			);
		}
		if(version_compare($context->getVersion(), '1.0.7', '<')) {
			$customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
			
			$customerSetup->addAttribute(
				Customer::ENTITY,
				'cover_picture',
				[
					'type' => 'text',
					'label' => 'Cover Picture',
					'frontend_input' => 'text',
					'required' => false,
					'visible' => false,
					'system'=> 0,
					'position' => 80,
					'is_used_in_grid' => false,
					'is_visible_in_grid' => false,
					'is_filterable_in_grid' => false,
					'is_searchable_in_grid' => false
				]
			);
		}
		
		if(version_compare($context->getVersion(), '1.0.8', '<')) {
			$customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
			
			$customerSetup->addAttribute(
				Customer::ENTITY,
				'image_one',
				[
					'type' => 'text',
					'label' => 'Certificate 1',
					'frontend_input' => 'text',
					'required' => false,
					'visible' => false,
					'system'=> 0,
					'position' => 80,
					'is_used_in_grid' => false,
					'is_visible_in_grid' => false,
					'is_filterable_in_grid' => false,
					'is_searchable_in_grid' => false
				]
			);
			$customerSetup->addAttribute(
				Customer::ENTITY,
				'image_two',
				[
					'type' => 'text',
					'label' => 'Certificate 2',
					'frontend_input' => 'text',
					'required' => false,
					'visible' => false,
					'system'=> 0,
					'position' => 80,
					'is_used_in_grid' => false,
					'is_visible_in_grid' => false,
					'is_filterable_in_grid' => false,
					'is_searchable_in_grid' => false
				]
			);
			$customerSetup->addAttribute(
				Customer::ENTITY,
				'image_three',
				[
					'type' => 'text',
					'label' => 'Certificate 3',
					'frontend_input' => 'text',
					'required' => false,
					'visible' => false,
					'system'=> 0,
					'position' => 80,
					'is_used_in_grid' => false,
					'is_visible_in_grid' => false,
					'is_filterable_in_grid' => false,
					'is_searchable_in_grid' => false
				]
			);
		}
		
		if(version_compare($context->getVersion(), '1.0.9', '<')) {
			$customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
			
			$customerSetup->addAttribute(
				Customer::ENTITY,
				'business_type_id',
				[
					'type' => 'text',
					'label' => 'Business Type Id',
					'frontend_input' => 'text',
					'required' => false,
					'visible' => false,
					'system'=> 0,
					'position' => 80,
					'is_used_in_grid' => false,
					'is_visible_in_grid' => false,
					'is_filterable_in_grid' => false,
					'is_searchable_in_grid' => false
				]
			);						
		}
		
		if(version_compare($context->getVersion(), '1.1.0', '<')) {
			$customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
			
			$customerSetup->addAttribute(
				Customer::ENTITY,
				'latitude',
				[
					'type' => 'text',
					'label' => 'Latitude',
					'frontend_input' => 'text',
					'required' => false,
					'visible' => false,
					'system'=> 0,
					'position' => 80,
					'is_used_in_grid' => false,
					'is_visible_in_grid' => false,
					'is_filterable_in_grid' => false,
					'is_searchable_in_grid' => false
				]
			);	

			$customerSetup->addAttribute(
				Customer::ENTITY,
				'longitude',
				[
					'type' => 'text',
					'label' => 'Longitude',
					'frontend_input' => 'text',
					'required' => false,
					'visible' => false,
					'system'=> 0,
					'position' => 80,
					'is_used_in_grid' => false,
					'is_visible_in_grid' => false,
					'is_filterable_in_grid' => false,
					'is_searchable_in_grid' => false
				]
			);				
		}
		if(version_compare($context->getVersion(), '1.1.1', '<')) {
			$customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
			
			$customerSetup->addAttribute(
				Customer::ENTITY,
				'privacy_status',
				[
					'type' => 'text',
					'label' => 'Privacy Status',
					'frontend_input' => 'text',
					'required' => false,
					'visible' => false,
					'system'=> 0,
					'position' => 80,
					'is_used_in_grid' => false,
					'is_visible_in_grid' => false,
					'is_filterable_in_grid' => false,
					'is_searchable_in_grid' => false
				]
			);	
				
		}
        $setup->endSetup();
    }
}
