<?php
namespace PetzzcoApi\CustomerApi\Setup;
 
use Magento\Customer\Model\Customer;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
 
class InstallData implements InstallDataInterface
{
    private $customerSetupFactory;

    public function __construct(\Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory)
    {
        $this->customerSetupFactory = $customerSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
		
		$customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
 
		$customerSetup->addAttribute(
			Customer::ENTITY,
			'device_id',
			[
				'type' => 'text',
				'label' => 'Device Id',
				'frontend_input' => 'text',
				'required' => false,
				'visible' => false,
				'system'=> 0,
				'position' => 80,
				'is_used_in_grid' => false,
				'is_visible_in_grid' => false,
				'is_filterable_in_grid' => false,
				'is_searchable_in_grid' => false
			]
		);
		
		$customerSetup->addAttribute(
			Customer::ENTITY,
			'device_token',
			[
				'type' => 'text',
				'label' => 'Device Token',
				'frontend_input' => 'text',
				'required' => false,
				'visible' => false,
				'system'=> 0,
				'position' => 80,
				'is_used_in_grid' => false,
				'is_visible_in_grid' => false,
				'is_filterable_in_grid' => false,
				'is_searchable_in_grid' => false
			]
		);

		$customerSetup->addAttribute(
			Customer::ENTITY,
			'device_type',
			[
				'type' => 'text',
				'label' => 'Device Type',
				'frontend_input' => 'text',
				'required' => false,
				'visible' => false,
				'system'=> 0,
				'position' => 80,
				'is_used_in_grid' => false,
				'is_visible_in_grid' => false,
				'is_filterable_in_grid' => false,
				'is_searchable_in_grid' => false
			]
		);
		
		$customerSetup->addAttribute(
			Customer::ENTITY,
			'login_type',
			[
				'type' => 'text',
				'label' => 'Login Type',
				'frontend_input' => 'text',
				'required' => false,
				'visible' => false,
				'system'=> 0,
				'position' => 80,
				'is_used_in_grid' => false,
				'is_visible_in_grid' => false,
				'is_filterable_in_grid' => false,
				'is_searchable_in_grid' => false
			]
		);
		
		$customerSetup->addAttribute(
			Customer::ENTITY,
			'social_id',
			[
				'type' => 'text',
				'label' => 'Social Id',
				'frontend_input' => 'text',
				'required' => false,
				'visible' => false,
				'system'=> 0,
				'position' => 80,
				'is_used_in_grid' => false,
				'is_visible_in_grid' => false,
				'is_filterable_in_grid' => false,
				'is_searchable_in_grid' => false
			]
		);
		
		$customerSetup->addAttribute(
			Customer::ENTITY,
			'mobile_otp',
			[
				'type' => 'text',
				'label' => 'Mobile OTP',
				'frontend_input' => 'text',
				'required' => false,
				'visible' => false,
				'system'=> 0,
				'position' => 80,
				'is_used_in_grid' => false,
				'is_visible_in_grid' => false,
				'is_filterable_in_grid' => false,
				'is_searchable_in_grid' => false
			]
		);

		$customerSetup->addAttribute(
			Customer::ENTITY,
			'profile_picture',
			[
				'type' => 'text',
				'label' => 'Profile Picture',
				'frontend_input' => 'text',
				'required' => false,
				'visible' => false,
				'system'=> 0,
				'position' => 80,
				'is_used_in_grid' => false,
				'is_visible_in_grid' => false,
				'is_filterable_in_grid' => false,
				'is_searchable_in_grid' => false
			]
		);
		
		$setup->endSetup();
    }
}
