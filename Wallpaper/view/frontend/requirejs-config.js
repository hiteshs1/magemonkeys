var config = {
    paths: {
        jqueryCrop: 'Magemonkeys_Wallpaper/js/jquery',
        rcrop: 'Magemonkeys_Wallpaper/js/rcrop.min'
    },
    shim: {
        rcrop: {
            deps: ['jquery']
        }
    }
};
