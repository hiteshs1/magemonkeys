<?php
namespace Magemonkeys\Wallpaper\Helper;

use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

	protected $_mediaDirectory;
    protected $_fileUploaderFactory;
    public $_storeManager;
    protected $_helperData;
    protected $wallpaperSession;


	public function __construct(
		\Magento\Framework\App\Helper\Context $context,
		\Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,

        \Magento\Framework\Session\SessionManagerInterface $coreSession
	)
	{
		$this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_storeManager = $storeManager;
        $this->wallpaperSession = $coreSession;
		parent::__construct($context);
	}

	public function PerMeterPrice()
    {
        return $this->scopeConfig->getValue('wallpaper/general/meter_price', ScopeInterface::SCOPE_STORE);
    }
    public function Tooltip()
    {
        return $this->scopeConfig->getValue('wallpaper/general/tooltip', ScopeInterface::SCOPE_STORE);
    }

	public function getWallpaperSession()
	{	
		$this->wallpaperSession->start();
		return $this->wallpaperSession;
	}
}
?>