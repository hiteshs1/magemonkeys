<?php
namespace Magemonkeys\Wallpaper\Observer;

use Magento\Framework\Event\ObserverInterface;
 
class OrderObserver implements ObserverInterface
{   
    protected $_helperData;
    public function __construct(        
        \Magemonkeys\Wallpaper\Helper\Data $helperData
    )
    {        
        $this->_helperData = $helperData;        
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $sessionValue = $this->_helperData->getWallpaperSession()->unsWallpaperValue();     
    }
}
?>