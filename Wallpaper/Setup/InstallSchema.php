<?php
namespace Magemonkeys\Wallpaper\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{

	public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$installer = $setup;
		$installer->startSetup();
		if (!$installer->tableExists('wallpaper_custom')) {
			$table = $installer->getConnection()->newTable(
				$installer->getTable('wallpaper_custom')
			)
				->addColumn(
					'id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					null,
					[
						'identity' => true,
						'nullable' => false,
						'primary'  => true,
						'unsigned' => true,
					],
					'ID'
				)
				->addColumn(
					'product_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					10,
					['nullable => true'],
					'Product ID'
				)
				->addColumn(
					'product_name',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					['nullable => false'],
					'Product Name'
				)
				->addColumn(
					'image_path',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					['nullable => false'],
					'Image Path'
				)
				->addColumn(
					'attributes',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					'2M',
					[],
					'Attributes'
				)
				->addColumn(
						'created_at',
						\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
						null,
						['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
						'Created At'
				)
				->setComment('Custom Wallpaper Data');
			$installer->getConnection()->createTable($table);
		}

		if (!$installer->tableExists('wallpaper_predefined')) {
			$table = $installer->getConnection()->newTable(
				$installer->getTable('wallpaper_predefined')
			)
				->addColumn(
					'id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					null,
					[
						'identity' => true,
						'nullable' => false,
						'primary'  => true,
						'unsigned' => true,
					],
					'ID'
				)
				->addColumn(
					'product_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					10,
					[],
					'Product ID'
				)
				->addColumn(
					'product_name',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					['nullable => false'],
					'Product Name'
				)
				->addColumn(
					'attributes',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					'2M',
					[],
					'Attributes'
				)
				->addColumn(
					'created_at',
					\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
					null,
					['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
					'Created At'
				)
				->setComment('Predefined Wallpaper Data');
			$installer->getConnection()->createTable($table);
			
		}
		$installer->endSetup();
	}
}