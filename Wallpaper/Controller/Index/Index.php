<?php
namespace Magemonkeys\Wallpaper\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
    protected $_helperData;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magemonkeys\Wallpaper\Helper\Data $helperData
    )
	{
		$this->_pageFactory = $pageFactory;
        $this->_helperData = $helperData;
		return parent::__construct($context);
	}

	public function execute()
	{
		$pageFactory =  $this->_pageFactory->create();

		// Add title which is got by the configuration via backend
        $pageFactory->getConfig()->getTitle()->set('Wallpaper Customisation');

        // Add breadcrumb
        /** @var \Magento\Theme\Block\Html\Breadcrumbs */
        $breadcrumbs = $pageFactory->getLayout()->getBlock('breadcrumbs');
        $breadcrumbs->addCrumb('home',
            [
                'label' => __('Home'),
                'title' => __('Home'),
                'link' => $this->_url->getUrl('')
            ]
        );
        $breadcrumbs->addCrumb('wallpaper',
            [
                'label' => __('Wallpaper'),
                'title' => __('Wallpaper')
            ]
        );

        return $pageFactory;
	}
}
