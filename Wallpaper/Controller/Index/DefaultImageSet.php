<?php
namespace Magemonkeys\Wallpaper\Controller\Index;

use Magento\Framework\Json\Helper\Data as JsonHelper;

class DefaultImageSet extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
	protected $connection;
    protected $_mediaDirectory;
    protected $_fileUploaderFactory;
    public $_storeManager;
    protected $_helperData;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\ResourceConnection $connection,
        JsonHelper $jsonHelper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magemonkeys\Wallpaper\Helper\Data $helperData
    )
	{
		$this->_pageFactory = $pageFactory;
        $this->connection = $connection;      
        $this->jsonHelper = $jsonHelper;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_storeManager = $storeManager;
        $this->_helperData = $helperData;
		return parent::__construct($context);
	}

	public function execute(){
        
        $_postData = $this->getRequest()->getPost();        
        $defaultimage = $_postData['defaultimage'];        
        //$price = $_postData['price'];        
        
        try{
            $sessionValue = $this->_helperData->getWallpaperSession()->getData('wallpaper_value');
            foreach ($sessionValue as $key => $value) {
                $keyArray = explode('_', $key);
                if($defaultimage == $keyArray[0])
                {
                    $newkey = $keyArray[0] .'_'. $keyArray[1];
                    $sessionValue[$newkey]['defaultimg'] = 1;
                    $price = $sessionValue[$newkey]['price'];
                    $id = $sessionValue[$newkey]['id'];
                    $src = $sessionValue[$newkey]['src'];
                    $width = $sessionValue[$newkey]['width'];
                    $height = $sessionValue[$newkey]['height'];
                    $max_width = $sessionValue[$newkey]['max_width'];
                    $max_height = $sessionValue[$newkey]['max_height'];
                }
                else
                {
                    $sessionValue[$key]['defaultimg'] = 0;                    
                }
            }
            $this->_helperData->getWallpaperSession()->setData('wallpaper_value', $sessionValue);
            $data = array('');
            $data = array('pricedata' => $price, 'id' => $id, 'path' => $src, 'width'=>$width, 'height'=>$height, 'max_width'=>$max_width, 'max_height'=>$max_height);

            $message = __("session has been added successfully"); 
            $error = false;            
              
        } catch (\Exception $e) {
            $error = true;
            $message = $e->getMessage();
        }
        
        $resultJson = $this->resultJsonFactory->create();

        return $resultJson->setData([
                    'message' => $message,                    
                    'error' => $error,
                    'data' => $data
        ]);
    }
}
