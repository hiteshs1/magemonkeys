<?php
namespace Magemonkeys\Wallpaper\Controller\Index;

use Magento\Framework\Json\Helper\Data as JsonHelper;

class DeleteCustomize extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
	protected $connection;
    protected $_mediaDirectory;
    protected $_fileUploaderFactory;
    public $_storeManager;
    protected $_file;
    protected $_helperData;
    protected $_cart;    
    protected $_checkoutSession;   

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\ResourceConnection $connection,
        JsonHelper $jsonHelper,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem\Driver\File $file,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magemonkeys\Wallpaper\Helper\Data $helperData
    )
	{
		$this->_pageFactory = $pageFactory;
        $this->connection = $connection;      
        $this->jsonHelper = $jsonHelper;
        $this->_cart = $cart;
        $this->_checkoutSession = $checkoutSession;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_storeManager = $storeManager;
        $this->_file = $file;
        $this->_helperData = $helperData;
		return parent::__construct($context);
	}

	public function execute(){
        
        $_postData = $this->getRequest()->getPost();
        
        $message = "";
        $newFileName = "";        
        $success = false;
        
        $mediaRootDir = $this->_mediaDirectory->getAbsolutePath();
        $_fileName = $mediaRootDir .'wallpaper/'. $_postData['filename'];
        if ($this->_file->isExists($_fileName))  {
            try{
                /*$this->attachment->load($_postData['imageID']);
                $this->attachment->delete();*/
                //$sessionValue = $this->_helperData->getWallpaperSession()->unsWallpaperValue();
                $proSku = array("");
                $exist = "";
                $quote = $this->getCheckoutSession()->getQuote();
                $items = $quote->getAllItems();
                foreach($items as $item) {
                    $proSku = array($item->getSku());            
                }
                if($proSku!=''){
                    if(in_array($_postData['key'], $proSku)){
                        $exist = 'File is in the shopping cart';
                    } else{                    
                        $sessionValue = $this->_helperData->getWallpaperSession()->getData('wallpaper_value');
                        $imagekey =  $_postData['key'];
                        foreach ($sessionValue as $key => $value) {                    
                            $keyArray = explode('_', $key);
                            if($imagekey == $keyArray[0])
                            {                        
                                $newkey = $keyArray[0] .'_'. $keyArray[1];
                                unset($sessionValue[$newkey]);
                                $this->_helperData->getWallpaperSession()->setData('wallpaper_value', $sessionValue);
                            }
                        }                
                        $this->_file->deleteFile($_fileName);
                        $exist = 'done';
                    }
                }
                $message = __('File removed successfully.');
                $data = array('exist' => $exist);
                $success = true;
            } catch (Exception $ex) {
                $message = $ex->getMessage();
                $success = false;
            }
        }else{
            $message = "File not found.";
            $success = false;
        }
        
        $resultJson = $this->resultJsonFactory->create();

        return $resultJson->setData([
                    'message' => $message,
                    'data' => $data,
                    'success' => $success
        ]);
    }

    public function getCart()
    {        
        return $this->_cart;
    }
    
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }
}
