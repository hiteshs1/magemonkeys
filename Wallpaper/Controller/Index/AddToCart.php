<?php
namespace Magemonkeys\Wallpaper\Controller\Index;

use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\UrlInterface;

class AddToCart extends \Magento\Framework\App\Action\Action
{
    const XML_PATH_WEBSITEID = '1';
    const XML_PATH_ATTRIBUTESET = '9';
    const XML_PATH_VISIBILITY = '4';
    const XML_PATH_STATUS = '1';
    const XML_PATH_CUSTOMERID = '0000011'; 
    const XML_PATH_DEFAULT_PRICE = '0';
    const USE_CONFIG_MANAGE_STOCK = '0';
    const MANAGE_STOCK = '1';
    const MIN_SALE_QTY = '1';
    const IN_STOCK = '1';
    const OUT_OF_STOCK = '0';

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    
    /**
     *  @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     *  @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     *  @var \Magento\Catalog\Model\Category
     */
    protected $_category;

    /**
     *  @var \Magento\Framework\Stdlib\DateTime\DateTimeFactory
     */
    protected $dateTimeFactory;

    /**
     *  @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $collectionFactory;

    /**
     *  @var  \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;
    
    protected $_config;

    protected $_eavSetup;

    protected $_attributeSets;

    protected $indexFactory;
    protected $indexCollection;
    protected $serializer;
    protected $_cart;
    protected $messageManager;
    protected $_responseFactory;
    protected $_url;
    protected $_request;
    protected $_mediaDirectory;
    protected $_rootDirectory;
    protected $wallpaperSession;
    


    /**
     * Post Sendorder constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Catalog\Model\Category $category
     * @param \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateTimeFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\Category $category,
        \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateTimeFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Eav\Model\ConfigFactory $config,
        \Magento\Eav\Setup\EavSetupFactory $eavSetup,
        \Magento\Catalog\Model\Product\AttributeSet\Options $attributeSets,
        \Magento\Indexer\Model\IndexerFactory $indexFactory,
        \Magento\Indexer\Model\Indexer\CollectionFactory $indexCollection,
        \Magento\Framework\Serialize\SerializerInterface $serializer,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        UrlInterface $url,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Session\SessionManagerInterface $coreSession
    ) {
        
        $this->productFactory = $productFactory;
        $this->_productRepository = $productRepository;
        $this->_category = $category;
        $this->dateTimeFactory = $dateTimeFactory;
        $this->collectionFactory = $collectionFactory;
        $this->_resource = $resource;
        $this->resultPageFactory = $resultPageFactory;
        $this->_config = $config;
        $this->_eavSetup = $eavSetup;
        $this->_attributeSets = $attributeSets;
        $this->indexFactory = $indexFactory;
        $this->indexCollection = $indexCollection;
        $this->serializer = $serializer;
        $this->_cart = $cart;
        $this->_responseFactory = $responseFactory;
        $this->_url = $url;
        $this->messageManager = $messageManager;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->_rootDirectory = $filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::ROOT);
        $this->wallpaperSession = $coreSession;
        parent::__construct($context);
    }

    public function execute()
    {   
        $_postData = $this->getRequest()->getPost();
        $mediaRootDir = $this->_mediaDirectory->getAbsolutePath();
        $uniqid = $_postData['productnameid'];
        $productName = 'Wallpaper ' . $uniqid;
        $attributeSetId = $this->checkAttributeSetId();
        $category_id = '';
        $product = $this->productFactory->create();
        $product->setWebsiteIds(array(self::XML_PATH_WEBSITEID));
        $product->setAttributeSetId($attributeSetId);
        $product->setStatus(self::XML_PATH_STATUS);
        $product->setVisibility(self::XML_PATH_VISIBILITY);

        if($uniqid != ''){
            $product->setSku($uniqid);
        }

        $tempUrl = preg_replace('#[^0-9a-z]+#i', '-', $productName);
        $urlKey = strtolower($tempUrl);
        $urlKey = trim($urlKey, '-');
        $urlkeyFl =  $this->checkUrl($urlKey,0);
        $product->setUrlKey($urlkeyFl);
        
        if($productName != ''){
            $product->setName($productName);
            $product->setMetaTitle($productName);
            $product->setMetaKeyword($productName);
            $product->setMetaDescription($productName);
            $product->setDescription($productName);
            $product->setShortDescription($productName);
        }
        $product->setTaxClassId(10);
       
        $product->setCreatedAt(strtotime('now'));

        $product->setCategoryIds(array(26));

        $qty = 10;
        $stock_status = self::IN_STOCK;

        $product->setStockData(
        array(
            'use_config_manage_stock' => self::USE_CONFIG_MANAGE_STOCK,
            'manage_stock' => self::MANAGE_STOCK,
            'min_sale_qty' => self::MIN_SALE_QTY,
            'is_in_stock' => $stock_status,
            'qty' => $qty
            )
        );

        $productPrice = $_postData['productprice'];

        $imagePath = $mediaRootDir .'wallpaper/'. $_postData['selectImage'] .'.'.$_postData['WallpaperfileType']; // path of the image
        $product->addImageToMediaGallery($imagePath, array('image', 'small_image', 'thumbnail'), false, false);
        $product->save();

        if($productPrice != ''){
            $productload = $this->productFactory->create();
            $product = $productload->load($productload->getIdBySku($uniqid));
            
            if($productPrice != ''){
                $product->setPrice($productPrice);
            }else{
                $product->setPrice(self::XML_PATH_DEFAULT_PRICE);
            }
            $product->save();
        }
        
       

        $indexerCollection = $this->indexCollection->create();
        $indexids = $indexerCollection->getAllIds();
     
        foreach ($indexids as $indexid)
        {
            $indexidarray = $this->indexFactory->create()->load($indexid);
     
            //If you want reindex all use this code.
            $indexidarray->reindexAll($indexid);
        }
            
        

        $productload = $this->productFactory->create();
        $product = $productload->load($productload->getIdBySku($uniqid));

        $additionalOptions['height_width'] = [
            'label' => __('Enter the desired format'),
            'value' => $_postData['wallpaper_height'] . ' x ' . $_postData['wallpaper_width'] . ' ' . __('(height x width)'),
        ];

        if($_postData['wallpaper_size']!=''){
            $additionalOptions['wallpaper_size'] = [
                'label' => __('Size'),
                'value' => $_postData['wallpaper_size'],
            ];
        }

        $product->addCustomOption('additional_options', $this->serializer->serialize($additionalOptions));

        $params = array(
            'product' => $product->getId(),
            'qty' => 1,
            /*'options' => array('additional_options'=>$this->serializer->serialize($additionalOptions)) */
        );

        $this->_cart->addProduct($product,$params);
        $this->_cart->save();
        $message = "Products have been successfully added to your Shopping Cart.";
        $this->messageManager->addSuccess($message);
        $accUrl = $this->_url->getUrl('checkout/cart/');
        $this->_responseFactory->create()->setRedirect($accUrl)->sendResponse();
        $this->setRefererUrl($accUrl);
    }

    public function checkUrl($url,$i){
    
        $connection = $this->_resource->getConnection();
        $tableName = $this->_resource->getTableName('url_rewrite');
        $select = $connection->select()->from(['o' =>  $tableName])->where('o.request_path=?',$url);
        $query = "SELECT request_path FROM $tableName WHERE request_path like '".$url.".html' OR `request_path` LIKE '$url'";
        $result = $connection->fetchAll($query);

        if(count($result) == 0){
            return $url;            
        }else{

            if($i >= 0 && $i < 10)
            {   
                $urlnew = substr($url, -2);
                $checkstring = '-'.$i;
                if($urlnew == $checkstring)
                {
                    $url = substr($url, 0, -2);
                }
            } elseif ($i >= 10) {
                $urlnew = substr($url, -3);
                $checkstring = '-'.$i;
                if($urlnew == $checkstring)
                {
                    $url = substr($url, 0, -3);
                }
            }
            $i++;
            if(strpos($url,'html') !== false)
            {
                $newurl = explode('.', $url);
                $url = $newurl[0] .'-'. $i;
            }else{
                $url = $url .'-'. $i;
            }
            return $this->checkUrl($url,$i);
        }
    }

    public function checkAttributeSetId()
    {
        $options = array();

        $attributeSetId = self::XML_PATH_ATTRIBUTESET;

        foreach($this->_attributeSets->toOptionArray() as $set){
            if(strtolower(trim($set['label'])) == strtolower(trim('Wallpaper'))) {
                /*$options[] = ['label' => $set['label'], 'value' => $set['value']];*/
                $attributeSetId = $set['value'];
            }
        }
        
        return $attributeSetId;
    }

}
