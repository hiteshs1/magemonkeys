<?php
namespace Magemonkeys\Wallpaper\Controller\Index;

use Magento\Framework\Json\Helper\Data as JsonHelper;

class ImageCustomize extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
	protected $connection;
    protected $_mediaDirectory;    
    protected $_fileUploaderFactory;
    public $_storeManager;
    protected $_file;
    protected $_helperData;
    protected $moduleReader;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\ResourceConnection $connection,
        JsonHelper $jsonHelper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem\Driver\File $file,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        \Magemonkeys\Wallpaper\Helper\Data $helperData
    )
	{
		$this->_pageFactory = $pageFactory;
        $this->connection = $connection;      
        $this->jsonHelper = $jsonHelper;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);       
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_storeManager = $storeManager;
        $this->_file = $file;
        $this->moduleReader = $moduleReader;
        $this->_helperData = $helperData;
		return parent::__construct($context);
	}

	public function execute(){        
        $_postData = $this->getRequest()->getPost();        
        $message = "";
        $newFileName = "";
        $error = false;
        $data = array();
        $uniqid = uniqid();
        $mediaRootDir = $this->_mediaDirectory->getAbsolutePath();        
        $_fileType = $_postData['fileType'];
        $_fileAction = $_postData['fileAction'];
        $crop_height = $_postData['crop_height'];
        $crop_width = $_postData['crop_width'];
        $crop_x = $_postData['crop_x'];
        $crop_y = $_postData['crop_y'];
        $price = $_postData['price'];
        $main_height = $_postData['main_height'];
        $main_width = $_postData['main_width'];
        $max_height = $_postData['max_height'];
        $max_width = $_postData['max_width'];

        if($_postData['filename'] == '10221276943_45e198c6c6_4k.jpg')
        {
            $newFileName = $uniqid .'.'.$_fileType;
            $filePath = $this->getDirectory(). $_postData['filename'];
            $_fileName = $mediaRootDir .'wallpaper/'. $newFileName;
            $newFullPathOfImage = $_fileName;
            copy($filePath, $_fileName);
        }
        else
        {
            $_fileName = $mediaRootDir .'wallpaper/'. $_postData['filename'];
            $newFullPathOfImage = $mediaRootDir .'wallpaper/'. $uniqid .'.'.$_fileType;
            $newFileName = $uniqid .'.'.$_fileType;
        }

        if ($this->_file->isExists($_fileName)){
            try{
                if($_fileAction == 1) /*** Grayscale (Black & White) ***/
                {
                    if(strtolower($_fileType) == 'jpg' || strtolower($_fileType) == 'jpeg')
                    {
                        $im = ImageCreateFromJpeg($_fileName); 

                        $imgw = imagesx($im);
                        $imgh = imagesy($im);

                        for ($i=0; $i<$imgw; $i++)
                        {
                            for ($j=0; $j<$imgh; $j++)
                            {
                                // get the rgb value for current pixel
                                $rgb = ImageColorAt($im, $i, $j); 

                                // extract each value for r, g, b
                                $rr = ($rgb >> 16) & 0xFF;
                                $gg = ($rgb >> 8) & 0xFF;
                                $bb = $rgb & 0xFF;

                                // get the Value from the RGB value
                                $g = round(($rr + $gg + $bb) / 3);

                                // grayscale values have r=g=b=g
                                $val = imagecolorallocate($im, $g, $g, $g);

                                // set the gray value
                                imagesetpixel ($im, $i, $j, $val);
                            }
                        }

                        imagejpeg($im, $newFullPathOfImage);
                        $message = __('Image converted to grayscale.');
                        imagedestroy($im);
                    }
                    else
                    {
                        $im = imagecreatefrompng($_fileName);

                        if($im && imagefilter($im, IMG_FILTER_GRAYSCALE))
                        {
                            $message = __('Image converted to grayscale.');
                            
                            imagepng($im, $newFullPathOfImage);
                            imagedestroy($im);
                        }
                        else
                        {
                            $message = __('Conversion to grayscale failed.');
                        }
                    }
                }
                else if($_fileAction == 2) /*** Sepia Color Scale ***/
                {
                    if(strtolower($_fileType) == 'jpg' || strtolower($_fileType) == 'jpeg')
                    {
                        // Load
                        $im = ImageCreateFromJpeg($_fileName);

                        imagefilter($im, IMG_FILTER_GRAYSCALE);
                        imagefilter($im,IMG_FILTER_BRIGHTNESS,-10);
                        imagefilter($im, IMG_FILTER_COLORIZE, 65, 50, 0);
                        
                        // Output
                        imagejpeg($im, $newFullPathOfImage);
                        imagedestroy($im);
                    }
                    else
                    {
                        // Load
                        $im = imagecreatefrompng($_fileName);

                        imagefilter($im, IMG_FILTER_GRAYSCALE);
                        imagefilter($im,IMG_FILTER_BRIGHTNESS,-10);
                        imagefilter($im, IMG_FILTER_COLORIZE, 65, 50, 0);
                        
                        // Output
                        imagepng($im, $newFullPathOfImage);
                        imagedestroy($im);
                    }

                    $message = __('Image converted to sepia.');
                }
                else if($_fileAction == 3) /*** Flip Image ***/
                {
                    if(strtolower($_fileType) == 'jpg' || strtolower($_fileType) == 'jpeg')
                    {
                        // Load
                        $im = ImageCreateFromJpeg($_fileName);

                        // Flip it horizontally
                        imageflip($im, IMG_FLIP_HORIZONTAL);

                        // Output
                        imagejpeg($im, $newFullPathOfImage);
                        imagedestroy($im);
                    }
                    else
                    {
                        // Load
                        $im = imagecreatefrompng($_fileName);

                        // Flip it horizontally
                        imageflip($im, IMG_FLIP_HORIZONTAL);

                        // Output
                        imagepng($im, $newFullPathOfImage);
                        imagedestroy($im);
                    }

                    $message = __('Image flipped.');
                }
                else if($_fileAction == 4) /*** Crop Image ***/
                {
                    if(strtolower($_fileType) == 'jpg' || strtolower($_fileType) == 'jpeg')
                    {                        
                        $im = imagecreatefromjpeg($_fileName);
                        $dst_r = ImageCreateTrueColor( $crop_width, $crop_height );

                        imagecopyresampled($dst_r, $im, 0, 0, $crop_x, $crop_y, $crop_width, $crop_height, $crop_width,$crop_height);
                        imagejpeg($dst_r, $newFullPathOfImage);
                        imagedestroy($im);
                    }
                    else
                    {
                        // Load
                        $im = imagecreatefrompng($_fileName);

                        // Flip it horizontally
                        imageflip($im, IMG_FLIP_HORIZONTAL);

                        // Output
                        imagepng($im, $newFullPathOfImage);
                        imagedestroy($im);
                    }

                    $message = __('Image Crop.');
                }



                $_mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                
                $_src = $_mediaUrl.'wallpaper/'.$newFileName;
                
                $message = __("File has been successfully uploaded");


                $sessionValue = $this->_helperData->getWallpaperSession()->getData('wallpaper_value');
                $key = $uniqid . '_' . $_fileType;
                
                $array = array('src'=>$_src,'type'=>$_fileType,'name'=>$newFileName,'id'=>$uniqid, 'defaultimg'=>'1', 'price'=>$price, 'width'=>$main_width, 'height'=>$main_height, 'max_width'=>$max_width, 'max_height'=>$max_height);
                $sessionValue[$key] = $array;

                $this->_helperData->getWallpaperSession()->setData('wallpaper_value',$sessionValue);
                
                $html = '<div class="image item base-image" data-role="image" id="'. $uniqid .'">
                            <div class="wallpaper-image-wrapper">
                                <div class="radioContainer">
                                    <input data-src="'.$_src.'" data-srctype="'. $_fileType .'" data-srcfilename="'. $newFileName .'" class="selectImage selectImage-'. $uniqid .'" type="radio" name="selectImage" value="'. $uniqid .'" id="'. $uniqid .'" />
                                </div>
                                <div class="imageContainer">
                                    <img class="wallpaper-image" data-role="image-element" src="'.$_src.'" alt="">
                                    <span class="imgname">'. $newFileName .'</span>
                                </div>
                                <div class="delete-action">
                                <i class="fa fa-check"></i>
                                    <button type="button" class="action-remove" data-role="delete-button" data-image="'.$newFileName.'" title="'. __('Delete image') .'"><span>'. __('Delete image') .'</span></button>
                                </div>
                            </div>
                        </div>';
                
                $data = array('filename' => $newFileName, 'path' => $_mediaUrl.'wallpaper/'.$newFileName, 'fileType' => $_fileType, 'html' => $html, 'uniqid' => $uniqid, 'width'=>$main_width, 'height'=>$main_height, 'max_width'=>$max_width, 'max_height'=>$max_height);
                $error = false;
            }
            catch (Exception $ex) {
                $message = $ex->getMessage();
                $error = true;
            }
        }else{
            $message = __("File not found.");
            $error = true;
        }
        
        $resultJson = $this->resultJsonFactory->create();

        return $resultJson->setData([
            'message' => $message,
            'data' => $data,
            'error' => $error
        ]);
    }

    public function getDirectory()
    {
        $viewDir = $this->moduleReader->getModuleDir(
            \Magento\Framework\Module\Dir::MODULE_VIEW_DIR,
            'Magemonkeys_Wallpaper'
        );
        return $viewDir . '/frontend/web/images/';
    }

}
