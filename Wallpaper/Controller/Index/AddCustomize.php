<?php
namespace Magemonkeys\Wallpaper\Controller\Index;

use Magento\Framework\Json\Helper\Data as JsonHelper;

class AddCustomize extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
	protected $connection;
    protected $_mediaDirectory;
    protected $_fileUploaderFactory;
    public $_storeManager;
    protected $_helperData;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\ResourceConnection $connection,
        JsonHelper $jsonHelper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magemonkeys\Wallpaper\Helper\Data $helperData
    )
	{
		$this->_pageFactory = $pageFactory;
        $this->connection = $connection;      
        $this->jsonHelper = $jsonHelper;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_storeManager = $storeManager;
        $this->_helperData = $helperData;
		return parent::__construct($context);
	}

	public function execute(){
        
        $_postData = $this->getRequest()->getPost();
        //print_r($_POST); exit;
        
        $message = "";
        $newFileName = "";
        $error = false;
        $data = array();
        
        try{
            $target = $this->_mediaDirectory->getAbsolutePath('wallpaper/');        
            
            //attachment is the input file name posted from your form
            $uploader = $this->_fileUploaderFactory->create(['fileId' => 'attachment']);            
            
            $_fileType = $uploader->getFileExtension();
            $uniqid = uniqid();
            $newFileName = $uniqid .'.'. $_fileType;
            
            /** Allowed extension types */
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png']);
            /** rename file name if already exists */
            $uploader->setAllowRenameFiles(true);
            
            $result = $uploader->save($target, $newFileName); //Use this if you want to change your file name

            /** iomage dpi check */
            $path = $result['path'].'/'.$result['file'];
            $im = imagecreatefromstring(file_get_contents($path));
            $dpi = imageresolution($im);              
            if($dpi['0'] >= '72'){
            if ($result['file']) {
                
                $_mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                
                $_src = $_mediaUrl.'wallpaper/'.$newFileName;
                
                $error = false;
                $message = __("File has been successfully uploaded");

                $sessionValue = $this->_helperData->getWallpaperSession()->getData('wallpaper_value');
                $key = $uniqid . '_' . $_fileType;
                
                $array = array('src'=>$_src,'type'=>$_fileType,'name'=>$newFileName,'id'=>$uniqid,'defaultimg'=>'1' ,'price'=>'0', 'width'=>'0', 'height'=>'0', 'max_width'=>'0', 'max_height'=>'0');
                $sessionValue[$key] = $array;

                $this->_helperData->getWallpaperSession()->setData('wallpaper_value',$sessionValue);
                
                $html = '<div class="image item base-image" data-role="image" id="'. $uniqid .'">
                            <div class="wallpaper-image-wrapper">
                                <div class="radioContainer">
                                    <input data-src="'.$_src.'" data-srctype="'. $_fileType .'" data-srcfilename="'. $newFileName .'" checked="checked" class="selectImage selectImage-'. $uniqid .'" type="radio" name="selectImage" value="'. $uniqid .'" />
                                </div>
                                <div class="imageContainer">
                                    <img class="wallpaper-image" data-role="image-element" src="'.$_src.'" alt="">
                                    <span class="imgname">'. $newFileName .'</span>
                                </div>
                                <div class="delete-action">
                                <i class="fa fa-check"></i>
                                    <button type="button" class="action-remove" data-role="delete-button" data-image="'.$newFileName.'" title="'. __('Delete image') .'"><span>'. __('Delete image') .'</span></button>
                                </div>
                            </div>
                        </div>';
                
                $data = array('filename' => $newFileName, 'path' => $_mediaUrl.'wallpaper/'.$newFileName, 'fileType' => $_fileType, 'html' => $html);
                $dpimsg = '0';
            }
            }else{
                $dpimsg = '1';
                $message = 'Uploaded image must not be less then 72 DPI';    
            }
        } catch (\Exception $e) {
            $error = true;
            $message = $e->getMessage();
        }
        
        $resultJson = $this->resultJsonFactory->create();

        return $resultJson->setData([
                    'message' => $message,
                    'data' => $data,
                    'error' => $error,
                    'dpi' => $dpimsg
        ]);
    }
}
