<?php
namespace Magemonkeys\Wallpaper\Block;

class Images extends \Magento\Framework\View\Element\Template
{

	protected $_mediaDirectory;
    protected $_fileUploaderFactory;
    public $_storeManager;
    protected $_helperData;


	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		 array $data = array(),
		\Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magemonkeys\Wallpaper\Helper\Data $helperData
	)
	{
		$this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_storeManager = $storeManager;
        $this->_helperData = $helperData;
		parent::__construct($context,$data);
	}

	public function getImagesData()
	{
		$sessionValue = $this->_helperData->getWallpaperSession()->getData();		

		if(!empty($sessionValue['wallpaper_value'])){
			$sessionValues = $this->_helperData->getWallpaperSession()->getData('wallpaper_value');
		}else {
    		$sessionValues = $this->_helperData->getWallpaperSession()->setData('wallpaper_value',array());
		}
		
		return $sessionValues;
	}
}
?>