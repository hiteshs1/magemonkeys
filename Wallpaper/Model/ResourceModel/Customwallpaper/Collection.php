<?php
namespace Magemonkeys\Wallpaper\Model\ResourceModel\Customwallpaper;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'magemonkeys_wallpaper_custom_collection';
	protected $_eventObject = 'customwallpaper_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Magemonkeys\Wallpaper\Model\Customwallpaper', 'Magemonkeys\Wallpaper\Model\ResourceModel\Customwallpaper');
	}

}
