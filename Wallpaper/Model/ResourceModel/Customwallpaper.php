<?php
namespace Magemonkeys\Wallpaper\Model\ResourceModel;

class Customwallpaper extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	)
	{
		parent::__construct($context);
	}
	
	protected function _construct()
	{
		$this->_init('wallpaper_custom', 'id');
	}
	
}
