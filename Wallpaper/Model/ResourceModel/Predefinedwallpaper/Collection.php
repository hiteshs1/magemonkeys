<?php
namespace Magemonkeys\Wallpaper\Model\ResourceModel\Predefinedwallpaper;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'magemonkeys_wallpaper_predefined_collection';
	protected $_eventObject = 'predefinedwallpaper_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Magemonkeys\Wallpaper\Model\Predefinedwallpaper', 'Magemonkeys\Wallpaper\Model\ResourceModel\Predefinedwallpaper');
	}

}
