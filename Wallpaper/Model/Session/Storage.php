<?php
namespace Magemonkeys\Wallpaper\Model\Session;

class Storage extends \Magento\Framework\Session\Storage
{
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        $namespace = 'wallpapersession',
        array $data = []
    ) {
        parent::__construct($namespace, $data);
    }
}
?>