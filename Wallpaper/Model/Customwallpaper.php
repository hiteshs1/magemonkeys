<?php
namespace Magemonkeys\Wallpaper\Model;

class Bulklist extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'wallpaper_custom';

	protected $_cacheTag = 'wallpaper_custom';

	protected $_eventPrefix = 'wallpaper_custom';

	protected function _construct()
	{
		$this->_init('Magemonkeys\Wallpaper\Model\ResourceModel\Customwallpaper');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}