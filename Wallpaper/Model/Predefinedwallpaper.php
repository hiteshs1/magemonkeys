<?php
namespace Magemonkeys\Wallpaper\Model;

class Predefinedwallpaper extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'wallpaper_predefined';

	protected $_cacheTag = 'wallpaper_predefined';

	protected $_eventPrefix = 'wallpaper_predefined';

	protected function _construct()
	{
		$this->_init('Magemonkeys\Wallpaper\Model\ResourceModel\Predefinedwallpaper');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}
