<?php
namespace Magemonkeys\Wallpaper\Plugin\Cart;
class EditPlugin
{
    /*
    *   Override cart/item/renderer/actions/edit.phtml file
    *   \Magento\Checkout\Block\Cart\Item\Renderer\Actions\Edit $subject
    *   $result
    */
    
    public function afterGetTemplate(\Magento\Checkout\Block\Cart\Item\Renderer\Actions\Edit $subject,  $result) 
    { 
        return 'Magemonkeys_Wallpaper::cart/item/renderer/actions/edit.phtml';
    }

}