<?php
namespace Magemonkeys\ShowHomepageCategory\Block\Category;

class HomepageCategory extends \Magento\Framework\View\Element\Template
{

    protected $_categoryCollectionFactory;

    protected $_categoryFactory;
 
    protected $_categoryHelper;
  
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Helper\Category $categoryHelper,
        array $data = []
    ) {
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_categoryFactory = $categoryFactory;
        $this->_categoryHelper = $categoryHelper;
        parent::__construct($context, $data);
    }


    public function getSubCategoryModel($id)
    {
        $_category = $this->_categoryFactory->create();
        $_category->load($id);
        
        return $_category;
    }
  
    public function getCategoryCollection($isActive = true, $level = false, $sortBy = false, $pageSize = false) {
        $collection = $this->_categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');                        
        $collection->addFieldToFilter('show_homepage_category','1'); 
        $collection->getSelect()->limit(12);      
  
        // select only active categories
        if ($isActive) {
            $collection->addIsActiveFilter();
        }
  
        // select categories of certain level
        if ($level) {
            $collection->addLevelFilter($level);
        }
  
        // sort categories by some value
        if ($sortBy) {
            $collection->addOrderField($sortBy);
        }
  
        // set pagination
        if ($pageSize) {
            $collection->setPageSize($pageSize); 
        } 
  
        return $collection;
    }
  
    public function getStoreCategories($sorted = false, $asCollection = false, $toLoad = true) {
        return $this->_categoryHelper->getStoreCategories($sorted = false, $asCollection = false, $toLoad = true);
    }
}
