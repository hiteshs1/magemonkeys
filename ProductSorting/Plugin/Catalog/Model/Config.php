<?php

namespace Magemonkeys\ProductSorting\Plugin\Catalog\Model;

class Config
{
public function afterGetAttributeUsedForSortByArray(\Magento\Catalog\Model\Config $catalogConfig, $options)
    {
       
        unset($options['name']);
        unset($options['price']);
        unset($options['position']);
        unset($options['created_at']);

        //New sorting options
        $customOption['name_A_to_Z'] = __( 'Product Name - A-Z' );
        $customOption['name_Z_to_A'] = __( 'Product Name - Z-A' );
        $customOption['low_to_high'] = __( 'Price - Low To High' );            
        $customOption['high_to_low'] = __( 'Price - High To Low' );
        $customOption['newest_desc'] = __( 'Latest Product' );
        $customOption['newest_asc'] = __( 'Oldest Product' );

        //Merge default sorting options with custom options
        $options = array_merge($customOption, $options);

        return $options;
    }
}

/* run below query when sory by newest productg asc and desc */
/*
select attribute_id from eav_attribute where attribute_code = 'created_at' and entity_type_id=4;

update eav_attribute set frontend_label = 'Created At' where attribute_id=112;

update catalog_eav_attribute set used_for_sort_by = 1 where attribute_id=112;
*/