<?php
namespace Magemonkeys\ProductSorting\Plugin\Catalog\Block;

class Toolbar extends \Magento\Catalog\Block\Product\ProductList\Toolbar
{

    /**
     * Set collection to pager
     *
     * @param \Magento\Framework\Data\Collection $collection
     * @return \Magento\Catalog\Block\Product\ProductList\Toolbar
     */
    public function setCollection($collection)
    {
        $this->_collection = $collection;

        $this->_collection->setCurPage($this->getCurrentPage());

        // we need to set pagination only if passed value integer and more that 0
        $limit = (int)$this->getLimit();
        if ($limit) {
            $this->_collection->setPageSize($limit);
        }  
        
        if ($this->getCurrentOrder()) {            
                if ($this->getCurrentOrder() == 'name_Z_to_A') {
                    $this->_collection->setOrder('name', 'desc');
                }elseif ($this->getCurrentOrder() == 'name_A_to_Z') {
                    $this->_collection->setOrder('name', 'asc');
                }elseif ($this->getCurrentOrder() == 'high_to_low') {
                    $this->_collection->setOrder('price', 'desc');
                }elseif ($this->getCurrentOrder() == 'low_to_high') {
                    $this->_collection->setOrder('price', 'asc');
                }elseif ($this->getCurrentOrder() == 'newest_desc') {                    
                    $this->_collection->setOrder('created_at', 'desc');                                        
                }elseif ($this->getCurrentOrder() == 'newest_asc') {
                    $this->_collection->setOrder('created_at', 'asc');                                                            
                }else{
                    $this->_collection->addAttributeToSort(
                        $this->getCurrentOrder(),
                        $this->getCurrentDirection()
                    );
                }
        }
        return $this;
    }

}