<?php


namespace Grid\Crud\Controller\Adminhtml\Grid;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\Filesystem;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Manager\Grid\Model\GridFactory
     */
    var $gridFactory;

	protected $uploaderFactory;
    protected $adapterFactory;
    protected $filesystem;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Manager\Grid\Model\GridFactory $gridFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Grid\Crud\Model\GridFactory $gridFactory,
        UploaderFactory $uploaderFactory,
        AdapterFactory $adapterFactory,
        Filesystem $filesystem
    ) {
        parent::__construct($context);
        $this->gridFactory = $gridFactory;
		$this->uploaderFactory = $uploaderFactory;
        $this->adapterFactory = $adapterFactory;
        $this->filesystem = $filesystem;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
		
		$rowData = $this->gridFactory->create();
		
		
		if($_FILES['image']['name'] == ''){
            $data['image'] = '';
			//$this->getRequest()->setPostValue('image','');
        }
		
        if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
            try{
                $uploaderFactory = $this->uploaderFactory->create(['fileId' => 'image']);
                $uploaderFactory->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                $imageAdapter = $this->adapterFactory->create();
                $uploaderFactory->addValidateCallback('image',$imageAdapter,'validateUploadFile');
                $uploaderFactory->setAllowRenameFiles(true);
                $uploaderFactory->setFilesDispersion(true);
                $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
                $destinationPath = $mediaDirectory->getAbsolutePath('grid/crud');
                
                $result = $uploaderFactory->save($destinationPath);
                if (!$result) {
                    throw new LocalizedException(
                        __('File cannot be saved to path: $1', $destinationPath)
                    );
                }
                $imagePath = 'grid/crud'.$result['file'];
                $data['image'] = $imagePath;
            } catch (\Exception $e) {
                echo 'test';exit;
            }
        }


    	
		//echo '<pre>';print_r($data);exit;
        $rowData->setData($data);
		if (isset($data['id'])) {
            $rowData->setEntityId($data['id']);
        }
		if($rowData->save()){
            $this->messageManager->addSuccess(__('Row data has been successfully saved.'));
        }else{
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->_redirect('grid/grid/index');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Grid_Crud::save');
    }
}
