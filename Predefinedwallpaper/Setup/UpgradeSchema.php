<?php
namespace Magemonkeys\Predefinedwallpaper\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{	
	
	public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context ) {
		$installer = $setup;

		$installer->startSetup();

		if(version_compare($context->getVersion(), '2.0.1', '<')) {
				/*$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$salesSetup = $objectManager->create('Magento\Sales\Setup\SalesSetup');				
				$quoteSetup = $objectManager->create('Magento\Quote\Setup\QuoteSetup');				
                $quoteSetup->addAttribute('quote_item', 'custom_image', ['type' =>'varchar']);*/

				$connection = $installer->getConnection();

		        if ($connection->tableColumnExists('sales_order_item', 'custom_image') === false) {
		            $connection->addColumn(
		                    $setup->getTable('sales_order_item'),
		                    'custom_image',
		                    [
		                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
		                        'length' => 0,
		                        'comment' => 'custom image'
		                    ]
		                );
		        }

		        if ($connection->tableColumnExists('quote_item', 'custom_image') === false) {
		            $connection->addColumn(
		                    $setup->getTable('quote_item'),
		                    'custom_image',
		                    [
		                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
		                        'length' => 0,
		                        'comment' => 'custom image'
		                    ]
		                );
		        }
	    }



		$installer->endSetup();
	}
}
