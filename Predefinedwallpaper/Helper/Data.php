<?php
namespace Magemonkeys\Predefinedwallpaper\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

	protected $_mediaDirectory;
    protected $_fileUploaderFactory;
    public $_storeManager;
    protected $scopeConfig;
    protected $_helperData;
    protected $PredefinedWallpaperSession;


	public function __construct(
		\Magento\Framework\App\Helper\Context $context,
		\Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Session\SessionManagerInterface $coreSession
	)
	{
		$this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->PredefinedWallpaperSession = $coreSession;
		parent::__construct($context);
	}

    public function StandardPrice()
    {   
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        return $this->scopeConfig->getValue('predefinedwallpaper/general/standard', $storeScope);
    }
    public function PremiumPrice()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        return $this->scopeConfig->getValue('predefinedwallpaper/general/premium', $storeScope);
    }
    public function StandardTooltip()
    {   
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        return $this->scopeConfig->getValue('predefinedwallpaper/general/standard_tooltip', $storeScope);
    }
    public function PremiumTooltip()
    {   
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        return $this->scopeConfig->getValue('predefinedwallpaper/general/premium_tooltip', $storeScope);
    }

    public function getPredefinedWallpaperSession()
    {   
        $this->PredefinedWallpaperSession->start();
        return $this->PredefinedWallpaperSession;
    }
}
?>