<?php

namespace Magemonkeys\Predefinedwallpaper\Plugin\Quote;

class CutomImageToOrderItem
{
	public function aroundConvert(
        \Magento\Quote\Model\Quote\Item\ToOrderItem $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote\Item\AbstractItem $item,
        $additional = []
    ) {
        /** @var $orderItem Item */
        $orderItem = $proceed($item, $additional);
        $orderItem->setCustomImage($item->getCustomImage());
        return $orderItem;
      }

}