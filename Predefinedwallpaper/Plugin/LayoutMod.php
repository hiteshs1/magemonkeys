<?php
namespace Magemonkeys\Predefinedwallpaper\Plugin;

class LayoutMod
{
    public function beforeInitProductLayout(
          \Magento\Catalog\Helper\Product\View $subject,
          \Magento\Framework\View\Result\Page $resultPage, 
          \Magento\Catalog\Model\Product $product, 
          $params = null
    ) {
        //$resultPage->addPageLayoutHandles(['attribute_set_name' => 'Predefined']);
        $resultPage->addPageLayoutHandles(['attribute_set_id' => $product->getAttributeSetId()]);
        return [$resultPage, $product, $params];
    }
}
?>