<?php

namespace Magemonkeys\Predefinedwallpaper\Plugin\Minicart;

class Image

{
	protected $_helperData;    

	public function __construct(
		\Magemonkeys\Predefinedwallpaper\Helper\Data $helperData        
	) {        
        $this->_helperData = $helperData;        
    }

    public function aroundGetItemData($subject, $proceed, $item)
    {

        $result = $proceed($item);       
        $sessionValue = $this->_helperData->getPredefinedWallpaperSession()->getData('crop_image_value');          
        //print_r($sessionValue);
        if(!empty($sessionValue)){    
            if($sessionValue['item_id'] == $item->getProductId()){
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $storeManager = $objectManager->create('Magento\Store\Model\StoreManagerInterface'); 
                $currentStore = $storeManager->getStore();
                $mediaUrl = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                $result['product_image']['src'] = $mediaUrl.'Predefinedwallpaper/'.$sessionValue['src'];
            }
            else{
                $result['product_image']['src'];
            }
        }else{
            $result['product_image']['src'];
        }

        return $result;
    }

}