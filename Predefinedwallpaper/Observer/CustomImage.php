<?php
	
	namespace Magemonkeys\Predefinedwallpaper\Observer;

	use Magento\Framework\Event\ObserverInterface;
	use Magento\Framework\App\RequestInterface;

	class CustomImage implements ObserverInterface
	{
		protected $_helperData;    

		public function __construct(
			\Magemonkeys\Predefinedwallpaper\Helper\Data $helperData		
		) {        
	        $this->_helperData = $helperData;        
	    }

		public function execute(\Magento\Framework\Event\Observer $observer) {
			$sessionValue = $this->_helperData->getPredefinedWallpaperSession()->getData('crop_image_value'); 
			if(!empty($sessionValue))
            {       			
				$item = $observer->getEvent()->getData('quote_item');							
				$item = ( $item->getParentItem() ? $item->getParentItem() : $item );			
				if($sessionValue['item_id'] == $item->getProductId()){			
					$item->setCustomImage($sessionValue['src']);		
					$price = $sessionValue['price'];
					$item->setOriginalCustomPrice($price);
				}				
				$item->getProduct()->setIsSuperMode(true);
			}
		}

	}