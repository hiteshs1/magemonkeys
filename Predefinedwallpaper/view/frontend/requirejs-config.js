var config = {
    paths: {
        jqueryCrop1: 'Magemonkeys_Predefinedwallpaper/js/jquery',
        rcrop1: 'Magemonkeys_Predefinedwallpaper/js/rcrop.min'
    },
    shim: {
        rcrop1: {
            deps: ['jquery']
        }
    }
};
