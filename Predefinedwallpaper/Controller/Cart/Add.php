<?php
namespace Magemonkeys\Predefinedwallpaper\Controller\Cart;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Controller for processing add to cart action.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Add extends \Magento\Checkout\Controller\Cart
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    protected $serializer;

    protected $_helperData;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     * @param CustomerCart $cart
     * @param ProductRepositoryInterface $productRepository
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        CustomerCart $cart,
        \Magento\Framework\Serialize\SerializerInterface $serializer,
        ProductRepositoryInterface $productRepository,
        \Magemonkeys\Predefinedwallpaper\Helper\Data $helperData
    ) {
        $this->productRepository = $productRepository;
        $this->_helperData = $helperData;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);       
        $this->serializer = $serializer;
        parent::__construct(
            $context,
            $scopeConfig,
            $checkoutSession,
            $storeManager,
            $formKeyValidator,
            $cart
        );
    }

   /**
     * Initialize product instance from request data
     *
     * @return \Magento\Catalog\Model\Product|false
     */
    protected function _initProduct()
    {
        $productId = (int)$this->getRequest()->getParam('product');
        if ($productId) {
            $storeId = $this->_objectManager->get(
                \Magento\Store\Model\StoreManagerInterface::class
            )->getStore()->getId();
            try {
                return $this->productRepository->getById($productId, false, $storeId);
            } catch (NoSuchEntityException $e) {
                return false;
            }
        }
        return false;
    }

    /**
     * Add product to shopping cart action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute()
    {

        $params = $this->getRequest()->getParams();                
        if(isset($params['height'])){        
            $crop_height = $params['crop_height'];
            $crop_width = $params['crop_width'];
            $crop_x = $params['crop_x'];
            $crop_y = $params['crop_y'];
            $proimg = $params['proimg'];
            $item = $params['item'];
            $item = $params['item'];
            $price = $params['price'];            
        }
        $mediaRootDir = $this->_mediaDirectory->getAbsolutePath();        

        try {
            if (isset($params['qty'])) {
                $filter = new \Zend_Filter_LocalizedToNormalized(
                    ['locale' => $this->_objectManager->get(
                        \Magento\Framework\Locale\ResolverInterface::class
                    )->getLocale()]
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                return $this->goBack();
            }

            /** crop image**/


            /*if((isset($params['full']) && isset($params['flip']))){
                $uniqid = uniqid();
                $proimg = $params['proimg'];
                $newFileName = $uniqid.'_'.$item.'.jpeg';
                $filePath = $proimg;
                $_fileName = $mediaRootDir .'Predefinedwallpaper/'. $newFileName;
                $newFullPathOfImage = $_fileName;
                copy($filePath, $_fileName);
                $im = imagecreatefromjpeg($filePath);
                imageflip($im, IMG_FLIP_HORIZONTAL);
                imagejpeg($im, $newFullPathOfImage);
                imagedestroy($im);

                $sessionValue = $this->_helperData->getPredefinedWallpaperSession()->getData('crop_image_value');
                $array = array('src'=>$newFileName,'item_id'=>$item, 'full'=>'1');
                $sessionValue = $array;

                $this->_helperData->getPredefinedWallpaperSession()->setData('crop_image_value',$sessionValue);
            }*/

            

            if((isset($params['height']) && !isset($params['full']))){                            
                $uniqid = uniqid();
                $newFileName = $uniqid.'_'.$item.'.jpeg';
                $filePath = $proimg;                
                $directoryName = $mediaRootDir.'Predefinedwallpaper';
                if(!is_dir($directoryName)){
                    mkdir($directoryName, 0775);
                }
                $_fileName = $mediaRootDir .'Predefinedwallpaper/'. $newFileName;
                $newFullPathOfImage = $_fileName;
                copy($filePath, $_fileName);

                $im = imagecreatefromjpeg($filePath);
                $dst_r = ImageCreateTrueColor( $crop_width, $crop_height );            
                imagecopyresampled($dst_r, $im, 0, 0, $crop_x, $crop_y, $crop_width, $crop_height, $crop_width,$crop_height);
                if(isset($params['flip'])){
                    imageflip($dst_r, IMG_FLIP_HORIZONTAL);
                }
                imagejpeg($dst_r, $newFullPathOfImage);
                imagedestroy($im);
                
                $sessionValue = $this->_helperData->getPredefinedWallpaperSession()->getData('crop_image_value');
                
                $array = array('src'=>$newFileName,'item_id'=>$item, 'price'=>$price);
                $sessionValue = $array;

                $this->_helperData->getPredefinedWallpaperSession()->setData('crop_image_value',$sessionValue);                
                //$this->_helperData->getPredefinedWallpaperSession()->unsCropImageValue();
                //print_r($sessionValue);
            }

            

            if(isset($params['full'])){
                $uniqid = uniqid();
                $newFileName = $uniqid.'_'.$item.'.jpeg';
                $filePath = $proimg;
                $_fileName = $mediaRootDir .'Predefinedwallpaper/'. $newFileName;
                $newFullPathOfImage = $_fileName;
                copy($filePath, $_fileName);

                $sessionValue = $this->_helperData->getPredefinedWallpaperSession()->getData('crop_image_value');
                $array = array('src'=>$newFileName,'item_id'=>$item, 'price'=>$price);
                $sessionValue = $array;

                $this->_helperData->getPredefinedWallpaperSession()->setData('crop_image_value',$sessionValue);
            }


            /** crop image end **/ 

            if((isset($params['full']))){
                $additionalOptions['wallpaper_full_size'] = [
                    'label' => __('Image area'),
                    'value' => 'Full Image',
                ];
            }

            if((isset($params['flip']))){
                $additionalOptions['wallpaper_flip'] = [
                    'label' => __('Image shape'),
                    'value' => 'Flip',
                ];
            }

            if((isset($params['wallpaper_size'])) && $params['wallpaper_size']!=''){
                $additionalOptions['wallpaper_size'] = [
                    'label' => __('Wallpaper Size'),
                    'value' => $params['wallpaper_size'],
                ];
            }

            if((isset($params['package'])) && $params['package']=='standard'){
                $additionalOptions['package_standard'] = [
                    'label' => __('Chosen material'),
                    'value' => 'Wallpaper - Standard (Wallpaper paste included)',
                ];
            }
            if((isset($params['package'])) && $params['package']=='premium'){
                $additionalOptions['package_premium'] = [
                    'label' => __('Chosen material'),
                    'value' => 'Wallpaper - Premium (Wallpaper paste included)',
                ];
            }
            if((isset($params['height']) && isset($params['width'])))
            {
                $additionalOptions['height_width'] = [
                    'label' => __('Enter the desired format'),
                    'value' => $params['height'] . ' x ' . $params['width'] . ' ' . __('(height x width)'),
                ];
                
                $product->addCustomOption('additional_options', $this->serializer->serialize($additionalOptions));
            }

            $this->cart->addProduct($product, $params);
            if (!empty($related)) {
                $this->cart->addProductsByIds(explode(',', $related));
            }

            

            $this->cart->save();

            /**
             * @todo remove wishlist observer \Magento\Wishlist\Observer\AddToCart
             */
            $this->_eventManager->dispatch(
                'checkout_cart_add_product_complete',
                ['product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
            );

            if (!$this->_checkoutSession->getNoCartRedirect(true)) {
                if (!$this->cart->getQuote()->getHasError()) {
                    if ($this->shouldRedirectToCart()) {
                        $message = __(
                            'You added %1 to your shopping cart.',
                            $product->getName()
                        );
                        $this->messageManager->addSuccessMessage($message);
                    } else {
                        $this->messageManager->addComplexSuccessMessage(
                            'addCartSuccessMessage',
                            [
                                'product_name' => $product->getName(),
                                'cart_url' => $this->getCartUrl(),
                            ]
                        );
                    }
                }
                return $this->goBack(null, $product);
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            if ($this->_checkoutSession->getUseNotice(true)) {
                $this->messageManager->addNoticeMessage(
                    $this->_objectManager->get(\Magento\Framework\Escaper::class)->escapeHtml($e->getMessage())
                );
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->messageManager->addErrorMessage(
                        $this->_objectManager->get(\Magento\Framework\Escaper::class)->escapeHtml($message)
                    );
                }
            }

            $url = $this->_checkoutSession->getRedirectUrl(true);

            if (!$url) {
                $url = $this->_redirect->getRedirectUrl($this->getCartUrl());
            }

            return $this->goBack($url);
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage(
                $e,
                __('We can\'t add this item to your shopping cart right now.')
            );
            $this->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);
            return $this->goBack();
        }
    }

    /**
     * Resolve response
     *
     * @param string $backUrl
     * @param \Magento\Catalog\Model\Product $product
     * @return $this|\Magento\Framework\Controller\Result\Redirect
     */
    protected function goBack($backUrl = null, $product = null)
    {
        if (!$this->getRequest()->isAjax()) {
            return parent::_goBack($backUrl);
        }

        $result = [];

        if ($backUrl || $backUrl = $this->getBackUrl()) {
            $result['backUrl'] = $backUrl;
        } else {
            if ($product && !$product->getIsSalable()) {
                $result['product'] = [
                    'statusText' => __('Out of stock')
                ];
            }
        }

        $this->getResponse()->representJson(
            $this->_objectManager->get(\Magento\Framework\Json\Helper\Data::class)->jsonEncode($result)
        );
    }

    /**
     * Returns cart url
     *
     * @return string
     */
    private function getCartUrl()
    {
        return $this->_url->getUrl('checkout/cart', ['_secure' => true]);
    }

    /**
     * Is redirect should be performed after the product was added to cart.
     *
     * @return bool
     */
    private function shouldRedirectToCart()
    {
        return $this->_scopeConfig->isSetFlag(
            'checkout/cart/redirect_to_cart',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
?>