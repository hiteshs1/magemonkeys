<?php

declare(strict_types=1);

namespace Magemonkeys\ExportProductCsv\Controller\Adminhtml\Export;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\MessageQueue\PublisherInterface;
use Magento\ImportExport\Controller\Adminhtml\Export as ExportController;
use Magento\ImportExport\Model\Export as ExportModel;
use Magento\ImportExport\Model\Export\Entity\ExportInfoFactory;


class Export extends \Magento\ImportExport\Controller\Adminhtml\Export\Export
{
    private $sessionManager;
   
    public function execute()
    {
        if ($this->getRequest()->getPost(ExportModel::FILTER_ELEMENT_GROUP)) {
            try {                

                $params = $this->getRequest()->getParams();
                $model = $this->_objectManager->create(\Magento\ImportExport\Model\Export::class);
                $model->setData($this->getRequest()->getParams());
                //$this->sessionManager->writeClose();

                return $this->fileFactory->create(
                    $model->getFileName(),
                    $model->export(),
                    \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR,
                    $model->getContentType()
                );

                /** @var ExportInfoFactory $dataObject */
                $dataObject = $this->exportInfoFactory->create(
                    $params['file_format'],
                    $params['entity'],
                    $params['export_filter']
                );

                $this->messagePublisher->publish('import_export.export', $dataObject);
                $this->messageManager->addSuccessMessage(
                    __('Message is added to queue, wait to get your file soon')
                );
            } catch (\Exception $e) {
                $this->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);
                $this->messageManager->addErrorMessage(__('Please correct the data sent value.'));
            }
        } else {
            $this->messageManager->addErrorMessage(__('Please correct the data sent value.'));
        }
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('adminhtml/*/index');
        return $resultRedirect;
    }    
}
