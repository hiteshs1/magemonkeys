<?php
namespace Magemonkeys\ExportProductCsv\Block\Adminhtml\Export;

use Magento\Eav\Model\Entity\Attribute;
use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Framework\App\ObjectManager;
use Magento\ImportExport\Model\ResourceModel\Export\AttributeGridCollectionFactory;

class Filter extends \Magento\ImportExport\Block\Adminhtml\Export\Filter
{   
   
    protected function _getSelectHtmlWithValue(Attribute $attribute, $value)
    {
        if ($attribute->getFilterOptions()) {
            $options = [];

            foreach ($attribute->getFilterOptions() as $optionValue => $label) {
                $options[] = ['value' => $optionValue, 'label' => $label];
            }
        } else {
            $options = $attribute->getSource()->getAllOptions(false);
        }
        $options = $options ?? [];
        if ($size = count($options)) {
            // add empty value option
            $firstOption = reset($options);

            if ('' === $firstOption['value']) {
                $options[key($options)]['label'] = '';
            } else {
                array_unshift($options, ['value' => '', 'label' => __('-- Not Selected --')]);
            }
            $arguments = [
                'name' => $this->getFilterElementName($attribute->getAttributeCode()),
                'id' => $this->getFilterElementId($attribute->getAttributeCode()),
                'class' => 'admin__control-select select select-export-filter',
            ];
            /** @var $selectBlock \Magento\Framework\View\Element\Html\Select */
            $selectBlock = $this->_layout->createBlock(
                \Magento\Framework\View\Element\Html\Select::class,
                '',
                ['data' => $arguments]
            );
            return $selectBlock->setOptions($options)->setValue($value !== '' ? $value : null)->getHtml();
        } else {
            return __('We can\'t filter an attribute with no attribute options.');
        }
    }    
}
