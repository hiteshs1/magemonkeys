<?php
namespace Magemonkeys\Onsalewidget\Block\Widget;

use Zend_Db_Expr;

class Onsaleproduct extends \Magento\Framework\View\Element\Template implements \Magento\Widget\Block\BlockInterface
{
    protected $_template = 'widget/onsaleproduct.phtml';
        /**
    * Default value for products count that will be shown
    */
    const DEFAULT_PRODUCTS_COUNT = 10;
    const DEFAULT_IMAGE_WIDTH = 150;
    const DEFAULT_IMAGE_HEIGHT = 150;

    /**
     * @var DateTime
     */
    protected $_date;

    /**
    * Products count
    *
    * @var int
    */
    protected $productsCount;
    /**
    * @var \Magento\Framework\App\Http\Context
    */
    protected $httpContext;
    protected $resourceCollection;
    protected $productloader;
 
    /**
    * Catalog product visibility
    *
    * @var \Magento\Catalog\Model\Product\Visibility
    */
    protected $catalogProductVisibility;
       
    /**
    * Image helper
    *
    * @var Magento\Catalog\Helper\Image
    */
    protected $imageHelper;
    /**
    * @var \Magento\Checkout\Helper\Cart
    */
    protected $cartHelper;
    /**
    * @param Context $context
    * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    * @param \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility
    * @param \Magento\Framework\App\Http\Context $httpContext
    * @param array $data
    */

   public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $resourceCollection,
        \Magento\Catalog\Model\ProductFactory $productloader,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        array $data = []
    ) {
        $this->imageHelper = $context->getImageHelper();
        $this->productloader = $productloader;
        $this->cartHelper = $context->getCartHelper();
        $this->resourceCollection = $resourceCollection;
        $this->_date = $dateTime;
        parent::__construct($context, $data);
    }
     /**
    * Image helper Object
    */
     public function imageHelperObj(){
        return $this->imageHelper;
    }
    /**
    * get featured product collection
    */
   public function getOnsaleProduct(){
    $limit = $this->getProductLimit();
     $resourceCollection = $this->resourceCollection->create();
     
        $resourceCollection->addAttributeToFilter(
                'special_from_date',
                ['date' => true, 'to' => $this->getEndOfDayDate()],
                'left'
            )->addAttributeToFilter(
                'special_to_date',
                ['or' => [0 => ['date' => true, 'from' => $this->getStartOfDayDate()],
                          1 => ['is' => new Zend_Db_Expr('null')],]],
                'left'
            )->addAttributeToSort(
                'news_from_date',
                'desc'
            )->addStoreFilter($this->getStoreId());

     $resourceCollection->setPageSize($limit);

     //echo $resourceCollection->getSelect();


     return $resourceCollection;
   }

   /**
     * Get End of Day Date
     *
     * @return string
     */
    public function getEndOfDayDate()
    {
        return $this->_date->date(null, '23:59:59');
    }

    /**
     * Get Start of Day Date
     *
     * @return string
     */
    public function getStartOfDayDate()
    {
        return $this->_date->date(null, '0:0:0');
    }
    
    /**
    * Get the configured limit of products
    * @return int
    */
    public function getProductLimit() {
     if($this->getData('productcount')==''){
         return DEFAULT_PRODUCTS_COUNT;
     }
        return $this->getData('productcount');
    }
    /**
    * Get the widht of product image
    * @return int
    */
    public function getProductimagewidth() {
     if($this->getData('imagewidth')==''){
         return DEFAULT_IMAGE_WIDTH;
     }
        return $this->getData('imagewidth');
    }
    /**
    * Get the height of product image
    * @return int
    */
    public function getProductimageheight() {
     if($this->getData('imageheight')==''){
         return DEFAULT_IMAGE_HEIGHT;
     }
        return $this->getData('imageheight');
    }
    /**
    * Get the add to cart url
    * @return string
    */
    public function getAddToCartUrl($product, $additional = [])
    {
         return $this->cartHelper->getAddUrl($product, $additional);
    }
    /**
    * Return HTML block with price
    *
    * @param \Magento\Catalog\Model\Product $product
    * @param string $priceType
    * @param string $renderZone
    * @param array $arguments
    * @return string
    * @SuppressWarnings(PHPMD.NPathComplexity)
    */
    public function getProductPriceHtml(
        \Magento\Catalog\Model\Product $product,
        $priceType = null,
        $renderZone = \Magento\Framework\Pricing\Render::ZONE_ITEM_LIST,
        array $arguments = []
    ) {
        if (!isset($arguments['zone'])) {
            $arguments['zone'] = $renderZone;
        }
        $arguments['zone'] = isset($arguments['zone'])
            ? $arguments['zone']
            : $renderZone;
        $arguments['price_id'] = isset($arguments['price_id'])
            ? $arguments['price_id']
            : 'old-price-' . $product->getId() . '-' . $priceType;
        $arguments['include_container'] = isset($arguments['include_container'])
            ? $arguments['include_container']
            : true;
        $arguments['display_minimal_price'] = isset($arguments['display_minimal_price'])
            ? $arguments['display_minimal_price']
            : true;
            /** @var \Magento\Framework\Pricing\Render $priceRender */
        $priceRender = $this->getLayout()->getBlock('product.price.render.default');
        $price = '';
        if ($priceRender) {
            $price = $priceRender->render(
                \Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE,
                $product,
                $arguments
            );
        }
        return $price;
    }
    public function loadProduct($id)
    {
        return $this->productloader->create()->load($id);
    }
}