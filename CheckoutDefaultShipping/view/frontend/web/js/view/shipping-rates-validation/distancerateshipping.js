define([
    'uiComponent',
    'Magento_Checkout/js/model/shipping-rates-validator',
    'Magento_Checkout/js/model/shipping-rates-validation-rules',
    '../../model/shipping-rates-validator/distancerateshipping',
    '../../model/shipping-rates-validation-rules/distancerateshipping'
], function (Component,
             defaultShippingRatesValidator,
             defaultShippingRatesValidationRules,
             customShippingRatesValidator,
             customShippingRatesValidationRules) {
    'use strict';
 
    defaultShippingRatesValidator.registerValidator('distancerateshipping', customShippingRatesValidator);
    defaultShippingRatesValidationRules.registerRules('distancerateshipping', customShippingRatesValidationRules);
 
    return Component;
});