<?php
namespace Magemonkeys\Customergrouphideprice\Plugin;
use Magento\Catalog\Model\Product;

class Hidecartbutton
{
	protected $_customerSession;
	protected $_customerGroupCollection;

	public function __construct(	    
	    \Magento\Customer\Model\Session $customerSession,
	    \Magento\Customer\Model\Group $customerGroupCollection
	) {
	    $this->_customerSession = $customerSession;	   
	    $this->_customerGroupCollection = $customerGroupCollection;
	}

	public function getCustomerGroup()
	{
        $currentGroupId = $this->_customerSession->getCustomer()->getGroupId();
        $collection = $this->_customerGroupCollection->load($currentGroupId); 
        $customer_group = $collection->getCustomerGroupCode();
        return $customer_group;
	}

    public function afterIsSaleable(Product $product)
    {
    	$cgroupname = $this->getCustomerGroup();
    	$attribute = $product->getResource()->getAttribute('prescription_info')->getFrontend()->getValue($product);
    	if ($attribute == 'Non Prescription' || $attribute == '') {
    		return 1;    		
    	}elseif($this->_customerSession->getCustomer()->getGroupId()==2){
			return 1;
    	}else{	
        	return 0;
        }
    }
}