<?php

namespace Magemonkeys\Customergrouphideprice\Plugin;

class HidePriceBox
{
	protected $_customerSession;
	protected $_customerGroupCollection;

	public function __construct(	    
	    \Magento\Customer\Model\Session $customerSession,
	    \Magento\Customer\Model\Group $customerGroupCollection
	) {
	    $this->_customerSession = $customerSession;	   
	    $this->_customerGroupCollection = $customerGroupCollection;
	}

	public function getCustomerGroup()
	{		
        $currentGroupId = $this->_customerSession->getCustomer()->getGroupId();
        $collection = $this->_customerGroupCollection->load($currentGroupId); 
        $customer_group = $collection->getCustomerGroupCode();
        return $customer_group;
	}


    function afterToHtml(\Magento\Catalog\Pricing\Render\FinalPriceBox $subject, $result)
    {
    	$cgroupname = $this->getCustomerGroup();
    	$attribute = $subject->getSaleableItem()->getResource()->getAttribute('prescription_info')->getFrontend()->getValue($subject->getSaleableItem());      	
		if ($attribute == 'Non Prescription' || $attribute == '') {
			return $result;
    	}elseif($this->_customerSession->getCustomer()->getGroupId()==2){
			return $result;	    		
    	}else{
        	//return '<div class="pre_product"><span class="price">Prescription Product</span></div>';
        	return '';
        }    		
    }
}