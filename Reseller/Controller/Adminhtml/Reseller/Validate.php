<?php 
namespace Magemonkeys\Reseller\Controller\Adminhtml\Reseller;

use Magento\Backend\App\Action;
use Magento\User\Model\ResourceModel\User\CollectionFactory as UserCollectionFactory;
use Magento\Store\Model\WebsiteFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class Validate extends Action
{
    protected $userCollectionFactory;
    protected $websiteFactory;
    protected $resultJsonFactory;

    public function __construct(
        Action\Context $context,
        UserCollectionFactory $userCollectionFactory,
        WebsiteFactory $websiteFactory,  // Inject the website factory
        JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->userCollectionFactory = $userCollectionFactory;
        $this->websiteFactory = $websiteFactory;
        $this->resultJsonFactory = $resultJsonFactory;
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $username = $this->getRequest()->getParam('username');
        $email = $this->getRequest()->getParam('email');
        $website = $this->getRequest()->getParam('website');

        $response = [
            'username_exists' => false,
            'email_exists' => false,
            'website_exists' => false
        ];

        // Check if the username exists
        if ($username) {
            $userCollection = $this->userCollectionFactory->create();
            $user = $userCollection->addFieldToFilter('username', $username)->getFirstItem();

            if ($user->getId()) {
                $response['username_exists'] = true;
                $response['username_message'] = 'Username already exists';
            } else {
                $response['username_message'] = 'Username is available';
            }
        }

        // Check if the email exists
        if ($email) {
            $userCollection = $this->userCollectionFactory->create();
            $userByEmail = $userCollection->addFieldToFilter('email', $email)->getFirstItem();

            if ($userByEmail->getId()) {
                $response['email_exists'] = true;
                $response['email_message'] = 'Email already exists';
            } else {
                $response['email_message'] = 'Email is available';
            }
        }

        // Check if the website/store exists
        if ($website) {
            $websiteExists = $this->checkWebsiteExists($website);
            if ($websiteExists) {
                $response['website_exists'] = true;
                $response['website_message'] = 'Website/Store already exists';
            } else {
                $response['website_message'] = 'Website/Store is available';
            }
        }

        return $result->setData($response);
    }

    // Function to check if the website or store exists in Magento
    protected function checkWebsiteExists($website)
    {
        // Load website by code or ID
        $website = $website.'_website';
        $websiteModel = $this->websiteFactory->create();
        $websiteInstance = $websiteModel->load($website, 'code'); // You can also use 'id'

        // If a website or store with this code exists, return true
        if ($websiteInstance->getId()) {
            return true;
        }

        return false;
    }
}
