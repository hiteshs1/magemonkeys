<?php
namespace Magemonkeys\Reseller\Controller\Adminhtml\Reseller;

use Magento\Framework\Controller\ResultFactory;

class Edit extends \Magento\Backend\App\Action {

    protected $resultPageFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }
    
    public function execute() {
       $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
       $resultPage->getConfig()->getTitle()->prepend(__('Edit Record'));
       return $resultPage;
    }
}