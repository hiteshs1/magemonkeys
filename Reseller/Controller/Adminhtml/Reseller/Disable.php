<?php
namespace Magemonkeys\Reseller\Controller\Adminhtml\Reseller;

use Magento\Backend\App\Action;
use Magemonkeys\Reseller\Model\Reseller;
use Magento\Framework\Registry;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Config\Model\ResourceModel\Config;
use Magento\User\Model\UserFactory;
use Magemonkeys\Reseller\Helper\Data;

class Disable extends \Magento\Backend\App\Action
{
    protected $resellerModel;    

    protected $_registry;
    protected $_userFactory;
    protected $configWriter;
    protected $helperData;

    public function __construct(
        Action\Context $context,
        Reseller $resellerModel,                
        Registry $registry,
        Config $configWriter,
        UserFactory $userFactory,
        Data $helperData
    ) {
        parent::__construct($context);
        $this->resellerModel = $resellerModel;        
        $this->_registry = $registry;
        $this->configWriter = $configWriter;
        $this->_userFactory = $userFactory;
        $this->helperData = $helperData;
    }
 
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');                      
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            $reseller = $this->resellerModel->load($id);
        }            
        try {
            $websiteId = $reseller['user_websiteid'];
            $storeId = $reseller['user_storeviewid'];
            $websiteSecureUrl = $reseller['user_websitesecureurl'];
            $websiteUnsecureUrl = $reseller['user_websiteunsecureurl'];
      
            $this->configWriter->deleteConfig('web/secure/base_link_url', ScopeInterface::SCOPE_WEBSITES, $websiteId);
            $this->configWriter->deleteConfig('web/secure/base_url', ScopeInterface::SCOPE_WEBSITES, $websiteId);
            $this->configWriter->deleteConfig('web/unsecure/base_link_url', ScopeInterface::SCOPE_WEBSITES, $websiteId);
            $this->configWriter->deleteConfig('web/unsecure/base_url', ScopeInterface::SCOPE_WEBSITES, $websiteId);

            $this->configWriter->deleteConfig('web/secure/base_link_url', ScopeInterface::SCOPE_STORES, $storeId);
            $this->configWriter->deleteConfig('web/secure/base_url', ScopeInterface::SCOPE_STORES, $storeId);
            $this->configWriter->deleteConfig('web/unsecure/base_link_url', ScopeInterface::SCOPE_STORES, $storeId);
            $this->configWriter->deleteConfig('web/unsecure/base_url', ScopeInterface::SCOPE_STORES, $storeId);

            //admin user deactive
            $userModel = $this->_userFactory->create()->load($reseller['adminuser_id']);
            $userModel->setIsActive(0)->save();                                

            // save data in table
            $this->resellerModel->setUserIsActive(0);
            $this->resellerModel->save();

            //cache flush
            $this->helperData->flushCache();
            
            $this->messageManager->addSuccess(__('The reseller website has been successfully disabled.'));                                
            return $resultRedirect->setPath('*/*/index');            

        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
        }            
        
        return $resultRedirect->setPath('*/*/');
    }
}