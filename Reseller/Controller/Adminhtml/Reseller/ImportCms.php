<?php
namespace Magemonkeys\Reseller\Controller\Adminhtml\Reseller;

use Magento\Backend\App\Action\Context;
use Magento\Cms\Model\BlockFactory;
use Magento\Cms\Model\PageFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\State;
use Magemonkeys\Reseller\Helper\Data;

class ImportCms extends \Magento\Backend\App\Action {

    protected $resultPageFactory;
    protected $blockFactory;
    protected $pageFactory;
    protected $storeManager;
    protected $appState;
    protected $helperData;

    public function __construct(
        Context $context,
        BlockFactory $blockFactory,
        PageFactory $pageFactory,
        StoreManagerInterface $storeManager,
        State $appState,
        Data $helperData        
    ) {
        parent::__construct($context);
        $this->blockFactory = $blockFactory;
        $this->pageFactory = $pageFactory;
        $this->storeManager = $storeManager;
        $this->appState = $appState;
        $this->helperData = $helperData;
    }
    
    public function execute() {
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            //Assign CMS Blocks respective website
            $this->assignCmsBlocks();

            //Assign CMS Pages respective website
            $this->assignCmsPages();

            //cache flush
            $this->helperData->flushCache();

            $this->messageManager->addSuccess(__('Assign Cms page and block successfully.'));
            return $resultRedirect->setPath('*/*/index');           

        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
        return $resultRedirect->setPath('*/*/');
    }

    public function assignCmsBlocks() {
        $websiteid = $this->getRequest()->getParam('user_websiteid');
        $basewebsiteid = $this->helperData->getConfigBaseWebsiteId();
        $sourceWebsiteId = $basewebsiteid; // ID of the original website
        $destinationWebsiteId = $websiteid; // ID of the new website

        $sourceStoreIds = $this->getStoreIdsByWebsite($sourceWebsiteId);
        $destinationStoreIds = $this->getStoreIdsByWebsite($destinationWebsiteId);

        // Load CMS blocks from the source website
        $cmsBlockCollection = $this->blockFactory->create()->getCollection()
            ->addStoreFilter($sourceStoreIds);

        // Assign these blocks to the new website's stores
        foreach ($cmsBlockCollection as $cmsBlock) {
            // Check if a block with the same identifier already exists in the destination stores
            $existingBlock = $this->blockFactory->create()->getCollection()
                ->addFieldToFilter('identifier', $cmsBlock->getIdentifier())
                ->addStoreFilter($destinationStoreIds)
                ->getFirstItem();

            if ($existingBlock->getId()) {
                // Block with the same identifier exists, so skip or update logic can go here
                continue; // Skip if already exists
            }

            // Get current store IDs and merge with destination store IDs
            $existingStoreIds = $cmsBlock->getStoreId();
            $newStoreIds = array_merge($existingStoreIds, $destinationStoreIds);
            $newStoreIds = array_unique($newStoreIds); // Ensure no duplicate store IDs

            // Update the block with new store assignments
            $cmsBlock->setStores($newStoreIds);
            $cmsBlock->save();
        }
    }


    public function assignCmsPages() {
        $websiteid = $this->getRequest()->getParam('user_websiteid');
        $basewebsiteid = $this->helperData->getConfigBaseWebsiteId();
        $sourceWebsiteId = $basewebsiteid; // ID of the original website
        $destinationWebsiteId = $websiteid; // ID of the new website

        $sourceStoreIds = $this->getStoreIdsByWebsite($sourceWebsiteId);
        $destinationStoreIds = $this->getStoreIdsByWebsite($destinationWebsiteId);

        // Load CMS pages from the source website
        $cmsPageCollection = $this->pageFactory->create()->getCollection()
            ->addStoreFilter($sourceStoreIds);

        // Assign these pages to the new website's stores
        foreach ($cmsPageCollection as $cmsPage) {
            // Check if a page with the same identifier already exists in the destination stores
            $existingPage = $this->pageFactory->create()->getCollection()
                ->addFieldToFilter('identifier', $cmsPage->getIdentifier())
                ->addStoreFilter($destinationStoreIds)
                ->getFirstItem();

            if ($existingPage->getId()) {
                // Page with the same identifier exists, so skip or update logic can go here
                continue; // Skip if already exists
            }

            // Get current store IDs and merge with destination store IDs
            $existingStoreIds = $cmsPage->getStoreId();
            $newStoreIds = array_merge($existingStoreIds, $destinationStoreIds);
            $newStoreIds = array_unique($newStoreIds); // Ensure no duplicate store IDs

            // Update the page with new store assignments
            $cmsPage->setStores($newStoreIds);
            $cmsPage->save();
        }
    }


    private function getStoreIdsByWebsite($websiteId)
    {
        $stores = $this->storeManager->getStores(true);
        $storeIds = [];

        foreach ($stores as $store) {
            if ($store->getWebsiteId() == $websiteId) {
                $storeIds[] = $store->getId();
            }
        }

        return $storeIds;
    }
}