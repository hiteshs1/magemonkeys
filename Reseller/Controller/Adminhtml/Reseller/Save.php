<?php
namespace Magemonkeys\Reseller\Controller\Adminhtml\Reseller;

use Magento\Backend\App\Action;
use Magento\Backend\Model\Session;
use Magemonkeys\Reseller\Model\Reseller;
use Magemonkeys\Reseller\Helper\Data;
use Magento\Framework\Registry;

class Save extends \Magento\Backend\App\Action
{
    /*
     * @var Reseller
     */
    protected $resellerModel;
    /**
     * @var Session
     */
    protected $adminsession;
    /**
     * @param Action\Context $context
     * @param Reseller $resellerModel
     * @param Session  $adminsession
     */
    protected $helperData;

    protected $_registry;

    public function __construct(
        Action\Context $context,
        Reseller $resellerModel,
        Session $adminsession,
        Data $helperData,
        Registry $registry
    ) {
        parent::__construct($context);
        $this->resellerModel = $resellerModel;
        $this->adminsession = $adminsession;
        $this->helperData = $helperData;
        $this->_registry = $registry;
    }
    /**
     * Save Reseller record action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $this->resellerModel->load($id);
            }            
            try {                

                $this->_registry->register('user_is_active', $data['user_is_active']);

                //create website, store, storeview

                $websiteCode = $data['user_websitename']."_website";
                $websiteName = $data['user_websitename'];
                $storeGroupName = $data['user_websitename']."_store";
                $storeCode = $data['user_websitename']."_store";
                $storeName = $data['user_websitename']."_store";
                $storeViewCode = $data['user_websitename']."_storeview";
                $storeViewName = $data['user_websitename']."_storeview";

                $this->helperData->createWebsiteStoreStoreView($websiteCode, $websiteName, $storeGroupName, $storeCode, $storeName, $storeViewCode, $storeViewName);

                //create admin user

                $username = $data['username'];
                $user_firstname = $data['user_firstname'];
                $user_lastname = $data['user_lastname'];
                $user_email = $data['user_email'];
                $user_password = $data['user_password'];
                
                //create admin user role
                $baseRoleid = $this->helperData->getConfigBaseRoleId();
                $this->helperData->duplicateAdminUserAndRole($baseRoleid, $websiteName, $username, $user_firstname, $user_lastname, $user_email, $user_password);

                //apply theme to reseller website
                $this->helperData->applyThemeToWebsite('Magemonkeys/mvtheme' ,(int)$this->_registry->registry('websiteId'));
                
                //insert some require data in the reseller table
                $data['user_websitecode'] = $this->_registry->registry('websiteCode');
                $data['user_websiteid'] = $this->_registry->registry('websiteId');
                $data['user_storeviewid'] = $this->_registry->registry('StoreViewId');
                $data['user_websitesecureurl'] = $this->_registry->registry('websiteSecureUrl');
                $data['user_websiteunsecureurl'] = $this->_registry->registry('websiteUnsecureUrl');
                $data['role_id'] = $this->_registry->registry('role_id');
                $data['adminuser_id'] = $this->_registry->registry('adminuser_id');
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');

                // save data in table
                $this->resellerModel->setData($data);
                $this->resellerModel->save();
                
                $this->messageManager->addSuccess(__('Reseller has been successfully created.'));
                $this->adminsession->setFormData(false);

                //cache flush
                $this->helperData->flushCache();
                
                return $resultRedirect->setPath('*/*/index');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}