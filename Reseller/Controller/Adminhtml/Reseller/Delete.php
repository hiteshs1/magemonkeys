<?php
namespace Magemonkeys\Reseller\Controller\Adminhtml\Reseller;

use Magento\Backend\App\Action;
use Magento\Authorization\Model\Acl\Role\Group as RoleGroup;

class Delete extends Action
{
    protected $resellerFactory;    
    protected $storeRepository;
    protected $websiteRepository;
    protected $userFactory;
    protected $roleFactory;
    protected $helperData;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magemonkeys\Reseller\Model\Reseller $resellerFactory,
        \Magento\Store\Model\StoreRepository $storeRepository,
        \Magento\Store\Model\WebsiteRepository $websiteRepository,
        \Magento\User\Model\UserFactory $userFactory,
        \Magento\Authorization\Model\RoleFactory $roleFactory,
        \Magemonkeys\Reseller\Helper\Data $helperData
    ) {
        parent::__construct($context);
        $this->resellerFactory = $resellerFactory;
        $this->storeRepository = $storeRepository;
        $this->websiteRepository = $websiteRepository;
        $this->userFactory = $userFactory;
        $this->roleFactory = $roleFactory;
        $this->helperData = $helperData;
    }
    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magemonkeys_Reseller::index_delete');
    }
    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $websiteCode = $this->getRequest()->getParam('user_websitecode');        
        $adminUserId = $this->getRequest()->getParam('adminuser_id'); 
        $roleId = $this->getRequest()->getParam('role_id'); 

        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id && $websiteCode) {
            try {                
                //delete website as well
                $this->deleteWebsiteStoreAndView($websiteCode);
                //delete website as well
                $this->deleteAdminUser($adminUserId);
                //delete user role
                $this->deleteUserRole($roleId);

                $model = $this->resellerFactory;
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('Record deleted successfully.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        //cache flush
        $this->helperData->flushCache();
        $this->messageManager->addError(__('Record does not exist.'));
        return $resultRedirect->setPath('*/*/');
    }

    public function deleteWebsiteStoreAndView($websiteCode)
    {
        $website = $this->websiteRepository->get($websiteCode);
        $website->getResource()->delete($website);
    }

    public function deleteAdminUser($adminUserId)
    {
        try {
            // Load admin user by ID
            $adminUser = $this->userFactory->create()->load($adminUserId);

            if ($adminUser->getId()) {
                // Delete the user
                $adminUser->delete();                
            } 
        } catch (\Exception $e) {
            return $this->messageManager->addError(__($e));
        }
    }

    /**
     * Delete user role by role ID.
     *
     * @param int $roleId
     * @return string
     */
    public function deleteUserRole($roleId)
    {
        try {
            // Load the role by its ID
            $role = $this->roleFactory->create()->load($roleId);

            if ($role->getId()) {
                // Check if role is a valid admin role and delete
                if ($role->getRoleType() == RoleGroup::ROLE_TYPE) {
                    $role->delete();
                    return "Role ID $roleId deleted successfully.";
                } else {
                    return "Role ID $roleId is not a valid admin role.";
                }
            } else {
                return "Role ID $roleId not found.";
            }
        } catch (\Exception $e) {
            return "Error deleting role: " . $e->getMessage();
        }
    }
}