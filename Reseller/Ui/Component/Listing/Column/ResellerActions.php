<?php

namespace Magemonkeys\Reseller\Ui\Component\Listing\Column;

class ResellerActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    const URL_EDIT_PATH = 'reseller/reseller/edit';
    const URL_DELETE_PATH = 'reseller/reseller/delete';
    const URL_IMPORT_PATH = 'reseller/reseller/importcms';
    const URL_ENABLE_PATH = 'reseller/reseller/enable';
    const URL_DISABLE_PATH = 'reseller/reseller/disable';
    /*
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;
    /**
     * @param \Magento\Framework\UrlInterface                              $urlBuilder
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory           $uiComponentFactory
     * @param array                                                        $components
     * @param array                                                        $data
     */
    public function __construct(
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                if (isset($item['id'])) {
                    $item[$this->getData('name')] = [
                        /*'edit' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_EDIT_PATH,
                                [
                                    'id' => $item['id'
                                    ],
                                ]
                            ),
                            'label' => __('Edit'),
                        ],*/
                        'importcms' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_IMPORT_PATH,
                                [
                                    'user_websiteid' => $item['user_websiteid'],
                                ]
                            ),
                            'label' => __('Import Data'),
                            'confirm' => [
                                'title' => __('Import Cms'),
                                'message' => __('Are you sure you want to Import Cms data this Reseller?'),
                            ],
                        ],
                        'enable' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_ENABLE_PATH,
                                [                                    
                                    'id' => $item['id'],
                                ]
                            ),
                            'label' => __('Enable'),
                            'confirm' => [
                                'title' => __('Enable'),
                                'message' => __('Are you sure you want to Enable this Reseller?'),
                            ],
                        ],
                        'disable' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_DISABLE_PATH,
                                [
                                 
                                    'id' => $item['id'],
                                ]
                            ),
                            'label' => __('Disable'),
                            'confirm' => [
                                'title' => __('Disable'),
                                'message' => __('Are you sure you want to Disable this Reseller?'),
                            ],
                        ],
                        'delete' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_DELETE_PATH,
                                [
                                    'id' => $item['id'],
                                    'user_websitecode' => $item['user_websitecode'],
                                    'adminuser_id' => $item['adminuser_id'],
                                    'role_id' => $item['role_id'],
                                ]
                            ),
                            'label' => __('Delete'),
                            'confirm' => [
                                'title' => __('Delete Item'),
                                'message' => __('Are you sure you want to delete this Reseller?'),
                            ],
                        ],
                    ];
                }
            }
        }
        return $dataSource;
    }
}