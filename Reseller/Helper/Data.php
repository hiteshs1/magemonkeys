<?php

namespace Magemonkeys\Reseller\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\WebsiteFactory;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\StoreFactory;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\User\Model\UserFactory;
use Magento\Framework\Registry;
use Magento\Authorization\Model\RoleFactory;
use Magento\Authorization\Model\RulesFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Theme\Model\ThemeFactory;
use Magento\Framework\App\Cache\Manager; 


class Data extends AbstractHelper
{
    protected $websiteFactory;
    protected $groupFactory;
    protected $storeFactory;
    protected $configWriter;
    protected $storeManager;
    protected $_userFactory;
    protected $_registry;
    protected $roleFactory;
    protected $rulesFactory;
    protected $themeFactory;
    protected $cacheManager;

    public function __construct(
        WebsiteFactory $websiteFactory,
        GroupFactory $groupFactory,
        StoreFactory $storeFactory,
        WriterInterface $configWriter,
        StoreManagerInterface $storeManager,
        UserFactory $userFactory,
        Registry $registry,
        RoleFactory $roleFactory,
        RulesFactory $rulesFactory,
        ScopeConfigInterface $scopeConfig,
        ThemeFactory $themeFactory,
        Manager $cacheManager
    ) {
        $this->websiteFactory = $websiteFactory;
        $this->groupFactory = $groupFactory;
        $this->storeFactory = $storeFactory;
        $this->configWriter = $configWriter;
        $this->storeManager = $storeManager;
        $this->_userFactory = $userFactory;
        $this->_registry = $registry;
        $this->roleFactory = $roleFactory;
        $this->rulesFactory = $rulesFactory;
        $this->scopeConfig = $scopeConfig;
        $this->themeFactory = $themeFactory;
        $this->cacheManager = $cacheManager;
    }

    public function getConfigParentDomain()
    {
        return $this->scopeConfig->getValue('reseller/general/parentdomain', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getConfigRootcategoryId()
    {
        return $this->scopeConfig->getValue('reseller/general/rootcategory', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getConfigBaseWebsiteId()
    {
        return $this->scopeConfig->getValue('reseller/general/basewebsiteid', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getConfigBaseRoleId()
    {
        return $this->scopeConfig->getValue('reseller/general/baseroleid', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function flushCache()
    {
        return $this->cacheManager->flush($this->cacheManager->getAvailableTypes());
    }

    public function applyThemeToWebsite($themeCode, $websiteId)
    {
        try {
            // Load the theme by its theme code (e.g., 'Vendor/theme-frontend-custom')
            $theme = $this->themeFactory->create()->load($themeCode, 'code');

            if (!$theme->getId()) {
                return "Theme with code '$themeCode' does not exist.";
            }

            // Set theme for the website scope
            $this->configWriter->save('design/theme/theme_id', $theme->getId(), \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES, $websiteId);

            return "Theme '{$themeCode}' has been successfully applied to website ID $websiteId.";
        } catch (\Exception $e) {
            return "Error applying theme: " . $e->getMessage();
        }
    }

    public function duplicateAdminUserAndRole($originalRoleId, $newRoleName, $username, $user_firstname, $user_lastname, $user_email, $user_password)
    {
        // Load the original role
        $originalRole = $this->roleFactory->create()->load($originalRoleId);
        if (!$originalRole->getId()) {
            throw new LocalizedException(__('Role with ID %1 not found.', $originalRoleId));
        }

        // Create the new role
        $websiteId = (array)$this->_registry->registry('websiteId');
        $newRole = $this->roleFactory->create();
        $newRole->setName($newRoleName);
        $newRole->setPid($originalRole->getPid()); // Set Parent ID
        $newRole->setRoleType($originalRole->getRoleType()); // Role Type (G for group/admin roles)
        $newRole->setUserType($originalRole->getUserType()); // User Type (2 for admin roles)
        $newRole->setGwsIsAll(0);
        $newRole->setGwsWebsites($websiteId);
        $newRole->setGwsStoreGroups($websiteId);

        try {
            // Save the new role
            $newRole->save();

            // Duplicate the role's permissions (rules)
            $originalRoleRules = $this->rulesFactory->create()->getCollection()->addFieldToFilter('role_id', $originalRoleId);
            foreach ($originalRoleRules as $rule) {
                      //echo '<pre>'; print_r($rule); echo '</pre>'; exit;
                      
                $newRule = $this->rulesFactory->create();
                $newRule->setRoleId($newRole->getId());
                $newRule->setResourceId($rule->getResourceId());
                $newRule->setPrivileges($rule->getPrivileges());
                $newRule->setPermission($rule->getPermission());
                $newRule->save();
            }

            if($this->_registry->registry('user_is_active')==1){ $is_active = 1;} else { $is_active = 0; }
            $adminInfo = [
                'username'  => $username,
                'firstname' => $user_firstname,
                'lastname'    => $user_lastname,
                'email'     => $user_email,
                'password'  => $user_password,       
                'interface_locale' => 'en_US',
                'is_active' => $is_active
            ];

            $userModel = $this->_userFactory->create();
            $userModel->setData($adminInfo);
            $userModel->setRoleId($newRole->getId());
            try{
                $this->_registry->register('role_id', $newRole->getId());
               $userModel->save(); 
               $this->_registry->register('adminuser_id', $userModel->getUserId());
            } catch (\Exception $e) {
                echo $e->getMessage();
            }

            return "Role duplicated successfully. New Role ID: " . $newRole->getId();
        } catch (\Exception $e) {
            echo  $e->getMessage();
        }
    }


    public function createAdminUser($username, $user_firstname, $user_lastname, $user_email, $user_password){
        if($this->_registry->registry('user_is_active')==1){ $is_active = 1;} else { $is_active = 0; }
        $adminInfo = [
            'username'  => $username,
            'firstname' => $user_firstname,
            'lastname'    => $user_lastname,
            'email'     => $user_email,
            'password'  => $user_password,       
            'interface_locale' => 'en_US',
            'is_active' => $is_active
        ];

        $userModel = $this->_userFactory->create();
        $userModel->setData($adminInfo);
        $userModel->setRoleId(1);
        try{
           $userModel->save(); 
           $this->_registry->register('adminuser_id', $userModel->getUserId());
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Create website, store, and store view programmatically
     */
    public function createWebsiteStoreStoreView($websiteCode, $websiteName, $storeGroupName, $storeCode, $storeName, $storeViewCode, $storeViewName)
    {
        // Create Website
        $website = $this->websiteFactory->create();
        $website->setCode($websiteCode);
        $website->setName($websiteCode);
        $website->save();
        $websiteId = (int)$website->getId();
        $this->_registry->register('websiteCode', $website->getCode());
        $this->_registry->register('websiteId', $website->getId());

        // Create Store
        try {
            $storeGroup = $this->groupFactory->create();
            $storeGroup->setWebsiteId($websiteId);
            $storeGroup->setName($storeGroupName);
            $storeGroup->setCode($storeCode);
            $storeGroup->setRootCategoryId($this->getConfigRootcategoryId());
            $storeGroup->save();
            $storeGroupId = (int)$storeGroup->getId();    
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        // Create Store View
        $store = $this->storeFactory->create();
        $store->setCode($storeViewCode);
        $store->setWebsiteId($websiteId);
        $store->setGroupId($storeGroupId);
        $store->setName($storeViewCode);
        $store->setSortOrder(0);
        $store->setIsActive(1);
        $store->save();
        $storeId = (int)$store->getId();
        $this->_registry->register('StoreViewId', $storeId);
        

        // Create Store View (Locale specific)
        /*$storeView = $this->storeFactory->create();
        $storeView->setCode($storeViewCode);
        $storeView->setWebsiteId($websiteId);
        $storeView->setGroupId($storeGroupId);
        $storeView->setName($storeViewName);
        $storeView->setIsActive(1);
        $storeView->setSortOrder(1);
        $storeView->save();*/

        $parentDomain = $this->getConfigParentDomain();

        if($this->_registry->registry('user_is_active')==1){
            $websiteUnsecureUrl = 'http://'.$websiteName.'.'.$parentDomain.'/';
            $websiteSecureUrl = 'http://'.$websiteName.'.'.$parentDomain.'/';
            $this->_registry->register('websiteUnsecureUrl', $websiteUnsecureUrl);
            $this->_registry->register('websiteSecureUrl', $websiteSecureUrl);

            $this->configWriter->save('web/secure/base_link_url', $websiteSecureUrl, ScopeInterface::SCOPE_WEBSITES, $websiteId);
            $this->configWriter->save('web/secure/base_url', $websiteSecureUrl, ScopeInterface::SCOPE_WEBSITES, $websiteId);
            $this->configWriter->save('web/unsecure/base_link_url', $websiteUnsecureUrl, ScopeInterface::SCOPE_WEBSITES, $websiteId);
            $this->configWriter->save('web/unsecure/base_url', $websiteUnsecureUrl, ScopeInterface::SCOPE_WEBSITES, $websiteId);

            $this->configWriter->save('web/secure/base_link_url', $websiteSecureUrl, ScopeInterface::SCOPE_STORES, $storeId);
            $this->configWriter->save('web/secure/base_url', $websiteSecureUrl, ScopeInterface::SCOPE_STORES, $storeId);
            $this->configWriter->save('web/unsecure/base_link_url', $websiteUnsecureUrl, ScopeInterface::SCOPE_STORES, $storeId);
            $this->configWriter->save('web/unsecure/base_url', $websiteUnsecureUrl, ScopeInterface::SCOPE_STORES, $storeId);
        }
        
        // Return IDs
        return [
            'website_id' => $websiteId, 
            'store_group_id' => $storeGroupId, 
            'store_id' => $storeId
        ];
        
    }
}