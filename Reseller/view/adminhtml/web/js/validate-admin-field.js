define([
    'jquery'
], function ($) {
    'use strict';

    return function (config) {
        var ajaxRequest = null;  // To track ongoing AJAX requests
        var debounceTimeout = null;  // To implement debouncing

        // Function to trigger the AJAX call
        function checkFields() {
            var username = $('input[name="username"]').val();
            var email = $('input[name="user_email"]').val();
            var website = $('input[name="user_websitename"]').val();
            $('.mage-error').remove();

            // Abort the previous request if it's still running
            if (ajaxRequest && ajaxRequest.readyState !== 4) {
                ajaxRequest.abort();
            }

            // Show loader or processing start
            $('body').trigger('processStart');

            // Make the AJAX request
            ajaxRequest = $.ajax({
                url: config.ajaxUrl,
                type: 'POST',
                showLoader: true,
                data: {
                    username: username,
                    email: email,
                    website: website
                },
                success: function (response) {
                    $('body').trigger('processStop');

                    // Handle validation errors
                    if (response.username_exists) {                     
                        $('input[name="username"]').after('<div class="mage-error">' + response.username_message + '</div>').fadeIn("slow");
                    }
                    if (response.email_exists) {                        
                        $('input[name="user_email"]').after('<div class="mage-error">' + response.email_message + '</div>').fadeIn("slow");
                    }
                    if (response.website_exists) {                        
                        $('input[name="user_websitename"]').after('<div class="mage-error">' + response.website_message + '</div>').fadeIn("slow");
                    }
                },
                error: function (xhr, status, error) {
                    $('body').trigger('processStop');
                    console.error("Error in AJAX request:", error);
                    // Optionally, you can show a user-friendly error message here
                }
            });
        }

        // Debounce function to limit the rate of AJAX calls
        function debounce(func, delay) {
            return function () {
                var context = this, args = arguments;
                clearTimeout(debounceTimeout);
                debounceTimeout = setTimeout(function () {
                    func.apply(context, args);
                }, delay);
            };
        }

        // Attach debounced blur/input event to all three fields
        $(document).on('input blur', 'input[name="user_websitename"], input[name="username"], input[name="user_email"]', debounce(checkFields, 500));
    };
});
