define([
    'jquery'
], function ($) {
    'use strict';

    return function (config) {
        // Function to trigger the AJAX call
        function checkFields() {
            var username = $('input[name="username"]').val();
            var email = $('input[name="user_email"]').val();
            var website = $('input[name="user_websitename"]').val();
            $('.mage-error').remove();
            //$('body').trigger('processStart');

            $.ajax({
                url: config.ajaxUrl,
                type: 'POST',
                showLoader: true,
                data: {
                    username: username,
                    email: email,
                    website: website
                },
                success: function (response) {
                    $('body').trigger('processStop');
                    if (response.username_exists) {                     
                        $('input[name="username"]').after('<div class="mage-error">' + response.username_message + '</div>').fadeIn("slow");
                    }
                    if (response.email_exists) {                        
                        $('input[name="user_email"]').after('<div class="mage-error">' + response.email_message + '</div>').fadeIn("slow");
                    }
                    if (response.website_exists) {                        
                        $('input[name="user_websitename"]').after('<div class="mage-error">' + response.website_message + '</div>').fadeIn("slow");
                    }
                }
            });
        }

        // Attach blur event to all three fields
        $(document).on('input blur', 'input[name="user_websitename"], input[name="username"], input[name="user_email"]', function () {
            checkFields();
        });
    };
});
