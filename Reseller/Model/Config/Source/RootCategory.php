<?php

namespace Magemonkeys\Reseller\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;

class RootCategory implements ArrayInterface
{
    protected $categoryCollectionFactory;

    public function __construct(CategoryCollectionFactory $categoryCollectionFactory)
    {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
    }

    /**
     * Return array of root categories
     *
     * @return array
     */
    public function toOptionArray()
    {
        $rootCategories = $this->categoryCollectionFactory->create()
            ->addFieldToFilter('level', 1) // Filter by level, 1 is the root level
            ->addAttributeToSelect('name');

        $options = [];

        foreach ($rootCategories as $category) {
            $options[] = [
                'value' => $category->getId(),
                'label' => $category->getName()
            ];
        }

        return $options;
    }
}
