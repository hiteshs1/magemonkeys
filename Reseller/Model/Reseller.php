<?php
namespace Magemonkeys\Reseller\Model;

use Magento\Framework\Model\AbstractModel;

class Reseller extends AbstractModel
{
    protected function _construct()
    {
        $this->_init('Magemonkeys\Reseller\Model\ResourceModel\Reseller');
    }
}