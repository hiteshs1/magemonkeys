<?php
namespace Magemonkeys\Reseller\Model\ResourceModel\Reseller;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Magemonkeys\Reseller\Model\Reseller', 'Magemonkeys\Reseller\Model\ResourceModel\Reseller');
    }
}
