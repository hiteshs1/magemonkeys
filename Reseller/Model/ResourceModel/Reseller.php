<?php
namespace Magemonkeys\Reseller\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Reseller extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('reseller', 'id');
    }
}