<?php
namespace Magemonkeys\General\Block;
class Subcategory extends \Magento\Framework\View\Element\Template
{
    protected $_registry;
    protected $categoryRepository;
    public function __construct(
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->_registry = $registry;
        parent::__construct($context);
     }

    public function getSubcategories()
    {   
        $currentId  = $this->_registry->registry('current_category'); 
        $categoryObj = $this->categoryRepository->get($currentId->getId());
        $subcategories = $categoryObj->getChildrenCategories();        
        return $subcategories;
    }

    public function getSubcateImages($catid)
    {           
        $childCategoryObj = $this->categoryRepository->get($catid);        
        return $childCategoryObj;
    }
}
