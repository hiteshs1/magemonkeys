<?php
namespace Magemonkeys\Crud\Model\ResourceModel\Crud;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Magemonkeys\Crud\Model\Crud', 'Magemonkeys\Crud\Model\ResourceModel\Crud');
    }
}
