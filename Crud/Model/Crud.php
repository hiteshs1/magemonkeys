<?php
namespace Magemonkeys\Crud\Model;

use Magento\Framework\Model\AbstractModel;

class Crud extends AbstractModel
{
    protected function _construct()
    {
        $this->_init('Magemonkeys\Crud\Model\ResourceModel\Crud');
    }
}