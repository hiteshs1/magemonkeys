<?php
namespace Magemonkeys\Crud\Controller\Adminhtml\Crud;

use Magento\Backend\App\Action;
use Magento\Backend\Model\Session;
use Magemonkeys\Crud\Model\Crud;
class Save extends \Magento\Backend\App\Action
{
    /*
     * @var Crud
     */
    protected $crudModel;
    /**
     * @var Session
     */
    protected $adminsession;
    /**
     * @param Action\Context $context
     * @param Crud           $crudModel
     * @param Session        $adminsession
     */
    public function __construct(
        Action\Context $context,
        Crud $crudModel,
        Session $adminsession
    ) {
        parent::__construct($context);
        $this->crudModel = $crudModel;
        $this->adminsession = $adminsession;
    }
    /**
     * Save Crud record action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $Crud_id = $this->getRequest()->getParam('id');
            if ($Crud_id) {
                $this->crudModel->load($Crud_id);
            }
            $this->crudModel->setData($data);
            try {
                $this->crudModel->save();
                $this->messageManager->addSuccess(__('The data has been saved.'));
                $this->adminsession->setFormData(false);
                /*if ($this->getRequest()->getParam('back')) {
                    if ($this->getRequest()->getParam('back') == 'add') {
                        return $resultRedirect->setPath('add');
                    } else {
                        return $resultRedirect->setPath('edit', ['id' => $this->crudModel->getCrudId(), '_current' => true]);
                    }
                echo "asdf"; exit;
                }*/
                return $resultRedirect->setPath('*/*/index');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}