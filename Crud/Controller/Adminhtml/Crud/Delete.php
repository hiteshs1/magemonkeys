<?php
namespace Magemonkeys\Crud\Controller\Adminhtml\Crud;
use Magento\Backend\App\Action;

class Delete extends Action
{
    /**
     * @var \Magemonkeys\Crud\Model\Blog
     */
    protected $crudFactory;
    /**
     * @param Context                  $context
     * @param \Magemonkeys\Crud\Model\Blog $crudFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magemonkeys\Crud\Model\Crud $crudFactory
    ) {
        parent::__construct($context);
        $this->crudFactory = $crudFactory;
    }
    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magemonkeys_Crud::index_delete');
    }
    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->crudFactory;
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('Record deleted successfully.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addError(__('Record does not exist.'));
        return $resultRedirect->setPath('*/*/');
    }
}