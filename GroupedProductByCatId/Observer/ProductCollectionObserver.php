<?php
namespace Magemonkeys\GroupedProductByCatId\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * ProductCollectionObserver
 */
class ProductCollectionObserver implements ObserverInterface
{
    /**
     * Handler for load product collection event
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /* set your filter here */
        /*$observer->getEvent()
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('type_id', ['eq' => 'grouped']);            
       return $this;*/
    }
}