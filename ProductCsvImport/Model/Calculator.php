<?php

namespace Magemonkeys\ProductCsvImport\Model;

class Calculator
{
    public function add($value1, $value2)
    {
        $total = $value1 + $value2;
        return $total;
    }
}
