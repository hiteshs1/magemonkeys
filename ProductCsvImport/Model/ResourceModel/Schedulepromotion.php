<?php
namespace Magemonkeys\ProductCsvImport\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Schedulepromotion extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('schedulepromotion', 'id');
    }

    public function insertCustomerData($data)
    {
        $connection = $this->getConnection();
        $connection->insert($this->getMainTable(), $data);
    }
}
