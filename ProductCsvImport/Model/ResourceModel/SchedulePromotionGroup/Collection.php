<?php
namespace Magemonkeys\ProductCsvImport\Model\ResourceModel\SchedulePromotionGroup;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magemonkeys\ProductCsvImport\Model\SchedulePromotionGroup as Model;
use Magemonkeys\ProductCsvImport\Model\ResourceModel\SchedulePromotionGroup as ResourceModel;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'group_id';    

    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}

