<?php
namespace Magemonkeys\ProductCsvImport\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class SchedulePromotionGroup extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('schedulepromotion_group', 'group_id');
    }    
}
