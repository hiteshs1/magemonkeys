<?php
namespace Magemonkeys\ProductCsvImport\Model\ResourceModel\Schedulepromotion;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magemonkeys\ProductCsvImport\Model\Schedulepromotion as Model;
use Magemonkeys\ProductCsvImport\Model\ResourceModel\Schedulepromotion as ResourceModel;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';    

    protected function _construct()
    {
        //$this->_init(Model::class, ResourceModel::class);
        $this->_init('Magemonkeys\ProductCsvImport\Model\Schedulepromotion', 'Magemonkeys\ProductCsvImport\Model\ResourceModel\Schedulepromotion');
    }
}

