<?php
namespace Magemonkeys\ProductCsvImport\Model;

use Magento\Framework\Model\AbstractModel;

class Schedulepromotion extends AbstractModel
{
    protected function _construct()
    {
        $this->_init('Magemonkeys\ProductCsvImport\Model\ResourceModel\Schedulepromotion');
    }
}
