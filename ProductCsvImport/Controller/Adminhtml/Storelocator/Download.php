<?php

namespace Magemonkeys\ProductCsvImport\Controller\Adminhtml\Storelocator;

use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class Download extends \Magento\Framework\App\Action\Action implements ActionInterface
{
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory
    ) {
        parent::__construct($context);
        $this->fileFactory = $fileFactory;
    }

    /**
     * Execute controller action
     */
    public function execute()
    {
        $csvFileName = 'skus_not_exist.csv';
        $csvFilePath = BP . '/pub/media/' . $csvFileName;
        
        $this->fileFactory->create(
            $csvFileName,
            [
                'type'  => "filename",
                'value' => $csvFilePath,
            ],
            DirectoryList::MEDIA,
            'application/octet-stream'
        );
    }
}
