<?php

namespace Magemonkeys\ProductCsvImport\Controller\Adminhtml\Storelocator;

use Magento\Backend\App\Action;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as categoryCollectionFactory;

class SaveImportData extends \Magento\Backend\App\Action
{
    const PROMO_CATE_ID = '624';

    protected $productcollection;
    protected $categoryLinkRepository;
    protected $categoryUnLinkRepository;
    protected $category;
    protected $stockRegistry;
    protected $productCategoryList;
    protected $request;    
    protected $indexerRegistry;
    protected $categoryFactory;
    protected $categoryCollectionFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkRepository,
        \Magento\Catalog\Model\CategoryLinkRepository $categoryUnLinkRepository,
        \Magento\Catalog\Model\Category $category,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Catalog\Model\ProductCategoryList $productCategoryList,
        \Magento\Catalog\Model\ProductFactory $productFactory,        
        \Magento\Framework\Indexer\IndexerRegistry $indexerRegistry,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\App\RequestInterface $request,
        categoryCollectionFactory $categoryCollectionFactory
    ) {
        parent::__construct($context);
        $this->collectionFactory = $collectionFactory;
        $this->categoryLinkRepository = $categoryLinkRepository;
        $this->categoryUnLinkRepository = $categoryUnLinkRepository;
        $this->category = $category;
        $this->stockRegistry = $stockRegistry;
        $this->productCategoryList = $productCategoryList;
        $this->_productFactory = $productFactory;        
        $this->indexerRegistry = $indexerRegistry;
        $this->categoryFactory = $categoryFactory;
        $this->request = $request;
        $this->categoryCollectionFactory = $categoryCollectionFactory;

    }    

    public function execute()
    {        
        $param = $this->getRequest()->getPostValue();        
                //echo "<pre>"; print_r($param); echo "</pre>"; exit;
        $resultRedirect = $this->resultRedirectFactory->create();
        if (isset($param['importfile'][0]['name'])) {
            $path = $param['importfile'][0]['path'];
            $file_name = $param['importfile'][0]['file'];
            $file_path = $path . "/" . $file_name;
            try
            {
                $f_object = fopen($file_path, "r");
                $column = fgetcsv($f_object);

                $subcateId= array('');
                $error= array('');
                $caterror= array('');
                $i=1;
                while (($data = fgetcsv($f_object, 1000, ",")) !== false) {

                    /*echo "<pre>"; print_r($data); echo "</pre>"; exit;*/
                    if($data[0]!='' || $data[1]!=''){
                        $collection = $this->collectionFactory->create();                        
                        $collection->addFieldToSelect('*');
                        if(trim($data[0])!=''){
                            $collection->addAttributeToFilter('ean_code',['eq'=>intval(trim($data[0]))]);                            
                            $collection->load();
                            /*echo "ean_code";echo "<pre>"; print_r($collection->getData()); echo "</pre>"; exit;*/                  
                            if(!$collection->getData()){
                                $error[] = $data[0];
                            }
                        }else if(trim($data[1])!=''){                            
                            $collection->addAttributeToFilter('sku',['eq'=>trim($data[1])]);
                            $collection->load();                            
                            /*echo "sku";echo "<pre>"; print_r($collection->getData()); echo "</pre>"; exit;*/
                            if(!$collection->getData()){
                                $error[] = $data[1];
                            }
                        }else{}
                        

                        $promodiscount = $data[2] ? $data[2] : $param['promotion_discount'];
                        $promostock = $data[3] ? $data[3] : $param['promotion_qty'];
                        $promofromdate = $data[4] ? $data[4] : $param['promotion_fromdate'];
                        $promotodate = $data[5] ? $data[5] : $param['promotion_todate'];

                        $subcateId = $this->getSubCategory();
                        $discount = intval(trim($promodiscount));
                         if ($discount >= 1 && $discount <= 20) {
                                $categoryId = $this->getCategoryIdByDiscountRange(20);
                            } elseif ($discount > 20 && $discount <= 40) {
                                $categoryId = $this->getCategoryIdByDiscountRange(40);
                            } elseif ($discount > 40 && $discount <= 50) {
                                $categoryId = $this->getCategoryIdByDiscountRange(50);
                            } elseif ($discount > 50 && $discount <= 60) {
                                $categoryId = $this->getCategoryIdByDiscountRange(60);
                            } elseif ($discount > 60 && $discount <= 70) {
                                $categoryId = $this->getCategoryIdByDiscountRange(70);
                            } else {
                                // Handle other cases or set a default category ID
                                $categoryId = self::PROMO_CATE_ID;
                            }
                     

                        unset($subcateId[array_search(trim($categoryId),$subcateId)]);  
                                                    
                        $productIds = [];
                        foreach ($collection as $product) {                                                        
                            $productIds[] = $product->getEntityId();    
                            $stockItem = $this->stockRegistry->getStockItemBySku($product->getSku());
                            $qty = '';
                            if(trim($promodiscount)!='' && trim($promostock)==''){
                                $qty = '99999'; 
                                $stockItem->setQty($qty);
                                $stockItem->setIsInStock((bool)$qty);
                                $this->stockRegistry->updateStockItemBySku($product->getSku(), $stockItem);
                            }elseif(trim($promodiscount)!='' && trim($promostock)!=''){
                                $qty = trim($promostock);                                
                                $stockItem->setQty($qty);
                                $stockItem->setIsInStock((bool)$qty);
                                $this->stockRegistry->updateStockItemBySku($product->getSku(), $stockItem);
                            }
                            else{}                           
                               
                            // remvore category id    
                            foreach($subcateId as $key => $val){
                                try{
                                    $this->categoryUnLinkRepository->deleteByIds($key, $product->getSku());
                                }catch(\Exception $e){
                                    //echo $e->getMessage();
                                }
                            }                            
                              
                           
                            $catCollection = $this->categoryCollectionFactory->create();                            

                            $catCollection->addAttributeToFilter('name', ['eq' => $categoryId]);

                            foreach ($catCollection as $catcategory) {
                                $categoryIds = $catcategory->getId();
                                // Assuming there is only one category with the given name
                                break;
                            }


                            $getProductCateList = $this->getProductCategories($product->getEntityId());

                             if (!empty($categoryIds) && !empty($getProductCateList)) {
                                    array_push($getProductCateList, $categoryIds, self::PROMO_CATE_ID);
                                    $getProductCateList = array_unique($getProductCateList);

                                    try {
                                        $this->categoryLinkRepository->assignProductToCategories($product->getSku(), $getProductCateList);
                                    } catch (\Exception $e) {
                                        // Handle the exception if needed
                                    }
                                }

                            
                            $discountexist = array_search (trim($categoryId), $this->getSubCategory()); 
                            if(trim($promodiscount)!='' && $discountexist){                    
                                $product->setSpecialDiscount(trim($promodiscount));                    
                                $product->setSpecialDiscountFromDate($promofromdate);                    
                                $product->setSpecialDiscountToDate($promotodate);                    
                            }else{
                                $caterror[] = $promodiscount;
                            }                                            

                            $product->save();
                            
                            
                    $i++; }
                    }
                } 
                
                array_unique($error);
                $errorlog = implode(', ', $error);
                $caterrorlog = implode(', ', $caterror);
                //reindex                               
                //$this->ProductIndexing($productIds);
                $this->messageManager->addSuccess(__("Import Done."));
                if($errorlog!=''){
                    $this->messageManager->addError(__('This SKU / GTIN does not exist '.$errorlog));

                    // create csv file with skus
                    $csvFileName = 'skus_not_exist.csv';
                    $csvFilePath = BP . '/pub/media/' . $csvFileName; 
                    $csvFile = fopen($csvFilePath, 'w');

                    // Add header to CSV file if needed
                    fputcsv($csvFile, ['sku', 'store_view_code', 'attribute_set_code', 'product_type', 'categories', 'product_websites', 'name', 'description', 'short_description', 'weight', 'product_online', 'tax_class_name', 'visibility', 'price', 'special_price', 'special_price_from_date', 'special_price_to_date', 'url_key', 'meta_title', 'meta_keywords', 'meta_description', 'created_at', 'updated_at', 'new_from_date', 'new_to_date', 'display_product_options_in', 'map_price', 'msrp_price', 'map_enabled', 'gift_message_available', 'custom_design', 'custom_design_from', 'custom_design_to', 'custom_layout_update', 'page_layout', 'product_options_container', 'msrp_display_actual_price_type', 'country_of_manufacture', 'additional_attributes', 'qty', 'out_of_stock_qty', 'use_config_min_qty', 'is_qty_decimal', 'allow_backorders', 'use_config_backorders', 'min_cart_qty', 'use_config_min_sale_qty', 'max_cart_qty', 'use_config_max_sale_qty', 'is_in_stock', 'notify_on_stock_below', 'use_config_notify_stock_qty', 'manage_stock', 'use_config_manage_stock', 'use_config_qty_increments', 'qty_increments', 'use_config_enable_qty_inc', 'enable_qty_increments', 'is_decimal_divided', 'website_id', 'deferred_stock_update', 'use_config_deferred_stock_update', 'related_skus', 'crosssell_skus', 'upsell_skus', 'hide_from_product_page', 'custom_options', 'bundle_price_type', 'bundle_sku_type', 'bundle_price_view', 'bundle_weight_type', 'bundle_values', 'associated_skus', 'ean_code', 'marca','how_to_use', 'ingredients']);

                    // Add SKUs to CSV file
                    foreach ($error as $sku) {
                        fputcsv($csvFile, [$sku]);
                    }

                    fclose($csvFile);
                          
                    $downloadLink = $this->_url->getUrl('storelocator/storelocator/download'); // Update the URL here
                    $this->messageManager->addSuccess(
                        __("You can download the CSV file: <a href='%1' download='%2'>%2</a>", $downloadLink, $csvFileName)
                    );
                }
                if($caterrorlog!=''){
                    // $this->messageManager->addError(__('This Promoções category does not exist '.$caterrorlog));
                }
            } catch (\Exception $e) {  
                //echo $e->getMessage();echo "<br>"; exit;
                $this->messageManager->addError(__($e->getMessage()));
            }
        } else {
            $this->messageManager->addError(__("Please Select File."));
        }
        return $resultRedirect->setPath('*/*/importdata');
    }

    public function getSubCategory(){
        $catId = self::PROMO_CATE_ID;
        $subcategory = $this->category->load($catId);
        $suballcats = $subcategory->getChildrenCategories(); 
        foreach ($suballcats as $key => $subcate) {
             // $catename = strstr($subcate->getName(), '%', true);
             //$subcateId[] = array('id'=>$subcate->getId(), 'name'=>$catename);                 
             $subcateId[$subcate->getId()] =$subcate->getName();                 
        }
        return $subcateId;
    }

    public function CreateProductCategory($categoryName)
    {       
        $parentId = self::PROMO_CATE_ID;
        $parentCategory = $this->categoryFactory->create()->load($parentId);
        $category = $this->categoryFactory->create();
        $cate = $category->getCollection()
            ->addAttributeToFilter('name', $categoryName)
            ->getFirstItem();

        if (!$cate->getId()) {
            $category->setPath($parentCategory->getPath())
                ->setParentId($parentId)
                ->setName($categoryName)
                ->setIsActive(true);
            $category->save();
        }

        return $category;
    }
    
    public function getProductCategories($pid)
    {
      $product = $this->_productFactory->create()->load($pid); // $pid = Product_ID
      return  $product->getCategoryIds();      
    }

    public function ProductIndexing($productIds)
    {                
            $indexList = [
                'catalog_category_product',
                'catalog_product_category',
                'catalog_product_attribute',
                'cataloginventory_stock',
                'inventory',
                'catalogsearch_fulltext',
                'catalog_product_price',
                'catalogrule_product',
                'catalogrule_rule'
            ];

            foreach ($indexList as $index) {
                $categoryIndexer = $this->indexerRegistry->get($index);
                $categoryIndexer->reindexList($productIds);                    
            }
    }


private function getCategoryIdByDiscountRange($range)
{
    // Implement logic to get the category ID based on the discount range
    // For example, you can return different category IDs for different discount ranges
    switch ($range) {
        case 20:
            return 'Até 20%'; // Category ID for up to 20% discount
        case 40:
            return 'Até 40%'; // Category ID for up to 40% discount
        case 50:
            return 'Até 50%'; // Category ID for up to 50% discount
        case 60:
            return 'Até 60%'; // Category ID for up to 60% discount
        case 70:
            return 'Até 70%'; // Category ID for up to 70% discount
        default:
            return self::PROMO_CATE_ID; // Default category ID
    }
}
    
}