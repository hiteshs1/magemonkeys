<?php

namespace Magemonkeys\ProductCsvImport\Controller\Adminhtml\Storelocator;

use Magento\Backend\App\Action;

class SaveImportData extends \Magento\Backend\App\Action
{
    protected $productcollection;
    protected $categoryLinkRepository;
    protected $categoryUnLinkRepository;
    protected $category;
    protected $stockRegistry;
    protected $productCategoryList;
    protected $request;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkRepository,
        \Magento\Catalog\Model\CategoryLinkRepository $categoryUnLinkRepository,
        \Magento\Catalog\Model\Category $category,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Catalog\Model\ProductCategoryList $productCategoryList,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\App\RequestInterface $request
    ) {
        parent::__construct($context);
        $this->collectionFactory = $collectionFactory;
        $this->categoryLinkRepository = $categoryLinkRepository;
        $this->categoryUnLinkRepository = $categoryUnLinkRepository;
        $this->category = $category;
        $this->stockRegistry = $stockRegistry;
        $this->productCategoryList = $productCategoryList;
        $this->_productFactory = $productFactory;
        $this->request = $request;

    }    

    public function execute()
    {
        
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if (isset($data['importfile'][0]['name'])) {
            $path = $data['importfile'][0]['path'];
            $file_name = $data['importfile'][0]['file'];
            $file_path = $path . "/" . $file_name;
            try
            {
                $f_object = fopen($file_path, "r");
                $column = fgetcsv($f_object);

                $subcateId= array('');
                $i=1;
                //while (($data = fgetcsv($f_object, 1000, ",")) !== false) {                    
                $csv = $this->csvToArray($file_path);
                array_shift($csv);
                foreach ($csv as $data) {
                
                    //print_r($data);
                    if(!empty($data[0])){
                        $collection = $this->collectionFactory->create();
                        $collection->addFieldToSelect('*');
                        $collection->addAttributeToFilter('ean_code',['eq'=>$data[0]])->load(); 

                        $subcateId = $this->getSubCategory();

                        unset($subcateId[array_search($data[2],$subcateId)]);                              
                        
                        foreach ($collection as $product) {
                                    
                            $stockItem = $this->stockRegistry->getStockItemBySku($product->getSku());
                            $qty = '';
                            if($data[2]!='' && $data[3]==''){
                                $qty = '99999'; 
                                $stockItem->setQty($qty);
                                $stockItem->setIsInStock((bool)$qty);
                                $this->stockRegistry->updateStockItemBySku($product->getSku(), $stockItem);
                            }elseif($data[2]!='' && $data[3]!=''){
                                $qty = $data[3];                                
                                $stockItem->setQty($qty);
                                $stockItem->setIsInStock((bool)$qty);
                                $this->stockRegistry->updateStockItemBySku($product->getSku(), $stockItem);
                            }
                            else{
                                /*$qty = $data[3];                                
                                $stockItem->setQty($qty);
                                $stockItem->setIsInStock((bool)$qty);
                                $this->stockRegistry->updateStockItemBySku($product->getSku(), $stockItem);*/
                            }

                            
                            

                            //$getProductCateList = $this->getProductCategories($product->getEntityId());
                                  
                            foreach($subcateId as $key => $val){
                                try{
                                    $this->categoryUnLinkRepository->deleteByIds($key, $product->getSku());
                                }catch(\Exception $e){
                                    //echo $e->getMessage();
                                }

                            }                            
                            
                            foreach ($subcateId as $key => $cid) {
                                if($data[2]!='' && !empty($key)){
                                    //assign category
                                    if($data[2]==$key){                                        
                                        $allcat = $this->getCategoryIds($product->getEntityId());
                                        unset($allcat[array_search('2',$allcat)]);
                                        array_push($allcat,$key);                                                                                                
                                        $this->categoryLinkRepository->assignProductToCategories($product->getSku(), $allcat);
                                    }
                                }
                            } 

                                
                            if($data[2]!=''){                    
                                $product->setSpecialDiscount($data[2]);                    
                                $product->setSpecialDiscountFromDate($data[4]);                    
                                $product->setSpecialDiscountToDate($data[5]);                    
                            }                

                            $product->save();

                            /*echo 'No.---'. $i.'<br>';
                            echo 'ean_code---'. $product->getEanCode().'<br>';
                            echo 'Special Discount---'. $data[2].'<br>';                
                            echo 'Qty---'. $qty.'<br>';
                            echo 'sku---'. $product->getSku().'<br>';
                            echo '<br>';*/

                    $i++; }
                    }
                              
                }                
            } catch (\Exception $e) {                
                $this->messageManager->addError(__("Something went wrong while Reading the file."));
            }
        } else {
            $this->messageManager->addError(__("Please Select File."));
        }
        $this->messageManager->addSuccess(__("Import Done."));
        return $resultRedirect->setPath('*/*/importdata');
    }

    public function getSubCategory(){
        $catId = 624;
        $subcategory = $this->category->load($catId);
        $suballcats = $subcategory->getChildrenCategories(); 
        foreach ($suballcats as $key => $subcate) {
             $catename = strstr($subcate->getName(), '%', true);
             //$subcateId[] = array('id'=>$subcate->getId(), 'name'=>$catename);                 
             $subcateId[$subcate->getId()] = $catename;                 
        }
        return $subcateId;
    }

    public function getCategoryIds(int $productId)
    {        
        $categoryIds = $this->productCategoryList->getCategoryIds($productId);
        $category = [];
        if ($categoryIds) {
            $category = array_unique($categoryIds);
        }
        return $category;
    }

    public function getProductCategories($pid)
    {
      $product = $this->_productFactory->create()->load($pid); // $pid = Product_ID
      return  $product->getCategoryIds();      
    }

    public function csvToArray($csvFile){
        $file_to_read = fopen($csvFile, 'r');
        while (!feof($file_to_read) ) {
            $lines[] = fgetcsv($file_to_read, 1000, ',');
        }
        fclose($file_to_read);
        return $lines;
    }
}
