<?php

namespace Magemonkeys\ProductCsvImport\Controller\Adminhtml\Exportmarca;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResponseInterface;

class Exportdata extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;
    /**
     * @var \Magento\Framework\View\Result\LayoutFactory
     */
    protected $resultLayoutFactory;
    /**
     * @var \Magento\Framework\File\Csv
     */
    protected $csvProcessor;
    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $directoryList;

    protected $collectionFactory;

    protected $stockRegistry; 

    protected $resultFactory;



    /**
     * @param \Magento\Framework\App\Action\Context            $context
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Catalog\Model\ProductFactory            $productFactory
     * @param \Magento\Framework\View\Result\LayoutFactory     $resultLayoutFactory
     * @param \Magento\Framework\File\Csv                      $csvProcessor
     * @param \Magento\Framework\App\Filesystem\DirectoryList  $directoryList
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $optionFactory,
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attributeOptionCollection
    ) {
        $this->fileFactory = $fileFactory;
        $this->productFactory = $productFactory;
        $this->resultLayoutFactory = $resultLayoutFactory;
        $this->csvProcessor = $csvProcessor;
        $this->directoryList = $directoryList;
        $this->collectionFactory = $collectionFactory;
        $this->stockRegistry = $stockRegistry;
        $this->optionFactory = $optionFactory;
        $this->resultFactory = $resultFactory;
        $this->_attributeOptionCollection = $attributeOptionCollection;
        parent::__construct($context);
    }
    /**
     * CSV Create and Download
     *
     * @return ResponseInterface
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();        
        $content[] = [
            'ean_code' => __('Codigo'),
            'sku' => __('sku'),
            'special_discount' => __('Discount'),
            'qty' => __('stock'),
            'special_discount_from_date' => __('FromDate'),
            'special_discount_to_date' => __('ToDate'),
            'marca' => __('marca'),
        ];
        $resultLayout = $this->resultLayoutFactory->create();        

        $productCollection = $this->collectionFactory->create();
        $productCollection->addAttributeToSelect('*');        
        $productCollection->load();        
        $name = date('m-d-Y-H-i-s');        
        $fileName = 'exportmarca-' .$name. '.csv'; // Add Your CSV File name
        $filePath =  $this->directoryList->getPath(DirectoryList::MEDIA) . "/" . $fileName;

        foreach ($productCollection as $product){

            $productId = $product->getId();

            $productStock = $this->stockRegistry->getStockItem($productId);
            $productQty = $productStock->getQty();

            if($product->getMarca() == $data['attribute_option'] && $product->getStatus()==1 && $productQty!=0){
                $sku = $product['sku'];
                $qty = $this->stockRegistry->getStockItemBySku($sku)->getQty();
                $marcaId = $data['attribute_option'];

                $optionFactory = $this->optionFactory->create();
                $optionFactory->load($marcaId); // load by option value
                $attributeId = $optionFactory->getAttributeId(); // atribute id of given option value
                $attributeOptionCollectionFac = $this->_attributeOptionCollection->create();
                $optionData = $attributeOptionCollectionFac
                                    ->setPositionOrder('asc')
                                    ->setAttributeFilter($attributeId)
                                    ->setIdFilter($marcaId)
                                    ->setStoreFilter()
                                    ->load();
                $marca = $optionData->getData();                
                $marcaLabel = '';
                if(!empty($marca)){
                    $marcaLabel = $marca[0]['value'];
                }
                $content[] = [$product['ean_code'],
                $product['sku'],$product['special_discount'],$qty,$product['special_discount_from_date'],$product['special_discount_to_date'],$marcaLabel,];
            }
            
        }         

        $this->csvProcessor->setEnclosure('"')->setDelimiter(',')->saveData($filePath, $content);
        return $this->fileFactory->create(
            $fileName,
            [
                'type'  => "filename",
                'value' => $fileName,
                'rm'    => true, // True => File will be remove from directory after download.
            ],
            DirectoryList::MEDIA,
            'text/csv',
            null
        );        
       
    }
}