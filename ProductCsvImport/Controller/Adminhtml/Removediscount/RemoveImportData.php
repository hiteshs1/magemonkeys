<?php

namespace Magemonkeys\ProductCsvImport\Controller\Adminhtml\Removediscount;

use Magento\Backend\App\Action;

class RemoveImportData extends \Magento\Backend\App\Action
{
    protected $productcollection;
    protected $categoryLinkRepository;
    protected $categoryUnLinkRepository;
    protected $category;
    protected $stockRegistry;
    protected $productCategoryList;
    protected $request;    
    protected $indexerRegistry;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkRepository,
        \Magento\Catalog\Model\CategoryLinkRepository $categoryUnLinkRepository,
        \Magento\Catalog\Model\Category $category,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Catalog\Model\ProductCategoryList $productCategoryList,
        \Magento\Catalog\Model\ProductFactory $productFactory,        
        \Magento\Framework\Indexer\IndexerRegistry $indexerRegistry,
        \Magento\Framework\App\RequestInterface $request
    ) {
        parent::__construct($context);
        $this->collectionFactory = $collectionFactory;
        $this->categoryLinkRepository = $categoryLinkRepository;
        $this->categoryUnLinkRepository = $categoryUnLinkRepository;
        $this->category = $category;
        $this->stockRegistry = $stockRegistry;
        $this->productCategoryList = $productCategoryList;
        $this->_productFactory = $productFactory;        
        $this->indexerRegistry = $indexerRegistry;
        $this->request = $request;

    }    

    public function execute()
    {
        
        $data = $this->getRequest()->getPostValue();

        $resultRedirect = $this->resultRedirectFactory->create();
        if (isset($data['importfile'][0]['name'])) {
            $path = $data['importfile'][0]['path'];
            $file_name = $data['importfile'][0]['file'];
            $file_path = $path . "/" . $file_name;
            try
            {
                $f_object = fopen($file_path, "r");
                $column = fgetcsv($f_object);

                $subcateId = array('');

                while (($data = fgetcsv($f_object, 1000, ",")) !== false) {  
                    if($data[0]!='' || $data[1]!=''){
                        $collection = $this->collectionFactory->create();
                        $collection->addFieldToSelect('*');
                        if(trim($data[0])!=''){                            
                            $collection->addAttributeToFilter('ean_code',['eq'=>trim($data[0])]);
                        }else if(trim($data[1])!=''){
                            $collection->addAttributeToFilter('sku',['eq'=>trim($data[1])]);
                        }else{}

                        $subcateId = $this->getSubCategory();
                        $productIds = [];
                        foreach ($collection as $product) {
                            $productIds[] = $product->getEntityId();    
                               
                            // remvore category id    
                            foreach($subcateId as $key => $val){
                                try{
                                    $this->categoryUnLinkRepository->deleteByIds($key, $product->getSku());
                                }catch(\Exception $e){
                                    //echo $e->getMessage();
                                }
                            }                            
                            
                            $product->setSpecialDiscount('');                    
                            $product->setSpecialDiscountFromDate('');                    
                            $product->setSpecialDiscountToDate('');                    
                            $product->save();

                         } 
                    }
                }
                //reindex                               
                $this->ProductIndexing($productIds);
                $this->messageManager->addSuccess(__("Removed discount Done."));
            } catch (\Exception $e) {  
                //echo $e->getMessage(); exit;
                $this->messageManager->addError(__($e->getMessage()));
            }
        } else {
            $this->messageManager->addError(__("Please Select File."));
        }
        return $resultRedirect->setPath('*/*/importdata');
    }

    public function getSubCategory(){
        $catId = 624;
        $subcategory = $this->category->load($catId);
        $suballcats = $subcategory->getChildrenCategories(); 
        foreach ($suballcats as $key => $subcate) {
             $catename = strstr($subcate->getName(), '%', true);             
             $subcateId[$subcate->getId()] = $catename;                 
        }
        return $subcateId;
    }
    
    public function getProductCategories($pid)
    {
      $product = $this->_productFactory->create()->load($pid); // $pid = Product_ID
      return  $product->getCategoryIds();      
    }

    public function ProductIndexing($productIds)
    {                
            $indexList = [
                'catalog_category_product',
                'catalog_product_category',
                'catalog_product_attribute',
                'cataloginventory_stock',
                'inventory',
                'catalogsearch_fulltext',
                'catalog_product_price',
                'catalogrule_product',
                'catalogrule_rule'
            ];

            foreach ($indexList as $index) {
                $categoryIndexer = $this->indexerRegistry->get($index);
                $categoryIndexer->reindexList($productIds);                    
            }
    }
    
}
