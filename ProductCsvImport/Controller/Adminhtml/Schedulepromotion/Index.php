<?php

namespace Magemonkeys\ProductCsvImport\Controller\Adminhtml\Schedulepromotion;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Backend\App\Action
{
    private $coreRegistry;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry
    ) {

        parent::__construct($context);
        $this->coreRegistry = $coreRegistry;
    }
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Schedule Promotion List'));
        return $resultPage;
    }
}
