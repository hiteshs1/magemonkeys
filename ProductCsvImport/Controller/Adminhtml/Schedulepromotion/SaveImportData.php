<?php

namespace Magemonkeys\ProductCsvImport\Controller\Adminhtml\Schedulepromotion;

use Magento\Backend\App\Action;

class SaveImportData extends \Magento\Backend\App\Action
{

    protected $schedulepromotion;
    protected $schedulePromotionGroupFactory;

    
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magemonkeys\ProductCsvImport\Model\ResourceModel\Schedulepromotion $schedulepromotion,
        \Magemonkeys\ProductCsvImport\Model\SchedulePromotionGroupFactory $schedulePromotionGroupFactory
        
    ) {
        parent::__construct($context);
        $this->schedulepromotion = $schedulepromotion; 
        $this->schedulePromotionGroupFactory = $schedulePromotionGroupFactory;
    }    

    public function execute()
    {        
        $param = $this->getRequest()->getPostValue();        
                /*echo "<pre>"; print_r($param); echo "</pre>"; exit;*/
        $resultRedirect = $this->resultRedirectFactory->create();
        if (isset($param['importfile'][0]['name'])) {
            $path = $param['importfile'][0]['path'];
            $file_name = $param['importfile'][0]['file'];
            $file_path = $path . "/" . $file_name;
            try
            {

                $group = $this->schedulePromotionGroupFactory->create();
                $group->setData([
                    'promotion_name' => $param['promotion_name'],
                    'promotion_fromdate' => $param['promotion_fromdate']
                ]);
                $group->save();

                $groupId = $group->getId();

                $f_object = fopen($file_path, "r");
                $column = fgetcsv($f_object);

                $error= array('');
                $data_post= array('');
                $i=1;
                while (($data = fgetcsv($f_object, 1000, ",")) !== false) {

                    $promodiscount = $data[2] ? $data[2] : $param['promotion_discount'];
                    $promostock = $data[3] ? $data[3] : $param['promotion_qty'];
                    /*$promofromdate = $data[4] ? $data[4] : $param['promotion_fromdate'];
                    $promotodate = $data[5] ? $data[5] : $param['promotion_todate'];*/

                    // from date 
                    if($data[4]!=''){
                        $promofromdate = date('m/d/Y',strtotime($data[4]));
                    }elseif($data[4]=='' && $param['promotion_fromdate']!=''){
                        $promofromdate = date('m/d/Y',strtotime($param['promotion_fromdate']));
                    }else{
                        $promofromdate = NULL;                        
                    }

                    // to date 
                    if($data[5]!=''){
                        $promotodate = date('m/d/Y',strtotime($data[4]));
                    }elseif($data[5]=='' && $param['promotion_todate']!=''){
                        $promotodate = date('m/d/Y',strtotime($param['promotion_todate']));
                    }else{
                        $promotodate = NULL;                        
                    }


                    $data_post = array(
                        'codigo' => $data[0],
                        'group_id' => $groupId,
                        'sku' => $data[1],
                        'discount' => $promodiscount,
                        'stock' => $promostock,
                        'fromdate' => $promofromdate,
                        'todate' => $promotodate,
                        'marca' => $data[6],
                        'status' => 1
                    );

                    $this->schedulepromotion->insertCustomerData($data_post);
                } 
                
                $this->messageManager->addSuccess(__("Schedule Promotion Import Done."));
                
            } catch (\Exception $e) {  
                //echo $e->getMessage();echo "<br>"; exit;
                $this->messageManager->addError(__($e->getMessage()));
            }
        } else {
            $this->messageManager->addError(__("Please Select File."));
        }
        return $resultRedirect->setPath('*/*/importdata');
    }

    
    
}