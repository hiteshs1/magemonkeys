<?php

namespace Magemonkeys\ProductCsvImport\Block\Adminhtml\Exportmarca\Edit\Button;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class ExportMarcaData extends Generic implements ButtonProviderInterface
{
    public function getButtonData()
    {
        return [
            'label' => __('Export Marca'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => [
                    'buttonAdapter' => [
                        'actions' => [
                            [
                                'targetName' => 'exportmarca_exportmarca_form.exportmarca_exportmarca_form_data_source',
                                'actionName' => 'save',
                                'params' => [
                                    true,
                                    [
                                        'back' => 'import',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    protected function getOptions()
    {
        $options[] = [
            'id_hard' => 'save_and_new',
            'label' => __('Save & New'),
            'data_attribute' => [
                'mage-init' => [
                    'buttonAdapter' => [
                        'actions' => [
                            [
                                'targetName' => 'exportmarca_exportmarca_form.exportmarca_exportmarca_form_data_source',
                                'actionName' => 'save',
                                'params' => [
                                    true,
                                    [
                                        'back' => 'add',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $options[] = [
            'id_hard' => 'save_and_close',
            'label' => __('Save & Close'),
            'data_attribute' => [
                'mage-init' => [
                    'buttonAdapter' => [
                        'actions' => [
                            [
                                'targetName' => 'exportmarca_exportmarca_form.exportmarca_exportmarca_form_data_source',
                                'actionName' => 'save',
                                'params' => [
                                    true,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        return $options;
    }
}
