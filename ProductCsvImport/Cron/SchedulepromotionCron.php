<?php
namespace Magemonkeys\ProductCsvImport\Cron;

class SchedulepromotionCron
{
    protected $helper;
    protected $logger;

    public function __construct(
        \Magemonkeys\ProductCsvImport\Helper\Data $helper,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
    }

    public function execute()
    {
        try {
            $promotion = $this->helper->importSchedulePromotion();        
            $this->logger->info($promotion. date('Y-m-d H:i:s'));
        } catch (\Exception $e) {
            $this->logger->error('CustomCron error: ' . $e->getMessage());
        }

        return $this;
    }
}
