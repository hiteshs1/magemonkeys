<?php 
namespace Magemonkeys\ProductCsvImport\Ui\Component\Listing\Column;

use Magento\Framework\Data\OptionSourceInterface;

class StatusOptions implements OptionSourceInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => '0', 'label' => __('Success')],
            ['value' => '1', 'label' => __('Pending')],
        ];
    }
}

?>