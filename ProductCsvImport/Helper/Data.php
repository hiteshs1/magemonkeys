<?php 
namespace Magemonkeys\ProductCsvImport\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{   
    const PROMO_CATE_ID = '624';

    protected $productcollection;
    protected $categoryLinkRepository;
    protected $categoryUnLinkRepository;
    protected $category;
    protected $stockRegistry;
    protected $productCategoryList;
    protected $request;    
    protected $indexerRegistry;
    protected $categoryFactory;
    protected $categoryCollectionFactory;  
    protected $dateTime; 
    protected $schedulepromotionFactory;   
    protected $resourceConnection;   
        
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkRepository,
        \Magento\Catalog\Model\CategoryLinkRepository $categoryUnLinkRepository,
        \Magento\Catalog\Model\Category $category,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Catalog\Model\ProductCategoryList $productCategoryList,
        \Magento\Catalog\Model\ProductFactory $productFactory,        
        \Magento\Framework\Indexer\IndexerRegistry $indexerRegistry,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magemonkeys\ProductCsvImport\Model\SchedulepromotionFactory $schedulepromotionFactory,
        \Magemonkeys\ProductCsvImport\Model\SchedulePromotionGroupFactory $schedulepromotionGroupFactory,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    )
    {
        parent::__construct($context);
        $this->collectionFactory = $collectionFactory;
        $this->categoryLinkRepository = $categoryLinkRepository;
        $this->categoryUnLinkRepository = $categoryUnLinkRepository;
        $this->category = $category;
        $this->stockRegistry = $stockRegistry;
        $this->productCategoryList = $productCategoryList;
        $this->_productFactory = $productFactory;        
        $this->indexerRegistry = $indexerRegistry;
        $this->categoryFactory = $categoryFactory;
        $this->request = $request;
        $this->dateTime = $dateTime;
        $this->schedulepromotionFactory = $schedulepromotionFactory;
        $this->schedulepromotionGroupFactory = $schedulepromotionGroupFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->resourceConnection = $resourceConnection;
    }

    public function importSchedulePromotion()
    { 
        $scheduleCollection = $this->schedulepromotionFactory->create()->getCollection();
        $subcateId= array('');
        $error= array('');
        $caterror = array('');
        $success = ''; 
        $currentDate = $this->dateTime->gmtDate();       
        foreach ($scheduleCollection as $promotion) {     

            $currentDateTime = new \DateTime($currentDate);            
            $fromDateTime = date("d/m/Y", strtotime($promotion->getFromdate()) ?? '');

            /*echo "current date == ".$currentDateTime->format('d/m/Y').'<br>';
            echo "from date == ". $fromDateTime.'<br>'; exit;*/

            if($promotion->getStatus()==1 && $currentDateTime->format('d/m/Y') === $fromDateTime){
            //echo "<pre>"; print_r($promotion->getData()); exit;     
                $collection = $this->collectionFactory->create();                        
                $collection->addFieldToSelect('*');
                if(trim($promotion->getCodigo())!=''){
                    $collection->addAttributeToFilter('ean_code',['eq'=>intval(trim($promotion->getCodigo()))]);                            
                    $collection->load();                
                    if(!$collection->getData()){
                        $error[] = $promotion->getCodigo();
                    }
                }else if(trim($promotion->getSku())!=''){                            
                    $collection->addAttributeToFilter('sku',['eq'=>trim($promotion->getSku())]);
                    $collection->load();                                                                  
                    if(!$collection->getData()){
                        $error[] = $promotion->getSku();
                    }
                }else{}

                $subcateId = $this->getSubCategory();
                $discount = intval(trim($promotion->getDiscount()));
                if ($discount >= 1 && $discount <= 20) {
                    $categoryId = $this->getCategoryIdByDiscountRange(20);
                } elseif ($discount > 20 && $discount <= 40) {
                    $categoryId = $this->getCategoryIdByDiscountRange(40);
                } elseif ($discount > 40 && $discount <= 50) {
                    $categoryId = $this->getCategoryIdByDiscountRange(50);
                } elseif ($discount > 50 && $discount <= 60) {
                    $categoryId = $this->getCategoryIdByDiscountRange(60);
                } elseif ($discount > 60 && $discount <= 70) {
                    $categoryId = $this->getCategoryIdByDiscountRange(70);
                } else {
                    // Handle other cases or set a default category ID
                    $categoryId = self::PROMO_CATE_ID;
                }         

                unset($subcateId[array_search(trim($categoryId),$subcateId)]);  
                                            
                $productIds = [];
                foreach ($collection as $product) {                                                        
                    $productIds[] = $product->getEntityId();    
                    $stockItem = $this->stockRegistry->getStockItemBySku($product->getSku());
                    $qty = '';
                    if(trim($promotion->getDiscount())!='' && trim($promotion->getStock())==''){
                        $qty = '99999'; 
                        $stockItem->setQty($qty);
                        $stockItem->setIsInStock((bool)$qty);
                        $this->stockRegistry->updateStockItemBySku($product->getSku(), $stockItem);
                    }elseif(trim($promotion->getStock())!='' && trim($promotion->getDiscount())!=''){
                        $qty = trim($promotion->getStock());                                
                        $stockItem->setQty($qty);
                        $stockItem->setIsInStock((bool)$qty);
                        $this->stockRegistry->updateStockItemBySku($product->getSku(), $stockItem);
                    }
                    else{}                           
                       
                    // remvore category id    
                    foreach($subcateId as $key => $val){
                        try{
                            $this->categoryUnLinkRepository->deleteByIds($key, $product->getSku());
                        }catch(\Exception $e){
                            //echo $e->getMessage();
                        }
                    }  
                   
                    $catCollection = $this->categoryCollectionFactory->create();                            

                    $catCollection->addAttributeToFilter('name', ['eq' => $categoryId]);

                    foreach ($catCollection as $catcategory) {
                        $categoryIds = $catcategory->getId();
                        // Assuming there is only one category with the given name
                        break;
                    }


                    $getProductCateList = $this->getProductCategories($product->getEntityId());

                     if (!empty($categoryIds) && !empty($getProductCateList)) {
                            array_push($getProductCateList, $categoryIds, self::PROMO_CATE_ID);
                            $getProductCateList = array_unique($getProductCateList);

                            try {
                                $this->categoryLinkRepository->assignProductToCategories($product->getSku(), $getProductCateList);
                            } catch (\Exception $e) {
                                // Handle the exception if needed
                            }
                        }

                    
                    $discountexist = array_search (trim($categoryId), $this->getSubCategory());

                    /*echo $promotion->getFromdate().'<br>';                     
                    echo $promotion->getTodate().'<br>';   exit; */                 
                    if(trim($promotion->getDiscount())!='' && $discountexist){   
                        $product->setSpecialDiscount(trim((string) $promotion->getDiscount()));                    
                        $product->setSpecialDiscountFromDate($promotion->getFromdate());                    
                        $product->setSpecialDiscountToDate($promotion->getTodate());                    
                    }else{
                        $caterror[] = $product->getSku();
                    }                                            

                    $product->save();  
                }

                $promotion->setStatus(0)->save();
                //update status to promotiongroup
                $this->updateParentStatusIfAllChildrenInactive($promotion->getGroupId());                  
                
                $success = "Promotion import successfully done";
            }else{
                $success = "Promotion date does not match";
            } 
        }

        return $success;
      
    }

    public function getSubCategory(){
        $catId = self::PROMO_CATE_ID;
        $subcategory = $this->category->load($catId);
        $suballcats = $subcategory->getChildrenCategories(); 
        foreach ($suballcats as $key => $subcate) {             
             $subcateId[$subcate->getId()] =$subcate->getName();                 
        }
        return $subcateId;
    }
    
    
    public function getProductCategories($pid)
    {
      $product = $this->_productFactory->create()->load($pid); // $pid = Product_ID
      return  $product->getCategoryIds();      
    }
 
    public function getCategoryIdByDiscountRange($range)
    {
        // Implement logic to get the category ID based on the discount range
        // For example, you can return different category IDs for different discount ranges
        switch ($range) {
            case 20:
                return 'Até 20%'; // Category ID for up to 20% discount
            case 40:
                return 'Até 40%'; // Category ID for up to 40% discount
            case 50:
                return 'Até 50%'; // Category ID for up to 50% discount
            case 60:
                return 'Até 60%'; // Category ID for up to 60% discount
            case 70:
                return 'Até 70%'; // Category ID for up to 70% discount
            default:
                return self::PROMO_CATE_ID; // Default category ID
        }
    }

    public function removePromotionStautsZero()
    {
        $collection = $this->schedulepromotionFactory->create()->getCollection();
        $collection->addFieldToFilter('status', ['eq' => 0]);
        foreach ($collection as $item) {
            $item->delete();
        }

        // If child all items are deleted then run below script

        $collection = $this->schedulepromotionGroupFactory->create()->getCollection();
        $collection->addFieldToFilter('status', ['eq' => 1]);
        foreach ($collection as $item) {
            $this->deleteParentIfNoChildren($item->getGroupId());
        }

        return 'Removed promotion executed successfully';
    }

    public function deleteParentIfNoChildren($parentId)
    {
        // Get the database connection
        $connection = $this->resourceConnection->getConnection();

        // Check if there are any remaining child entries for the parent
        $select = $connection->select()
            ->from('schedulepromotion', 'id')
            ->where('group_id = ?', $parentId);
        $remainingChildren = $connection->fetchAll($select);

        // If no remaining children, delete the parent entity
        if (empty($remainingChildren)) {
            $where = ['group_id = ?' => $parentId];
            $connection->delete('schedulepromotion_group', $where);
        }
    }

    public function updateParentStatusIfAllChildrenInactive($parentId) {
        // Get the database connection
        $connection = $this->resourceConnection->getConnection();

        // Check if all child statuses are 0 for the given parent_id
        $select = $connection->select()
            ->from('schedulepromotion', 'status')
            ->where('group_id = ?', $parentId);
        $statuses = $connection->fetchCol($select);

        // If all statuses are 0, update the parent's status to 0
        if (!empty($statuses) && count(array_filter($statuses)) === 0) {
            $where = ['group_id = ?' => $parentId];
            $connection->update('schedulepromotion_group', ['status' => 0], $where);
        }
    }
}